package uk.co.nickthecoder.fluffy.calculator.numbers

import junit.framework.TestCase
import org.junit.Test

class TestComplex : TestCase() {

    @Test
    fun testString() {
        assertEquals("(1+1*i)", Complex(1, 1).toFluffyString())
        assertEquals("1", Complex(1, 0).toFluffyString())
        assertEquals("(1*i)", Complex(0, 1).toFluffyString())
    }

    @Test
    fun testBasic() {
        assertEquals(Complex(3.0, 1.5), Complex(2.0, 1.5) + 1.0)
        assertEquals(Complex(3.0, 1.5), 1.0 + Complex(2.0, 1.5))

        assertEquals(Complex(1.0, 1.5), Complex(2.0, 1.5) - 1.0)
        assertEquals(Complex(-1.0, 01.5), 1.0 - Complex(2.0, 1.5))

        assertEquals(Complex(4.0, 3.0), Complex(2.0, 1.5) * 2.0)
        assertEquals(Complex(4.0, 3.0), 2.0 * Complex(2.0, 1.5))

        assertEquals(Complex(2.0, 1.5), Complex(4.0, 3.0) / 2.0)
        assertEquals(Complex(24.0, -32.0), 200.0 / Complex(3.0, 4.0))
        assertEquals(Complex(3.0, 4.0), 200.0 / Complex(24.0, -32.0))
    }
}
