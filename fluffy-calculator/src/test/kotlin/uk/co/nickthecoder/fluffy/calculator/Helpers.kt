package uk.co.nickthecoder.fluffy.calculator

import junit.framework.TestCase
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

fun assertFails(action: () -> Any?) {
    TestCase.assertTrue(fails(action))
}

fun fails(action: () -> Any?): Boolean {
    try {
        action()
        return false
    } catch (e: Exception) {
        return true
    }
}

fun assertEqualsString(expected: String, actual: Any?) = TestCase.assertEquals("'$expected'", "'$actual'")


fun assertEvalToString(expected: String, compiler: FluffyCompiler, binding: Binding, code: String) {
    assertEqualsString(expected, compiler.compile(binding, code).eval())
}

fun assertEvaluation(expected: String, expression: String) {
    val calculator = Calculator().apply { binding = CalculatorApp.createBinding() }
    val value = calculator.evaluate(expression)
    assertEqualsString(expected, calculator.processResult(value).second)
}
