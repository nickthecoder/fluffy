package uk.co.nickthecoder.fluffy.calculator.units

import junit.framework.TestCase
import org.junit.Test

class TestPrefix : TestCase() {

    @Test
    fun testPrefix() {
        val none = Prefix.NONE
        val k = Prefix.KILO
        val M = Prefix.MEGA

        assertEquals(0, none.tenToThe)
        assertEquals(3, k.tenToThe)
        assertEquals(6, M.tenToThe)

        assertEquals(1.0, none.factor)
        assertEquals(1_000.0, k.factor)
        assertEquals(1_000_000.0, M.factor)
    }

    @Test
    fun testTimes() {

        val kk = Prefix.KILO * Prefix.KILO

        assertEquals(6, kk.tenToThe)
        assertEquals(1_000_000.0, kk.factor)
        assertEquals(kk, Prefix.MEGA)

    }

    @Test
    fun testDiv() {

        val k = Prefix.MEGA / Prefix.KILO

        assertEquals(3, k.tenToThe)
        assertEquals(1_000.0, k.factor)
        assertEquals(k, Prefix.KILO)

    }

    @Test
    fun testInverse() {

        val k = Prefix.MILLI.inverse()

        assertEquals(3, k.tenToThe)
        assertEquals(1_000.0, k.factor)
        assertEquals(k, Prefix.KILO)
    }

    @Test
    fun testConversion() {
        assertEquals(1_000.0, Prefix.KILO.conversionTo(Prefix.NONE))
        assertEquals(1_000.0, Prefix.MEGA.conversionTo(Prefix.KILO))
        assertEquals(1_000_000.0, Prefix.KILO.conversionTo(Prefix.MILLI))
    }

    @Test
    fun testToString() {
        assertEquals("k", Prefix.KILO.toString())
    }
}
