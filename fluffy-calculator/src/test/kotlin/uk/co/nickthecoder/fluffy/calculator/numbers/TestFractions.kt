package uk.co.nickthecoder.fluffy.calculator.numbers

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.calculator.CalculatorApp

class TestFractions : TestCase() {

    @Test
    fun testEquals() {
        assertTrue(Fraction(1, 2) == Fraction(1, 2))
        assertFalse(Fraction(2, 4) == Fraction(1, 2))
        assertFalse(Fraction(2, 4) == Fraction(1, 2))
        val one: Number = 1
        assertFalse(one == Fraction(1, 1))
        assertFalse(Fraction(1, 1) == one)
    }

    @Test
    fun testWithInt() {
        assertEquals(Fraction(11, 4), Fraction(3, 4) + 2)
        assertEquals(Fraction(11, 4), 2 + Fraction(3, 4))
    }

    @Test
    fun testCreateMixed() {
        val compiler = FluffyCompiler()
        val binding = Binding.extra().with(CalculatorApp.NUMBER)
        assertEquals(Fraction(11, 4), compiler.compile(binding, "2\\3\\4").eval())
    }

    @Test
    fun testGcd() {
        assertEquals(7, Fraction.gcd(14, 35))
        assertEquals(7, Fraction.gcd(-14, 35))
        assertEquals(7, Fraction.gcd(-14, -35))
        assertEquals(7, Fraction.gcd(14, -35))

        assertEquals(1, Fraction.gcd(1, -35))
        assertEquals(1, Fraction.gcd(14, 1))

        // I'm not sure of the mathematical "truth" of this, but this is how I'm treating zeros!
        assertEquals(10, Fraction.gcd(0, 10))
        assertEquals(10, Fraction.gcd(10, 0))
    }

    @Test
    fun testLcd() {
        assertEquals(Fraction(1, 2), Fraction.normalised(2, 4))
        assertEquals(Fraction(5, 6), Fraction.normalised(5 * 14, 6 * 14))

        // Always put the "negative" value on the numerator.
        assertEquals(Fraction(-5, 6), Fraction.normalised(-5 * 14, 6 * 14))
        assertEquals(Fraction(-5, 6), Fraction.normalised(5 * 14, -6 * 14))
        assertEquals(Fraction(5, 6), Fraction.normalised(-5 * 14, -6 * 14))

        assertEquals(Fraction(0, 1), Fraction.normalised(0, 10))
        assertEquals(Fraction(1, 0), Fraction.normalised(10, 0))
    }

    @Test
    fun testTimes() {
        assertEquals(Fraction(1, 2), Fraction(1, 4) * 2)
        assertEquals(Fraction(3, 2), Fraction(3, 4) * 2)

        assertEquals(Fraction(1, 3), Fraction(2, 3) * Fraction(1, 2))
        assertEquals(Fraction(-1, 3), Fraction(-2, 3) * Fraction(1, 2))
        assertEquals(Fraction(-1, 3), Fraction(2, 3) * Fraction(1, -2))
        assertEquals(Fraction(-1, 3), Fraction(2, 3) * Fraction(-1, 2))
    }

    @Test
    fun testDiv() {
        assertEquals(Fraction(3, 16), Fraction(3, 8) / 2)
        assertEquals(Fraction(2, 5), Fraction(4, 5) / 2)
        assertEquals(Fraction(1, 6), Fraction(1, 3) / 2)
    }

    @Test
    fun testToString() {
        assertEquals("1\\3", Fraction(1, 3).toFluffyString())
        assertEquals("-1\\3", Fraction(-1, 3).toFluffyString())
        assertEquals("-1\\3", Fraction(1, -3).toFluffyString())
        assertEquals("1\\3", Fraction(-1, -3).toFluffyString())

        // For non-top-heavy fractions toMixedString must give the same answers as toFluffyString
        assertEquals("1\\3", Fraction(1, 3).toMixedString())
        assertEquals("-1\\3", Fraction(-1, 3).toMixedString())
        assertEquals("-1\\3", Fraction(1, -3).toMixedString())
        assertEquals("1\\3", Fraction(-1, -3).toMixedString())

        // Top-heavy fractions give different results
        assertEquals("1\\1\\3", Fraction(4, 3).toMixedString())
        assertEquals("-1\\1\\3", Fraction(-4, 3).toMixedString())
        assertEquals("1\\1\\3", Fraction(-4, -3).toMixedString())
        assertEquals("-1\\1\\3", Fraction(4, -3).toMixedString())
    }

}
