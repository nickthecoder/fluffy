package uk.co.nickthecoder.fluffy.calculator

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.calculator.units.Units
import uk.co.nickthecoder.fluffy.calculator.units.Measure

class TestFluffyMeasure : TestCase() {

    val compiler = FluffyCompiler()

    lateinit var binding: Binding

    fun assertEvalToString(expected: String, code: String) {
        assertEvalToString(expected, compiler, binding, code)
    }

    override fun setUp() {
        binding = Binding.standard().with(CalculatorApp.NUMBER, CalculatorApp.MATH, CalculatorApp.UNITS)
        binding.importPackage("uk.co.nickthecoder.fluffy.calculator.units")
    }

    @Test
    fun testCreation() {
        assertEvalToString("1(m)", "Measure( 1.0, Units.METERS )")
    }

    @Test
    fun testDimensionsFluffy() {

        assertEvalToString("1(m)", "1.0 * m")
        assertEvalToString("1(m)", "1.0(m)")
    }

    @Test
    fun testIn() {
        // Stays as a double
        assertEquals(Measure(1000.0, Units.METERS), compiler.compile(binding, "1.0(km) in m").eval())
        assertEquals(Measure(6.0, Units.FEET), compiler.compile(binding, "2.0(yards) in feet").eval())

        // Stays as an int
        assertEquals(Measure(1000, Units.METERS), compiler.compile(binding, "1(km) in m").eval())
        assertEquals(Measure(6, Units.FEET), compiler.compile(binding, "2(yards) in feet").eval())

        assertEquals(Measure(3.28, Units.FEET), compiler.compile(binding, "1(m) in feet sigfigs 3").eval())
        assertEquals(Measure(39.4, Units.INCHES), compiler.compile(binding, "1(m) in inches sigfigs 3").eval())
        assertEquals(Measure(1.09, Units.YARDS), compiler.compile(binding, "1(m) in yards sigfigs 3").eval())
    }
}
