package uk.co.nickthecoder.fluffy.calculator

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.calculator.numbers.Complex
import uk.co.nickthecoder.fluffy.calculator.numbers.Fraction
import uk.co.nickthecoder.fluffy.calculator.numbers.plus
import uk.co.nickthecoder.fluffy.calculator.numbers.toFluffyString

class TestNumbers : TestCase() {

    @Test
    fun testDoubleToString() {
        assertEquals("0.09999999999999998", (1.0 - 0.1 * 9).toString())
        assertEquals("0.1", (1.0 - 0.1 * 9).toFluffyString())
        assertEquals("1", 1.0.toFluffyString())

    }

    /**
     * NOTE, these are tested in TestFractions, but I've left them here anyway!
     */
    @Test
    fun testFractionToString() {
        assertEquals("2\\3", Fraction(2, 3).toFluffyString())
        assertEquals("2\\3", Fraction(-2, -3).toFluffyString())
        assertEquals("-2\\3", Fraction(-2, 3).toFluffyString())
        assertEquals("-2\\3", Fraction(2, -3).toFluffyString())
        assertEquals("4\\3", (0 + Fraction(8, 6)).toFluffyString())
    }

    @Test
    fun testComplexToString() {
        assertEquals("(2+4*i)", Complex(2, 4).toFluffyString())
        assertEquals("(2+4*i)", Complex(2.0, 4.0).toFluffyString())
        assertEquals("(2\\3+1\\2*i)", Complex(Fraction(2, 3), Fraction(1, 2)).toFluffyString())
    }

    /**
     * Nalin found a bug in Fraction. I han't implemented Fraction * Double, fluffy was then
     * coercing the Double to an Int, and then doing the multiply. Doh!
     */
    @Test
    fun testNalinsBug() {
        assertEquals(2.0 / 3.0 * 0.5, Fraction(2, 3) * 0.5)
    }
}
