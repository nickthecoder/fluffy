package uk.co.nickthecoder.fluffy.calculator.units

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.calculator.assertFails

class TestUnits : TestCase() {

    val m = Units.METERS
    val km = m * Prefix.KILO
    val sq_m = (m * m) as SimpleUnits
    val sq_km = (km * km) as SimpleUnits

    @Test
    fun testTimes() {

        assertEquals(0, m.prefix.tenToThe)
        assertEquals(1.0, m.prefix.factor)

        assertEquals(1, km.power)
        assertEquals(3, km.prefix.tenToThe)
        assertEquals(1_000.0, km.prefix.factor)

        assertEquals(2, sq_m.power)
        assertEquals(0, sq_m.prefix.tenToThe)
        assertEquals(1.0, sq_m.prefix.factor)

        assertEquals(2, sq_km.power)
        assertEquals(3, sq_km.prefix.tenToThe)
        assertEquals(1_000.0, sq_km.prefix.factor)

        val meterGrams = m * Units.GRAMS
        // TODO Add tests

        val km_m = (km * m) as CompoundUnits
        // TODO Add tests
    }

    @Test
    fun testDiv() {
        val m = Units.METERS

        val unitless = (m / m) as SimpleUnits
        assertEquals(0, unitless.power)
        assertEquals(0, unitless.prefix.tenToThe)
        assertEquals(1.0, unitless.prefix.factor)

        val sq_m = (m * m * m / m) as SimpleUnits
        assertEquals(2, sq_m.power)
        assertEquals(0, sq_m.prefix.tenToThe)
        assertEquals(1.0, sq_m.prefix.factor)

    }

    @Test
    fun testToString() {
        assertEquals("m", m.toString())

        assertEquals("km", km.toString())

        assertEquals("m²", sq_m.toString())

    }

    @Test
    fun testInverse() {
        val mInverse = m.inverse()

        assertEquals(-1, mInverse.power)
        assertEquals(Prefix.NONE, mInverse.prefix)
        assertEquals(BaseUnits.METERS, mInverse.baseUnits)
    }

    @Test
    fun testIsCompatibleWith() {
        val m = Units.METERS
        val sqM = m * m

        assertFalse(m.isCompatibleWith(sqM))
        assertFalse(m.isCompatibleWith(Units.DAYS))

        assertTrue(Units.SECONDS.isCompatibleWith(Units.DAYS))
    }

    @Test
    fun testConversionTo() {

        val Mm = m * Prefix.MEGA

        val hours = Units.HOURS
        val minutes = Units.MINUTES
        val s = Units.SECONDS


        assertEquals(60.0, minutes.conversionTo(s))
        assertEquals(60.0, hours.conversionTo(minutes))
        assertEquals(60.0 * 60.0, Units.HOURS.conversionTo(Units.SECONDS))

        assertEquals(1_000.0, (km).conversionTo(m))
        assertEquals(1_000.0, (Mm).conversionTo(km))

        assertEquals(1_000_000.0, (Mm).conversionTo(m))

        assertEquals(1_000_000.0, sq_km.conversionTo(sq_m))

        assertFails { minutes.conversionTo(m) }
        assertFails { m.conversionTo(sq_m) }

        assertEquals(1_000.0, (m * m * m).conversionTo(Units.LITERS))
    }


    @Test
    fun testImperialDistances() {
        assertEquals(12.0, Units.FEET.conversionTo(Units.INCHES))

        assertEquals(3.280839895013123, m.conversionTo(Units.FEET))
        assertEquals(12.0, Units.FEET.conversionTo(Units.INCHES))
        assertEquals(3.0, Units.YARDS.conversionTo(Units.FEET))
    }

}