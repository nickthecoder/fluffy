package uk.co.nickthecoder.fluffy.calculator.units

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.calculator.CalculatorApp
import uk.co.nickthecoder.fluffy.calculator.assertEqualsString
import uk.co.nickthecoder.fluffy.calculator.assertEvaluation
import uk.co.nickthecoder.fluffy.calculator.assertFails

class TestMeasure : TestCase() {

    val m = Units.METERS
    val km = m * Prefix.KILO
    val s = Units.SECONDS

    @Test
    fun testPlus() {

        assertEqualsString("10(m)", 6.0(m) + 4.0(m))
        assertEqualsString("2010(m)", 10(m) + 2(km))

        assertFails { 10(m) + 2 }
        assertFails { 10(m) + 2(s) }
    }

    @Test
    fun testMinus() {

        assertEqualsString("17(m)", 20.0(m) - 3.0(m))
        assertEqualsString("1000(m)", 2000.0(m) - 1.0(km))

        assertFails { 10(m) - 2 }
        assertFails { 10(m) - 2(s) }
    }

    @Test
    fun testTimes() {

        assertEqualsString("15(m²)", 5(m) * 3(m))

        val binding = CalculatorApp.createBinding()
        assertEqualsString("10(m s)", FluffyCompiler().compile(binding, "2(m) * 5(s)").eval())
        assertEqualsString("10(cm²)", FluffyCompiler().compile(binding, "2(cm) * 5(cm)").eval())

        // Currently fails : 100(cm mm) ie the units aren't simplified.
        assertEqualsString("10(cm²)", FluffyCompiler().compile(binding, "2(cm) * 50(mm)").eval())
    }

    @Test
    fun testDiv() {
        assertEqualsString("5", 15(m) / 3(m))

        val binding = CalculatorApp.createBinding()
        assertEqualsString("2(m s⁻¹)", FluffyCompiler().compile(binding, "10(m) / 5(s)").eval())
        assertEqualsString("2", FluffyCompiler().compile(binding, "10(cm) / 5(cm)").eval())

        // Currently fails : 0.2(cm mm⁻¹) ie the units aren't simplified.
        assertEqualsString("2", FluffyCompiler().compile(binding, "10(cm) / 50(mm)").eval())
    }

    /**
     * Tests that dimensionless units are automatically eliminated.
     * For example, how many 10cm tiles fit across a 2 meter wall?
     * We don't want the answer in meters per cm.
     *
     * This doesn't affect units of quantity, such a moles or dozens.
     * These do NOT have zero powers, they have a power of 1.
     */
    fun testZeroPowers() {
        assertEvaluation("20", "2(m) / 10(cm)")

        assertEvaluation("3(mol)", "2(mol) + 1(mol)")
        assertEvaluation("12(mol)", "2(mol/g) * 6(g)")
    }

}
