package uk.co.nickthecoder.fluffy.calculator.units

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.calculator.assertFails

class TestCompoundUnits : TestCase() {

    @Test
    fun testTricky() {
        val m = Units.METERS
        val km = m * Prefix.KILO

        val km_m = (km * m) as CompoundUnits
        assertEquals(1, km_m.unitsLists.size)
        assertEquals(2, km_m.unitsLists[Dimension.LENGTH]!!.units.size)

        assertTrue(km_m.unitsLists[Dimension.LENGTH]?.units?.contains(Units.METERS)!!)
        assertTrue(km_m.unitsLists[Dimension.LENGTH]?.units?.contains(Units.METERS * Prefix.KILO)!!)
    }

    /**
     * I've bodged litres by defining it using a base unit of decimeters, in the same way you would define feet or yards.
     * This bodge breaks down if you divide litres by a length Dimension to get an area.
     * The label would be horribly broken :-(
     * It's also very noticeable that there's rounding errors within the conversions.
     * l.conversionTo(dm3) should be 1.0, but is actually something like 1.0000000000002
     */
    @Test
    fun testLitres() {
        val l = Units.LITRES
        val m = Units.METERS
        val m3 = m * m * m
        val dm = m * Prefix.DECI
        val dm3 = dm * dm * dm

        assertEquals(1.0, l.conversionTo(dm3).ish())
        assertEquals(0.001, l.conversionTo(m3).ish())
        assertEquals("l", l.toString())

        val ml = l * Prefix.MILLI
        assertEquals("ml", (Units.LITRES * Prefix.MILLI).toString())
        assertEquals("ml", ml.toString())
    }

    @Test
    fun testIsCompatible() {
        val m = Units.METERS
        val km = m * Prefix.KILO
        val sq_m = m * m
        val sq_km = km * km
        val s = Units.SECONDS

        val mPerS = m / s
        val kmPerHour = km / Units.HOURS

        assertTrue(m.isCompatibleWith(km))
        assertTrue(sq_m.isCompatibleWith(sq_km))
        assertTrue(mPerS.isCompatibleWith(kmPerHour))

        assertFalse(m.isCompatibleWith(s))
        assertFalse(m.isCompatibleWith(sq_m))

        val m2 = SimpleUnits(BaseUnits.METERS, Prefix.NONE, 1)
        val sq_m2 = Units.create(Units.METERS, Units.METERS)
        val sq_km2 = Units.create(Units.METERS * Prefix.KILO, Units.METERS * Prefix.KILO)

        assertTrue(m.isCompatibleWith(m2))
        assertTrue(sq_m2.isCompatibleWith(sq_m))
        assertTrue(sq_km2.isCompatibleWith(sq_km))

        assertTrue(sq_m.isCompatibleWith(m * km))
    }

    @Test
    fun testConverstionTo() {
        val m = Units.METERS
        val km = m * Prefix.KILO
        val sq_m = m * m
        val sq_km = km * km
        val s = Units.SECONDS

        val mPerS = m / s
        val kmPerHour = km / Units.HOURS

        assertEquals(1_000.0, km.conversionTo(m))
        assertEquals(1_000_000.0, sq_km.conversionTo(sq_m))

        // 1 km.m = 1,000 sq m
        assertEquals(1_000.0, (km * m).conversionTo(sq_m))

        assertEquals(3.6, mPerS.conversionTo(kmPerHour))
        assertFails { m.conversionTo(sq_m) }
        assertFails { m.conversionTo(s) }
    }

    @Test
    fun testTimes() {
        val m = Units.METERS
        val km = Units.METERS * Prefix.KILO

        val km_m = (km * m) as CompoundUnits
        val distances = km_m.unitsLists[Dimension.LENGTH]!!
        assertEquals(2, distances.units.size)
        assertEquals(1, distances.units[0].power)
        assertEquals(1, distances.units[1].power)
        assertEquals(Prefix.KILO, distances.units[0].prefix)
        assertEquals(Prefix.NONE, distances.units[1].prefix)

    }


    @Test
    fun testDiv() {
        val km = Units.METERS * Prefix.KILO
        val s = Units.SECONDS

        val kmPerS = (km / s) as CompoundUnits

        val distances = kmPerS.unitsLists[Dimension.LENGTH]!!
        val times = kmPerS.unitsLists[Dimension.TIME]!!

        assertEquals(1, distances.units.size)
        assertEquals(1, times.units.size)

        assertEquals(1, distances.units[0].power)
        assertEquals(Prefix.KILO, distances.units[0].prefix)
        assertEquals("m", distances.units[0].baseUnits.label)

        assertEquals(-1, times.units[0].power)
        assertEquals(Prefix.NONE, times.units[0].prefix)
        assertEquals("s", times.units[0].baseUnits.label)
    }

    @Test
    fun testToString() {
        val m = Units.METERS
        val s = Units.SECONDS

        val mPerS = m / s

        assertEquals("m", m.toString())
        assertEquals("s", s.toString())
        assertEquals("m s⁻¹", mPerS.toString())

        assertEquals("m²", (m * m).toString())
    }
}

fun Double.ish() = Math.round(this * 1_000_000_000_000.0) / 1_000_000_000_000.0
