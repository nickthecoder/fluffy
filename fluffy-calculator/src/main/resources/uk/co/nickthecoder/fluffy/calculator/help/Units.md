# Dimensions

NASA once lost an expensive satellite because the design mixed up feet and
meters. Wouldn't it be nice if your calculation were "type safe",
and such mistakes were impossible? Read on...

The simplest dimensions are lengths, such as meters, kilometers etc.
You use them like this :

    500(m) + 1(km)      // 1500(m)

If we so the sum the other way round, we get the answer in km :

    1(km) + 500(m)      // 1.5(km)

We aren't restricted to just lengths, we can do areas too :

    1(m^2) + 200(cm^2)

Which can also be written :

    1(m*m) + 200(cm*cm)

If we try to mix a length with an area, we get an error :

    1(m^2) + 1(m)       // Does not compute ;-)

There are many pre-defined dimensions :

* g         grams
* mg        milligrams
* kg        kilograms

* m         meters
* mm        millimeters
* cm        centimeters
* km        kilometers

* l         litres
* cl        centilitres
* ml        millilitres

* s         seconds
* seconds   (the same as s)
* milliseconds
* minutes
* hours
* days
* weeks
* years

* bytes
* kB        A kilobyte 1,000 bytes (not 1024)
* MB        A megabyte 1,000,000 bytes etc.
* GB        A gigabyte
* TB        A terabyte

* bits
* kb        A kilo-bit 1,000 bits (not 1024)
* Mb
* Gb
* Tb

If you are scientist, or engineer, you need to use less common units.
In which case, you can use any of the ISO prefixes like so :

    val Mm = m * Prefix.MEGA

You can now use mega-meters as you would any of the built-in units. e.g.

    10(Mm) + 1000(km)

We can also create other units of distance. Astronomers may like to use
parsecs, which are  1.9174×10¹³ meters :

    val parsecs = SimpleUnits( "parsecs", Dimension.LENGTH, 1.9174e13 )
    1(parsecs) + 1000000000(km)

We can combine dimensions. For example, to add speeds :

    10(m/s) + 1(km/hour)    // 1 meter per second + 1 km per hour.

The numbers are still type-safe, so this will fail :

    10(m/s) + 10(m)

Similarly, we use acceleration :

    10(m/s^2) + 1(km/hour^2)    // Meters per second squared etc.

To convert from one unit to another, we can use the "in" function :

    100(m) in km          // 0.1(km)

FYI, if you have a variable which is a plain number, you can give it
the correct dimensions :

    var foo = 100            // A plain number without units
    var fooMeters = foo * m  // fooMeters is now 100 meters

If you need to find the raw (unit-less value) :

    fooMeters.value         // 100

We've seen that adding things with different Dimensions gives an error,
but times and divide doesn't :

    10(m) / 2(s)            // 5 meters per second.

Imagine you have a wall 3m wide and 15cm square tiles, we can
calculate how many tiles will fit in one row :

    300(cm) / 15(cm)

The answer will look like a regular number (as it should), but it
isn't a Double (or any other regular number type), it is a
"Measure", just like all the other examples above.
This one just happens to have no dimensions.
So we could use 'value' as we did above, to get a Double :

    ( 300(cm) / 15(cm) ).value

However, a safer way is :

    ( 300(cm) / 15(cm) ).dimensionlessValue

It is safer, because if we made a mistake such as :

    ( 300(cm) * 300(cm) / 15(cm) ).dimensionlessValue

It will fail (because it is a length, not dimensionless!)

If you are really adventurous, you can create your own dimensions.
Feel free to skip this part, it is very niche!

For example, if we want to measure how long a project will take, we
may need units of person-hours. We already have hours,
so we just need to create "person" :

    val worker = Dimension( "worker" )
    val person = SimpleUnits( "person", worker )

I'll explain why it is in two parts shortly.

We can now calculate a project's duration :

    10(person*weeks) + 3 * 4(person*days)

Now imagine you have a team of 12 people. You may like to define some
tasks in team-hours :

    val team = SimpleUnits( "team", worker, 12 )

Now we can do things like :

    10(person*hours) + 2(team*hours)    // 10 * 2*12 perons-hours

This example is a little tenuous, but hopefully, it gives you an idea
why we needed to create "person" in two parts.
We needed "worker" for both person and team, so that the calculator
knows how to convert from one to the other.

Note that Fluffy cannot deal with some tricky units, such as temperatures,
because the various temperature scales do not have a common "zero" value.
Fluffy cannot deal with varying units, such as money... The conversion from
Euros to USD is not constant.


Back to the [Index.md].
