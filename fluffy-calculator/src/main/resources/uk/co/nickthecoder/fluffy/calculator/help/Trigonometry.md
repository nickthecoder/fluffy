## Trigonometry

This text is interactive, you can try out all the examples.
Move the caret any expression below, and hit = to evaluate it.

    sin( 30 )
    cos( 60 )
    tan( 45 )

    asin( 0.5 )     // Arc sine, also called inverse sine
    acos( 0.5 )     // Arc cosine, also called inverse cosine
    atan( 0.5 )     // Arc tangent, also called inverse tangent
    atan2( 4, 5 )   // Similar to atan, but you give it the y and x values instead of an angle.

    sinh( 30 )      // Hyperbolic sine
    cosh( 60 )      // Hyperbolic cosine
    tanh( 45 )      // Hyperbolic tangent

Note, by default, the calculator uses degrees.
To switch to radians, use the shortcut Ctrl+R (and Ctrl+D for degrees),
or use the "hamburger" icon near the top left of the window.

The examples above use the "normal" trig functions, but Fluffy has an
alternate set, which know the difference between degrees and radians,
and automatically convert when appropriate.
See [Units.md] for more information.

    sin( 30(degrees) )
    arcsin( 0.5 )

Note, that when an expression is an angle, it is displayed
in your preferred units. So the following show the same answer
(depending on your angle preferences in the hamburger menu).

    180(degrees)
    PI(radians)

You can add angles with different units :

    90(degrees) + PI(radians)   // 270(degrees)
    90(degrees) + 2(revs)       // 1 rev == 360 degrees ;-)    

Back to the [Index.md].
Next page [Constants and Variables.md].
