# Basic Maths

This text is interactive, you can try out all of the examples.
Move the caret to the expression below, and hit Ctrl+Enter to evaluate it.

    5 + 3 - 1

Here's are the basic operators :

* Raise to the Power : ^

* Times, Divide, Remainder : * /

* Plus, Minus : + -

For a complete list see [Operators.md]

Expressions are NOT evaluated left to right. Instead each operator
has "precedence". ^ is performed before * and / and + and - are last.

For example :

    1 + 2 * 3

Is 7. If you expected the answer 9, then this calculator may not be
right for you. Sorry.

You can use brackets to change the order of evaluation, like so :

    (1+2) * 3

This time, the answer is 9.

Back to the [Index.md].
Next page [Operators.md].
