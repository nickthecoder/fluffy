while
=====

Loops while the condition is true. For example :

    var sum = 0
    var i = 1
    while ( i < 100 ) {
        i *= 2
        sum += i
    }
    sum

Back to the [Index.md].
Next page [with.md].
