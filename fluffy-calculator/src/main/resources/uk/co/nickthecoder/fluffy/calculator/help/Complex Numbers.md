# Complex Numbers

What's the square root of -1? If you don't know the answer, you can
skip this section! Or you learn something new :

[https://www.khanacademy.org/math/precalculus-2018/imaginary-and-complex-numbers]

The calculator has a variable called 'i' which is the square root of minus 1.
You can then use that within you expressions, like so :

    (1+2*i) / (2-i)

Note that you can NOT write this in the more traditional form :

    (1+2i) / (2-i)      // Fails. Sorry!

To create a complex number from polar form (using a magnitude and angle) :

    polarComplex( 4, 90 )       // 4*i If you are in degrees mode
    polarComplex( 4, PI / 2 )   // 4*i If you are in radians mode.

To switch between radians and degrees, use the hamburger icon
in the top right of the window.

FYI, If you want to ignore the mode, and ALWAYS use radians,
then you can do this instead :

    Complex.polar( 4, PI / 2 )  // Always 4*i, regarless of the mode.

To convert to polar form :

    (1+i).magnitude()   // = sqrt(2)
    (1+i).angle()       // = 45 if you are in degrees mode

FYI, angle() will return either radians or degrees depending on
the mode. If you want to ignore the mode, and always use radians, then :

    (1+i).theta()       // PI / 4, regardless of the mode.

Also, a complex number is NOT a sub-class of Number, so functions
which take a Number cannot handle a complex number.

FYI, internally, the class is called Complex, which you need to know
if you want to create your own complex numbers functions.
For example, at the time of writing, you cannot raise a complex number
to the power of another complex number, but you can fix that by
completing this function :

    fun Complex.pow( other : Complex ) : Complex {
        // Fill in the blanks
        return ...
    }

and then you can do :
    (1 + 2*i).pow(2 - 3*i)  // Currently fails :-(

Which is the same as :
    (1 + 2*i) ^ (2 - 3*i)   // Currently fails :-(

You can raise a complex number to a non-complex number though :

    (1+2*i) ^ 4     // This does work!

You may notice that the results are in brackets.
This is to help use the result in other calculations.
For example, if you want to multiply the result by two, you can move the
cursor to the end of the result, and append " * 3 ".
Without the brackets this would not work, as multiplication has a
higher precedence than +.

Any calculation using complex numbers always returns a
complex number, even when the imaginary part is zero. e.g.

    (1 + i) - i

The result appears as "1", but has type Complex, not Int or Double.
Therefore, you cannot use it where an Int or Double is expected.

Here's a rather contrived example, which fails :
We are trying to do "Hello".substring( 1 ) = "ello"

    "Hello".substring( (1+i) - i ) // Fails, expected an Int, but found a Complex.

There are two solutions :

    "Hello".substring( (1+i-i).real ) // Ignores the imaginary part
    "Hello".substring( (1+i-i).asReal() )

The second version is safer, as it throws an exception if the imaginary part
is non-zero.

The real and imaginary parts are stored as type Number, therefore you can
use any Number type, including Int, Double and Fraction.
Mixing Complex with Fraction can be fun ;-)

    (2\3 + 3\4 * i) + (1\2 + 5\6 * i)


Back to the [Index.md].
Next page [Units.md].
