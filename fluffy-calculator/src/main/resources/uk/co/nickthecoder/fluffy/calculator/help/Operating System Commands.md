# Operating System Commands

This section deals with running operating systems, such as "ls", "wc"
etc. This is aimed as Unix-y systems. If you are using Windows, you
may like to install Bash (google is your friend).

Let's start simple, how do we list a directory, and get the results
back as a list. You could use Java's File class (and in many cases this
is the best approach). But let's use "ls" here.

To run a command, we put it in back-ticks (next to the 1 key on my keyboard).

    `ls`

It returns an object of type CommandResult.
To get the text (from stdout), we have two options :

    `ls`.lines

    `ls`.out

The first returns a List of Strings, the second returns a single string.
Using .lines makes more sense in this case because we can easily
iterate over the results :

    for ( file in `ls`.lines ) {
        // file is a variable of type, File
    }

Using .out makes more sense when we have only one piece of data e.g. :

    val fileCountStr = `ls | wc -l`.out

fileCountStr will contain the number of files in the current directory,
but it is a String, not an Int. so we should really convert it to an
Int :

    val fileCount = `ls | wc -l`.out.toInt()

FYI, String.toInt can fail (throwing an exception) if the string isn't
a valid integer, so we could have used the "safe" version, which takes
a default valuek, which never throws :

    val fileCount = `ls | wc -l`.out.toInt(0)

What if we want to list a different directory, not the current one.
We can use expressions within commands :

    val dir = "\tmp"
    `ls $dir`.lines

The example above will work, but isn't good style. If "dir" contained a
space, or some special characters, such as "*", then the command will
fail. We need to quote the argument to ls :

    `ls '$dir'`.lines

That's better, but not perfect. What if "dir" starts with a dash "-".
The shell will treat it a flag, and not a directory name. Here's the
rock solid version :

    `ls -- '$dir'`.lines

"--" is a posix standard which means "there are no more flags"; it isn't
anything special to Fluffy.

But what if "dir" contains a single quote? e.g. dir = "It's nasty".
Try it! It will still work ;-)
Any expressions within a command string will automatically be escaped
if they are within single quotes.

We can see the command fluffy actually built using ".command". This
is handy for debugging :

    val nasty = "It's nasty"
    `ls -- '$nasty'`.command

There is another way of running commands :

    ${ls}

It's similar to using back-ticks, but the output of the command isn't
captured (into .out or .lines). You should use this version when you
don't care about the output from the command :

    $(mv 'fileA' 'fileB')     // Warning, will rename a file, if it exists!


## CommandResult

Here's a list of all the attributes of CommandResult :

* command       The operating system command that was executed (a String)
* state         either CommandState.FINISHED, CommandState.INTERRUPTED or CommandState.FAILED
* out           The result of the command as a String
* err           Like out, but contains stderr's output instead of stdout
* lines         out, converted to a List of Strings split by newline characters.
* errLines      err, converted to a List of Strings split by newline characters.
* exitValue     0 if the command completed normally.
* ok            true if the command completed with an exit status of 0.
* finished      true if the command finished (the exit status is ignored).
* process       The Process object (google java Process for more info)
                It's unlikely you will need this!

## Current Directory

The following script won't do what you might expect :

    ${cd /tmp}      // Change directory
    `ls`            // List which directory?

The ls command will NOT list /tmp Why?
    Each process has its own current directory, so changing the current
    directory in the first process has no impact on the second process.

There are a couple of solutions :
    `cd /tmp && ls`     // && is a standard shell trick. Google it!
    `ls /tmp`           // Avoid the problem by never using the current directory.

I prefer the 2nd solution.

## Shell Variables

What if we want to use shell variables within out commands, such as ${HOME}?

    `ls ${HOME}`        // Fails
    `ls $HOME`          // Also fails

These fail because HOME is assumed to be a Fluffy variable, not a shell
variable.
To fix this, we need to escape the $, so that Fluffy doesn't think it
is part of an expression :

    `ls \${HOME}`        // Works
    `ls \$HOME`          // Also works

It's a little ugly, sorry.
BTW, we should also quote it, just in case ${HOME} contains spaces or
special characters, so the final thing is even uglier :

    `ls -- "\${HOME}"`        // Works
    `ls -- "\$HOME"`          // Also works

Note that I used double quotes this time, not single quotes. That's
because single quotes do not allow shell variables within them.

Also note that these double quotes are for the shell, not fluffy.
There are no fluffy String objects in the two examples above.


## CommandRunner

Every command is executed via a command runner. The default command
runner is UnixCommandRunner, which behind the scenes uses the java code :

ProcessBuilder("sh", "-c", command)

On a plain install of Windows, the "sh" command won't exist, you'll need
to install bash (or another unix-y shell).
All other platforms should work fine out of the box (AFAIK).

## Timing Out Commands

If a command hangs (never ends), then your script will also hang.
Not good! So you may like to set timeout, so that if a command takes
too long, then the command will fail, and your script can continue
running.

The simplest way to set a timeout is :

    setCommandTimeout(1)    // 1 second
    $(sleep 10)             // Will timeout after 1 second

(I'm using the sleep command which also uses seconds)

Note, this only works within a script. If you evaluate each line
separately, then the timeout will NOT work, because each time you
compile a script, it creates a new CommandRunner with its own timeout.

So to test this feature, you need to copy/paste both lines into a
new tab, and run the whole script using F9.

You can also use the more complicated version, if you don't want to
use seconds :

    import java.util.concurrent.TimeUnit
    setCommandTimeout(100, TimeUnit.MILLISECONDS)
    $(sleep 10)

A value of 0, means timeouts are disabled (which is the default).
So this will wait 10 seconds :

    setCommandTimeout(0)
    $(sleep 10)


Next page : [Java Classes.md]
Back to the [Index.md]
