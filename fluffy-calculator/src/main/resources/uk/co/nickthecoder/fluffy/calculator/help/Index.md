# Fluffy Calculator

* [Using the Text Area.md]
* [Basic Maths.md]
* [Operators.md]
* [Functions.md]
* [Trigonometry.md]
* [Constants and Variables.md]
* [Fractions.md]
* [Complex Numbers.md]
* [Units.md]

* [Built-In Scripts.md]

Numbers aren't everything, this calculator does a whole lot more!
There's a whole language at your fingertips.
Fluffy isn't anywhere near as powerful, as languages like Java or Python,
but it does have some features that Java and Python lack.

* [Programming.md]
* [fun.md] (declaring functions)
* [if.md]
* [for.md]
* [while.md]
* [with.md]
* [try.md]
* [Number Types.md]
* [Strings and Characters.md]
* [Booleans.md]
* [Lists and Maps.md]
* [Operating System Commands.md]
* [Java Classes.md]

Download (or browse) the code for Fluffy, and its calculator :
[https://gitlab.com/nickthecoder/fluffy]

You can find more of my projects :
[https://gitlab.com/nickthecoder]
[http://nickthecoder.co.uk/wiki/view/software/Software]
