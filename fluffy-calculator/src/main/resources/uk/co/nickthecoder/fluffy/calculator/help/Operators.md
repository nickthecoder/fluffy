# Operators

Here's a list of all the operators, in order of precedence.
If you are performing simple maths, then many of these won't be
relevant to you, and can be ignored.

```
= += -= /= *= ~/= %=    // Assignments
||                      // Logical OR
&&                      // Logical AND
==  ===  !=  !==        // Test for equality
<  >  <=  >=            // Comparisons
is !is                  // Type checking
?:                      // Elvis (Presley's quiff)
pow xor                 // Infix function calls (there are many more)
..   ..<    =>          // Inclusive range, exclusive range, maps-to
-  +                    // Plus, minus
/  *  %  ~/             // Divide, times, remainder, integer division
^                       // Power
\                       // See [Fractions.md]
as  as?                 // Type casting
+  -  !                 // (Pre-Unary) Positive, negative, Logical NOT
(args)                  // Invoke a function
++  --  !  [index]      // (Post-Unary) Increment, decrement, factorial, array access
```

If no brackets are used, then the operator lower down is performed first.
e.g. ^ (power) is performed before * (times)

Note, "Unary" means the operator takes only one argument.
Pre-Unary operators appear before the argument (such as -3)
Post-Unary operators appear after the argument (such as 4!)
All other operators take two arguments, and appear between them e.g. 2+3


## Examples

This text is interactive, you can try out all of the examples.
Move the caret to any expression below, and hit Ctrl+Enter to evaluate it.

Each example has a comment after it. // is the start of a comment,
and is ignored when evaluating an expression.

    7 / 2       // = 3.5 (as you might have guessed ;-)

    7 ~/ 2      // = 3 (Integer division; the fractional part is thrown away)

    7 % 2       // = 1 (seven divided by 2 is 3, with 1 remainder)

    2 \ 3 + 1   // = 5 \ 3 (two thirds plus 1 = five thirds)

See [Fractions.md] for more information.

    4 ^ 2       // = 16 (four squared)

    2 ^ 3       // = 8 (two cubed)

FYI, to get the square root, use either of :

    sqrt(16)    // = 4

    16 ^ 0.5    // = 4

    16.pow(0.5) // = 4 (pow is short for "power")

For cube roots (or any other roots) :

    16^(1/3)    // = 2

    16.pow(1/3) // = 2

Finally, here's a factorial example :

    4 !         // = 24 ( 1 * 2 * 3 * 4 )

And if you want to play with big numbers :

    BigInteger(100)!


Back to the [Index.md].
Next page [Functions.md].
