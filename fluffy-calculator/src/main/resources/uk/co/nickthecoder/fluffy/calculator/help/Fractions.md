# Fractions

If you use fractions, wouldn't it be nice if this :

    1/2 + 1/4   // = 0.75

Evaluated to 3/4 instead of 0.75

Well, if you use the "fractions" operator instead of "divide", it will :

    1\2 + 1\4   // = 3\4

To use the "wrong" slash is weird at first, but you'll soon get used to it.

By default, the results are displayed as top-heavy fractions, so

    7 * 2\3   // = 14/3

If you prefer to see the answer as four and two thirds,
then head to the "hamburger" menu in the top right, and toggle
the "Top Heavy Fractions". The shortcut is Ctrl+F.

Now re-evaluate, and the result should be different :

    7 * 2\3   // = 4\2\3

Be careful when using fractions, and multiplication. For example :

    1+1\3 * 2  // Guess the result before evaluating!

The answer is 1\2\3 (or 5\3), not two and two thirds.
This is because "*" has higher precedence than "+", so it is the same as :

    1 + (1\3 * 2)

Therefore, it is better to write "one and two thirds" like this :
(Yes, it may look weird, but I think you'll get used to it ;-)
    1\2\3

Going back to the problematic example :
    1\1\3 * 2     // This time the answer is 2\2\3

This works because \ takes precedence over *.


Back to the [Index.md].
Next page [Complex Numbers.md].
