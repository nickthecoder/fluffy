If
==

Ifs are expressions (not statements), i.e. they have a value.

For example, the expression :

    if (true) 12 else 24
    
evaluates to 12.

Each half of the if can be a block of statements, with curly brackets.
In this case, the value is the last expression in the block.

For example :

    if (true) {
        println( "Hello World" )
        12
    } else {
        println( "Goodbye" )
        24
    }


Back to the [Index.md].
Next page [for.md].
