# Number Types

The simplest number types are Int and Double.
An Int stores a whole number, whereas Double can store real numbers
(non-integers).

However, there are more exotic types :

* [Fractions.md] e.g. two thirds. Written as 2\3).
* [Complex Numbers.md]
* [Units.md] e.g. 10 meters. Written as 10(m)

There's also BigInteger, which is similar to an Int, but can store a
number as large as you want (assuming you have enough memory).

And BigDecimal, which can store numbers with very large degree of accuracy.

    BigInteger( 1234 )
    BigDecimal( 1234.567 )

So far, BigDecimal isn't really supported (for example, you cannot even
use "*" to multiply, you have to use its .multiply() method)
BigInteger is a little better, the basic operators work, including
factorial.

Most of the time, you do not need to worry about the different types,
conversions will be automatic. For example, adding a Fraction and a Double
will result in a Double.

One last thing to note, there is another type called "Number".
Number is the base for the following types :

* Int
* Double
* BigInteger
* BigDecimal
* Fraction

It is NOT the base for Complex numbers, nor Dimensional numbers.

This means you can write a function which takes a Number as an argument,
and pass it any of : Int, Double, BigInteger, BigDecimal, Fraction,
but not a Complex number nor a Dimensional number.


Back to the [Index]
Next page : [Strings and Characters.md]
