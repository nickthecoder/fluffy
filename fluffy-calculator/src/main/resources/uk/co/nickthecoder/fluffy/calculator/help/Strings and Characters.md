# Strings and Characters

It may not seem useful to use strings in a calculator,
but you have a complete language to play with.
So if you want to write programs, then strings are very handy ;-)

    "Hello, " + "World"

    "Hello " * 3 + "what's going on here then?"

You can access all of the Java methods of strings too.
[https://docs.oracle.com/javase/7/docs/api/java/lang/String.html]

    "Hello".substring(2)

String literals can contain expressions :

    val forename = "Nick"
    val surname = "Robinson"

    "Hello $forename"
    "Mr. ${name.substring(0,1)} $surname"

If you need new-line characters inside a string, you have two choices :

    "First Line\nNext Line"   // \n means newline.

    """First Line
    Next Line"""

(You can use expressions within triple quotes too)

Characters use single quotes

    'a' * 3     // Returns a String "aaa"


Next page [Booleans.md].
Back to the [Index.md]

