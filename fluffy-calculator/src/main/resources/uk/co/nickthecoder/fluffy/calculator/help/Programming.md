# Programming

Here's a simple function which finds the mean (average) of two numbers :

    fun mean( a : Number, b : Number ) = (a + b) / 2

"fun" is short for "function". 'a' and 'b' are names of the arguments,
and we have to say what "type" they are.
In this case, they are both "Number".
(This calculator works on more than just numbers!)

Type = on the green text above.
You won't see any results yet, as you haven't used the function yet,
you've only declared it.

You can then call the function like so (type = as normal to calculate the value) :

    mean( 2, 6 )    // = 4


More complicated functions can't be declared on a single line.
Here's an example, which finds the mean (average) of a list of numbers :

    fun mean( list : List ) : Number {
        var total = 0
        for ( item in list ) {
            total += item
        }
        return total / list.size()
    }
    mean( [ 1, 5, 7, 3 ] )

But, as it isn't a single line, you can't use =
(because that only works with a single line).

So, instead, highlight the section, and press F9 evaluate the whole selection.
(Pressing F9 when there is no text highlighted evaluates the whole text).

Notice that we create a List, using square brackets.
You can read more about Lists in [Lists and Maps.md].

Using the default shortcuts can be annoying if you plan to use Fluffy for programming.
In particular, by default the = key evaluates the current line, and you have to type
Ctrl+= to insert an actual equals symbol.
So consider customising the shortcuts (F12). I personally use Shift+/ to evaluate a line.

## Automatically loading Scripts

If you plan on using your function at a later date, you can save it.
Then open it again at a later date, hit F9 (or the gears icon)
and then start using it. But that can be annoying, wouldn't it be nice
if your functions were available to use straight away,
every time you use the calculator?

To do this, we can start the calculator from the command line like so :

    fluffy-calculator --preload=myScript.fluffy

(Assuming you saved your script with the name "myScript.fluffy).

Now all the functions, constants and variables are ready to go,
but that command is annoying long, so you should create an alias for it.
Google "bash alias" if you don't how to do this.

If you don't like using the command line, you can create a desktop
icon for the command. (Again google is your friend).

For you Windows folks, I do sympathise, I used to use Windows too,
many years ago. Horrible isn't it ;-)
But seriously, creating a desktop icon may be tricky.
The calculator uses a ".bat" file to start, and AFAIK, you can't
create a desktop icon for a ".bat" file. Hopefully I'm wrong;
it has been a very long time since I've used Windows.
(Maybe google is your friend?)

Having said all that, if you are defining your own functions, you
probably aren't the average Window's user, and you should probably
install a "proper" command line shell, such as bash, instead of CMD.EXE
and then you can set up an alias, just like us unixy folks.


Next page : [fun.md]
Back to the [Index.md]
