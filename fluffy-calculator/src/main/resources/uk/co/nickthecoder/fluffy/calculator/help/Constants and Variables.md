# Constants and Variables

The calculator has the following built-in constants :

* e     // Euler's number
* PI
* TAU   // 2 * PI
* i     // The square root of minus 1

For more information about i, see [Complex Numbers.md].

You can declare your own constants like so :

    val meaningOfLife = 42

You can then use it in an expression :

    meaningOfLife / 2   // = 21

## Variables

Unlike most calculators, you can have as many "memories" as you want.
To declare a variable :

    var total = 0

Then re-assign it :

    total = 1

    total = total + 2

or more succinctly :

    total += 2

To show the value of a variable, just evaluate the expression
(using Ctrl+Enter as usual) :

    total

But often, you don't need variables (or "memories"),
Instead, use copy and paste asyou would within any text editor
(Ctrl+C and Ctrl+V)

Or just use the result of one expression within the next expression.

For example, if we want to total some figures, and then divide the result
by 10, just do the sum first ...

    1 + 5 + 7   // Use Ctrl+Enter to evaluate it
    13

Then just type "/10" after the result.

    13/10

Isn't that much better than a traditional calculator (I think so ;-)


Back to the [Index.md].
Next page [Fractions.md].
