# Built-In Scripts

You can take a peek behind the scenes, and see the function definitions
from the built-in scrips :

* [builtin:standard.fluffy]
* [builtin:extra.fluffy]

* [builtin:number.fluffy]
* [builtin:math.fluffy]
* [builtin:trig.fluffy]
* [builtin:complex.fluffy]
* [builtin:dimensions.fluffy]

Back to the [Index.md]
