# Functions

This text is interactive, you can try out all the examples.
Move the caret any expression below, and hit = to evaluate it.

There are some built-in functions :

    ceil( 1.7 )         // Rounds upwards, towards positive infinity

    floor( 1.7 )        // the largest (closest to positive infinity) value that
                        // is less than or equal to the argument and is equal to
                        // a mathematical integer.

    round( 1.7 )        // The closest integer to the argument,
                        // with ties rounding to positive infinity.

    sigfigs( 1234, 2 )  // 1200
    1234 sigfigs 2      // The same, but using infix notation.

    sign( -2 )          // Returns -1, 0 or 1 depending on the sign of the argument.
    signum( -2 )        // Another name for "sign" (above)

    log10( 1000 )       // Log base 10
    ln( 4 )             // The nature log.

FYI, For the inverse of log10 and ln :

    10^3
    e^3

Now back to more example functions :

    max( 10, 20 )   // Finds the maximum of two numbers
    min( 10, 20 )   // Finds the minimum of two numbers

    sqrt( 16 )      // Square root

    random()        // A random number between 0 (inclusive) and 1 (exclusive)
    random( 10 )    // A random integer, in this case between 0 and 10 (exclusive)
    random( .3, .5) // A random number between the two arguments.


If you are very adventurous, head over to the [Programming.md] section,
to learn how to define your own functions.


Back to the [Index.md].
Next page [Trigonometry.md].
