# Booleans

Examples :

    2 == 1 + 1      // true
    2 != 1          // true
    true && false   // And
    true || false   // Or
    true xor true   // Exclusive or (false)

Most of the time, you use booleans with if expressions :

    if ( 2 == 1 + 1 ) {
         println( "Yes" )
    }

    if ( 2 == 1 + 1 ) println("Yes")

    if ( 2 == 1+ 1 ) {
        println( "Yes" )
    } else {
        println( "No" )
    }

You can use if as expression for example :

    if ( 2 == 1 ) "Yep" else "No"

Or in the multi-line form :

    val  message = if ( 2 == 1 + 1 ) {
         "Yes"
    } else {
        "No"
    }
    println( "The answer is $message" )



Next Page [Lists and Maps.md]
Back to the [Index.md]

