# Using the Text Area

The calculator is a text editor, you can move around using the arrow keys,
or the mouse.

Type anywhere you like, and edit the text as you would in any text editor.

So, how do we perform calculations in a text editor?

Type the expression you want to calculate, and type

    =
    
The result will appear on the next line.

Try it, move the cursor to the expression below, and type =
(the cursor does not have to be at the end of the line)

    7 * 6

NOTE. If you want to type an equals sign, then use Ctrl+= instead.

Because this calculator is a text editor, you can do all the usual things,
such as :

* Copy and paste (Ctrl+C and Ctrl+V)
* Undo and redo (Ctrl+Z and Ctrl+Shift+Z).
* Select all (Ctrl+A)
* Open and Save (Ctrl+O and Ctrl+S)
* Create a new tab (Ctrl+T)

If you don't like these shortcuts, you can change them.
Use the "Edit Shortcuts" button near the top right of the screen.
(The shortcut for that button is F12 ;-)

Back to the [Index.md].
Next page [Basic Maths.md].
