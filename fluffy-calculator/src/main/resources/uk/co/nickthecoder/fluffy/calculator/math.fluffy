import java.lang.Math
import java.math.BigDecimal
import java.math.MathContext
import java.util.Random

val e = Math.E

fun abs( a : Number ) = Math.abs( a )

fun ceil( a : Number ) = Math.ceil( a )
fun floor( a : Number ) = Math.floor( a )
fun round( a : Number ) = Math.round( a )
fun sign( a : Number ) = Math.signum( a ) // An alias for signum
fun signum( a : Number ) = Math.signum( a )

fun log10( a : Number ) = Math.log10( a )
fun ln( a : Number ) = Math.log( a )

fun max( a : Number, b : Number ) = Math.max( a, b )
fun min( a : Number, b : Number ) = Math.min( a, b )

fun sqrt( a : Number ) = Math.sqrt( a )

fun random() = Math.random()
fun random( a : Number ) = Random().nextInt(a)
fun random( from : Number, to : Number) = from + Math.random() * (to - from)

// Returns the value to 'n' significant figures (as a double).
// NOTE, as this is returned as a Double, it may not be exactly represented.
// e.g. 0.11 sigfigs 1 == 0.1, but 0.1 cannot be exactly represented as a Double.
infix fun sigfigs( value : Number, n : Int ) = BigDecimal(value).round(MathContext(n)).doubleValue()
infix fun sigfigs( value : Int, n : Int ) : Int = BigDecimal(value).round(MathContext(n)).doubleValue() // coerced to Int
