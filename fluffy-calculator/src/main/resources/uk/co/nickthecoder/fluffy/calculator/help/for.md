# for

Iterate over a sequence.

For example :

    for ( i in 1..5 ) {
        // i will be 1,2,3,4,5
    }
    
You can also make the range end one earlier, using 'until'
instead of '..' :

    for ( i in 1 until 5 ) {
        // i will be 1, 2, 3, 4
    }

You can also iterate over ANY iterable, such as a list :

    for ( item in myList ) {
    }

You can also iterate over each character in a string :

    for ( c in "Hello" ) {
    }


Back to the [Index.md].
Next page [while.md].
