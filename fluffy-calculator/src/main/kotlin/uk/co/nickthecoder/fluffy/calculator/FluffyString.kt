package uk.co.nickthecoder.fluffy.calculator

/**
 * An object that can converted to a String, which is a valid fluffy expression.
 * For example, a Complex number, or a Fraction.
 * The Calculator will use this string when displaying the results.
 */
interface FluffyString {
    fun toFluffyString(): String
}
