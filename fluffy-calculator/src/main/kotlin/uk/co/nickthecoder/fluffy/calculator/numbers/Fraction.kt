package uk.co.nickthecoder.fluffy.calculator.numbers

import uk.co.nickthecoder.fluffy.calculator.FluffyString
import java.util.*


/**
 * Holds a rational number as a [numerator] and a [denominator].
 * The [numerator] and [denominator] are stored as Int, so will blow up with large values.
 *
 * Note, if you use the constructor directly, then the fraction may not be normalised, e.g.
 *
 *      Fraction( 2, 4 ).numerator == 2 // and the denominator is 4
 *
 * So, it is often more useful to use the static method [normalised] to create a Fraction.
 *
 *      Fraction.normalised( 2, 4 ).numerator == 1 // and the denominator is 2
 */
class Fraction(
        val numerator: Int,
        val denominator: Int
) : Number(), FluffyString {

    override fun toByte() = toFloat().toByte()

    override fun toChar() = toFloat().toChar()

    override fun toShort() = toFloat().toShort()

    override fun toInt() = toFloat().toInt()

    override fun toLong() = toDouble().toLong()

    override fun toFloat() = numerator.toFloat() / denominator

    override fun toDouble() = numerator.toDouble() / denominator


    operator fun plus(value: Int) = Fraction.normalised(numerator + value * denominator, denominator)
    operator fun plus(value: Number) = if (value is Int) this + value else toDouble() + value
    operator fun plus(value: Fraction) = Fraction.normalised(
            numerator * value.denominator + value.numerator * this.denominator,
            denominator * value.denominator
    )

    operator fun minus(value: Int) = Fraction.normalised(numerator - value * denominator, denominator)
    operator fun minus(value: Number) = if (value is Int) this - value else toDouble() - value
    operator fun minus(value: Fraction) = Fraction.normalised(
            numerator * value.denominator - value.numerator * this.denominator,
            denominator * value.denominator
    )

    operator fun times(value: Fraction) = Fraction.normalised(numerator * value.numerator, denominator * value.denominator)
    operator fun times(value: Int) = Fraction.normalised(numerator * value, denominator)
    operator fun times(value: Number) = if (value is Int) this * value else toDouble() * value

    operator fun div(value: Fraction) = Fraction.normalised(numerator * value.denominator, denominator * value.numerator)
    operator fun div(value: Int) = Fraction.normalised(numerator, denominator * value)
    operator fun div(value: Number) = if (value is Int) this.div(value) else toDouble() / value


    fun pow(value: Int) = Fraction(
            Math.pow(numerator.toDouble(), value.toDouble()).toInt(),
            Math.pow(denominator.toDouble(), value.toDouble()).toInt()
    )

    fun signnum() = numerator.signum()

    operator fun unaryMinus() = Fraction(-numerator, denominator)

    /**
     * If [value] is 0/0, then 0/0 is returned (which is undefined).
     * If [value] is a whole number, then a Fraction is returned,
     * otherwise a Double is returned.
     */
    fun pow(value: Fraction): Number {

        return if (value.numerator % value.denominator == 0) {
            if (value.numerator == 0) {
                this
            } else {
                pow(value.numerator / value.denominator)
            }
        } else {
            Math.pow(toDouble(), value.toDouble())
        }
    }

    fun gcd() = gcd(denominator, numerator)

    /**
     * Returns a "normalised" version of this fraction, or this, if it is already normalised.
     * A "normalised" fraction never has a negative denominator, divides the numerator and denominator by the
     * greatest common divisor.
     * e.g.
     *      1/2 => this
     *      1/-2 => -1/2
     *      2/4 => 1/2
     *      -2/-4 => 1/2
     *      -2/4 => -1/2
     */
    fun normalised(): Fraction {
        val gcd = gcd()
        if (gcd == 1) return this

        return if (denominator < 0) {
            Fraction(-numerator / gcd, -denominator / gcd)
        } else {
            Fraction(numerator / gcd, denominator / gcd)
        }
    }

    /**
     * Looking at BigDecimal, it appears that "equals" is not mathematical, i.e. two numbers with the same
     * mathematical value can return false. So we will do the same here.
     * e.g.
     *      Int(1) != Fraction(1,1)
     *      Fraction( 2,2 ) != Fraction( 1,1 )
     *      Fraction( n,m ) == Fraction( n,m )
     *
     * Consider normalising before testing for equality.
     * Note, if you never use the constructor directly, but use [normalised] instead, then all of your fractions
     * will be normalised.
     */
    override fun equals(other: Any?): Boolean {
        return if (other is Fraction) {
            numerator == other.numerator && denominator == other.denominator
        } else {
            false
        }
    }

    override fun hashCode() = Objects.hash(numerator, denominator)

    override fun toString() = "Fraction( $numerator / $denominator )"

    override fun toFluffyString(): String =
            if (numerator < 0) {
                if (denominator < 0) {
                    "${-numerator}\\${-denominator}"
                } else {
                    "$numerator\\$denominator"
                }
            } else {
                if (denominator < 0) {
                    "${-numerator}\\${-denominator}"
                } else {
                    "$numerator\\$denominator"
                }
            }

    fun toMixedString(): String {
        val whole = numerator / denominator
        return if (whole == 0) {
            toFluffyString()
        } else if (whole < 0) {
            "${whole.toFluffyString()}\\${Math.abs(numerator % denominator).toFluffyString()}\\${Math.abs(denominator).toFluffyString()}"
        } else if (numerator < 0) {
            "${whole.toFluffyString()}\\${(-numerator % -denominator).toFluffyString()}\\${(-denominator).toFluffyString()}"
        } else {
            "${whole.toFluffyString()}\\${(numerator % denominator).toFluffyString()}\\${denominator.toFluffyString()}"
        }
    }


    companion object {

        @JvmStatic
        fun gcd(a: Int, b: Int): Int = positiveGcd(Math.abs(a), Math.abs(b))

        @JvmStatic
        fun normalised(numerator: Int, denominator: Int): Fraction {
            val gcd = Fraction.gcd(numerator, denominator)
            return if (denominator < 0) {
                Fraction(-numerator / gcd, -denominator / gcd)
            } else {
                Fraction(numerator / gcd, denominator / gcd)
            }
        }

        private tailrec fun positiveGcd(a: Int, b: Int): Int {
            return if (b == 0) {
                a
            } else {
                positiveGcd(b, a % b)
            }
        }
    }

}

operator fun Int.plus(value: Fraction) = Fraction.normalised(this * value.denominator + value.numerator, value.denominator)
operator fun Number.plus(value: Fraction) = this + value.toDouble()

operator fun Int.minus(value: Fraction) = Fraction.normalised(this * value.denominator - value.numerator, value.denominator)
operator fun Number.minus(value: Fraction) = this - value.toDouble()

operator fun Int.times(value: Fraction) = Fraction.normalised(this * value.numerator, value.denominator)
operator fun Number.times(value: Fraction) = this * value.toDouble()

operator fun Int.div(value: Fraction) = Fraction.normalised(value.numerator, this * value.denominator)
operator fun Number.div(value: Fraction) = this / value.toDouble()

// TODO fun Int.pow(value: Fraction): Fraction =

fun Number.toFraction(): Fraction {
    return when (this) {
        is Int -> Fraction(this, 1)
        is Long, is Short, is Byte -> Fraction.normalised(this.toInt(), 1)
        else -> TODO()
    }
}
