package uk.co.nickthecoder.fluffy.calculator.units

import uk.co.nickthecoder.fluffy.calculator.FluffyString
import uk.co.nickthecoder.fluffy.calculator.numbers.div
import uk.co.nickthecoder.fluffy.calculator.numbers.minus
import uk.co.nickthecoder.fluffy.calculator.numbers.plus
import uk.co.nickthecoder.fluffy.calculator.numbers.times
import uk.co.nickthecoder.fluffy.calculator.numbers.toFluffyString
import java.util.*

class Measure(
    val value: Number,
    val units: Units
) : FluffyString {

    val dimensionlessValue: Number
        get() {
            if (units.isDimensionless()) {
                return value * units.conversionTo(Units.SINGLE)
            } else {
                throw UnitsException("Not unitless : $units")
            }
        }

    operator fun plus(other: Number) = dimensionlessValue + other

    operator fun plus(other: Measure): Measure {
        val conversion = other.units.conversionTo(this.units)
        val floor = Math.floor(conversion)

        return if (floor == conversion && Math.abs(floor) < Int.MAX_VALUE) {
            // Use integer maths where possible, so that fractions will stay as fractions, integers will stay as integers etc.
            (Measure(value + other.value * floor.toInt(), units))
        } else {
            // Otherwise, the value will be converted to a double
            (Measure(value + other.value * conversion, units))
        }
    }

    operator fun minus(other: Number) = dimensionlessValue - other

    operator fun minus(other: Measure): Measure {
        val conversion = other.units.conversionTo(this.units)
        val floor = Math.floor(conversion)

        return if (floor == conversion && Math.abs(floor) < Int.MAX_VALUE) {
            // Use integer maths where possible, so that fractions will stay as fractions, integers will stay as integers etc.
            (Measure(value - other.value * floor.toInt(), units))
        } else {
            // Otherwise, the value will be converted to a double
            (Measure(value - other.value * conversion, units))
        }
    }

    operator fun times(other: Number) = Measure(value * other, units)

    operator fun times(other: Measure): Measure {
        return Measure(value * other.value, units * other.units)
    }

    operator fun div(other: Number) = Measure(value / other, units)

    operator fun div(other: Measure): Measure {
        return Measure(value / (other.value), units / other.units)
    }

    fun removeZeroPowers(): Measure {
        val (newUnits, conversion) = units.removeZeroPowers()
        return Measure(value * conversion, newUnits)
    }

    override fun equals(other: Any?) =
        if (other !is Measure) {
            false
        } else {
            units == other.units && value == other.value
        }

    override fun hashCode() = Objects.hash(value, units)

    override fun toString(): String {
        val units = units.toString()
        return if (units.isEmpty()) value.toFluffyString() else "${value.toFluffyString()}($units)"
    }

    override fun toFluffyString() = removeZeroPowers().toString()
}

operator fun Number.times(other: Measure) = Measure(this * other.value, other.units)
operator fun Number.div(other: Measure) = Measure(this / other.value, other.units.inverse())

operator fun Number.plus(other: Measure) = this + other.dimensionlessValue
operator fun Number.minus(other: Measure) = this - other.dimensionlessValue

operator fun Number.invoke(units: Units) = Measure(this, units)


