package uk.co.nickthecoder.fluffy.calculator.units

class UnitsException(msg: String) : RuntimeException(msg)
