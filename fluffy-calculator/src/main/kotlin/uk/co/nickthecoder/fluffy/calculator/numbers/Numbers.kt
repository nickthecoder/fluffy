package uk.co.nickthecoder.fluffy.calculator.numbers

import java.math.BigDecimal
import java.math.BigInteger

// Allow operators + - * / to be applied to Number instances.
// The types known about are the primitive types byte, short, int, long, float and double,
// as well as Fraction.
// For other subclasses of Number, such as BigDecimal and BigInteger, they are converted to Double
// before the operator is applied.
//
// The returned type is often the same as the 1st operands type, except for "unknown" types, including
// BigDecimal and BigInteger.
// Also, when mixing Double or Float with Fraction, the return type will be Double.

operator fun Number.times(other: Number): Number =
        when (this) {
            is Int -> when (other) {
                is Int -> this * other
                is Short -> this * other
                is Byte -> this * other
                is Fraction -> this * other
                is BigInteger -> BigInteger.valueOf(this.toLong()).multiply(other)
                else -> this.toDouble() * other.toDouble()
            }
            is Short -> when (other) {
                is Int -> this * other
                is Short -> this * other
                is Byte -> this * other
                is Fraction -> this.toInt() * other
                is BigInteger -> BigInteger.valueOf(this.toLong()).multiply(other)
                else -> this.toDouble() * other.toDouble()
            }
            is Byte -> when (other) {
                is Int -> this * other
                is Short -> this * other
                is Byte -> this * other
                is Fraction -> this.toInt() * other
                is BigInteger -> BigInteger.valueOf(this.toLong()).multiply(other)
                else -> this.toDouble() * other.toDouble()
            }
            is Fraction -> when (other) {
                is Int -> this * other
                is Short -> this.toInt() * other
                is Byte -> this.toInt() * other
                is Fraction -> this * other
                // is BigInteger ->  // TODO
                else -> this.toDouble() * other.toDouble()
            }
            is BigInteger -> when (other) {
                is Int -> this.multiply(BigInteger.valueOf(other.toLong()))
                is Byte -> this.multiply(BigInteger.valueOf(other.toLong()))
                is Short -> this.multiply(BigInteger.valueOf(other.toLong()))
                is Long -> this.multiply(BigInteger.valueOf(other))
                is BigInteger -> this.multiply(other)
                else -> this.toDouble() * other.toDouble()
            }
            else -> this.toDouble() * other.toDouble()
        }

operator fun Number.div(other: Number): Number =
        when (this) {
            is Int -> when (other) {
                is Int -> this.toDouble() / other
                is Short -> this.toDouble() / other
                is Byte -> this.toDouble() / other
                is Fraction -> this / other
                is BigInteger -> BigDecimal.valueOf(this.toLong()).divide(BigDecimal(other))
                else -> this.toDouble() / other.toDouble()
            }
            is Short -> when (other) {
                is Int -> this / other
                is Short -> this / other
                is Byte -> this / other
                is Fraction -> this.toInt() / other
                is BigInteger -> BigDecimal.valueOf(this.toLong()).divide(BigDecimal(other))
                else -> this.toDouble() / other.toDouble()
            }
            is Byte -> when (other) {
                is Int -> this / other
                is Short -> this / other
                is Byte -> this / other
                is Fraction -> this.toInt() / other
                is BigInteger -> BigDecimal.valueOf(this.toLong()).divide(BigDecimal(other))
                else -> this.toDouble() / other.toDouble()
            }
            is Fraction -> when (other) {
                is Int -> this / other
                is Short -> this.toInt() / other
                is Byte -> this.toInt() / other
                is Fraction -> this / other
                // is BigInteger ->  // TODO
                else -> this.toDouble() / other.toDouble()
            }
            else -> this.toDouble() / other.toDouble()
        }

operator fun Number.plus(other: Number): Number =
        when (this) {
            is Int -> when (other) {
                is Int -> this + other
                is Short -> this + other
                is Byte -> this + other
                is Fraction -> this + other
                is BigInteger -> BigInteger.valueOf(this.toLong()).plus(other)
                else -> this.toDouble() + other.toDouble()
            }
            is Short -> when (other) {
                is Int -> this + other
                is Short -> this + other
                is Byte -> this + other
                is Fraction -> this.toInt() + other
                is BigInteger -> BigInteger.valueOf(this.toLong()).plus(other)
                else -> this.toDouble() + other.toDouble()
            }
            is Byte -> when (other) {
                is Int -> this + other
                is Short -> this + other
                is Byte -> this + other
                is Fraction -> this.toInt() + other
                is BigInteger -> BigInteger.valueOf(this.toLong()).plus(other)
                else -> this.toDouble() + other.toDouble()
            }
            is Fraction -> when (other) {
                is Int -> this + other
                is Short -> this.toInt() + other
                is Byte -> this.toInt() + other
                is Fraction -> this + other
                // is BigInteger ->  // TODO
                else -> this.toDouble() + other.toDouble()
            }
            else -> this.toDouble() + other.toDouble()
        }


operator fun Number.minus(other: Number): Number =
        when (this) {
            is Int -> when (other) {
                is Int -> this - other
                is Short -> this - other
                is Byte -> this - other
                is Fraction -> this - other
                is BigInteger -> BigInteger.valueOf(this.toLong()).minus(other)
                else -> this.toDouble() - other.toDouble()
            }
            is Short -> when (other) {
                is Int -> this - other
                is Short -> this - other
                is Byte -> this - other
                is Fraction -> this.toInt() - other
                is BigInteger -> BigInteger.valueOf(this.toLong()).minus(other)
                else -> this.toDouble() - other.toDouble()
            }
            is Byte -> when (other) {
                is Int -> this - other
                is Short -> this - other
                is Byte -> this - other
                is Fraction -> this.toInt() - other
                is BigInteger -> BigInteger.valueOf(this.toLong()).minus(other)
                else -> this.toDouble() - other.toDouble()
            }
            is Fraction -> when (other) {
                is Int -> this - other
                is Short -> this.toInt() - other
                is Byte -> this.toInt() - other
                is Fraction -> this - other
                is BigInteger -> BigInteger.valueOf(this.toLong()).minus(other)
                // is BigInteger ->  // TODO
                else -> this.toDouble() - other.toDouble()
            }
            else -> this.toDouble() - other.toDouble()
        }

fun Number.pow(other: Number): Number =
        when (this) {
            is Int -> when (other) {
                is Fraction -> this.pow(other)
                else -> Math.pow(this.toDouble(), other.toDouble()).toInt()
            }
            is Short -> when (other) {
                is Fraction -> this.toInt() - other
                else -> Math.pow(this.toDouble(), other.toDouble()).toShort()
            }
            is Byte -> when (other) {
                is Fraction -> this.toInt() - other
                else -> Math.pow(this.toDouble(), other.toDouble()).toByte()
            }
            is Fraction -> when (other) {
                is Int -> this.pow(other)
                is Short -> this.toInt().pow(other)
                is Byte -> this.toInt().pow(other)
                is Long -> this.toInt().pow(other)
                is Fraction -> this.pow(other)
                else -> Math.pow(this.toDouble(), other.toDouble())
            }

            else -> Math.pow(this.toDouble(), other.toDouble())
        }

fun Number.signum(): Int = when (this) {
    is Fraction -> this.denominator.signum() * this.numerator.signum()
    is BigInteger -> this.signum()
    else -> Math.signum(this.toDouble()).toInt()
}

/**
 * * Byte, Short, Int and Long all use toString().
 * * Floats and Doubles appear without a decimal point if they are integers.
 * * Float and Doubles are rounded, to hide rounding errors. e.g. `1.0 - 0.1 * 9` is `"0.1"`.
 * * Fractions use [Fraction.toFluffyString].
 */
fun Number.toFluffyString() =
        when (this) {
            is Int, is Short, is Byte, is Long -> toString()
            is Float -> if (Math.round(this).toFloat() == this) {
                Math.round(this).toString()
            } else if (this > 1_000_000_000.0f || this < -1_000_000_000.0f) {
                this.toString()
            } else {
                (Math.round(this * 1_000_000.0) / 1_000_000.0).toString()
            }
            is Double -> if (Math.round(this).toDouble() == this) {
                Math.round(this).toString()
            } else if (this > 1_000_000_000_000.0 || this < -1_000_000_000_000.0) {
                this.toString()
            } else {
                (Math.round(this * 1_000_000_000_000.0) / 1_000_000_000_000.0).toString()
            }
            is Fraction -> toFluffyString()
            else -> toString()
        }


/**
 * Allows fluffy script `number.fluffy` to add operators for the type Number.
 */
class Numbers {
    companion object {

        @JvmStatic
        fun plus(a: Number, b: Number) = a + b

        @JvmStatic
        fun minus(a: Number, b: Number) = a - b

        @JvmStatic
        fun times(a: Number, b: Number) = a * b

        @JvmStatic
        fun div(a: Number, b: Number) = a / b

        @JvmStatic
        fun pow(a: Number, b: Number) = a.pow(b)

    }
}
