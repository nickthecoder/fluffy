package uk.co.nickthecoder.fluffy.calculator.units

/**
 * SI Prefixes, such as "k" for kilo-, and "m" for milli-
 */
enum class Prefix(val prefix: String, val tenToThe: Int) {

    NONE("", 0),

    DECCA("da", 1),
    DECTO("ha", 2),
    KILO("k", 3),
    MEGA("M", 6),
    GIGA("G", 9),
    TERRA("T", 12),
    PETTA("P", 15),
    EXA("E", 18),
    ZETTA("Z", 21),
    YOTTA("Y", 24),

    DECI("d", -1),
    CENTI("c", -2),
    MILLI("m", -3),
    MICRO("u", -6),
    NANO("n", -9),
    PICO("p", -12),
    FEMTO("f", -15),
    ATTO("a", -18),
    ZEPTO("z", -21),
    YOCTA("y", -24);

    val factor: Double = Math.pow(10.0, tenToThe.toDouble())

    /**
     * Add the powers.
     *
     * e.g. [KILO] * [KILO] = [MEGA]
     */
    operator fun times(other: Prefix) = findFactor(tenToThe + other.tenToThe)

    operator fun div(other: Prefix) = findFactor(tenToThe - other.tenToThe)

    fun inverse() = powerToFactor.getValue(-tenToThe)

    /**
     * The multiplication factor needed to converts from this factor to [other].
     * For example to convert from [KILO] to [NONE], the result is 1000
     */
    fun conversionTo(other: Prefix): Double = this.factor / other.factor

    override fun toString() = prefix

    companion object {
        @JvmStatic
        val powerToFactor = Prefix.values().associateBy({ it.tenToThe }, { it })

        @JvmStatic
        fun findFactor(tenToThe: Int) = powerToFactor[tenToThe] ?: throw UnitsException("No prefix for ten to $tenToThe")
    }
}
