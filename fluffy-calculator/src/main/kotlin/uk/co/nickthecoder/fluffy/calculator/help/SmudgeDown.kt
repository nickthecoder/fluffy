package uk.co.nickthecoder.fluffy.calculator.help

import uk.co.nickthecoder.scarea.*

/**
 * A very basic markdown-like parser. It does just enough for the help pages to be rendered in a
 * TediArea. This doesn't abide by most of the Markdown rules!
 *
 * The syntax :
 *
 * * Headings (lines beginning with `"# "` or `"## "`)
 * * Bullet lists (lines beginning with `"* "`)
 * * Links (in the form `[linkText.fileExtension]`)
 * * Blocks (lines beginning with exactly four spaces)
 * * Comments (in the form : `Plain text // A comment till the end of line`)
 *
 */
class SmudgeDown(val scarea: Scarea) {

    private val highlights = mapOf<String, Highlight>(
            "heading1" to StyleHighlight("-fx-font-size: 160%; -fx-font-weight: bold;"),
            "heading2" to StyleHighlight("-fx-font-size: 120%; -fx-font-weight: bold;"),
            "comment" to StyleHighlight("-fx-fill: #888;"),
            "block" to StyleHighlight("-fx-fill: #0c0;"),
            "bullet" to StyleHighlight("-fx-font-style: italic;"),
            "link" to StyleHighlight("-fx-fill: #33f; -fx-font-weight: bold; -fx-underline: true;")
    )

    fun toTedi(text: String) {
        val lines = text.split("\n")
        for (line in lines) {

            if (line == "```") {
                // Do nothing. A code block
            } else if (line.startsWith("# ")) {
                append(line.substring(2), "heading1")

            } else if (line.startsWith("## ")) {
                append(line.substring(3), "heading2")

            } else if (line.startsWith("* ")) {
                append("  ∙ " + line.substring(2), "bullet")

            } else if (line.startsWith("    ")) {
                append(line.substring(4), "block")

            } else {
                append(line, null)
            }

            appendText("\n")
        }
    }

    private fun append(text: String, highlight: String?) {

        val tediStart = scarea.document.endPosition()
        var i = 0
        while (i < text.length) {

            val commentStartIndex = text.indexOf("//, i")
            val linkOpen = text.indexOf("[", i)

            if (commentStartIndex >= 0 && (commentStartIndex < linkOpen || linkOpen < 0)) {
                // We have found a comment, and no (more) links
                append(text.substring(i, commentStartIndex), highlight)
                val commentText = text.substring(commentStartIndex)
                scarea.document.insert(scarea.document.endPosition(), commentText)
                val commentStart = scarea.document.endPosition()
                val commendEnd = ScareaPosition(commentStart.line, commentStart.column + commentText.length)
                val hr = HighlightRange(commentStart, commendEnd, highlights["comment"]!!)
                scarea.document.addHighlightRange(hr)
                return
            }

            if (linkOpen >= 0) {
                val linkClose = text.indexOf(']', linkOpen)
                if (linkClose >= 0) {
                    appendText(text.substring(i, linkOpen)) // Plain text
                    appendLink(text.substring(linkOpen + 1, linkClose)) // The link.
                    i = linkClose + 1
                    continue
                } else {
                    // No matching close, so let's stop here. The open square bracket will be displayed as plain text.
                    break
                }
            } else {
                break
            }
        }

        appendText(text.substring(i))
        val end = scarea.document.endPosition()
        if (end > tediStart && highlight != null) {
            val hr = HighlightRange(tediStart, end, highlights[highlight]!!)
            scarea.document.addHighlightRange(hr)
        }
    }

    private fun appendLink(link: String) {
        // A Bodge to allow lists within the help text.
        if (link.startsWith(' ')) {
            appendText("[ $link]")
            return
        }

        val dot = link.lastIndexOf('.')

        val text = if (link.startsWith("http://")) {
            link.substring(7)
        } else if (link.startsWith("https://")) {
            link.substring(8)
        } else if (link.startsWith("builtin:")) {
            link.substring(8)
        } else if (dot > 0) {
            link.substring(0, dot)
        } else {
            link
        }

        val start = scarea.document.endPosition()
        scarea.document.insert(start, text)
        val end = ScareaPosition(start.line, start.column + text.length)
        val hr = LinkHighlightRange(start, end, highlights["link"]!!, link)
        scarea.document.addHighlightRange(hr)
    }

    private fun appendText(text: String) {
        scarea.document.insert(scarea.document.endPosition(), text)
    }

}

/**
 * The gui can use the [link], to jump to a different document when the text within this range is clicked.
 */
class LinkHighlightRange(start: ScareaPosition, end: ScareaPosition, highlight: Highlight, val link: String)
    : HighlightRange(start, end, highlight)
