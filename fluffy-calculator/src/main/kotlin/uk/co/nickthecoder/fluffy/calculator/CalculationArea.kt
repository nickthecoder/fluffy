package uk.co.nickthecoder.fluffy.calculator

import javafx.beans.InvalidationListener
import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.ReadOnlyStringProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import uk.co.nickthecoder.fluffy.FluffyException
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.fxessentials.requestFocusSoon
import uk.co.nickthecoder.fxessentials.selectedWith
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import uk.co.nickthecoder.scarea.syntax.HighlightMatchedPairs
import java.io.File

/**
 * Uses a text area (actually a [Scarea]) where you can type in expressions.
 * The results are inserted as text into the same text area, so you can easily perform
 * addition maths on the results.
 *
 * Hit = to perform a calculation of the current line.
 *
 * A syntax highlighter is employed, so that strings and other literal constants stand out.
 * A special highlighter is used for punctuation (), [] and {} : when the caret is next to a
 * punctuation character, both it and its partner are highlighted, so that it is easier to
 * understand complex expression.
 */
open class CalculationArea(
    val calculator: Calculator,
    syntaxHighlighter: FluffySyntaxHighlighter? = FluffySyntaxHighlighter()
) : BorderPane() {

    val inputArea = createTextArea(false)

    private val resultsToolBar = ToolBar()

    private val resultsArea = createTextArea(true)

    protected val defaultStatus = "To evaluate an expression, move the cursor, then press ${CALCULATE.shortcutLabel()}"

    protected val status = Label(defaultStatus)

    protected val statusArea = BorderPane().apply {
        center = status
        BorderPane.setAlignment(status, Pos.CENTER_LEFT)
        this@CalculationArea.bottom = this
        setMargin(status, Insets(2.0, 6.0, 2.0, 6.0))
    }

    private val resultsPane = BorderPane().apply {
        top = resultsToolBar
        center = resultsArea
    }

    /**
     * Allows two TediAreas with the same content side by side.
     * (So that you can copy/paste from different areas of the same document easily).
     */
    private val hSplit = SplitPane(inputArea)

    /**
     * The top half is the [inputArea], or the [hSplit]. The bottom is the [resultsPane].
     * The [resultsPane] is hidden until needed.
     */
    private var vSplit = SplitPane(hSplit).apply { orientation = Orientation.VERTICAL }

    private val showResults = SimpleBooleanProperty(false).apply {
        addListener(InvalidationListener { toggleResults() })
    }

    /**
     * Should the results be appended or cleared? Bound to the RESULTS_APPEND toggle button.
     */
    private val appendResultsProperty = SimpleBooleanProperty(false)

    protected val actionGroup = ActionGroup().apply {
        toggle(RESULTS_TOGGLE, showResults)
        toggle(RESULTS_APPEND, appendResultsProperty)
        action(CLOSE) { showResults.value = false; Unit }
        action(SPLIT_TOGGLE) { toggleSplit() }

        //⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻
        action(SUPER_PLUS) { insert('⁺') }
        action(SUPER_MINUS) { insert('⁻') }
        action(SUPER_0) { insert('⁰') }
        action(SUPER_1) { insert('¹') }
        action(SUPER_2) { insert('²') }
        action(SUPER_3) { insert('³') }
        action(SUPER_4) { insert('⁴') }
        action(SUPER_5) { insert('⁵') }
        action(SUPER_6) { insert('⁶') }
        action(SUPER_7) { insert('⁷') }
        action(SUPER_8) { insert('⁸') }
        action(SUPER_9) { insert('⁹') }

        attachTo(this@CalculationArea)
    }

    private fun insert(c: Char) {
        val (from, to) = inputArea.selection
        inputArea.document.replace(from, to, c.toString())
    }

    /*
     * null if not saved (or loaded)
     */
    private var mutableFile: File? = null
        set(v) {
            field = v
            titleProperty.value = if (v == null) {
                "Untitled"
            } else {
                if (v.extension == "fluffy") {
                    v.nameWithoutExtension
                } else {
                    v.name
                }
            }
        }

    val file: File?
        get() = mutableFile

    // Dirty
    private val dirtyProperty = SimpleBooleanProperty(false)

    fun dirtyProperty(): ReadOnlyBooleanProperty = dirtyProperty

    // Title
    protected val titleProperty = SimpleStringProperty("Untitled")

    fun titleProperty(): ReadOnlyStringProperty = titleProperty


    init {
        center = vSplit
        prefWidth = 600.0
        prefHeight = 300.0

        syntaxHighlighter?.attach(inputArea)
        with(inputArea) {
            HighlightMatchedPairs(this)
            document.textProperty().addListener(InvalidationListener { dirtyProperty.value = true })
        }

        with(actionGroup) {
            resultsToolBar.items.addAll(
                button(CLOSE), Label("Results"),
                Separator(),
                toggleButton(RESULTS_APPEND).selectedWith(appendResultsProperty)
            )
        }

        calculator.resetBinding()
    }

    /**
     * NOTE, this is used within the constructor, but this is an open method.
     * This is very bad style, because the [CalculationArea] object isn't fully instantiated yet.
     * So be very careful that we don't use any fields that aren't valid!
     *
     * I'm pretty sure I can get away with it though, as we don't use any fields here or in the
     * sub-class HelpArea.
     */
    protected open fun createTextArea(isResultsArea: Boolean = false, linkedTo: Scarea? = null): Scarea {

        val scarea: Scarea = (if (linkedTo == null) Scarea() else Scarea(linkedTo.document)).apply {
            styleClass.add("code")
            displayLineNumbers = true
        }

        if (!isResultsArea) {

            ActionGroup().apply {

                action(CALCULATE) {
                    evaluateCurrentLine(scarea)
                }
                action(SCRIPT_RUN) {
                    evaluateSelectionOrAll()
                }
                action(TYPE_NEW_LINE) {
                    scarea.replaceSelection("\n")
                }
                action(TYPE_EQUALS) {
                    scarea.replaceSelection("=")
                }

                action(TEXT_CLEAR_TO_EOL) {
                    val pos = scarea.caretPosition
                    val end = ScareaPosition(pos.line, Int.MAX_VALUE)
                    scarea.document.delete(pos, end)
                }

                val canNotCopy = scarea.hasSelectionProperty().not()
                action(EDIT_COPY, canNotCopy) { scarea.copy() }
                action(EDIT_CUT, canNotCopy) { scarea.cut() }
                action(EDIT_PASTE) { scarea.paste() }

                attachTo(scarea)

                scarea.contextMenu = ContextMenu().apply {
                    items.add(menuItem(EDIT_CUT))
                    items.add(menuItem(EDIT_COPY))
                    items.add(menuItem(EDIT_PASTE))
                    items.add(SeparatorMenuItem())
                    items.add(menuItem(CALCULATE))
                    items.add(menuItem(SCRIPT_RUN))
                }

            }
        }

        return scarea
    }

    fun open(file: File): Boolean {
        return try {
            inputArea.text = file.readText()
            dirtyProperty.value = false
            mutableFile = file
            true

        } catch (e: Exception) {
            Alert(Alert.AlertType.ERROR, "Load Failed").apply {
                title = "Load Failed"
                show()
            }
            false
        }
    }

    fun saveTo(file: File): Boolean {
        return try {
            file.writeText(inputArea.text)
            dirtyProperty.value = false
            this.mutableFile = file
            true
        } catch (e: Exception) {
            Alert(Alert.AlertType.ERROR, "Save Failed").apply {
                title = "Save Failed"
                show()
            }
            false
        }
    }

    fun toggleSplit() {
        if (hSplit.items.size == 1) {
            hSplit.items.add(createTextArea(false, inputArea))
        } else {
            hSplit.items.removeAt(1)
        }
        inputArea.requestFocusSoon()
    }

    //fun evaluate(code: String) = calculator.evaluate(code)

    /**
     * Runs the whole script (or the selected text if there is a selection).
     * The results are displayed in the results area, not in-line with the
     * text.
     */
    fun evaluateSelectionOrAll() {
        calculator.resetBinding()
        status.text = defaultStatus

        val selection = inputArea.selection
        val selectedText = inputArea.selectedText()
        val toEvaluate = if (selectedText.isBlank()) inputArea.text else selectedText
        try {
            val result = calculator.evaluate(toEvaluate)
            val (_, str) = calculator.processResult(result)
            updateResults(str)
        } catch (e: Exception) {
            if (e is FluffyException) {
                if (toEvaluate === selectedText) {
                    val line = selection.first.line + e.position.line - 1
                    val column = if (line == selection.first.line) {
                        selection.first.column + e.position.column
                    } else {
                        e.position.column
                    }
                    inputArea.caretPosition = ScareaPosition(line, column)
                } else {
                    inputArea.caretPosition = ScareaPosition(e.position.line - 1, e.position.column)
                }
                inputArea.requestFocus()
            }
            status.text = "${e.javaClass.simpleName} ${e.message}"
        }
    }

    /**
     * When "=" is pressed, evaluates the current line.
     */
    private fun evaluateCurrentLine(inputArea: Scarea) {
        status.text = defaultStatus

        val line = inputArea.caretPosition.line
        val lineText = inputArea.document.paragraphs[line]

        try {
            val result = calculator.evaluate(lineText)
            if (result != Unit) {
                val (valid, str) = calculator.processResult(result)

                val toInsert = if (valid) {
                    "\n$str"

                } else {
                    if (str.contains('\n')) {
                        updateResults(str)
                        "" // Insert nothing
                    } else {
                        "\n// $str"
                    }
                }

                val insertPosition = ScareaPosition(line, lineText.length)
                inputArea.setSelection(insertPosition, insertPosition)
                inputArea.replaceSelection(toInsert)

            }

        } catch (e: Exception) {
            if (e is FluffyException) {
                inputArea.caretPosition = ScareaPosition(line, e.position.column)
                inputArea.requestFocus()
            }
            status.text = "${e.javaClass.simpleName} ${e.message}"
            //e.printStackTrace()
        }

    }

    /**
     * Adds [text] to the [resultsArea]. If [appendResultsProperty] is false, then the
     * existing text is cleared.
     */
    private fun updateResults(text: String) {
        if (text.isEmpty()) return
        showResults.value = true
        if (appendResultsProperty.value == true) {
            resultsArea.document.insert(resultsArea.document.endPosition(), text)
        } else {
            resultsArea.text = text
        }

        resultsArea.caretPosition = resultsArea.document.endPosition()
    }

    private fun toggleResults() {
        if (showResults.value) {
            if (!vSplit.items.contains(resultsPane)) {
                vSplit.items.add(resultsPane)
            }
        } else {
            vSplit.items.remove(resultsPane)
            inputArea.requestFocus()
        }
    }

}
