package uk.co.nickthecoder.fluffy.calculator

import javafx.beans.InvalidationListener
import javafx.beans.binding.Bindings
import javafx.beans.binding.When
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.scene.control.*
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import javafx.stage.Stage
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.calculator.help.HelpArea
import uk.co.nickthecoder.fxessentials.*
import java.io.File

/**
 * A GUI containing a TabPane of [CalculationArea]s, plus toolbars etc.
 * Calculation areas can be loaded/saved.
 */
class CalculatorGUI() : VBox() {

    var shared: Boolean = false

    var bindingFactory: () -> Binding = { Binding() }
        set(v) {
            field = v
            sharedCalculator = createCalculator()
        }

    var sharedCalculator = createCalculator()

    private val toolBar = ToolBar()

    private val tabPane = TabPane()

    private val titleProperty = SimpleStringProperty("Calculator")

    private val titleSeparator = SimpleStringProperty(" - ")

    // I hate using negatives, but as I want to use it with [Button.disableProperty],
    // I feel that I should. Grr.
    private val noTabs = Bindings.isNull(tabPane.selectionModel.selectedItemProperty())

    val angleProperty = SimpleDoubleProperty(Math.PI / 180.0).apply {
        addListener(InvalidationListener { updateOptions() })
    }

    /**
     * Should Fractions be displayed as top-heavy (n\m) or as (I + n\m)
     */
    private val topHeavyProperty = SimpleBooleanProperty(true).apply {
        addListener(InvalidationListener { updateOptions() })
    }

    private val actionGroup = ActionGroup().apply {
        action(DOCUMENT_OPEN) { open() }
        action(DOCUMENT_SAVE, disabled = noTabs) { save() }
        action(DOCUMENT_SAVE_AS, disabled = noTabs) { saveAs() }
        action(TAB_NEW) { newTab(null) }
        action(TAB_CLOSE) { currentTab()?.let { tabPane.tabs.remove(it) } }
        action(WINDOW_NEW) { newWindow() }
        action(SCRIPT_RUN, disabled = noTabs) { currentCalculationArea()?.evaluateSelectionOrAll() } // TODO Replace this!
        action(OPTIONS_MENU) {}
        choices(angleProperty).apply {
            toggle(OPTION_DEGREES, Math.PI / 180.0)
            toggle(OPTION_RADIANS, 1.0)
            // I could add gradians, but who the heck uses them! No, I don't want to pollute the GUI with useless crap!
        }
        action(EDIT_SHORTCUTS) { actions.edit() }
        toggle(OPTION_TOP_HEAVY, topHeavyProperty)
        action(HELP) { help() }
    }

    private val optionsMenuButton = actionGroup.menuButton(OPTIONS_MENU)

    private val openButton: SplitMenuButton = actionGroup.splitMenuButton(DOCUMENT_OPEN)

    // We MUST keep a reference to the handler, otherwise it will be gc'd, and the menu won't update.
    private val recentFilesHandler = ::openFile


    init {

        CalculatorApp.recentFiles.addMenu(openButton, recentFilesHandler)

        prefWidth = 600.0
        prefHeight = 400.0
        children.addAll(toolBar, tabPane)
        actionGroup.attachTo(this)

        with(actionGroup) {
            optionsMenuButton.items.addAll(
                    checkMenuItem(OPTION_DEGREES),
                    checkMenuItem(OPTION_RADIANS),
                    SeparatorMenuItem(),
                    checkMenuItem(OPTION_TOP_HEAVY)
            )

            toolBar.items.addAll(
                    openButton,
                    button(DOCUMENT_SAVE),
                    button(DOCUMENT_SAVE_AS),
                    button(TAB_NEW),
                    button(WINDOW_NEW),
                    button(SCRIPT_RUN),
                    createHGrow(),
                    button(HELP),
                    button(EDIT_SHORTCUTS),
                    optionsMenuButton
            )
        }

        with(tabPane) {
            // Demonstrates how to use tabPane.hideTabs extension var.
            // Hides the header area of the tab pane when there is only one tab.
            tabs.addListener(ListChangeListener { tabPane.hideTabs = tabPane.tabs.size < 2 })
            selectionModel.selectedItemProperty().addListener(InvalidationListener { tabChanged() })
        }

        VBox.setVgrow(tabPane, Priority.ALWAYS)
        updateOptions()
    }

    fun createCalculator() = Calculator().apply {
        bindingFactory = this@CalculatorGUI.bindingFactory
    }

    private fun updateOptions() {
        currentCalculationArea()?.calculator?.let { calculator ->
            calculator.evaluate("_trigToRadians = ${angleProperty.value}")
            calculator.topHeavyFractions = topHeavyProperty.value
        }
    }

    private fun tabChanged() {
        val stageTitleProperty = (scene.window as? Stage)?.titleProperty()
        val tab = currentTab()
        if (tab == null) {
            stageTitleProperty?.bind(titleProperty)
        } else {
            stageTitleProperty?.bind(tab.textProperty().concat(titleSeparator).concat(titleProperty))
        }
        currentCalculationArea()?.inputArea?.requestFocusLater()
        updateOptions()
    }

    private fun currentTab(): Tab? = tabPane.selectionModel.selectedItem

    private fun currentCalculationArea() = currentTab()?.content as? CalculationArea

    private fun createTab(): Pair<Tab, CalculationArea> {
        val ca = CalculationArea(if (shared) sharedCalculator else createCalculator())

        val tab = Tab().apply {
            content = ca
        }
        // The text of the tab is the name of the script (or "Untitled") with a "*" at the end when it needs saving
        tab.textProperty().bind(ca.titleProperty().concat(When(ca.dirtyProperty()).then(" *").otherwise("")))
        return Pair(tab, ca)
    }

    /**
     * If the file is already open in one of the tabs, selects that tab.
     * Otherwise opens the file in a new tab.
     */
    fun openFile(file: File) {
        val abs = file.absoluteFile
        for (tab in tabPane.tabs) {
            val content = tab.content
            if (content is CalculationArea) {
                if (content.file == abs) {
                    tabPane.selectionModel.select(tab)
                    return
                }
            }
        }
        newTab(abs)
    }

    fun newTab() {
        newTab(null)
    }

    private fun newTab(file: File?): Boolean {
        val (tab, ca) = createTab()

        file?.let {
            if (ca.open(it)) {
                CalculatorApp.recentFiles.add(it)
            } else {
                return false
            }
        }

        tabPane.tabs.add(tab)
        tabPane.selectionModel.select(tab)
        return true
    }

    private fun newWindow() {
        val newGui = CalculatorApp.windowNew()
        newGui.bindingFactory = bindingFactory
        newGui.shared = shared
        newGui.angleProperty.value = angleProperty.value
        newGui.newTab()
    }

    private fun addFiltersTo(extensionFilters: ObservableList<FileChooser.ExtensionFilter>) {
        extensionFilters.addAll(
                FileChooser.ExtensionFilter("Fluffy Files", "*.fluffy"),
                FileChooser.ExtensionFilter("Text Files", "*.txt"),
                FileChooser.ExtensionFilter("All Files", "*")
        )
    }

    fun open(): Boolean {
        val f = FileChooser().apply {
            title = "Open Fluffy File"
            addFiltersTo(extensionFilters)

        }.showOpenDialog(scene.window) ?: return false

        // If the current tab has an empty unsaved document, then open in that tab
        currentCalculationArea()?.let {
            if (it.inputArea.document.isEmpty() && it.file == null) {
                return it.open(f)
            }
        }

        return newTab(f)
    }

    private fun chooseSaveFile(dir: File? = null): File? {
        return FileChooser().apply {
            title = "Save Fluffy File"
            dir?.let { initialDirectory = it }
            addFiltersTo(extensionFilters)

        }.showSaveDialogWithExtension(scene.window)
    }


    fun save(): Boolean {
        currentCalculationArea()?.let { ca ->
            ca.file?.let { return ca.saveTo(it) }
            chooseSaveFile()?.let { file ->
                CalculatorApp.recentFiles.add(file)
                return ca.saveTo(file)
            }
        }
        return false
    }

    fun saveAs(): Boolean {
        currentCalculationArea()?.let { ca ->
            chooseSaveFile(ca.file?.parentFile)?.let { file ->
                CalculatorApp.recentFiles.add(file)
                return ca.saveTo(file)
            }
        }
        return false
    }

    fun help() {
        val ca = HelpArea(createCalculator(), "Index.md")

        val tab = Tab().apply {
            content = ca
        }

        // The text of the tab is the name of the script (or "Untitled") with a "*" at the end when it needs saving
        tab.textProperty().bind(ca.titleProperty())

        tabPane.tabs.add(tab)
        tabPane.selectionModel.select(tab)
    }

}
