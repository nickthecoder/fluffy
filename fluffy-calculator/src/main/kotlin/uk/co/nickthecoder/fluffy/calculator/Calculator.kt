package uk.co.nickthecoder.fluffy.calculator

import javafx.application.Application
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.PermissiveSandbox
import uk.co.nickthecoder.fluffy.calculator.numbers.Fraction
import uk.co.nickthecoder.fluffy.calculator.numbers.toFluffyString
import uk.co.nickthecoder.fluffy.calculator.units.Measure
import uk.co.nickthecoder.fluffy.calculator.units.Units
import uk.co.nickthecoder.fluffy.commands.UnixCommandRunner
import java.io.File

/**
 * Parses expressions, and evaluates them. There are currently two user interfaces which use Calculator,
 * a command line, in the companion object's [main] method, and a JavaFX Application called
 * CalculatorApp.
 *
 * While this can be used as a simple calculator, it also has access to all java objects, and therefore
 * can be used for much more complex things!
 *
 */
class Calculator() {

    var bindingFactory: () -> Binding = { Binding() }
        set(v) {
            field = v
            binding = v()
        }

    var binding = bindingFactory()

    val compiler = FluffyCompiler().apply {
        sandbox = PermissiveSandbox()
        commandRunner = UnixCommandRunner()
    }

    val listeners = mutableListOf<CalculatorListener>()

    var topHeavyFractions = true

    fun resetBinding() {
        binding = bindingFactory()
    }

    fun evaluate(code: String): Any? {

        try {
            val result = compiler.compile(binding, code).eval()

            listeners.forEach {
                try {
                    it.evaluated(code, result)

                } catch (e: Exception) {
                    // Ignore errors by the listener
                }

            }
            return result

        } catch (e: Exception) {

            listeners.forEach {
                try {
                    it.error(code, e)
                } catch (e: Exception) {
                    // Ignore errors by the listener
                }
            }
            throw e
        }
    }

    fun evaluate(file: File): Any? {
        return compiler.compile(binding, file.readText()).eval()
    }

    /**
     * Converts the result from [evaluate] into a String.
     * Numbers, boolean literals, null etc are returned using toString().
     * Strings are quoted (those containing newline character use tripple quotes, with actual newlines.
     * Quotes and backslashes are escaped.
     *
     * Everything else is returned using toString, but each line is prefixed with "// ", so that it wont interfere
     * with parsing.
     *
     * @return A pair, the first indicates if the result is a value fluffy expression, the second is the result as a string.
     */
    fun processResult(result: Any?): Pair<Boolean, String> {
        return when (result) {
            is Fraction -> Pair(true, if (topHeavyFractions) result.toFluffyString() else result.toMixedString())
            is Number -> Pair(true, result.toFluffyString())
            is String -> Pair(true, quoteResult(result))
            Unit -> Pair(true, "")
            null -> Pair(true, "null")
            is Enum<*> -> Pair(true, "${result.javaClass.name}.${result.name}")
            is Iterable<*> -> Pair(
                false,
                result.joinToString(prefix = "{\n    ", separator = ",\n    ", postfix = "\n}")
            )
            is Measure -> {
                if (result.units.isCompatibleWith(Units.RADIANS)) {
                    val ttr = evaluate("_trigToRadians")
                    when (ttr) {
                        1.0 -> Pair(true, (Measure(0.0, Units.RADIANS) + result).toFluffyString())
                        Math.PI / 180.0 -> Pair(true, (Measure(0.0, Units.DEGREES) + result).toFluffyString())
                        else -> Pair(true, result.toFluffyString())
                    }
                } else {
                    Pair(true, result.toString())
                }
            }
            is FluffyString -> Pair(true, result.toFluffyString())
            else -> Pair(false, result.toString())
        }
    }

    private fun quoteResult(str: String): String {
        val quoted = '"' + str.replace("\"", "\\\"") + '"'
        return if (str.contains('\n')) {
            "\"\"" + quoted + "\"\""
        } else {
            quoted
        }
    }

    companion object {
        /**
         * If we use the main in CalculatorApp, then Java gets confused (or just horrible annoyed) that
         * my application doesn't use modules, and throws a fit.
         * This is why people hate Java so much. Too much nonsense!
         */
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(CalculatorApp::class.java, * args)
        }
    }

}

fun Throwable.rootCause(): Throwable = this.cause?.rootCause() ?: this
