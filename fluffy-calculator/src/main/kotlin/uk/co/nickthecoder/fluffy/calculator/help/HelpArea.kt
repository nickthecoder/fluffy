package uk.co.nickthecoder.fluffy.calculator.help

import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventHandler
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.calculator.*
import uk.co.nickthecoder.fxessentials.ButtonGroup
import uk.co.nickthecoder.scarea.Scarea
import uk.co.nickthecoder.scarea.ScareaPosition
import java.awt.Desktop
import java.awt.EventQueue
import java.net.URI


class HelpArea(calculator: Calculator, page: String)
    : CalculationArea(calculator, null) {

    /**
     * Used to keep track of which pages we've visited, so that we can got forwards/backwards
     * using Alt+LEFT, Alt+RIGHT.
     */
    private val history = mutableListOf<String>()

    /**
     * This is a little flakey, because if we change the history AFTER changing the this,
     * then things will go wrong.
     */
    private var currentHistory = 0
        set(v) {
            field = v
            disableBack.value = v == 0
            disableForward.value = v == history.size - 1
        }

    private val disableBack = SimpleBooleanProperty(true)

    private val disableForward = SimpleBooleanProperty(true)

    init {
        with(actionGroup) {
            action(BACK, disableBack) { back() }
            action(FORWARD, disableForward) { forward() }
        }
        history.add(page)
        openHelp(page)

        with(actionGroup) {
            statusArea.left = ButtonGroup(button(BACK), button(FORWARD))
        }
    }

    fun openHelp(name: String): Boolean {
        HelpArea::class.java.getResource(name)?.readText()?.let { rawText ->
            inputArea.text = ""
            inputArea.document.clearHighlightRanges()

            titleProperty.value = "Help : ${name.removeSuffix(".md")}"
            SmudgeDown(inputArea).toTedi(rawText)

            inputArea.caretPosition = ScareaPosition.START
            return true
        }
        return false
    }

    fun openBuiltIn(name: String): Boolean {
        val resource = CalculatorApp::class.java.getResource(name)
                ?: Binding::class.java.getResource(name)
                ?: return false

        inputArea.document.clearHighlightRanges()
        inputArea.text = resource.readText()
        titleProperty.value = "Built-In : ${name.removeSuffix(".fluffy")}"
        inputArea.caretPosition = ScareaPosition.START
        return true
    }

    override fun createTextArea(isResultsArea: Boolean, linkedTo: Scarea?) =
            super.createTextArea(isResultsArea, linkedTo).apply {
                displayLineNumbers = false
                onMouseClicked = EventHandler { event ->
                    if (event.button.ordinal == 1 && event.clickCount == 1) {
                        val position = caretPosition
                        document.ranges.firstOrNull { hr ->
                            hr.from <= position && hr.to >= position && hr is LinkHighlightRange
                        }?.let { linkHr ->
                            (linkHr as? LinkHighlightRange)?.link?.let {
                                linkClicked(it)
                            }
                        }
                    }
                }
            }

    private fun linkClicked(link: String) {

        if (link.startsWith("http:") || link.startsWith("https:")) {

            try {
                CalculatorApp.instance.hostServices.showDocument(link)
            } catch (e: Exception) {
                // There's a bug in openjfx :
                // https://bugs.openjdk.java.net/browse/JDK-8160464
                // So let's try using AWT instead
                EventQueue.invokeLater { Desktop.getDesktop().browse(URI(link)) }
                // FYI, it seems that the "ClassNotFoundException" is caught, and the stack trace dumped
                // within HostServicesDelegate. Can't do anything about that (it is in a com.sun... package)
                // So the exception we are catching is actually a NullPointerException. Grr.
            }

        } else {
            val opened = if (link.startsWith("builtin:")) {
                openBuiltIn(link.substring(8))
            } else {
                openHelp(link)
            }
            if (opened) {
                // Chop off the end of the history
                while (currentHistory < history.size - 1) {
                    history.removeAt(history.size - 1)
                }

                history.add(link)
                currentHistory++
            }
        }
    }


    private fun back() {
        if (currentHistory > 0) {
            currentHistory--
            openHelp(history[currentHistory])
        }
    }

    private fun forward() {
        if (currentHistory < history.size - 1) {
            currentHistory++
            openHelp(history[currentHistory])
        }
    }
}
