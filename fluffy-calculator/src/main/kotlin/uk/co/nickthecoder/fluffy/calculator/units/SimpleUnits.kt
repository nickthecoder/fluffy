package uk.co.nickthecoder.fluffy.calculator.units

import java.util.*

/**
 *
 */
class SimpleUnits(

    val baseUnits: BaseUnits,

    /**
     * e.g. [Prefix.KILO], [Prefix.MILLI] etc.
     */
    val prefix: Prefix,

    /**
     * e.g. 2 for a "square" unit such as areas. 0 for unit-less.
     */
    val power: Int,

    /**
     * The label (excluding the prefix)
     */
    val label: String?
) : Units {

    constructor(baseUnits: BaseUnits, prefix: Prefix, power: Int) : this(baseUnits, prefix, power, null)

    constructor(baseUnits: BaseUnits) : this(baseUnits, Prefix.NONE, 1, null)

    constructor(name: String, dimension: Dimension) : this(BaseUnits(name, name, dimension, 1.0))

    constructor(name: String, dimension: Dimension, conversionFactor: Double) : this(
        BaseUnits(
            name,
            name,
            dimension,
            conversionFactor
        )
    )

    constructor(name: String, dimension: Dimension, conversionFactor: Number) : this(
        BaseUnits(
            name,
            name,
            dimension,
            conversionFactor.toDouble()
        )
    )

    override fun inverse() = SimpleUnits(baseUnits, prefix, -power)

    /**
     * What do we need to multiply a number by to convert it from these units to other units.
     * e.g. if the part is for length, then converting from m to mm would return 1000.
     * Converting from square mm to square m would return 1000 * 1000.
     * A unit-less value (with a power of 0) is the same as those with a power of 1.
     * So, we could have 1 k (which is 1,000) an convert it to "M" (million) this will return 0.001
     */

    override fun conversionTo(to: Units): Double {
        if (to is SimpleUnits) {

            baseUnits.ensureCompatibleWith(to.baseUnits)

            if (power != to.power) throw UnitsException("powers are different")
            if (baseUnits.dimension != to.baseUnits.dimension) throw UnitsException("base units are different")

            val unitsConversion = baseUnits.conversionTo(to.baseUnits)

            return when (power) {
                0 -> prefix.factor / to.prefix.factor
                1 -> unitsConversion * prefix.factor / to.prefix.factor
                else -> Math.pow(unitsConversion * prefix.factor / to.prefix.factor, power.toDouble())
            }

        } else if (to is CompoundUnits) {
            val thisAsCompound =
                CompoundUnits(null, mapOf(baseUnits.dimension to UnitsList(baseUnits.dimension, listOf(this))))
            return thisAsCompound.conversionTo(to)
        }

        throw UnitsException("SimpleUnits '$this' cannot be converted to CompoundUnits '$to'")

    }

    override fun times(other: Units): Units {
        return if (other is SimpleUnits) {
            // TODO Optimise? If units are the same, could create a SingleDimension quickly
            Units.create(this, other)
        } else {
            (other as CompoundUnits).times(this)
        }
    }

    override fun isDimensionless() = power == 0

    override operator fun times(prefix: Prefix) =
        SimpleUnits(baseUnits, this.prefix * prefix, power, label)

    operator fun times(other: SimpleUnits): Units {
        return if (baseUnits != other.baseUnits) {
            Units.create(this, other)
        } else if (this.prefix != other.prefix) {
            Units.create(this, other)
        } else {
            SimpleUnits(baseUnits, prefix, power + other.power)
        }
    }

    /**
     * Raise to the power of [exponent].
     * e.g. kilometers.pow(3) = cubic kilometers
     */
    override fun pow(exponent: Int): SimpleUnits {
        return SimpleUnits(baseUnits, prefix, power * exponent)
    }

    override fun isCompatibleWith(other: Units): Boolean {
        return if (other is SimpleUnits) {
            this.baseUnits.dimension == other.baseUnits.dimension && power == other.power
        } else {
            // Let MultiDimension do the tricky part!
            other.isCompatibleWith(this)
        }
    }

    override fun removeZeroPowers(): Pair<Units, Double> {
        return Pair(this, 1.0)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is SimpleUnits) return false
        return this.baseUnits == other.baseUnits && this.prefix == other.prefix && this.power == other.power
    }

    override fun hashCode() = Objects.hash(baseUnits, prefix, power)

    override fun toString(): String = if (power == 0) "" else prefix.toString() + (label
        ?: (baseUnits.toString() + if (power == 1) "" else power.toSuperscriptString()))
}


private val digitToSuperscript = mapOf(
    '-' to '⁻',
    '+' to '⁺',
    '0' to '⁰',
    '1' to '¹',
    '2' to '²',
    '3' to '³',
    '4' to '⁴',
    '5' to '⁵',
    '6' to '⁶',
    '7' to '⁷',
    '8' to '⁸',
    '9' to '⁹'
)

/**
 * Converts an integer to a string, using superscript characters : ⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻
 */
fun Int.toSuperscriptString() = toString().map {
    digitToSuperscript[it]
}.joinToString(separator = "")
