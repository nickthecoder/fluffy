package uk.co.nickthecoder.fluffy.calculator

interface CalculatorListener {
    fun evaluated(code: String, result: Any?)
    fun error(code: String, e: Throwable)
}
