package uk.co.nickthecoder.fluffy.calculator

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.BuiltInScript
import uk.co.nickthecoder.fluffy.calculator.CalculatorApp.Companion.help
import uk.co.nickthecoder.fxessentials.RecentFiles
import uk.co.nickthecoder.fxessentials.addFxEssentialsCSS
import uk.co.nickthecoder.fxessentials.createPosix
import uk.co.nickthecoder.scarea.Scarea
import java.io.File
import java.math.BigDecimal
import java.math.BigInteger
import java.util.prefs.Preferences

/**
 * For command line usage see [help] (or use fluffy-calculator --help from the command line)
 */
class CalculatorApp : Application() {

    override fun start(stage: Stage) {
        // See comment on the declaration of instance below.
        instance = this

        actions.loadShortCuts()

        val gui = windowNew(stage)
        with(stage) {
            icons.add(Image(CalculatorApp::class.java.getResourceAsStream("fluffy.png")))
        }

        with(parameters.createPosix()) {

            if (flag("help", "h")) {
                help()
                System.exit(0)
            }

            if (flag("shared")) gui.shared = true

            if (flag("radians")) gui.angleProperty.value = 1.0

            val preload = mutableListOf<File>()
            namedLists["preload"]?.forEach { name ->
                val file = File(name)
                if (file.exists() && file.isFile) {
                    preload.add(file)
                }
            }

            gui.bindingFactory = { createBinding(named["binding"], preload) }

            var added = false
            unnamed.forEach {
                val file = File(it)
                if (file.exists() && file.isFile) {
                    gui.openFile(file)
                    added = true
                } else {
                    // TODO Error
                }
            }
            if (!added) {
                gui.newTab()

            }
        }
    }


    companion object {

        internal fun createBinding() = createBinding(null, emptyList())

        internal fun createBinding(name: String?, preLoad: List<File>) =
            when (name) {
                "plain" -> Binding.plain()
                "standard" -> Binding.standard()
                "extra" -> Binding.extra()
                else -> Binding.extra().with(NUMBER, MATH, UNITS, TRIG, COMPLEX)
            }.with(* preLoad.toTypedArray()).apply {
                importPackage("uk.co.nickthecoder.fluffy.calculator.units")
                importPackage("uk.co.nickthecoder.fluffy.calculator.numbers")
                importClass(BigInteger::class.java)
                importClass(BigDecimal::class.java)
            }

        val recentFiles = RecentFiles(
            Preferences.userNodeForPackage(CalculatorApp::class.java).node("recent")
        )

        /**
         * This is a horrible bodge to get access to the Application instance from HelpArea.
         * We need it to get access to hostServices to open a document in a browser.
         * Grr. JavaFX has some really crappy design in places!
         */
        internal lateinit var instance: CalculatorApp

        fun help() {
            println(
                """Fluffy Calculator Usage :

    fluffy-calculator [OPTION]... [SCRIPT_FILE]...

        --radians :
            Trig functions use radians (the default is degrees).

        --shared :
            All tabs share the same binding, so that variables etc can be shared between tabs.

        --binding=NAME :
            Where name is either plain, standard or extra.

        --preload=FILE :
            The script is evaluated ONCE just before the window first appears
            This can be used to load utility functions and standard variables/constants.
            You can specify more than one preload file.

        SCRIPT_FILE :
            All unnamed arguments are treated as files, which are loaded as tabs into the calculator window,
            but are NOT evaluated.
            If no script files are given, and a single, empty tab is created.
"""
            )
        }

        fun windowNew(stage: Stage? = null): CalculatorGUI {
            val gui = CalculatorGUI()

            (stage ?: Stage()).apply {
                scene = Scene(gui)

                // Style sheets
                Scarea.style(scene)
                scene.addFxEssentialsCSS()

                // Application icon
                this::class.java.getResource("calculator.png")?.let {
                    icons.add(Image(it.toExternalForm()))
                }

                centerOnScreen()
                show()
            }

            return gui
        }

        /**
         * Allows type Number to work with operators `+, - *, / and ^`.
         * Known Number types are the usual primitives as will as Fraction.
         */
        val NUMBER = BuiltInScript("number.fluffy", CalculatorApp::class.java)

        /**
         * Adds standard math functions, such as sqrt, log10, ln, max, min.
         * Also the constant e (the euler number).
         */
        val MATH = BuiltInScript("math.fluffy", CalculatorApp::class.java)

        /**
         *
         * Allows calculations with units of measure, such as lengths, areas, time etc.
         * Includes global variables for some standard units
         *  * Masses : mg, g, kg
         *  * Lengths : mm, cm, m, km
         *  * Time : s, minute, hour, day, week, month, year
         *  * Data : byte, bit
         *
         * You can form more compound units, by multiplying and dividing them e.g.
         *
         *  * Area : m*m, km*km or m^2, km^2
         *  * Speed : m/s, km/hour
         *
         * You can then perform calculations, which are "type safe" :
         *
         *      20(m) + 1(km) = 1020 m
         *      20(m) + 2(s) FAILS
         *      20(m) / 2(s) = 10 m s¯¹ (meters per second)
         *
         */
        val UNITS = BuiltInScript("units.fluffy", CalculatorApp::class.java)

        /**
         * Extension functions for complex numbers. Also the constant `i`.
         */
        val COMPLEX = BuiltInScript("complex.fluffy", CalculatorApp::class.java)

        /**
         * Adds trig functions, and the constants `E`, `PI` and `TAU`.
         * variable `_trigToRadians` is set to PI/90, so that the trig functions operate on degrees.
         * Change it to 1.0, and they will work on radians.
         */
        val TRIG = BuiltInScript("trig.fluffy", CalculatorApp::class.java)

    }
}
