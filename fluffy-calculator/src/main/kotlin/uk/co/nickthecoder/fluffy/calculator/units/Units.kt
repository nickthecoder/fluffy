package uk.co.nickthecoder.fluffy.calculator.units

interface Units {

    fun isCompatibleWith(other: Units): Boolean

    fun ensureCompatibleWith(other: Units) {
        if (!isCompatibleWith(other)) {
            throw UnitsException("`$this` is not compatible with `$other`")
        }
    }

    fun conversionTo(to: Units): Double

    fun inverse(): Units

    /**
     * This is intended to act as a scaling factor, for example, meter * kilo = kilometers.
     * You are NOT allowed to multiply square meters by kilo, as square kilometers is 1 MILLION
     * times more than a square meter.
     */
    operator fun times(prefix: Prefix): Units

    operator fun times(other: Units): Units

    operator fun div(other: Units): Units = this * other.inverse()

    fun pow(exponent: Int): Units

    fun isDimensionless(): Boolean

    fun removeZeroPowers(): Pair<Units, Double>

    companion object {

        fun create(vararg dimensions: SimpleUnits): Units = create(null, *dimensions)

        fun create(label: String? = null, vararg dimensions: SimpleUnits): Units {
            return if (dimensions.size == 1) {
                if (label == null) {
                    dimensions[0]
                } else {
                    SimpleUnits(dimensions[0].baseUnits, dimensions[0].prefix, dimensions[0].power, label)
                }
            } else {
                val combined = combine(* dimensions)
                //println("Combined = $combined ")
                return if (combined.isEmpty()) {
                    SINGLE
                } else if (combined.size == 1) {
                    val list = combined.values.first().units
                    if (list.size == 1) {
                        list[0]
                    } else {
                        CompoundUnits(label, combined)
                    }
                } else {
                    CompoundUnits(label, combined)
                }
            }
        }

        @JvmStatic
        val GRAMS = SimpleUnits(BaseUnits.GRAMS, Prefix.NONE, 1)

        @JvmStatic
        val KILOGRAMS = GRAMS * Prefix.KILO

        @JvmStatic
        val METERS = SimpleUnits(BaseUnits.METERS, Prefix.NONE, 1)

        @JvmStatic
        val YARDS = SimpleUnits(BaseUnits.YARDS, Prefix.NONE, 1)

        @JvmStatic
        val FEET = SimpleUnits(BaseUnits.FEET, Prefix.NONE, 1)

        @JvmStatic
        val INCHES = SimpleUnits(BaseUnits.INCHES, Prefix.NONE, 1)


        /**
         * This is a bit of a bodge, which allows us to create Dimensions of litre with prefixes, such as ml (millilitres), cl (centilitres).
         * So we need a SingleDimension of litre, with a prefix of NONE. So we create a Unit of length : decimeter (which is 1/10th of a meter),
         * and then define a litre as a cubic decimeter, with [Prefix.NONE].
         */
        @JvmStatic
        val LITRES = SimpleUnits(BaseUnits("Decimeter", "dm", Dimension.LENGTH, 0.1), Prefix.NONE, 3, "l")

        /**
         * A synonym for [LITRES] for US folks.
         */
        @JvmStatic
        val LITERS = LITRES


        @JvmStatic
        val SECONDS = SimpleUnits(BaseUnits.SECONDS, Prefix.NONE, 1)

        @JvmStatic
        val MINUTES = SimpleUnits(BaseUnits.MINUTES, Prefix.NONE, 1)

        @JvmStatic
        val HOURS = SimpleUnits(BaseUnits.HOURS, Prefix.NONE, 1)

        @JvmStatic
        val DAYS = SimpleUnits(BaseUnits.DAYS, Prefix.NONE, 1)

        @JvmStatic
        val WEEKS = SimpleUnits(BaseUnits.WEEKS, Prefix.NONE, 1)

        // This is only approximate, because years have variable number of days
        @JvmStatic
        val YEARS = SimpleUnits(BaseUnits.YEARS, Prefix.NONE, 1)

        // This is only approximate, because years have variable number of days
        @JvmStatic
        val MONTHS = SimpleUnits(BaseUnits.MONTHS, Prefix.NONE, 1)


        @JvmStatic
        val BYTES = SimpleUnits(BaseUnits.BYTES, Prefix.NONE, 1)

        @JvmStatic
        val BITS = SimpleUnits(BaseUnits.BITS, Prefix.NONE, 1)


        @JvmStatic
        val SINGLE = SimpleUnits(BaseUnits.SINGLE, Prefix.NONE, 1)

        @JvmStatic
        val MOLE = SimpleUnits(BaseUnits.MOLE, Prefix.NONE, 1)

        @JvmStatic
        val DOZEN = SimpleUnits(BaseUnits.DOZEN, Prefix.NONE, 1)


        @JvmStatic
        val DEGREES = SimpleUnits(BaseUnits.DEGREES, Prefix.NONE, 1)

        @JvmStatic
        val RADIANS = SimpleUnits(BaseUnits.RADIANS, Prefix.NONE, 1)

        @JvmStatic
        val REVS = SimpleUnits(BaseUnits.REVS, Prefix.NONE, 1)


        // TODO Add more standards, such as Volts, Amps, etc. Maybe even eV and its ilk.
        @JvmStatic
        val NEWTON = Units.create("N", Units.KILOGRAMS, Units.METERS, Units.SECONDS.pow(-2))

        @JvmStatic
        val JOULE = Units.create("J", Units.KILOGRAMS, Units.METERS.pow(2), Units.SECONDS.pow(-2))

        @JvmStatic
        val WATT = Units.create("W", Units.KILOGRAMS, Units.METERS.pow(2), Units.SECONDS.pow(-3))

        @JvmStatic
        val WATT_HOURS = Units.create("Wh", Units.KILOGRAMS, Units.METERS.pow(2), Units.SECONDS.pow(-3), Units.HOURS)

        @JvmStatic
        val KILOWATT_HOURS =
            Units.create("kWh", Units.KILOGRAMS * Prefix.KILO, Units.METERS.pow(2), Units.SECONDS.pow(-3), Units.HOURS)
    }
}

/**
 * Takes a list of [SimpleUnits] and combines them into a form suitable for the constructor to [CompoundUnits].
 * [SimpleUnits]s of "similar" units are combined into a single item. For example {meters,meters} are combined into a
 * single Dimension with a power of 2 (i.e. square meters).
 *
 * "similar" means has the same [BaseUnits] and the same [Prefix], so "meters" and "meters" can be combined, but
 * "meters" and "kilometers" cannot. Neither can "meters" and "feet" be combined. In such cases the result is a weird
 * (but mathematically correct) "km m" and "meters feet". Both of which are areas.
 *
 * If after combining, the power is zero, then the item is excluded form the result.
 * e.g. "meters" and "per meters" combine into an empty list.
 *
 * e.g.
 *      m m hour^-1 ( sq meters per hour) or
 *      km m hour^-1 (kilometer meters per hour) maybe a rate of metal detecting along a beach???
 *
 *      Then we collect all of the distances and all of the times together.
 *      In the first example, we combine the m m together to form a single "square meter"
 *      In the second example we cannot combine them.
 *
 *      The final results are :
 *      distance -> (sq_m) , time -> hour^-1
 *      distance -> (km,m) , time -> hour^-1
 *
 *      Notice that the 1st has 1 item in the DimensionList, whereas the 2nd has 2 items.
 */
private fun combine(vararg units: SimpleUnits): Map<Dimension, UnitsList> {

    val result = mutableMapOf<Dimension, UnitsList>()

    units.groupBy { it.baseUnits.dimension }.forEach { dimensionType, sameDimensionType ->
        val list = mutableListOf<SimpleUnits>()
        sameDimensionType.groupBy { Pair(it.baseUnits, it.prefix) }.forEach { pair, similar ->
            if (similar.size == 1 && similar[0].power != 0) {
                // Only one Dimension, use it unchanged, including a custom label if it has one.
                list.add(similar[0])
            } else {
                // Combine all of the Dimensions into a single Dimension.
                // e.g. if similar = [m,m], then combine into a square meter.
                val power = similar.sumBy { it.power }
                if (power != 0) {
                    list.add(SimpleUnits(pair.first, pair.second, power))
                }
            }
        }
        if (list.isNotEmpty()) {
            result[dimensionType] = UnitsList(dimensionType, list)
        }
    }
    return result
}

