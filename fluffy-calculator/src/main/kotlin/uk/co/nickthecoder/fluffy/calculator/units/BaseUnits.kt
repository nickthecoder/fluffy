package uk.co.nickthecoder.fluffy.calculator.units

/**
 * A unit of measure for example, meters, feet, seconds, hours.
 * A BaseUnit only has one dimension, and a power of 1.
 * So, for example, a BaseUnit can be a length, such as meter or foot, but not
 * square feet, or square meters. Nor can it be a speed (which has two dimensions, length and time).
 */
class BaseUnits(

    /**
     * A human readable name of the units. Not used by [toString]. e.g. "meters"
     */
    val name: String,

    /**
     * An abbreviation of the units, used by [toString], e.g. "m" for meters.
     */
    val label: String,

    /**
     * For example, [Dimension.LENGTH] for meters, feet, light years etc.
     */
    val dimension: Dimension,

    /**
     * Used to convert from one [Unit] to another.
     * For example, the standard unit of data is a BYTE, and the [conversionFactor] of a BIT is 1/8.
     * It is important that every dimensionType has only one standard [BaseUnits], i.e.
     * we can't have meters and feet both being standard, otherwise we couldn't convert between them.
     *
     * Note, some units, such as money cannot be easily converted, as the [conversionFactor] is not constant.
     * In these cases, the [conversionFactor] is null, and [conversionTo] will throw a [UnitsException].
     *
     * The "standard" SI units are [GRAMS], [METERS], [SECONDS].
     * I put it in quotes, because "kilogram" is actually the standard SI unit of mass.
     * So by "standard" I mean "without prefix" in that case.
     *
     * Some conversions are subjective. e.g. a [YEARS] is defined here as 365.25 days, and a [MONTHS] is 1/12 of a year.
     * However, if you want you could define another "month", which is exactly 30 days, and "year" as
     * exactly 365 days if you wish.
     *
     * If you use something fluctuating, like money, I suggest each currency has its own [BaseUnits], such as "US Money"
     * and "Euro Money".
     * You could then convert between dollars and cents, but not between dollars and euros.
     *
     * Note, you can create additional units. For example, you could create a "Person" unit. You could then
     * do calculations such as :
     *
     *      63 million people / area of the uk
     *
     * to arrive at the population density in `people per square meter`.
     * And then adjust the answer to `people per square kilometer`.
     */
    val conversionFactor: Double

) {

    fun isCompatibleWith(other: BaseUnits) = dimension == other.dimension


    fun ensureCompatibleWith(other: BaseUnits) {
        if (!isCompatibleWith(other)) {
            throw UnitsException("Units '$this' and '${other}' are not compatible")
        }
    }

    fun conversionTo(other: BaseUnits): Double {
        ensureCompatibleWith(other)

        return if (this === other) 1.0 else this.conversionFactor / other.conversionFactor
    }

    override fun toString() = label

    companion object {

        @JvmStatic
        val SINGLE = BaseUnits("Single", "", Dimension.QUANTITY, 1.0)

        @JvmStatic
        val MOLE = BaseUnits("Mole", "mol", Dimension.QUANTITY, 6.02214076e23)

        @JvmStatic
        val DOZEN = BaseUnits("Dozen", "dozen", Dimension.QUANTITY, 12.0)


        @JvmStatic
        val GRAMS = BaseUnits("Grams", "g", Dimension.MASS, 1.0)

        /**
         * An avoirdupois pound - a unit of MASS.
         * @see [Units.POUND_FORCE]
         */
        val POUND = BaseUnits("Pounds", "lb", Dimension.MASS, 1 / 0.45359237)

        @JvmStatic
        val METERS = BaseUnits("Meters", "m", Dimension.LENGTH, 1.0)
        val FEET = BaseUnits("Feet", "feet", Dimension.LENGTH, 0.3048)
        val INCHES = BaseUnits("Inches", "inches", Dimension.LENGTH, FEET.conversionFactor / 12.0)
        val YARDS = BaseUnits("Yards", "yards", Dimension.LENGTH, FEET.conversionFactor * 3.0)

        @JvmStatic
        val SECONDS = BaseUnits("Seconds", "s", Dimension.TIME, 1.0)

        @JvmStatic
        val MINUTES = BaseUnits("Minutes", "minutes", Dimension.TIME, 60.0)

        @JvmStatic
        val HOURS = BaseUnits("Hours", "hours", Dimension.TIME, MINUTES.conversionFactor * 60.0)

        @JvmStatic
        val DAYS = BaseUnits("Days", "days", Dimension.TIME, HOURS.conversionFactor * 24)

        @JvmStatic
        val WEEKS = BaseUnits("Weeks", "weeks", Dimension.TIME, DAYS.conversionFactor * 7.0)

        @JvmStatic
        val YEARS = BaseUnits("Weeks", "weeks", Dimension.TIME, DAYS.conversionFactor * 365.25)

        @JvmStatic
        val MONTHS = BaseUnits("Months", "months", Dimension.TIME, YEARS.conversionFactor / 12.0)

        @JvmStatic
        val BYTES = BaseUnits("Bytes", "B", Dimension.DATA, 1.0)

        @JvmStatic
        val BITS = BaseUnits("Bits", "b", Dimension.DATA, 1.0 / 8)


        @JvmStatic
        val RADIANS = BaseUnits("Radians", "radians", Dimension.ANGLE, 1.0)

        @JvmStatic
        val DEGREES = BaseUnits("Degrees", "degrees", Dimension.ANGLE, Math.PI / 180.0)

        @JvmStatic
        val REVS = BaseUnits("Revolutions", "revs", Dimension.ANGLE, Math.PI * 2.0)
    }
}