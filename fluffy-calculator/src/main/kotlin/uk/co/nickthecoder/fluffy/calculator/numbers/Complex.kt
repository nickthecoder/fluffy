package uk.co.nickthecoder.fluffy.calculator.numbers

import uk.co.nickthecoder.fluffy.calculator.FluffyString
import java.util.*

class Complex(val real: Number, val imaginary: Number) : FluffyString {

    operator fun plus(other: Complex) = Complex(real + other.real, imaginary + other.imaginary)

    operator fun minus(other: Complex) = Complex(real - other.real, imaginary - other.imaginary)

    operator fun times(other: Complex) = Complex(
            real * other.real - imaginary * other.imaginary,
            real * other.imaginary + imaginary * other.real
    )

    operator fun div(other: Complex): Complex {
        val foo = other.real * other.real + other.imaginary * other.imaginary
        return Complex(
                (real * other.real + imaginary * other.imaginary) / foo,
                (imaginary * other.real - real * other.imaginary) / foo
        )
    }
    // TODO fun pow(other : Complex)

    operator fun plus(other: Number) = Complex(real + other, imaginary)
    operator fun minus(other: Number) = Complex(real - other, imaginary)
    operator fun times(other: Number) = Complex(real * other, imaginary * other)
    operator fun div(other: Number) = Complex(real / other, imaginary / other)
    fun pow(power: Double) = Complex.polar(magnitude().pow(power).toDouble(), theta() * power)

    /**
     * If the imaginary part is zero, returns the real part, otherwise throws an excpetion.
     */
    fun asReal() = if (imaginary.toDouble() == 0.0) real else throw IllegalArgumentException("Has an imaginary part")

    /**
     * The magnitude as a Double.
     * @see theta
     */
    fun magnitude() = Math.sqrt((real * real + imaginary * imaginary).toDouble())

    /**
     * The angle in radians.
     * I've called this theta, and not `angle`, because within the calculator I've defined an
     * extension function called `angle`, which converts to degrees if you are in `degrees mode`.
     *
     * theta is a greek letter, which is often used in maths when dealing with angles.
     * @see magnitude
     */
    fun theta() = Math.atan2(imaginary.toDouble(), real.toDouble())

    override fun equals(other: Any?) = other is Complex && real == other.real && imaginary == other.imaginary

    override fun hashCode() = Objects.hash(real, imaginary)

    override fun toString() = "Complex( $real, $imaginary )"

    override fun toFluffyString() =
            if (imaginary.toDouble() == 0.0) {
                real.toFluffyString()
            } else if (real.toDouble() == 0.0) {
                "(${imaginary.toFluffyString()}*i)"
            } else if (imaginary.toDouble() < 0) {
                "(${real.toFluffyString()}-${(0 - imaginary).toFluffyString()}*i)"
            } else {
                "(${real.toFluffyString()}+${imaginary.toFluffyString()}*i)"
            }

    companion object {
        @JvmStatic
        val i = Complex(0.0, 1.0)

        @JvmStatic
        fun plus(a: Number, b: Complex) = a + b

        @JvmStatic
        fun minus(a: Number, b: Complex) = a - b

        @JvmStatic
        fun times(a: Number, b: Complex) = a * b

        @JvmStatic
        fun div(a: Number, b: Complex) = a / b

        @JvmStatic
        fun polar(magnitude: Double, angle: Double) = Complex(magnitude * Math.cos(angle), magnitude * Math.sin(angle))

    }

}

operator fun Number.plus(other: Complex) = other + this
operator fun Number.minus(other: Complex) = Complex(this - other.real, other.imaginary)
operator fun Number.times(other: Complex) = other * this
operator fun Number.div(other: Complex): Complex {
    val foo = other.real * other.real + other.imaginary * other.imaginary
    return Complex(
            this * other.real / foo,
            (0 - (this * other.imaginary)) / foo
    )
}

// TODO fun Number.pow( other : Complex)

