package uk.co.nickthecoder.fluffy.calculator

import javafx.scene.input.KeyCode
import uk.co.nickthecoder.fxessentials.AllActions


val actions = AllActions(CalculatorApp::class.java)

val DOCUMENT_OPEN = actions.create("document-open", "Open", KeyCode.O, control = true)
val DOCUMENT_SAVE = actions.create("document-save", "Save", KeyCode.S, control = true)
val DOCUMENT_SAVE_AS = actions.create("document-save-as", "Save As", KeyCode.S, shift = true, control = true)

val EDIT_COPY = actions.create("edit-copy", "Copy", KeyCode.C, control = true)
val EDIT_CUT = actions.create("edit-cut", "Cut", KeyCode.X, control = true)
val EDIT_PASTE = actions.create("edit-PASTE", "Paste", KeyCode.V, control = true)

val TAB_NEW = actions.create("tab-new", "New Tab", KeyCode.T, control = true)
val TAB_CLOSE = actions.create("tab-close", "Close Tab", KeyCode.W, control = true)

val WINDOW_NEW = actions.create("window-new", "New Window", KeyCode.N, control = true)

val SCRIPT_RUN = actions.create("script-run", "Run Script", KeyCode.F9, control = false)

val RESULTS_TOGGLE = actions.create("results-toggle", "Hide/Show Results", KeyCode.DIGIT1, control = true)
val RESULTS_APPEND = actions.create("results-append", "Append Results", KeyCode.PLUS, control = true, iconName = "add")

val SPLIT_TOGGLE = actions.create("split-toggle", "Toggle split view", KeyCode.DIGIT2, control = true)

val TEXT_CLEAR_TO_EOL = actions.create("text-clear-eol", "Clear to end-of-line", KeyCode.K, control = true)

val TYPE_NEW_LINE = actions.create("type-new-line", "Insert New Line", KeyCode.ENTER, control = false)
val TYPE_EQUALS = actions.create("type-equals", "Insert =", KeyCode.EQUALS, control = true)
val CALCULATE = actions.create("calculate", "Calculate", KeyCode.EQUALS, control = false)

val CLOSE = actions.create("close", "Close")

val OPTIONS_MENU = actions.create("options", "Options")
val OPTION_DEGREES = actions.create("option-degrees", "Degrees", KeyCode.D, control = true)
val OPTION_RADIANS = actions.create("option-radians", "Radians", KeyCode.R, control = true)
val OPTION_TOP_HEAVY = actions.create("option-top-heavy", "Top Heavy Fractions", KeyCode.F, control = true)

val EDIT_SHORTCUTS = actions.create("edit-shortcuts", "Edit Shortcuts", KeyCode.F12)
val HELP = actions.create("help", "Help", KeyCode.F1)

// Used by HelpArea to navigate back/forward through the history.
val BACK = actions.create("go-back", "Back", KeyCode.LEFT, alt = true)
val FORWARD = actions.create("go-forward", "Forward", KeyCode.RIGHT, alt = true)

//⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻
val SUPER_PLUS = actions.create("super-plus", "⁺", KeyCode.EQUALS, control = true, shift = true)
val SUPER_MINUS = actions.create("super-minus", "⁻", KeyCode.MINUS, control = true, shift = true)
val SUPER_0 = actions.create("super-0", "⁰", KeyCode.DIGIT0, control = true, shift = true)
val SUPER_1 = actions.create("super-1", "¹", KeyCode.DIGIT1, control = true, shift = true)
val SUPER_2 = actions.create("super-2", "²", KeyCode.DIGIT2, control = true, shift = true)
val SUPER_3 = actions.create("super-3", "³", KeyCode.DIGIT3, control = true, shift = true)
val SUPER_4 = actions.create("super-4", "⁴", KeyCode.DIGIT4, control = true, shift = true)
val SUPER_5 = actions.create("super-5", "⁵", KeyCode.DIGIT5, control = true, shift = true)
val SUPER_6 = actions.create("super-6", "⁶", KeyCode.DIGIT6, control = true, shift = true)
val SUPER_7 = actions.create("super-7", "⁷", KeyCode.DIGIT7, control = true, shift = true)
val SUPER_8 = actions.create("super-8", "⁸", KeyCode.DIGIT8, control = true, shift = true)
val SUPER_9 = actions.create("super-9", "⁹", KeyCode.DIGIT9, control = true, shift = true)
