package uk.co.nickthecoder.fluffy.calculator.units

/**
 * Should eventually get renamed to Dimension (but that name is being used for something else).
 */
class Dimension(
    val name: String,
    /**
     * The order used within [CompoundUnits.toString]
     */
    val order: Int
) {

    constructor(name: String) : this(name, 0)

    override fun toString() = "Dimension Type '$name'"

    companion object {
        // For units of quantities, such as mole or dozen.
        @JvmStatic
        val QUANTITY = Dimension("quantity", 0)

        @JvmStatic
        val MASS = Dimension("mass", 1)

        @JvmStatic
        val ANGLE = Dimension("angle", 2) // Degrees, Radians, Revolution

        @JvmStatic
        val LENGTH = Dimension("length", 2)

        @JvmStatic
        val TIME = Dimension("time", 3)

        @JvmStatic
        val DATA = Dimension("data", 10) // Bytes, Bits

    }
}