package uk.co.nickthecoder.fluffy.calculator.units

// NOTE. I considered making units a Set, instead of a List, but decided against it.
// because we may want to choose the order such as (km,m) rather than (m,km).
/**
 * Used by [CompoundUnits] to hold [SimpleUnits]s of a single [Dimension].
 * For example, we could have an area measure in feet-meters. So [dimension] would be "length",
 * and [units] would be "feet" and "meters".
 *
 */
class UnitsList(
    val dimension: Dimension,
    val units: List<SimpleUnits>
) {
    init {
        units.firstOrNull { it.baseUnits.dimension != dimension }?.let {
            throw UnitsException("Cannot mix base units $dimension and $it")
        }
    }

    val power = units.sumBy { it.power }

    fun conversionTo(other: UnitsList): Double {
        var conversion = 1.0
        for (dimension in units) {
            conversion *= Math.pow(
                dimension.baseUnits.conversionFactor * dimension.prefix.factor,
                dimension.power.toDouble()
            )
        }
        for (dimension in other.units) {
            conversion /= Math.pow(
                dimension.baseUnits.conversionFactor * dimension.prefix.factor,
                dimension.power.toDouble()
            )
        }
        return conversion
    }

    override fun toString() = "Dimension list for $dimension : $units"
}