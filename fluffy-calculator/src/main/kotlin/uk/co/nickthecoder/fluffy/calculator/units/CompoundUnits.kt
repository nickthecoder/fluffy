package uk.co.nickthecoder.fluffy.calculator.units


/**
 * A unit of measure, such as meters, square meters, kilometers, seconds, meters per seconds, newtons etc.
 *
 * We can also have "weird" things such as areas measured in feet meters.
 *
 * There are 4 basic SI Dimensions [GRAMS], [METERS], [SECONDS] and [UNITLESS]
 * and we can derive all other SI Dimensions using the [times] and [div] operators. e.g. :
 *
 * kg (kilogram) = [GRAMS] * [Prefix.KILO]
 *
 *  meters per second = [METERS] / [SECONDS]
 *
 */
class CompoundUnits internal constructor(
    val label: String? = null,
    val unitsLists: Map<Dimension, UnitsList>
) : Units {

    init {
        /*
        if (unitsLists.isEmpty()) {
            println("WARNING : Created a MultiDimension with no SingleDimensions")
            Thread.dumpStack()
        }

        if (unitsLists.size == 1) {
            if (unitsLists.values.first().units.size == 1) {
                println("WARNING : Created a MultiDimension with only 1 SingleDimension")
                Thread.dumpStack()
            }
        }
        */
    }

    /**
     * Are the dimensions compatible, such that adding two [Measure] values would make sense?
     * e.g.
     *      meters is compatible with kilometers
     *      meters is compatible with feet
     *      square meters is compatible with square kilometers
     *      square meters is compatible with square feet
     *      square meters is compatible with feet meters
     *
     *      square meters is NOT compatible with meters
     */
    override fun isCompatibleWith(other: Units): Boolean {
        if (other is CompoundUnits) {
            if (unitsLists.keys != other.unitsLists.keys) return false

            for (dimensionList in unitsLists.values) {
                val otherDimensionList = other.unitsLists[dimensionList.dimension]
                if (otherDimensionList == null || dimensionList.power != otherDimensionList.power) return false
            }
            return true
        } else {
            other as SimpleUnits
            if (unitsLists.size != 1) return false
            val list = unitsLists.values.first()
            if (list.dimension != other.baseUnits.dimension) return false
            val totalPower = list.units.sumBy { it.power }
            return totalPower == other.power
        }
    }

    override fun conversionTo(to: Units): Double {
        ensureCompatibleWith(to)

        var conversion = 1.0
        unitsLists.forEach { dt: Dimension, list: UnitsList ->
            conversion *= if (to is SimpleUnits) {
                list.conversionTo(UnitsList(to.baseUnits.dimension, listOf(to)))
            } else {
                list.conversionTo((to as CompoundUnits).unitsLists.getValue(dt))
            }
        }
        return conversion
    }

    /**
     * e.g.
     *      meters -> per meter
     *      square meters -> per square meter
     *      meters per second -> seconds per meter
     */
    override fun inverse(): Units {
        val dimensions = mutableListOf<SimpleUnits>()
        for (list in unitsLists.values) {
            for (d in list.units) {
                dimensions.add(d.inverse())
            }
        }
        return Units.create(* dimensions.toTypedArray())
    }

    /**
     * e.g.
     *      meters * kilo = milometers
     *      per second (Hz) * kilo = kHz (power = -1, but ok as there is only one dimension type)
     *      square meters * kilo = square kilometers (power = 2, but ok as there is only one dimension type)
     *      meters per second * kilo = kilometers per second.
     *      square meters per second * kilo = ERROR (mixing parameter types, neither of which are power 1).
     *      kilometer meters * kilo = ERROR (a single parameter type, but with more than one SingleDimension).
     */
    override operator fun times(prefix: Prefix): Units {
        val dimensions = mutableListOf<SimpleUnits>()
        val singleType = unitsLists.size == 1 && unitsLists.values.first().units.size == 1
        var found = false

        for (list in unitsLists.values) {
            if (singleType || (!found && list.units.size == 1 && list.units[0].power == 1)) {
                found = true
                val d = list.units[0]
                dimensions.add(SimpleUnits(d.baseUnits, d.prefix * prefix, d.power, d.label))
            } else {
                for (d in list.units) {
                    dimensions.addAll(list.units)
                }
            }
        }
        if (!found) {
            throw UnitsException("Multiplying `$this` by a prefix is ambiguous")
        }
        return Units.create(* dimensions.toTypedArray())
    }

    override operator fun times(other: Units): Units {
        val dimensions = mutableListOf<SimpleUnits>()

        for (list in unitsLists.values) {
            for (d in list.units) {
                dimensions.add(d)
            }
        }

        if (other is SimpleUnits) {
            dimensions.add(other)
        } else {
            other as CompoundUnits

            for (list in other.unitsLists.values) {
                for (d in list.units) {
                    dimensions.add(d)
                }
            }
        }
        return Units.create(* dimensions.toTypedArray())
    }

    override operator fun div(other: Units): Units {

        val dimensions = mutableListOf<SimpleUnits>()
        for (list in unitsLists.values) {
            for (d in list.units) {
                dimensions.add(d)
            }
        }
        if (other is CompoundUnits) {
            for (list in other.unitsLists.values) {
                for (d in list.units) {
                    dimensions.add(d.inverse())
                }
            }
        } else {
            dimensions.add((other as SimpleUnits).inverse())
        }
        return Units.create(* dimensions.toTypedArray())
    }

    /**
     * Raise to the power of [exponent].
     * e.g.
     *
     *      meters.pow(3) = cubic meters.
     *      seconds.pow(-1) = per seconds (Hz)
     */
    override fun pow(exponent: Int): CompoundUnits {
        val dlists = unitsLists.entries.associate { entry ->
            entry.key to
                    UnitsList(entry.key, entry.value.units.map {
                        it.pow(exponent)
                    })
        }
        return CompoundUnits(null, dlists)
    }

    /**
     * Is this a unitless value (i.e. has no dimensions).
     * This isn't quite as simple as checking that [unitsLists] is empty,
     * because we may have "meters per feet", which is unitless, but [unitsLists] isn't empty.
     */
    override fun isDimensionless() = false

    /**
     * Simplifies these units by removing any dimensions with a power of zero.
     * For example "inches per foot" has a length dimension with a power of zero,
     * and a conversion factor of 12.
     *
     * This is useful when doing calculations such as : How man tiles of size 15cm fit along
     * a wall of length 3 meters.
     * The answer is dimensionless, but without simplification would be in "meters per cm".
     */
    override fun removeZeroPowers(): Pair<Units, Double> {

        if (unitsLists.values.firstOrNull { it.power == 0 } == null) {
            return Pair(this, 1.0)
        } else {
            var conversion = 1.0
            val newUnits = mutableMapOf<Dimension, UnitsList>()
            unitsLists.forEach { dimension, list ->
                if (list.power == 0) {
                    val emptyUnits = CompoundUnits(null, mapOf(dimension to UnitsList(dimension, emptyList())))
                    val foo = CompoundUnits(null, mapOf(dimension to list))
                    conversion *= foo.conversionTo(emptyUnits)

                } else {
                    newUnits[dimension] = list
                }
            }

            return Pair(CompoundUnits(null, newUnits), conversion)
        }
    }

    override fun toString(): String {
        val result = StringBuffer()
        var first = true
        for (positive in listOf(true, false)) {
            unitsLists.keys.sortedBy { it.order }.forEach { dimension ->
                unitsLists[dimension]?.let { unitsList ->
                    // Do the positive powers before the negative powers.
                    if ((unitsList.power > 0) == positive) {
                        if (first) {
                            first = false
                        } else {
                            result.append(" ")
                        }
                        unitsLists[dimension]?.units?.joinTo(result, separator = " ")
                    }
                }
            }
        }
        return result.toString()
    }

    companion object {

    }
}
