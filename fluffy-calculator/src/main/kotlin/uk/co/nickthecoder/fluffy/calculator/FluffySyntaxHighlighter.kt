package uk.co.nickthecoder.fluffy.calculator

import org.antlr.v4.runtime.*
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import uk.co.nickthecoder.fluffy.parser.FluffyLexer
import uk.co.nickthecoder.fluffy.parser.FluffyParser
import uk.co.nickthecoder.fluffy.parser.FluffyParserBaseListener
import uk.co.nickthecoder.scarea.HighlightRange
import uk.co.nickthecoder.scarea.PairedHighlightRange
import uk.co.nickthecoder.scarea.syntax.SyntaxHighlighter
import uk.co.nickthecoder.scarea.syntax.endPosition
import uk.co.nickthecoder.scarea.syntax.startPosition

class FluffySyntaxHighlighter : SyntaxHighlighter() {

    override fun createRanges(text: String): List<HighlightRange> = Worker().createRanges(text)

    /**
     * There will typically be a single [FluffySyntaxHighlighter] used by many clients, and therefore
     * it cannot store data while parsing a document (as it is possible for multiple documents to be
     * parsed in different threads).
     *
     * Therefore this [Worker] class is used to store data while parsing a document.
     * NOTE. the owner of all [HighlightRange]s is the [FluffySyntaxHighlighter], and therefore we can
     * find ranges created during a previous run, so that they can be removed if no longer pertinent.
     */
    private inner class Worker {

        val ranges = mutableListOf<HighlightRange>()

        fun createRanges(text: String): List<HighlightRange> {
            val lexer = CommentedFluffyLexer(CharStreams.fromString(text))
            lexer.removeErrorListeners()
            val tokens = CommonTokenStream(lexer)

            val parser = FluffyParser(tokens)
            parser.removeErrorListeners()

            val tree = parser.script()
            val listener = Listener()

            try {
                ParseTreeWalker.DEFAULT.walk(listener, tree)
            } catch (e: Exception) {
                // Do nothing
            }

            return ranges
        }

        /**
         * Intercepts each token, so that comments can be highlighted.
         */
        private inner class CommentedFluffyLexer(source: CharStream) : FluffyLexer(source) {

            override fun nextToken(): Token {
                val token = super.nextToken()
                if (token.channel == 1) {
                    highlight(token, "comment")
                }

                return token
            }
        }

        /**
         * Listens as the parse tree is walked through.
         * Add highlight ranges to [ranges] as it does so.
         */
        private inner class Listener : FluffyParserBaseListener() {

            override fun visitErrorNode(node: ErrorNode) {
                highlight(node.symbol, "error")
            }

            // Matched punctuation [] () and {}

            override fun exitFunctionValueParameters(ctx: FluffyParser.FunctionValueParametersContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN(), "paren")
            }

            override fun exitCallSuffix(ctx: FluffyParser.CallSuffixContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN(), "paren")
            }

            override fun exitParenthesizedExpression(ctx: FluffyParser.ParenthesizedExpressionContext) {
                highlightPair(ctx.LPAREN(), ctx.RPAREN(), "paren")
            }

            override fun exitBlock(ctx: FluffyParser.BlockContext) {
                highlightPair(ctx.LCURL(), ctx.RCURL(), "brace")
            }

            override fun exitArrayAccess(ctx: FluffyParser.ArrayAccessContext) {
                highlightPair(ctx.LSQUARE(), ctx.RSQUARE(), "bracket")
            }

            // String Literals and Commands

            override fun exitLineStringLiteral(ctx: FluffyParser.LineStringLiteralContext) {
                highlightPair(ctx.QUOTE_OPEN(), ctx.QUOTE_CLOSE(), "string")
            }

            override fun exitMultiLineStringLiteral(ctx: FluffyParser.MultiLineStringLiteralContext) {
                highlightPair(ctx.TRIPLE_QUOTE_OPEN(), ctx.TRIPLE_QUOTE_CLOSE(), "string")
            }

            override fun exitCommandLiteral(ctx: FluffyParser.CommandLiteralContext) {
                highlightPair(ctx.COMMAND_OPEN(), ctx.COMMAND_CLOSE(), "command")
            }

            override fun exitBackTickCommandLiteral(ctx: FluffyParser.BackTickCommandLiteralContext) {
                highlightPair(ctx.BACKTICK_OPEN(), ctx.BACKTICK_CLOSE(), "command")
            }


            private fun exitStringOrCommand(
                ctx: ParserRuleContext,
                ref: TerminalNode?,
                highlightName: String = "string"
            ) {
                if (ref == null) {
                    highlight(ctx, highlightName)
                } else {
                    highlight(ref, "string-exp")
                }
            }


            override fun exitLineStringContent(ctx: FluffyParser.LineStringContentContext) {
                exitStringOrCommand(ctx, ctx.LineStrRef())
            }

            override fun exitMultiLineStringContent(ctx: FluffyParser.MultiLineStringContentContext) {
                exitStringOrCommand(ctx, ctx.MultiLineStrRef())
            }

            override fun exitCommandContent(ctx: FluffyParser.CommandContentContext) {
                exitStringOrCommand(ctx, ctx.CommandRef(), "command")
            }

            override fun exitBackTickCommandContent(ctx: FluffyParser.BackTickCommandContentContext) {
                exitStringOrCommand(ctx, ctx.BackTickRef(), "command")
            }


            override fun exitLineStringExpression(ctx: FluffyParser.LineStringExpressionContext) {
                highlight(ctx.expression(), "string-exp")
                highlightPair(ctx.LineStrExprStart(), ctx.RCURL(), "brace")
            }

            override fun exitMultiLineStringExpression(ctx: FluffyParser.MultiLineStringExpressionContext) {
                highlight(ctx.expression(), "string-exp")
                highlightPair(ctx.MultiLineStrExprStart(), ctx.RCURL(), "brace")
            }

            override fun exitCommandExpression(ctx: FluffyParser.CommandExpressionContext) {
                highlight(ctx.expression(), "string-exp")
                highlightPair(ctx.CommandExprStart(), ctx.RCURL(), "brace")
            }

            override fun exitBackTickExpression(ctx: FluffyParser.BackTickExpressionContext) {
                highlight(ctx.expression(), "string-exp")
                highlightPair(ctx.BackTickExprStart(), ctx.RCURL(), "brace")
            }

            // Numbers

            override fun exitIntegerLiteral(ctx: FluffyParser.IntegerLiteralContext) {
                highlight(ctx, "number")
            }

            override fun exitRealLiteral(ctx: FluffyParser.RealLiteralContext) {
                highlight(ctx, "number")
            }

            // KEYWORDS

            override fun exitImportStatement(ctx: FluffyParser.ImportStatementContext) {
                highlight(ctx.IMPORT())
            }

            override fun exitFunctionDeclarationSetup(ctx: FluffyParser.FunctionDeclarationSetupContext) {
                highlight(ctx.INFIX())
                highlight(ctx.FUN())
            }

            override fun exitNullLiteral(ctx: FluffyParser.NullLiteralContext) {
                highlight(ctx)
            }

            override fun exitPropertyDeclaration(ctx: FluffyParser.PropertyDeclarationContext) {
                highlight(ctx.def)
            }

            override fun exitTypeRHS(ctx: FluffyParser.TypeRHSContext) {
                highlight(ctx.bop)
            }

            override fun exitReturnStatement(ctx: FluffyParser.ReturnStatementContext) {
                highlight(ctx.RETURN())
            }

            override fun exitBreakStatement(ctx: FluffyParser.BreakStatementContext) {
                highlight(ctx.BREAK())
            }

            override fun exitContinueStatement(ctx: FluffyParser.ContinueStatementContext) {
                highlight(ctx.CONTINUE())
            }

            override fun exitThrowStatement(ctx: FluffyParser.ThrowStatementContext) {
                highlight(ctx.THROW())
            }

            override fun exitBooleanLiteral(ctx: FluffyParser.BooleanLiteralContext) {
                highlight(ctx)
            }

            override fun exitForInSetup(ctx: FluffyParser.ForInSetupContext) {
                highlight(ctx.FOR())
                highlight(ctx.IN())
                highlight(ctx.COUNTER())
                highlightPair(ctx.LPAREN(), ctx.RPAREN(), "paren")
            }

            override fun exitIfExpression(ctx: FluffyParser.IfExpressionContext) {
                highlight(ctx.IF())
                highlight(ctx.ELSE())
            }

            override fun exitWhileExpression(ctx: FluffyParser.WhileExpressionContext) {
                highlight(ctx.WHILE())
            }

            override fun exitWithSetup(ctx: FluffyParser.WithSetupContext) {
                highlight(ctx.WITH())
            }

            override fun exitDoWhileExpression(ctx: FluffyParser.DoWhileExpressionContext) {
                highlight(ctx.DO())
                highlight(ctx.WHILE())
            }

            override fun exitTryExpression(ctx: FluffyParser.TryExpressionContext) {
                highlight(ctx.TRY())
                highlight(ctx.catchBlock()?.CATCH())
                highlight(ctx.finallyBlock()?.FINALLY())
            }

        }

        // Helper functions

        private fun highlightPair(tokenA: Token?, tokenB: Token?, highlightName: String) {
            tokenA ?: return
            tokenB ?: return

            val a = PairedHighlightRange(
                tokenA.startPosition(),
                tokenA.endPosition(),
                highlights[highlightName]!!,
                transient = true,
                owner = this@FluffySyntaxHighlighter,
                opening = null
            )
            val b = PairedHighlightRange(
                tokenB.startPosition(),
                tokenB.endPosition(),
                highlights[highlightName]!!,
                transient = true,
                owner = this@FluffySyntaxHighlighter,
                opening = a
            )

            ranges.add(a)
            ranges.add(b)
        }

        private fun highlightPair(tokenA: TerminalNode?, tokenB: TerminalNode?, highlightName: String) {
            tokenA ?: return
            tokenB ?: return

            highlightPair(tokenA.symbol, tokenB.symbol, highlightName)
        }

        private fun highlight(ctx: ParserRuleContext?, highlightName: String = "keyword") {
            ctx ?: return
            ranges.add(
                HighlightRange(
                    ctx.startPosition(),
                    ctx.endPosition(),
                    highlights[highlightName]!!,
                    transient = true,
                    owner = this@FluffySyntaxHighlighter
                )
            )
        }

        private fun highlight(token: Token?, highlightName: String = "keyword") {
            token ?: return
            ranges.add(
                HighlightRange(
                    token.startPosition(),
                    token.endPosition(),
                    highlights[highlightName]!!,
                    transient = true,
                    owner = this@FluffySyntaxHighlighter
                )
            )
        }

        private fun highlight(token: TerminalNode?, highlightName: String = "keyword") {
            highlight(token?.symbol, highlightName)
        }


    }
}
