package uk.co.nickthecoder.fluffy.calculator


/**
 * To have access to the history of expressions, you can add the following into the Calculator :
 *
 *      import uk.co.nickthecoder.droneops.calculator.EvaluationHistory
 *      val history = EvaluationHistory()
 *      calc.listeners.add( h )
 *      val h = history.latestFirst
 *
 * Note, the history isn't trimmed, so if you evaluate lots of complicated values, they will NOT be garbage collected,
 * and therefore take up more and more memory! Clear the history every one in a while :
 *      h.clear()
 *
 * Or use [TrimmedEvaluationHistory] instead.
 */
open class EvaluationHistory : CalculatorListener {

    val earliestFirst = mutableListOf<Any?>()

    val latestFirst = earliestFirst.asReversed()

    override fun evaluated(code: String, result: Any?) {
        if (result != Unit) {
            earliestFirst.add(result)
        }
    }

    override fun error(code: String, e: Throwable) {
        // Do nothing
    }
}

/**
 * Similar to [EvaluationHistory], but the history does not exceed a given size, and therefore
 * old results can be garbage collected.
 */
class TrimmedEvaluationHistory(val maxSize: Int) : EvaluationHistory() {

    override fun evaluated(code: String, result: Any?) {
        super.evaluated(code, result)
        if (earliestFirst.size > maxSize) {
            earliestFirst.removeAt(0)
        }
    }
}
