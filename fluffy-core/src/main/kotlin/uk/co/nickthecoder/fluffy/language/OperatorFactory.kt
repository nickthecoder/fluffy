package uk.co.nickthecoder.fluffy.language

import org.antlr.v4.runtime.Token
import uk.co.nickthecoder.fluffy.parser.FluffyParser


fun coerceOperand(a: Expression): Expression {
    return when (a.type()) {
        byteClass -> IntExpression.numberToInt(a)
        shortClass -> IntExpression.numberToInt(a)
        longClass -> DoubleExpression.numberToDouble(a)
        floatClass -> DoubleExpression.numberToDouble(a)
        else -> a
    }
}

fun createPrefixUnaryOperator(prefix: Token, inA: Expression): Expression? {

    val a = coerceOperand(inA)
    val art = a.klass()

    return when (prefix.type) {

        FluffyParser.ADD -> {
            when (art) {
                intClass -> a
                doubleClass -> a
                else -> null
            }
        }

        FluffyParser.SUB -> {
            when (art) {
                intClass -> IntExpression.unaryMinus(a)
                doubleClass -> DoubleExpression.unaryMinus(a)
                else -> null
            }
        }

        FluffyParser.EXCL -> {
            if (art == booleanClass) {
                BooleanExpression.not(a)
            } else {
                null
            }
        }

        else -> null
    }

}


fun createPostfixUnaryOperator(prefix: Token, inA: Expression): Expression? {

    val a = coerceOperand(inA)
    val art = a.klass()

    return when (prefix.type) {

        FluffyParser.INCR -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> IntExpression.increment(a)
                    doubleClass -> DoubleExpression.increment(a)
                    else -> null
                }
            } else {
                null
            }
        }

        FluffyParser.DECR -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> IntExpression.decrement(a)
                    doubleClass -> DoubleExpression.decrement(a)
                    else -> null
                }
            } else {
                null
            }
        }

        // NOTE, FluffyParser.EXCL is not defined here, but it IS available as a
        // postfix operator, and will use the extension function "factorial".
        // The calculator adds :
        // fun Int.factorial() = ...

        else -> null
    }

}

fun createBinaryOperator(bop: Token, inA: Expression, inB: Expression) = createBinaryOperator(bop.type, inA, inB)

fun createBinaryOperator(type: Int, inA: Expression, inB: Expression): Expression? {

    val a = coerceOperand(inA)
    val b = coerceOperand(inB)

    val art = a.klass()
    val brt = b.klass()

    return when (type) {

        FluffyParser.PAIR_TO -> {
            PairExpression(a, b)
        }

        FluffyParser.RANGE -> {
            when (art) {
                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.range(a, b)
                        else -> null
                    }
                }
                charClass -> {
                    when (brt) {
                        charClass -> CharExpression.range(a, b)
                        else -> null
                    }
                }
                else -> null
            }
        }

        FluffyParser.RANGE_EXCLUSIVE -> {
            when (art) {
                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.until(a, b)
                        else -> null
                    }

                }
                charClass -> {
                    when (brt) {
                        charClass -> CharExpression.until(a, b)
                        else -> null
                    }
                }
                else -> null
            }
        }

        FluffyParser.ADD -> {
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.plus(a, b)
                        doubleClass -> DoubleExpression.plus(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }

                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.plus(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.plus(a, b)
                        else -> null
                    }
                }

                stringClass -> StringExpression.concatenate(a, b)

                else -> null
            }
        }

        FluffyParser.SUB -> {
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.minus(a, b)
                        doubleClass -> DoubleExpression.minus(DoubleExpression.numberToDouble(a), (b))
                        else -> null
                    }
                }

                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.minus(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.minus(a, b)
                        else -> null
                    }
                }
                else -> null
            }
        }

        FluffyParser.MULT -> {
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.times(a, b)
                        doubleClass -> DoubleExpression.times(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }

                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.times(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.times(a, b)
                        else -> null
                    }
                }

                stringClass -> {
                    when (brt) {
                        intClass -> StringExpression.repeat(a, b)
                        else -> null
                    }
                }
                charClass -> {
                    when (brt) {
                        intClass -> CharExpression.repeat(a, b)
                        else -> null
                    }
                }

                else -> null
            }
        }

        FluffyParser.FLOOR_DIV -> { // Integer division or floorDiv
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.floorDiv(a, b)
                        doubleClass -> DoubleExpression.floorDiv(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }
                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.floorDiv(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.floorDiv(a, b)
                        else -> null
                    }
                }

                else -> null
            }
        }

        FluffyParser.DIVIDE -> { // Real division
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.divide(a, b)
                        doubleClass -> DoubleExpression.divide(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }

                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.divide(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.divide(a, b)
                        else -> null
                    }
                }

                else -> null
            }
        }

        FluffyParser.MOD -> {
            when (art) {

                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.mod(a, b)
                        doubleClass -> DoubleExpression.mod(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }

                doubleClass -> {
                    when (brt) {
                        intClass -> DoubleExpression.mod(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.mod(a, b)
                        else -> null
                    }
                }

                else -> null
            }
        }

        FluffyParser.POWER -> {
            when (art) {
                doubleClass -> {
                    when (brt) {
                        doubleClass -> DoubleExpression.power(a, b)
                        intClass -> DoubleExpression.power(a, DoubleExpression.numberToDouble(b))
                        else -> null
                    }
                }
                intClass -> {
                    when (brt) {
                        intClass -> IntExpression.power(a, b)
                        doubleClass -> DoubleExpression.power(DoubleExpression.numberToDouble(a), b)
                        else -> null
                    }
                }
                else -> null
            }
        }

        FluffyParser.DISJ -> {
            if (art == booleanClass && brt == booleanClass) {
                BooleanExpression.or(a, b)
            } else {
                null
            }
        }

        FluffyParser.CONJ -> {
            if (art == booleanClass && brt == booleanClass) {
                BooleanExpression.and(a, b)
            } else {
                null
            }
        }

        // NOTE. This is different to Kotlin. I allow int and double to be compared without needing to perform toDouble() on the int.
        FluffyParser.EQEQ -> {
            if (art == intClass && brt == doubleClass) {
                DoubleExpression.numberToDouble(a).eq(b)
            } else if (art == doubleClass && brt == intClass) {
                a.eq(DoubleExpression.numberToDouble(b))
            } else {
                a.eq(b)
            }
        }

        FluffyParser.EXCL_EQ -> {
            a.notEquals(b)
        }

        FluffyParser.EQEQEQ -> {
            a.same(b)
        }

        FluffyParser.EXCL_EQEQ -> {
            a.notSame(b)
        }

        FluffyParser.LANGLE -> if (art == intClass && brt == doubleClass) {
            Expression.lessThan(DoubleExpression.numberToDouble(a), b)
        } else if (art == doubleClass && brt == intClass) {
            Expression.lessThan(a, DoubleExpression.numberToDouble(b))
        } else {
            if (isComparable(art)) Expression.lessThan(a, b) else null
        }

        FluffyParser.LE -> if (art == intClass && brt == doubleClass) {
            Expression.lessThanOrEqual(DoubleExpression.numberToDouble(a), b)
        } else if (art == doubleClass && brt == intClass) {
            Expression.lessThanOrEqual(a, DoubleExpression.numberToDouble(b))
        } else {
            if (isComparable(art)) Expression.lessThanOrEqual(a, b) else null
        }

        FluffyParser.RANGLE -> if (art == intClass && brt == doubleClass) {
            Expression.greaterThan(DoubleExpression.numberToDouble(a), b)
        } else if (art == doubleClass && brt == intClass) {
            Expression.greaterThan(a, DoubleExpression.numberToDouble(b))
        } else {
            if (isComparable(art)) Expression.greaterThan(a, b) else null
        }

        FluffyParser.GE -> if (art == intClass && brt == doubleClass) {
            Expression.greaterThanOrEqual(DoubleExpression.numberToDouble(a), b)
        } else if (art == doubleClass && brt == intClass) {
            Expression.greaterThanOrEqual(a, DoubleExpression.numberToDouble(b))
        } else {
            if (isComparable(art)) Expression.greaterThanOrEqual(a, b) else null
        }

        FluffyParser.IS -> {
            if (brt == classClass) {
                a.instanceOf(b)
            } else {
                null
            }
        }

        FluffyParser.NOT_IS -> {
            if (brt == classClass) {
                a.notInstanceOf(b)
            } else {
                null
            }
        }

        FluffyParser.ELVIS -> {
            a.elvis(b)
        }

        FluffyParser.AS -> {
            if (b is ClassConstant) {
                a.cast(b.klass)
            } else {
                null
            }
        }

        FluffyParser.AS_SAFE -> {
            if (b is ClassConstant) {
                a.castSafe(b.klass)
            } else {
                null
            }
        }

        FluffyParser.ASSIGNMENT -> {
            if (a.isSettable()) {
                Assignment(a, b)
            } else {
                throw Exception("Only vars can be assigned a new value. Found $a")
            }
        }

        FluffyParser.ADD_ASSIGNMENT -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> when (brt) {
                        intClass -> IntExpression.addAssign(a, b)
                        else -> null
                    }
                    doubleClass -> when (brt) {
                        intClass -> DoubleExpression.addAssign(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.addAssign(a, b)
                        else -> null
                    }
                    stringClass -> StringExpression.addAssign(a, b)
                    else -> null
                }
            } else {
                null
            }
        }

        FluffyParser.SUB_ASSIGNMENT -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> when (brt) {
                        intClass -> IntExpression.subAssign(a, b)
                        else -> null
                    }
                    doubleClass -> when (brt) {
                        intClass -> DoubleExpression.subAssign(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.subAssign(a, b)
                        else -> null
                    }
                    else -> null
                }
            } else {
                null
            }
        }


        FluffyParser.MULT_ASSIGNMENT -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> when (brt) {
                        intClass -> IntExpression.multAssign(a, b)
                        else -> null
                    }
                    doubleClass -> when (brt) {
                        intClass -> DoubleExpression.multAssign(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.multAssign(a, b)
                        else -> null
                    }
                    else -> null
                }
            } else {
                null
            }
        }

        FluffyParser.DIVIDE_ASSIGNMENT -> {
            if (a.isSettable()) {
                when (art) {
                    doubleClass -> when (brt) {
                        intClass -> DoubleExpression.divideAssign(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.divideAssign(a, b)
                        else -> null
                    }
                    else -> null
                }
            } else {
                null
            }
        }

        FluffyParser.FLOOR_DIV_ASSIGNMENT -> {
            if (a.isSettable()) {
                when (art) {
                    intClass -> when (brt) {
                        intClass -> IntExpression.floorDivAssign(a, b)
                        else -> null
                    }
                    doubleClass -> when (brt) {
                        intClass -> DoubleExpression.floorDivAssign(a, DoubleExpression.numberToDouble(b))
                        doubleClass -> DoubleExpression.floorDivAssign(a, b)
                        else -> null
                    }
                    else -> null
                }
            } else {
                null
            }
        }

        else -> null
    }

}


fun isComparable(klass: Class<*>) = comparableClass.isAssignableFrom(klass)
