package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Method

class GetterSetterExpression(
        private val ref: Expression,
        private val getter: Expression,
        private val setMethod: Method
) : Expression {

    override fun type() = getter.type()
    override fun eval() = getter.eval()

    override fun isSettable() = true

    override fun set(value: Any?) {
        setMethod.invoke(ref.eval(), ObjectMapping.coerceInput(Value(value), setMethod.parameterTypes[0])!!.eval())
    }
}
