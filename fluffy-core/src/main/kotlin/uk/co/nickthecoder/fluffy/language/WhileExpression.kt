package uk.co.nickthecoder.fluffy.language

class WhileExpression(
        val condition: Expression,
        val whileBlock: Expression
) : Expression {

    override fun type() = unitClass

    override fun eval() {
        while (condition.eval() == true) {
            try {
                whileBlock.eval()
            } catch (e: Continue) {// Threw by a ContinueStatement
                // Do nothing
            } catch (e: Break) { // Threw by a BreakStatement
                return
            }
        }
    }
}
