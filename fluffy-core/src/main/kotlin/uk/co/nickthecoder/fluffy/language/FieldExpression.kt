package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.lang.reflect.Type


class FieldExpression(
        private val ref: Expression,
        private val field: Field,
        private val type: Type
) : Expression {

    override fun type() = type

    override fun eval(): Any? = ObjectMapping.coerceReturnValue(field.get(ref.eval()))
    override fun isSettable() = !Modifier.isFinal(field.modifiers)

    override fun set(value: Any?) {
        field.set(ref.eval(), value)
    }
}