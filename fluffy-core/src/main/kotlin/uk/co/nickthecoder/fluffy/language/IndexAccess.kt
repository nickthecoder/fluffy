package uk.co.nickthecoder.fluffy.language

class IndexAccess(
        private val getter: Expression,
        private val objectExpression: Expression,
        private val indexExpression: Expression)
    : Expression {

    override fun type() = getter.type()
    override fun eval() = getter.eval()
    override fun isSettable() = true

    override fun set(value: Any?) {

        @Suppress("UNCHECKED_CAST") // TODO This is WRONG. Need to test the generic types
        val obj = objectExpression.eval() as MutableList<Any?>
        val index = indexExpression.eval() as Int

        obj[index] = value
    }

}

class MapAccess(
        private val getter: Expression,
        private val objectExpression: Expression,
        private val indexExpression: Expression)
    : Expression {

    override fun type() = getter.type()
    override fun eval() = getter.eval()
    override fun isSettable() = true

    override fun set(value: Any?) {
        @Suppress("UNCHECKED_CAST") // TODO This is WRONG. Need to test the generic types
        val obj = objectExpression.eval() as MutableMap<Any?, Any?>
        val index = indexExpression.eval()

        obj[index] = value
    }

}
