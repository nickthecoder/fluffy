package uk.co.nickthecoder.fluffy.language

class Assignment(
        private val a: Expression,
        private val b: Expression
) : Expression {

    override fun type() = Unit::class.java

    override fun eval() {
        a.set(b.eval())
    }
}
