package uk.co.nickthecoder.fluffy.language

import org.antlr.v4.runtime.Token
import uk.co.nickthecoder.fluffy.*


class DynamicBinaryOperator(
        private val position: FluffyPosition,
        private val bop: Token,
        private val a: Expression,
        private val b: Expression
) : Expression {

    override fun type() = anyClass

    override fun eval(): Any? {
        return catchAndThrowFluffyEvalException(position, "Dynamic binary operator failed") {
            // Try evaluating vb first, leaving a untouched. This allows assignment operators to work, when the
            // right hand side needs to be dynamic.
            // If we were to wrap a in a Value, then the assignment wouldn't work (as the Value isn't settable).
            val vb = Value(b.eval())
            var op = createBinaryOperator(bop, a, vb)

            if (op == null) {
                // That failed, so let's evaluate a too, and try again.
                val va = Value(a.eval())
                op = createBinaryOperator(bop, Value(a.eval()), vb)

                if (op == null) {
                    throw FluffyEvalException(
                            "Operator ${bop.text} cannot be applied to ${va.klass().simpleName} and ${vb.klass().simpleName}",
                            position)
                }
            }

            op.eval()
        }
    }

}

class DynamicUnaryOperator(
        private val position: FluffyPosition,
        private val uop: Token,
        private val a: Expression,
        private val isPrefix: Boolean
) : Expression {

    override fun type() = anyClass

    override fun eval(): Any? {
        return catchAndThrowFluffyEvalException(position, "Dynamic Unary operator failed") {

            val va = Value(a.eval())
            val op = (if (isPrefix) createPrefixUnaryOperator(uop, va) else createPostfixUnaryOperator(uop, va))
                    ?: throw FluffyEvalException(
                            "Operator ${uop.text} cannot be applied to ${va.klass().simpleName}",
                            position)

            op.eval()
        }
    }
}

class DynamicMethodCall(
        private val position: FluffyPosition,
        private val sandbox: Sandbox,
        private val binding: Binding,
        private val name: String,
        private val ref: Expression,
        private val arguments: List<Expression>
) : Expression {

    override fun type() = anyClass

    override fun eval(): Any? {
        return catchAndThrowFluffyEvalException(position, "Dynamic method failed") {

            val wrappedRef = Value(ref.eval())
            val wrappedArguments = arguments.map { Value(it.eval()) }
            ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, name, wrappedRef, wrappedArguments)?.eval()
                    ?: throw FluffyEvalException("Method '$name' not found in ${wrappedRef.klass().name}", position)
        }
    }

}

class DynamicField(
        val position: FluffyPosition,
        val sandbox: Sandbox,
        val binding: Binding,
        val name: String,
        val ref: Expression
) : Expression {

    override fun type() = anyClass

    override fun eval(): Any? {
        return catchAndThrowFluffyEvalException(position, "Dynamic field failed") {

            val wrappedRef = Value(ref.eval())
            ObjectMapping.findFieldOrExtGetter(position, sandbox, binding, name, wrappedRef)?.eval()
                    ?: throw FluffyEvalException("Field '$name' not found in ${wrappedRef.klass().name}", position)
        }
    }

}
