package uk.co.nickthecoder.fluffy

import uk.co.nickthecoder.fluffy.language.Block

class Script internal constructor(
        private val block: Block
) {

    /**
     */
    @Throws(FluffyEvalException::class)
    fun eval(): Any? {
        return try {
            block.eval()
        } catch (e: FluffyEvalException) {
            e.fillInStackTrace()
            val elements = e.callStack.map { pos ->
                StackTraceElement(
                        "..",
                        pos.functionName ?: "<top-level>",
                        pos.scriptId?.toString() ?: "<unnamed-script>",
                        pos.line)
            }.toMutableList()
            elements.addAll(e.stackTrace)
            e.stackTrace = elements.toTypedArray()
            throw e
        }
    }

    /**
     * The return type you should expect from [eval].
     * This may be a Class or a ParameterizedType. If you only care about the raw Class, then use
     * [returnClass] instead.
     *
     * For example, if [eval] will return a list of strings, then this will return a
     * ParameterizedType, with a rawType of class List, and the actualParameterType array will contain
     * class String.
     */
    fun returnType() = block.type()

    /**
     * The type you should expect from [eval] as a raw Class (i.e. without any generic type information).
     * If you need to know the full generic type, use [returnType] instead.
     *
     * For example, if [eval] will return a list of strings, then this will return class List.
     */
    fun returnClass() = block.klass()
}
