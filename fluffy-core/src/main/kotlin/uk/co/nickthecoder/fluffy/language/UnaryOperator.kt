package uk.co.nickthecoder.fluffy.language

class UnaryOperator<A : Any?>(
        val type: Class<*>,
        val a: Expression,
        val func: (A) -> Any?

) : Expression {

    override fun type() = type

    @Suppress("UNCHECKED_CAST")
    override fun eval(): Any? = func(a.eval() as A)

}
