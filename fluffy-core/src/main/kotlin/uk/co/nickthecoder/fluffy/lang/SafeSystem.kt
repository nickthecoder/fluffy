package uk.co.nickthecoder.fluffy.lang

/**
 * We don't want scripts to have access to java.lang.System, as it can be a vector for malware.
 * So we create our own version, containing only the "safe" stuff.
 */
class SafeSystem {
    companion object {

        @JvmStatic
        val out
            get() = java.lang.System.out

        @JvmStatic
        val err
            get() = java.lang.System.err

        /**
         * The same as java.lang.System.in We don't use "in", as it is a keyword in the fluffy language.
         */
        @JvmStatic
        val input
            get() = java.lang.System.`in`

    }
}