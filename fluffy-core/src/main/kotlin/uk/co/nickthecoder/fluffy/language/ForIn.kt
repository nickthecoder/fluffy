package uk.co.nickthecoder.fluffy.language

class ForIn(val iteratorExp: Expression, val loopVarName: String, val block: Block, val indexName: String?) : Expression {

    override fun type() = unitClass

    override fun eval() {
        val iterator = iteratorExp.eval() as Iterator<*>
        val loopVar = block.context.findVariable(loopVarName)!!
        val indexVar = indexName?.let { block.context.findVariable(it) }
        var index = 0


        while (iterator.hasNext()) {
            loopVar.set(iterator.next())
            indexVar?.set(index)
            try {
                block.eval()
            } catch (e: Continue) {// Threw by a ContinueStatement
                // Do nothing
            } catch (e: Break) { // Threw by a BreakStatement
                return
            }
            index++
        }
    }

}
