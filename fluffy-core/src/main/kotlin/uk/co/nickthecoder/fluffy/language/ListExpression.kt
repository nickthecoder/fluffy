package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Type

/**
 * Used by list literals e.g. val a = [1, 2, 3+1]
 *
 * During compilation, a is an Expression, which evaluates to a list.
 * It isn't a list! Each item in the list needs to be evaluated at runtime.
 */
class ListExpression(
        val itemExpressions: List<Expression>,
        val type: Type = List::class.java
) : Expression {

    override fun type() = type

    override fun eval() = itemExpressions.map { it.eval() }
}

class MapExpression(
        val itemExpressions: List<Expression>,
        val type: Type = Map::class.java
) : Expression {

    override fun type() = type

    override fun eval() = itemExpressions.map { it.eval() as Pair<*, *> }.toMap()
}

class PairExpression(
        val key: Expression,
        val value: Expression
) : Expression {

    val type by lazy {
        ParameterizedTypeImplementation.forPair(key.type(), value.type())
    }

    override fun type() = type

    override fun eval() = Pair(key.eval(), value.eval())
}
