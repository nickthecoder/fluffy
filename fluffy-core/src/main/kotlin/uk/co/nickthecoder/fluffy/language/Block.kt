package uk.co.nickthecoder.fluffy.language

class Block(val context: Context) : Expression {

    val operands = mutableListOf<Expression>()

    override fun type() = operands.lastOrNull()?.type() ?: Unit::class.java

    override fun eval(): Any? {
        var value: Any? = Unit
        operands.forEach { exp ->
            value = exp.eval()
        }
        return value
    }

}
