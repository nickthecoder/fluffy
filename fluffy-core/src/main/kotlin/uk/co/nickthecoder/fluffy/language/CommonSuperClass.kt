package uk.co.nickthecoder.fluffy.language

// Converted to Kotlin from :
// https://stackoverflow.com/questions/9797212/finding-the-nearest-common-superclass-or-superinterface-of-a-collection-of-cla

import java.lang.reflect.Type
import java.util.*

private fun getClassesBfs(clazz: Class<*>): Set<Class<*>> {
    val classes = LinkedHashSet<Class<*>>()
    val nextLevel = LinkedHashSet<Class<*>>()
    nextLevel.add(clazz)
    do {
        classes.addAll(nextLevel)
        val thisLevel = LinkedHashSet(nextLevel)
        nextLevel.clear()
        for (each in thisLevel) {
            val superClass = each.superclass
            if (superClass != null && superClass != Any::class.java) {
                nextLevel.add(superClass)
            }
            for (eachInt in each.interfaces) {
                nextLevel.add(eachInt)
            }
        }
    } while (!nextLevel.isEmpty())
    return classes
}

fun commonSuperClass(vararg classes: Class<*>): List<Class<*>> {
    // start off with set from first hierarchy
    val rollingIntersect = LinkedHashSet(
            getClassesBfs(classes[0]))
    // intersect with next
    for (i in 1 until classes.size) {
        rollingIntersect.retainAll(getClassesBfs(classes[i]))
    }
    return rollingIntersect.toList()
}

fun nearestSuperClass(vararg classes: Class<*>) = commonSuperClass(*classes).firstOrNull() ?: anyClass

fun commonType(a: Expression, b: Expression?): Type {

    return if (b == null) {
        unitClass
    } else {
        val aType = a.type()
        if (aType == b.type()) {
            aType
        } else {
            nearestSuperClass(a.klass(), b.klass())
        }
    }
}

