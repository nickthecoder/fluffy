package uk.co.nickthecoder.fluffy.language

import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyPosition
import uk.co.nickthecoder.fluffy.Sandbox
import java.lang.reflect.*

// Object versions of the primitives
val booleanClass = java.lang.Boolean::class.java
val byteClass = java.lang.Byte::class.java
val charClass = java.lang.Character::class.java
val shortClass = java.lang.Short::class.java
val intClass = java.lang.Integer::class.java
val longClass = java.lang.Long::class.java
val floatClass = java.lang.Float::class.java
val doubleClass = java.lang.Double::class.java
val voidClass = Void::class.java

// Primitives
val booleanPrimitive = Boolean::class.javaPrimitiveType!!
val bytePrimitive = Byte::class.javaPrimitiveType!!
val charPrimitive = Char::class.javaPrimitiveType!!
val shortPrimitive = Short::class.javaPrimitiveType!!
val intPrimitive = Int::class.javaPrimitiveType!!
val longPrimitive = Long::class.javaPrimitiveType!!
val floatPrimitive = Float::class.javaPrimitiveType!!
val doublePrimitive = Double::class.javaPrimitiveType!!
val voidPrimitive = Void::class.javaPrimitiveType!!

// Regular object classes
val stringClass = java.lang.String::class.java
val anyClass = Any::class.java
val unitClass = Unit::class.java
val numberClass = Number::class.java
val classClass = Class::class.java
val comparableClass = Comparable::class.java
val listClass = java.util.List::class.java
val mapClass = java.util.Map::class.java
val bigIntegerClass = java.math.BigInteger::class.java
val bigDecimalClass = java.math.BigDecimal::class.java

/**
 * Converts a primitive type, such as int to its class based cousin.
 * Other types are returned unchanged.
 */
fun convertPrimitiveType(a: Type) = when (a) {
    booleanPrimitive -> booleanClass
    bytePrimitive -> byteClass
    charPrimitive -> charClass
    intPrimitive -> intClass
    longPrimitive -> longClass
    floatPrimitive -> floatClass
    doublePrimitive -> doubleClass
    else -> a
}

/**
 * Controls how regular java objects interact with our language.
 * For example, are getter/setter methods accessible as if they were fields?
 * Are some types auto-cast to other types (e.g. can number values be silently cast to other number types)
 * when calling methods and/or setting fields.
 */
object ObjectMapping {

    private val NO_MATCH = Int.MAX_VALUE

    fun findMethodOrExtFunc(position: FluffyPosition, sandbox: Sandbox, binding: Binding, name: String, ref: Expression, arguments: List<Expression>): Expression? {
        // Look for a real method
        val method = ObjectMapping.findMethod(position, sandbox, name, ref, arguments)
        if (method != null) return method

        // Look for an extension function
        val extArgTypes = mutableListOf(ref.klass()).apply { addAll(arguments.map { it.klass() }) }
        val extFunc = binding.context.findExtensionFunction(name, extArgTypes)
        if (extFunc != null) {
            val receiverAndArguments = mutableListOf(ref).apply { addAll(arguments) }
            return FunctionCall(position, extFunc, receiverAndArguments)
        }
        return null
    }

    fun findFieldOrExtGetter(position: FluffyPosition, sandbox: Sandbox, binding: Binding, name: String, ref: Expression): Expression? {
        // Look for a real field/getter
        val field = findField(position, sandbox, name, ref)
        if (field != null) return field

        // Look for an extension getter
        val getterName = "get${name[0].toUpperCase()}${name.substring(1)}"
        val extFunc = binding.context.findExtensionFunction(getterName, listOf(ref.klass()))
        if (extFunc != null) {
            return FunctionCall(position, extFunc, listOf(ref))
        }

        return null
    }

    fun findField(position: FluffyPosition, sandbox: Sandbox, name: String, ref: Expression): Expression? {
        val klass = ref.klass()
        // NOTE. we can never access fields of the Class class itself.
        return if (klass == classClass) {
            // When looking for static fields, we do NOT check sandbox for Class, but the
            // class that ref contains.
            val refClass = ref.eval() as Class<*>
            sandbox.checkClass(refClass)
            findField(position, name, refClass, ref, true)
        } else {
            sandbox.checkClass(klass)
            findField(position, name, klass, ref, false)
        }
    }

    fun findMethod(position: FluffyPosition, sandbox: Sandbox, name: String, ref: Expression, arguments: List<Expression>): Expression? {
        val klass = ref.klass()
        // NOTE. we can never call methods of the Class class itself.
        return if (klass == classClass) {
            // When looking for static methods, we do NOT check sandbox for Class, but the
            // class that ref contains.
            val refClass = ref.eval() as Class<*>

            sandbox.checkClass(refClass)
            findMethod(position, name, refClass, ref, arguments, true)

        } else {
            sandbox.checkClass(klass)
            findMethod(position, name, klass, ref, arguments, false)
        }
    }


    /**
     * Given an [input] expression, and a requiredType, return an expression that will coerce [input].
     * If no coercion is possible, return null.
     */
    fun coerceInput(input: Expression, requiredType: Type): Expression? = scoredCoerceInput(input, requiredType)?.first

    /**
     * Given an [input] expression, and a requiredType, return an expression that will coerce [input] as well as a
     * score, indicating how good the mapping is.
     * If no coercion is possible, returns null
     */
    fun scoredCoerceInput(input: Expression, requiredType: Type): Pair<Expression, Int>? {
        val requiredType2 = convertPrimitiveType(requiredType)
        if (requiredType2 === anyClass) return Pair(input, 2)

        val requiredClass = requiredType2.rawType()

        val inputClass = input.klass()
        if (requiredType2 === inputClass) return Pair(input, 0)

        if (requiredClass.isAssignableFrom(inputClass)) return Pair(input, 1)

        if (numberClass.isAssignableFrom(inputClass) && numberClass.isAssignableFrom(requiredClass)) {
            // convert between different number types
            return when (requiredType2) {
                shortClass -> Pair(UnaryOperator(shortClass, input) { va: Number -> va.toShort() }, 3)
                intClass -> Pair(UnaryOperator(intClass, input) { va: Number -> va.toInt() }, 3)
                longClass -> Pair(UnaryOperator(longClass, input) { va: Number -> va.toLong() }, 3)
                floatClass -> Pair(UnaryOperator(floatClass, input) { va: Number -> va.toFloat() }, 3)
                doubleClass -> Pair(UnaryOperator(shortClass, input) { va: Number -> va.toDouble() }, 3)
                else -> null
            }
        }
        return null
    }


    /**
     * Must match the mappings in [coerceReturnType].
     *
     * Converts primitives. Also converts arrays to lists.
     */
    fun coerceReturnValue(value: Any?): Any? {
        val klass = value?.javaClass
        return if (klass?.isArray == true) {
            (value as Array<*>).toList()
        } else {
            value
        }
    }

    /**
     * Must match the conversions in [coerceReturnValue].
     */
    fun coerceReturnType(type: Type): Type {
        return if (type is Class<*>) {
            if (type.isPrimitive) {
                convertPrimitiveType(type) as Class<*>
            } else if (type.isArray) {
                val ct = type.componentType
                ParameterizedTypeImplementation.forList(ct)
            } else {
                type
            }
        } else {
            type
        }
    }


    /**
     * Takes a [type], which might be a Class, ParameterizedType of a TypeVariable, and resolves it so that
     * no TypeVariables are used with it definition.
     * TypeVariables are replaced by the typeParameters of the [source].
     *
     * There is a good chance that this is wrong, as I don't fully understand the generic type system.
     * It does work for lists and maps (see the TestGeneric, TestLists and TestMap test suites), which is
     * my main concern.
     */
    fun resolveGenericType(source: Type, type: Type): Type {
        if (type is Class<*>) {
            return convertPrimitiveType(type)
        }
        if (source !is ParameterizedType) return type

        return if (type is Class<*>) {
            type
        } else if (type is ParameterizedType) {
            // Does the ParameterizedType only contain Class types?
            if (type.actualTypeArguments.firstOrNull { it !is Class<*> } == null) {
                // Yep, no need to go any futher.
                type
            } else {
                // We need to resolve the non-class types (as they may contain TypeVariables.)
                ParameterizedTypeImplementation(type.actualTypeArguments.map { resolveGenericType(source, it) }.toTypedArray(), type.rawType as Class<*>)
            }
        } else if (type is TypeVariable<*>) {
            // Match up the type variable witht the actual type argument from [source].
            // I'm not sure if this is correct in all cases.
            val sourceClass = source.rawType as Class<*>
            val index = sourceClass.typeParameters.indexOfFirst { it.name == type.name }
            source.actualTypeArguments[index]
        } else {
            throw Exception("Could not resolve : $type : ${type.javaClass}")
        }
    }


    private fun findField(position: FluffyPosition, name: String, klass: Class<*>, ref: Expression, onlyStatic: Boolean): Expression? {
        return try {
            val field = klass.getField(name)

            if (onlyStatic && !Modifier.isStatic(field.modifiers)) {
                null
            } else {
                val fieldType = resolveGenericType(ref.type(), field.genericType)
                FieldExpression(ref, field, fieldType)
            }
        } catch (e: NoSuchFieldException) {
            findGetter(position, name, klass, ref, onlyStatic)
        }
    }

    private fun findGetter(position: FluffyPosition, name: String, klass: Class<*>, ref: Expression, onlyStatic: Boolean): Expression? {

        val ucName = name.substring(0, 1).toUpperCase() + name.substring(1)
        val getterExpression = findMethod(position, "get$ucName", klass, ref, emptyList(), onlyStatic)
                ?: return null

        val setterName = "set$ucName"
        val getterType = getterExpression.type()

        fun matches(m: Method): Boolean {
            if (m.name != setterName) return false
            if (m.returnType != voidPrimitive) return false
            if (onlyStatic && !Modifier.isStatic(m.modifiers)) return false
            if (m.parameterCount != 1) return false
            val argType = resolveGenericType(ref.type(), m.genericParameterTypes[0])
            return argType == getterType
        }

        val setMethod = klass.allMethods().firstOrNull { matches(it) } ?: return getterExpression
        return GetterSetterExpression(ref, getterExpression, setMethod)
    }


    private fun findMethod(position: FluffyPosition, name: String, klass: Class<*>, ref: Expression, arguments: List<Expression>, onlyStatic: Boolean): Expression? {

        val parameterCount = arguments.size

        fun score(m: Method): Int {
            var score = 0
            if (onlyStatic && !Modifier.isStatic(m.modifiers)) return NO_MATCH
            if (m.name != name) return NO_MATCH
            if (m.parameterCount != parameterCount) return NO_MATCH
            val paramTypes = m.genericParameterTypes.map { resolveGenericType(ref.type(), it) }

            paramTypes.forEachIndexed { index, actualType ->
                val pair = scoredCoerceInput(arguments[index], actualType) ?: return NO_MATCH
                score += pair.second * (index + 1)
            }
            return score
        }

        val (method, _) = klass.allMethods()
                .map { Pair(it, score(it)) }
                .filter { it.second != NO_MATCH }
                .sortedBy { it.second }.firstOrNull()
                ?: return null
        val returnType = resolveGenericType(ref.type(), method.genericReturnType)
        val coercedType = if (returnType is Class<*>) coerceReturnType(returnType) else returnType

        return MethodExpression(position, ref, arguments, method, coercedType)
    }

    fun findConstructor(sandbox: Sandbox, klass: Class<*>, arguments: List<Expression>): Expression? {
        sandbox.checkClass(klass)

        val parameterCount = arguments.size
        fun score(m: Constructor<*>): Int {
            var score = 0
            if (m.parameterCount != parameterCount) return NO_MATCH
            m.parameterTypes.forEachIndexed { index, actualType ->
                val pair = scoredCoerceInput(arguments[index], actualType) ?: return NO_MATCH
                score += pair.second * (index + 1)
            }
            return score
        }

        val (constructor, _) = klass.constructors
                .map { Pair(it, score(it)) }
                .filter { it.second != NO_MATCH }
                .sortedBy { it.second }.firstOrNull()
                ?: return null
        return ConstructorExpression(arguments, constructor)
    }

    fun findConstructor(sandbox: Sandbox, binding: Binding, name: String, arguments: List<Expression>): Expression? {
        val klass = binding.findClass(name) ?: return null

        return findConstructor(sandbox, klass, arguments)
    }

}

/**
 * [Class.getMethods] does NOT return methods declared in Object, if this is an interface.
 * We "fix" this by returning Object::class.java.methods + this.methods when this is an interface.
 */
fun Class<*>.allMethods() =
        if (isInterface) {
            Object::class.java.methods + methods
        } else {
            methods
        }

fun Type.rawType(): Class<*> = when (this) {
    is Class<*> -> this
    is ParameterizedType -> this.rawType as Class<*>
    is TypeVariable<*> -> anyClass
    else -> throw Exception("Can't find the rawType for type $this : ${this.javaClass}")
}
