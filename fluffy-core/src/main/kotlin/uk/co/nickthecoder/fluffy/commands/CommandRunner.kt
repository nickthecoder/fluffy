package uk.co.nickthecoder.fluffy.commands

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

interface CommandRunner {

    /**
     * @param inheritIO
     *      If true, then stdout, stderr and stdin are inherited from Java's process.
     *      Otherwise stdout and stderr are captured into Strings, which are available from the
     *      [CommandResult].
     */
    fun run(commandLine: String, inheritIO: Boolean): CommandResult

    /**
     * If shell supports arguments quoted using SINGLE QUOTES, then escape any single quotes in [str].
     * If the shell doesn't use single quote, then return [str] unchanged. The programmer will have
     * to handle the escaping of arguments manually.
     *
     * NOTE, [str] will NOT be surrounded by single quotes. e.g. the command `ls '$dir'`, then
     * $dir is passed here, not '$dir'. So if dir == Steve's then a valid reply for a bash-like shell
     * would be Steve'\''s
     *
     * This works by closing the quotes after Steve, then adding an escaped quote, then opening the
     * quotes again.
     */
    fun escapeQuote(str: String): String

    fun setTimeout(timeout: Long, timeUnit: TimeUnit)

    fun setTimeoutSeconds(seconds: Long) = setTimeout(seconds, TimeUnit.SECONDS)

    val process: Process?

}

/**
 * A command runner which refuses to run the commands ;-)
 */
class DisabledCommandRunner : CommandRunner {

    override val process: Process? = null
    override fun escapeQuote(str: String) = str
    override fun setTimeout(timeout: Long, timeUnit: TimeUnit) {}

    override fun run(commandLine: String, inheritIO: Boolean) = CommandResult(commandLine, "Commands disabled", null)
}


abstract class AbstractCommandRunner : CommandRunner {

    var timeout: Long = 0L

    var timeUnit: TimeUnit = TimeUnit.SECONDS

    override var process: Process? = null

    override fun setTimeout(timeout: Long, timeUnit: TimeUnit) {
        this.timeout = timeout
        this.timeUnit = timeUnit
    }

    abstract fun buildProcess(command: String): ProcessBuilder

    override fun run(commandLine: String, inheritIO: Boolean): CommandResult {

        val bp = buildProcess(commandLine)
        if (inheritIO) bp.inheritIO()

        process = bp.start()

        // Used to wait for stdout and stderr to be fully read before returning the CommandResult.
        val countdown = CountDownLatch(if (inheritIO) 0 else 2)
        var outText: String? = null
        var errText: String? = null

        if (!inheritIO) {
            Thread {
                try {
                    outText = process?.inputStream?.bufferedReader()?.readText() ?: ""
                } finally {
                    countdown.countDown()
                }
            }.start()

            Thread {
                try {
                    errText = process?.errorStream?.bufferedReader()?.readText() ?: ""
                } finally {
                    countdown.countDown()
                }
            }.start()
        }

        try {
            val completed = if (timeout == 0L) {
                process?.waitFor()
                true
            } else {
                if (process?.waitFor(timeout, timeUnit) == true) {
                    true
                } else {
                    timedOut()
                    false
                }
            }
            countdown.await()

            return if (completed) {
                CommandResult(commandLine, CommandState.FINISHED, process, process?.exitValue() ?: -1, outText, errText)
            } else {
                CommandResult(commandLine, CommandState.INTERRUPTED, process)
            }
        } catch (e: InterruptedException) {
            process?.destroy()
            return CommandResult(commandLine, CommandState.INTERRUPTED, process)
        } catch (e: Exception) {
            return CommandResult(commandLine, e.toString(), process)
        }
    }

    /**
     * What should we do with a process that has taken longer than our timeout?
     * The default behaviour does nothing. You may like to do process.destroy().
     *
     * NOTE. While testing, I was running the command "sleep 10", with a timeout of 1 second.
     * The timeout worked as expected, but even when I destroyed (or destroyedForcibly) the process,
     * I was seeing a thread called "process reaper", which waited the 10 seconds. Is sleep really
     * unstoppable? I don't know, but I didn't dig deeper. Instead I created this stub, to let you
     * try to handle the situation better than I did :-(
     */
    open fun timedOut() {
        // Do nothing.
    }

}


/**
 * Uses the operating system command "sh -c" to run a command.
 * This allows the command to take advantages of the shell, such as redirecting, piping etc.
 */
class UnixCommandRunner : AbstractCommandRunner() {

    override fun buildProcess(command: String) = ProcessBuilder("sh", "-c", command)

    /**
     * Escapes a quote mark, so that it is suitable within single quote marks.
     * For example if we want to build an operating system command :
     *
     *     "echo $message"
     *
     * Where message is a java variable, then we need to quote it, as it may contain
     * special characters.
     *
     * However just using "'$message'" won't work, because message may contain single quote characters.
     * The "normal" way of escaping with backslashes does NOT work with bash (or any similar shell).
     * So instead whenever we encounter a single quote, we must close the quoted text, then output an
     * escaped quote, then open the quoted text again.
     *
     *      val message = "I'm here"
     *      val command = "echo '${escapeQuote(message)}'"
     *
     * Note : The function only 'escapes' the quotes, it does NOT add opening and closing quotes.
     */
    override fun escapeQuote(str: String) = str.replace("'", "'\\''")
}
