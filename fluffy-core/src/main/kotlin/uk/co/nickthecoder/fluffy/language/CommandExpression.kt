package uk.co.nickthecoder.fluffy.language

import uk.co.nickthecoder.fluffy.commands.CommandResult
import uk.co.nickthecoder.fluffy.commands.CommandRunner

class CommandExpression(
        val commandRunner: CommandRunner,
        val commandLine: Expression,
        val inheritIO: Boolean
) : Expression {

    override fun type() = CommandResult::class.java

    override fun eval(): CommandResult {
        return commandRunner.run(commandLine.eval() as String, inheritIO)
    }
}
