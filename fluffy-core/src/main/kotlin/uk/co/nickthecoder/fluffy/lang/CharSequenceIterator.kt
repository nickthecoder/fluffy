package uk.co.nickthecoder.fluffy.lang

class CharSequenceIterator(private val cs: CharSequence) : Iterator<Char> {

    private var index = 0

    override fun hasNext() = index < cs.length
    override fun next() = cs[index++]

}
