package uk.co.nickthecoder.fluffy.language

/**
 * [isLocal] is false for [Context]s in a Binding, but true for all others.
 */
class Context(private val parent: Context? = null, val isLocal: Boolean = true) {

    private val variables = mutableMapOf<String, Expression>()

    private val functions = mutableMapOf<String, MutableList<Function>>()

    private val extensionFunctions = mutableMapOf<String, MutableList<Function>>()

    fun declareVariable(name: String, variable: Expression) {
        if (variables.containsKey(name)) {
            throw Exception("A variable called '$name' is already declared.")
        }
        variables[name] = variable
    }

    fun declareFunction(name: String, function: Function) {
        fun newList(): MutableList<Function> {
            val newList = mutableListOf<Function>()
            functions[name] = newList
            return newList
        }

        val list = functions[name] ?: newList()
        for (existing in list) {
            if (existing.parameters.map { it.type } == function.parameters.map { it.type }) {
                throw Exception("A function with this name, and these parameter types already exists: '$name'")
            }
        }

        list.add(function)
    }

    /**
     * The first parameter of [function] must be called 'this'.
     */
    fun declareExtensionFunction(name: String, function: Function) {
        fun newList(): MutableList<Function> {
            val newList = mutableListOf<Function>()
            extensionFunctions[name] = newList
            return newList
        }

        val list = extensionFunctions[name] ?: newList()
        for (existing in list) {
            if (existing.parameters.map { it.type } == function.parameters.map { it.type }) {
                throw Exception("A function with this name, and these parameter types already exists: '$name'")
            }
        }

        list.add(function)
    }

    fun findLocalVariable(name: String): Expression? = variables[name]
            ?: if (parent?.isLocal == true) parent.findLocalVariable(name) else null

    fun findVariable(name: String): Expression? = variables[name] ?: parent?.findVariable(name)

    fun valueOf(name: String) = findVariable(name)?.eval()


    fun findFunction(name: String, parameterTypes: List<Class<*>>): Function? {
        return functions[name]?.map { Pair(it, scoreTypesMatch(it.parameters.map { it.type }, parameterTypes)) }
                ?.filter { it.second != NO_MATCH }
                ?.sortedBy { it.second }
                ?.firstOrNull()?.first
                ?: parent?.findFunction(name, parameterTypes)
    }

    /**
     * NOTE, the first element in [parameterTypes] is the receiver type.
     * (Extension functions are just regular functions in disguise ;-)
     */
    fun findExtensionFunction(name: String, parameterTypes: List<Class<*>>): Function? {
        return extensionFunctions[name]?.map { Pair(it, scoreTypesMatch(it.parameters.map { it.type }, parameterTypes)) }
                ?.filter { it.second != NO_MATCH }
                ?.sortedBy { it.second }
                ?.firstOrNull()?.first
                ?: parent?.findExtensionFunction(name, parameterTypes)
    }

    /**
     * Used as a bodge. See the code for [FunctionCall] for details.
     */
    fun saveState(): Map<String, Any?> {
        val state = mutableMapOf<String, Any?>()
        variables.forEach { name, value ->
            state[name] = value.eval()
        }
        return state
    }

    fun restoreState(state: Map<String, Any?>) {
        state.forEach { name, value ->
            variables[name]?.set(value)
        }
    }

    private val NO_MATCH = Int.MAX_VALUE

    private fun scoreTypesMatch(required: List<Class<*>>, found: List<Class<*>>): Int {
        //println("Scoring $required vs $found")
        if (required.size != found.size) return NO_MATCH
        //println("Correct number of args")
        var score = 0
        required.forEachIndexed { i, a ->
            val b = found[i]
            if (a === b) {
                //println("$a and $b are the same")
                // A perfect match, do increase the score
            } else if (a.isAssignableFrom(b)) {
                //println("$a and $b are assignable")
                score += 2 * (i+1)
            } else {
                //println("$a and $b do not match")
                return NO_MATCH
            }
        }
        return score
    }

    override fun toString() = """Context :
vars = ${variables}
func = ${functions}
ext  = ${extensionFunctions}
"""
}
