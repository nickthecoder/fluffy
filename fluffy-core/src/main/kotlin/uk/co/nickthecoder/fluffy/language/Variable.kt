package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Type

class Variable(
        type: Type,
        override var value: Any?,
        val settable: Boolean

) : Value(type, value) {

    constructor(value: Any?, settable: Boolean) : this(if (value == null) voidPrimitive else value::class.java, value, settable)

    override fun isSettable() = settable

    override fun set(value: Any?) {
        if (value == null) {
            this.value = value
        } else {
            if (klass().isInstance(value)) {
                this.value = value
            } else {
                throw ClassCastException("Expected ${klass().name}, but found ${value.javaClass.name}")
            }
        }
    }

    companion object {

        fun field(ref: Any, fieldName: String): Expression {
            try {
                val field = ref.javaClass.getField(fieldName)
                val value = field.get(ref)
                return Variable(field.genericType, value, true)
            } catch (e: NoSuchFieldException) {
                return get(ref, "get${fieldName[0].toUpperCase()}${fieldName.substring(1)}")
            }
        }

        fun get(ref: Any, methodName: String): Variable {
            val method = ref.javaClass.methods.first { it.name == methodName && it.parameterCount == 0 }
            val value = method.invoke(ref)
            return Variable(method.genericReturnType, value, true)
        }

    }
}
