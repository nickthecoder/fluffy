package uk.co.nickthecoder.fluffy.language

class IfExpression(
        val condition: Expression,
        val ifBlock: Expression,
        val elseBlock: Expression?
) : Expression {

    // Avoid recalculating more than once
    val type by lazy { commonType(ifBlock, elseBlock) }

    override fun type() = type

    override fun eval(): Any? {
        return if (condition.eval() == true) {
            if (elseBlock == null) {
                ifBlock.eval()
                Unit
            } else {
                ifBlock.eval()
            }
        } else {
            if (elseBlock == null) {
                Unit
            } else {
                elseBlock.eval()
            }
        }
    }
}
