package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Constructor

class ConstructorExpression(
        arguments: List<Expression>,
        private val constructor: Constructor<*>
) : Expression {

    val arguments = arguments.mapIndexed { index, arg -> ObjectMapping.coerceInput(arg, constructor.parameterTypes[index])!! }

    override fun type() = ObjectMapping.coerceReturnType(constructor.declaringClass)
    override fun eval(): Any? {
        val result = constructor.newInstance(* arguments.map { it.eval() }.toTypedArray())
        return ObjectMapping.coerceReturnValue(result)
    }
}
