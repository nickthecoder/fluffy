package uk.co.nickthecoder.fluffy.language

interface IntExpression : Expression {

    override fun type() = intClass

    companion object {
        fun numberToInt(a: Expression) = UnaryOperator(intClass, a) { va: Number -> va.toInt() }

        fun unaryMinus(a: Expression) = UnaryOperator(intClass, a) { va: Int -> -va }

        fun range(a: Expression, b: Expression) = BinaryOperator(IntRange::class.java, a, b) { va: Int, vb: Int -> va..vb }
        fun until(a: Expression, b: Expression) = BinaryOperator(IntRange::class.java, a, b) { va: Int, vb: Int -> va until vb }

        fun plus(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> va + vb }
        fun minus(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> va - vb }
        fun times(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> va * vb }
        fun floorDiv(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> va / vb }
        fun divide(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Int, vb: Int -> va.toDouble() / vb }
        fun power(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> if (vb >= 0) Math.pow(va.toDouble(), vb.toDouble()).toInt() else Math.pow(va.toDouble(), vb.toDouble()) }
        fun mod(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int -> va % vb }

        fun addAssign(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int ->
            a.set(va + vb)
            a.eval()
        }

        fun subAssign(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int ->
            a.set(va - vb)
            a.eval()
        }

        fun increment(a: Expression) = UnaryOperator(intClass, a) { va: Int ->
            a.set(va + 1)
            a.eval()
        }


        fun multAssign(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int ->
            a.set(va * vb)
            a.eval()
        }

        fun floorDivAssign(a: Expression, b: Expression) = BinaryOperator(intClass, a, b) { va: Int, vb: Int ->
            a.set(va / vb)
            a.eval()
        }

        fun decrement(a: Expression) = UnaryOperator(intClass, a) { va: Int ->
            a.set(va - 1)
            a.eval()
        }
    }
}
