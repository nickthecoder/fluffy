package uk.co.nickthecoder.fluffy.language

import uk.co.nickthecoder.fluffy.FluffyPosition
import uk.co.nickthecoder.fluffy.catchAndThrowFluffyEvalException
import java.lang.reflect.Method
import java.lang.reflect.Type

class MethodExpression(
        private val position: FluffyPosition,
        private val ref: Expression,
        arguments: List<Expression>,
        private val method: Method,
        private val type: Type
) : Expression {

    val arguments = arguments.mapIndexed { index, arg ->
        ObjectMapping.coerceInput(arg, ObjectMapping.resolveGenericType(ref.type(), method.genericParameterTypes[index]))!!
    }

    override fun type() = type

    override fun eval(): Any? {
        return catchAndThrowFluffyEvalException(position, "Method call failed") {

            // We need to set isAccessible to true (see TestMethods.testArraysArrayList)
            // But let's ensure we can change the value back after the invoke.
            val acc = method.isAccessible
            try {
                method.isAccessible = true

                val refValue = ref.eval()
                // println("Method $method refValue : $refValue : ${refValue?.javaClass} vs ${ref.type()} args : ${arguments}")

                val result = method.invoke(refValue, * arguments.map { it.eval() }.toTypedArray())
                if (result == null && method.returnType == voidPrimitive) {
                    Unit
                } else {
                    ObjectMapping.coerceReturnValue(result)
                }
            } finally {
                method.isAccessible = acc
            }
        }
    }
}
