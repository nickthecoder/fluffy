package uk.co.nickthecoder.fluffy.language

interface CharExpression : Expression {

    override fun type() = charClass

    companion object {

        fun repeat(a: Expression, b: Expression) = BinaryOperator(charClass, a, b) { va: Char, vb: Int -> "$va".repeat(vb) }

        fun range(a: Expression, b: Expression) = BinaryOperator(CharRange::class.java, a, b) { va: Char, vb: Char -> va..vb }
        fun until(a: Expression, b: Expression) = BinaryOperator(CharRange::class.java, a, b) { va: Char, vb: Char -> va until vb }
    }
}
