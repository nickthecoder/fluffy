package uk.co.nickthecoder.fluffy

import uk.co.nickthecoder.fluffy.commands.CommandResult
import uk.co.nickthecoder.fluffy.language.*
import java.io.PrintStream

/**
 * Every time you call a constructor, call a method, or access a field, the class in question is checked to
 * see if the class is allowed by the [Sandbox].
 * It does NOT happen on return values from methods and field. Neither does it happen with arguments to
 * methods.
 *
 * For example, you may want to ban "File" (as it can delete files!). If you have another class Foo with a method "bar()"
 * that returns a File, it is ok to allow Foo. Any attempt to call bar() will be blocked.
 *
 * I believe it's possible to create an untrusted, yet safe environment, but you must be vigilant to ban
 * any potentially dangerous classes. Don't include classes just because they are commonly used, such as System,
 * or File (both ARE dangerous).
 *
 * Limit yourself to a minimal sets of classes that you know are benign, such as the collection classes.
 *
 * Never allow any reflection classes.
 *
 * If you need to give access to a subset of methods from a particular class, then create a wrapper class
 * which acts as a PROXY to the semi-dangerous class.
 * Do NOT rely on a method returning an interface or super-class of a dangerous implementing class,
 * hoping that the "hidden" methods can't be called. They can (maybe). I can't think how, but there are
 * clever malicious actors out there!
 *
 */
interface Sandbox {
    /**
     * Is [klass] allowed?
     */
    fun allowClass(klass: Class<*>): Boolean

    /**
     * Checks if the class is allowed, and if not, throws an exception.
     */
    fun checkClass(klass: Class<*>) {
        if (!allowClass(klass)) throw SandboxException("Class '${klass.name}' is not allowed within the sandbox.")
    }
}

class SandboxException(message: String) : Exception(message)

/**
 * Allows EVERY class to be used, and should therefore only be used in a trusted environment.
 */
class PermissiveSandbox : Sandbox {
    override fun allowClass(klass: Class<*>) = true

    companion object {
        val instance = PermissiveSandbox()
    }
}

/**
 * Limited to a set of classes and/or packages. It is risky to whitelist using packages, because even if you are
 * sure that the whole package is benign, a class may be added in the future which is dangerous.
 *
 * You could write a small program which lists every class in a package and copy/paste the results ;-)
 *
 * Having said that, I've used package level whitelisting in [addJavaUtil] and [addFluffyClasses].
 */
class WhitelistSandbox() : Sandbox {

    val whitelistPackages = mutableSetOf<String>()
    val whitelistClasses = mutableSetOf<String>()

    fun addPrimitives() {
        whitelistClasses.addAll(listOf(
                booleanClass, byteClass, charClass, shortClass, intClass, longClass, floatClass, doubleClass, voidClass,
                booleanPrimitive, bytePrimitive, charPrimitive, shortPrimitive, intPrimitive, longPrimitive, floatPrimitive, doublePrimitive, voidPrimitive
        ).map { it.name })
    }

    fun addFluffyClasses() {
        whitelistPackages.addAll(listOf(
                "uk.co.nickthecoder.fluffy.lang",
                "uk.co.nickthecoder.fluffy.calculator.units",
                "uk.co.nickthecoder.fluffy.calculator.numbers"
        ))
        whitelistClasses.addAll(listOf(
                CommandResult::class.java
        ).map { it.name })
    }

    fun addJavaUtil() {
        whitelistPackages.add("java.util")
    }

    fun addExtraClasses() {
        whitelistClasses.addAll(listOf(
                Object::class.java,
                stringClass,
                Math::class.java,

                IntProgression::class.java, // These are vital in ranges such as 1..5
                IntRange::class.java,
                IntIterator::class.java,

                CharProgression::class.java, // These are vital in ranges such as 'a'..'z'
                CharRange::class.java,
                CharIterator::class.java,

                PrintStream::class.java // e.g. System.out (Note, System itself is NOT whitelisted - it is dangerous.
        ).map { it.name })
    }

    override fun allowClass(klass: Class<*>): Boolean {
        if (whitelistClasses.contains(klass.name)) return true
        klass.`package`?.let { if (whitelistPackages.contains(it.name)) return true }
        return klass.enclosingClass?.let { allowClass(it) } ?: false
    }

    companion object {

        /**
         * Whitelists some standard classes which should be safe!
         */
        fun standard() = WhitelistSandbox().apply {
            addPrimitives()
            addFluffyClasses()
            addJavaUtil()
            addExtraClasses()
        }
    }
}
