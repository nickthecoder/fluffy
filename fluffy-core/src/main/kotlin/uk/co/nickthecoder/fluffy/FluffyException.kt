package uk.co.nickthecoder.fluffy

/**
 * The position (usually of an error) within code passed to the [FluffyCompiler].
 *
 * Note [line] starts at 1, whereas [column] starts at 0.
 * Therefore the first character is line 1, column 0.
 *
 * @see [FluffyException]
 */
class FluffyPosition(val line: Int, val column: Int, val scriptId: Any?) {

    /**
     * Filled in when a [FluffyEvalException] occurs.
     */
    internal var functionName: String? = null

    override fun toString() = "@ $line,$column" + (if (scriptId == null) "" else " ($scriptId)")

    companion object {
        /**
         * Used when an exception is thrown unrelated to a specific piece of Fluffy code.
         * e.g. when [Binding.bind] fails.
         */
        val NONE = FluffyPosition(0, 0, null)
    }
}

/**
 * Most errors that occur when compiling and running scripts are [FluffyException]s.
 *
 */
abstract class FluffyException(
        val rawMessage: String,
        val position: FluffyPosition,
        cause: Throwable? = null
) : Exception(if (position === FluffyPosition.NONE) rawMessage else "$rawMessage $position", cause)

class FluffyCompilerException(
        rawMessage: String,
        position: FluffyPosition,
        cause: Throwable? = null
) : FluffyException(rawMessage, position, cause)

class FluffyEvalException(
        rawMessage: String,
        position: FluffyPosition,
        cause: Throwable? = null
) : FluffyException(rawMessage, position, cause) {

    internal val callStack = mutableListOf(position)

}


internal fun catchAndThrowFluffyEvalException(position: FluffyPosition, message: String, action: () -> Any?): Any? {
    try {
        return action()
    } catch (e: FluffyEvalException) {
        throw e
    } catch (e: Exception) {
        throw FluffyEvalException(message, position, e)
    }
}
