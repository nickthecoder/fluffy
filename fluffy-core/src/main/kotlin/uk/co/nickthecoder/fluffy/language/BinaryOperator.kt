package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.Type

class BinaryOperator<A : Any?, B : Any?>(
        val type: Type,
        val a: Expression,
        val b: Expression,
        val func: (A, B) -> Any?

) : Expression {

    override fun type() = type

    @Suppress("UNCHECKED_CAST")
    override fun eval(): Any? = func(a.eval() as A, b.eval() as B)

}
