package uk.co.nickthecoder.fluffy.lang

/**
 * Gives access to Kotlin's progressions in a java.lang.reflect friendly manner.
 *
 * This allows us to create Fluffy infix functions for `downTo`, `until` etc.
 */
class Progression {
    
    companion object {

        @JvmStatic
        fun to (start: Int, end : Int) = start .. end

        @JvmStatic
        fun to (start: Char, end : Char) = start .. end

        @JvmStatic
        fun downTo(start: Int, end: Int) = start downTo end

        @JvmStatic
        fun downTo(start: Char, end: Char) = start downTo end

        @JvmStatic
        fun until(start: Int, end: Int) = start until end

        @JvmStatic
        fun until(start: Char, end: Char) = start until end

        @JvmStatic
        fun step(ip: IntProgression, step: Int) = ip step step

        @JvmStatic
        fun step(ip: CharProgression, step: Int) = ip step step

        @JvmStatic
        fun intProgression(start: Int, end: Int, step: Int) = IntProgression.fromClosedRange(start, end, step)

        @JvmStatic
        fun charProgression(start: Char, end: Char, step: Int) = CharProgression.fromClosedRange(start, end, step)

    }
}
