package uk.co.nickthecoder.fluffy.language

interface StringExpression : Expression {
    override fun type() = stringClass

    companion object {
        fun concatenate(a: Expression, b: Expression) = BinaryOperator(stringClass, a, b) { va: String, vb: Any? -> va + vb?.toString() }
        fun repeat(a: Expression, b: Expression) = BinaryOperator(stringClass, a, b) { va: String, vb: Int -> va.repeat(vb) }

        fun addAssign(a: Expression, b: Expression) = BinaryOperator(stringClass, a, b) { va: String, vb: Any? ->
            a.set(va + vb?.toString())
            a.eval()
        }
    }
}
