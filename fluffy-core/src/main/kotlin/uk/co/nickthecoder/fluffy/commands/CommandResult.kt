package uk.co.nickthecoder.fluffy.commands

enum class CommandState { FINISHED, INTERRUPTED, FAILED }

/**
 * The result of a back-tick command. e.g.
 *
 *      `echo Hello`
 *
 * or a simple command e.g.
 *
 *      $( cp foo bar )
 *
 * Will run the operating system command echo.
 * [out] will be "Hello\n", [err] will be "" and [exitValue] will be 0.
 */
class CommandResult(
        val command: String,
        val state: CommandState,
        val process: Process?,
        val exitValue: Int,
        val out: String?,
        val err: String?,
        val errorMessage: String? = null
) {

    constructor(commandLine: String, errorMessage: String, process: Process?) : this(commandLine, CommandState.FAILED, process, -1, "", "", errorMessage)

    constructor(commandLine: String, state: CommandState, process: Process?) : this(commandLine, state, process, -1, "", "", state.name)

    val finished get() = state == CommandState.FINISHED

    val ok get() = exitValue == 0 && state == CommandState.FINISHED

    val lines get() = out?.splitTerminator("\n")

    val errLines get() = err?.splitTerminator("\n")

    override fun toString() =
            (err ?: "") + (out ?: "") + "\n" +
                    command + " " +
                    state + (
                    if (state == CommandState.FINISHED) {
                        " (exit value=$exitValue)"
                    } else {
                        " message : $errorMessage"
                    }) + "\n"

}

fun CharSequence.splitTerminator(terminator: String): List<String> {
    return if (this.isEmpty()) {
        emptyList()
    } else {
        if (this.endsWith(terminator)) {
            this.subSequence(0, length - terminator.length).split(terminator)
        } else {
            this.split(terminator)
        }
    }
}