package uk.co.nickthecoder.fluffy.language

import uk.co.nickthecoder.fluffy.FluffyEvalException
import uk.co.nickthecoder.fluffy.FluffyPosition

class TryExpression(
        val tryBlock: Block,
        val catchBlock: Block?,
        val catchVariableName: String?,
        val finallyBlock: Block?
) : Expression {

    val type by lazy { commonType(tryBlock, catchBlock) }

    override fun type() = type

    override fun eval(): Any? {
        return try {
            tryBlock.eval()
        } catch (e: Jump) {
            // A return, continue or break. Leave them alone!
            throw e
        } catch (e: FluffyEvalException) {
            if (catchBlock != null) {
                catchBlock.context.findVariable(catchVariableName ?: "e")?.set(e.cause ?: e)
                catchBlock.eval()
            } else {
                Unit
            }
        } catch (e: Throwable) {
            if (catchBlock != null) {
                catchBlock.context.findVariable(catchVariableName ?: "e")?.set(e)
                catchBlock.eval()
            } else {
                Unit
            }

        } finally {
            finallyBlock?.eval()
        }
    }

}

class ThrowExpression(val position: FluffyPosition, val throwable: Expression) : Expression {
    override fun type() = unitClass

    override fun eval(): Any? {
        val t = throwable.eval() as Throwable
        throw FluffyEvalException(t.toString(), position, t)
    }
}
