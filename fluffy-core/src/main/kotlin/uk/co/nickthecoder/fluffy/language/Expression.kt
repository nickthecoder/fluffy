package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

interface Expression {

    fun type(): Type

    fun klass() = type().rawType()

    fun parameterizedType(): ParameterizedType? = type() as? ParameterizedType

    fun eval(): Any?

    fun set(value: Any?): Unit = throw Exception("Read only, cannot be assigned")

    fun isSettable() = false

    fun toStringExp() = if (klass() == stringClass) this else UnaryOperator(stringClass, this) { va: Any? -> va.toString() }

    fun eq(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any?, vb: Any? -> va == vb }

    fun same(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any?, vb: Any? -> va === vb }
    fun notEquals(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any?, vb: Any? -> va != vb }
    fun notSame(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any?, vb: Any? -> va !== vb }

    fun instanceOf(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any, vb: Class<*> -> vb.isInstance(va) }
    fun notInstanceOf(b: Expression) = BinaryOperator(booleanClass, this, b) { va: Any, vb: Class<*> -> !vb.isInstance(va) }

    fun elvis(b: Expression) = BinaryOperator(commonType(this, b), this, b) { va: Any, vb: Class<*> -> vb.isInstance(va) }

    fun cast(b: Class<*>) = UnaryOperator(b, this) { a: Any? ->
        if (b.isInstance(a)) a else throw Exception("${a?.javaClass?.name ?: "null"} cannot be cast to ${b.simpleName}")
    }

    fun castSafe(b: Class<*>) = UnaryOperator(b, this) { a: Any? ->
        if (b.isInstance(a)) a else null
    }

    companion object {

        fun lessThan(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Comparable<Any>, vb: Any -> va < vb }
        fun lessThanOrEqual(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Comparable<Any>, vb: Any -> va <= vb }
        fun greaterThan(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Comparable<Any>, vb: Any -> va > vb }
        fun greaterThanOrEqual(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Comparable<Any>, vb: Any -> va >= vb }

        fun toString(a: Expression) = UnaryOperator(String::class.java, a) { va: Any? -> "$va" }

    }
}


open class Value(
        val type: Type,
        open val value: Any?
) : Expression {

    constructor(value: Any?) : this(value?.javaClass ?: Nothing::class.java, value)

    override fun type() = type

    override fun eval() = value

    companion object {
        val NULL = Value(Nothing::class.java, null)
        val UNIT = Value(Unit::class.java, Unit)
        val EMPTY_STRING = Value("")
    }
}

class ClassConstant(val klass: Class<*>) : Value(klass)
