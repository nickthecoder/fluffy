package uk.co.nickthecoder.fluffy

import uk.co.nickthecoder.fluffy.lang.CharSequenceIterator
import uk.co.nickthecoder.fluffy.lang.Progression
import uk.co.nickthecoder.fluffy.language.*
import java.io.File
import java.lang.reflect.Type

/**
 * Allows bi-directional transfer of data between java and fluffy.
 *
 * It also allows imports to be added via java, rather then within the script.
 * This allows standard classes/packages to be imported automatically, without the
 * script writer having to add the import statements.
 *
 * This is called Binding, to match the class in Groovy's API which performs the same role.
 * I'm not sure why it isn't 'Bindings' (with an s). Also, the groovy version doesn't appear to
 * bind anything. (This does, see [bind]!)
 *
 */
class Binding private constructor(internal val context: Context) {

    constructor() : this(Context(isLocal = false))

    /**
     * If you have a FLuffy script with lots of useful functions/extensionFunctions/variables that you
     * would like to use from many other Fluffy scripts, then create a [Binding], and use it to
     * compile the utilities.
     *
     * Then that binding can be used as the [preCompiled] binding here.
     * e.g.
     *
     *      val compiler = FluffyCompiler()
     *      val utilityBinding = Binding()
     *      compiler.compile( utilityBinding, theUtilityCode )
     *
     * The utilityBinding can the be used when compiling other scripts.
     *
     *      val binding = Binding( utilityBinding )
     *      compiler.compile( binding, code ) // code can access the utility functions ;-)
     *
     */
    constructor(preCompiled: Binding) : this(Context(preCompiled.context, isLocal = false)) {
        importedPackages.addAll(preCompiled.importedPackages)
        importedClasses.putAll(preCompiled.importedClasses)
    }

    // TODO Make this private
    val importedPackages = mutableListOf<String>()
    // TODO Make this private
    val importedClasses = mutableMapOf<String, Class<*>>()

    /**
     * Bind the Fluffy variable [name] to a field called [fieldName] of the object [ref].
     * This is two way. If a Fluffy script assigns a new value to [name],
     * then [ref]'s field will be changed. and vice versa.
     *
     * [fieldName] can either be the name of a field, or pair of getter setter methods with the
     * get/set removed, and the next character in lower case (the usual bean convention).
     */
    fun bind(name: String, ref: Any, fieldName: String) {
        val refExp = Value(ref)
        val variable = ObjectMapping.findField(FluffyPosition.NONE, PermissiveSandbox.instance, fieldName, refExp)
                ?: throw Exception("Field or bean getter not found for name $ref on ${ref.javaClass}")
        context.declareVariable(name, variable)
    }

    fun declare(name: String, value: Any?) = context.declareVariable(name, Variable(value, true))
    fun declare(name: String, value: Any?, type: Type) = context.declareVariable(name, Variable(type, value, true))
    fun declare(name: String, ref: Any, fieldName: String) = context.declareVariable(name, Variable.field(ref, fieldName))
    fun declareFromGetter(name: String, ref: Any, fieldName: String) = context.declareVariable(name, Variable.get(ref, fieldName))

    fun valueOf(name: String) = context.valueOf(name)


    fun importPackage(name: String) {
        importedPackages.add(name)
    }

    fun importClass(klass: Class<*>) {
        importClass(klass.simpleName, klass)
    }

    fun importClass(name: String, klass: Class<*>) {
        importedClasses[name] = klass
    }

    private fun findClassInPackage(name: String, pack: String): Class<*>? {
        val fullName = if (pack.isEmpty()) name else pack + "." + name
        return try {
            Class.forName(fullName)
        } catch (e: Exception) {
            null
        }
    }

    fun findClass(name: String): Class<*>? {
        when (name) {
            "Boolean" -> booleanClass
            "Char" -> charClass
            "Byte" -> byteClass
            "Short" -> shortClass
            "Int" -> intClass
            "Long" -> longClass
            "Float" -> floatClass
            "Double" -> doubleClass
            "String" -> stringClass
            "List" -> MutableList::class.java
            "Any" -> Any::class.java
            else -> null
        }?.let { return it }

        importedClasses[name]?.let { return it }

        importedPackages.forEach { pack ->
            findClassInPackage(name, pack)?.let { return it }
        }

        return null
    }

    fun importStandardClasses() {
        listOf(
                booleanClass, charClass, intClass, doubleClass, numberClass,
                stringClass,
                CharSequence::class.java,
                Progression::class.java, CharProgression::class.java, IntProgression::class.java, CharSequenceIterator::class.java
        ).forEach {
            importClass(it)
        }
        listOf("uk.co.nickthecoder.fluffy.lang").forEach {
            importPackage(it)
        }
    }

    /**
     * Creates a new binding, using `this` as the base, and also compiling the built-in scripts.
     * Note the compiler uses a permissive sandbox, so do NOT do this from an untrusted environment.
     */
    fun with(vararg builtIns: BuiltInScript): Binding {
        val binding = Binding(this)
        for (b in builtIns) {
            FluffyCompiler.permissive().compile(binding, b.read(), b).eval()
        }
        return binding
    }

    /**
     * Creates a new binding, using `this` as the base, and also compiling the script files.
     * Note the compiler uses a permissive sandbox, so do NOT do this from an untrusted source.
     */
    fun with(vararg files: File): Binding {
        val binding = Binding(this)
        for (f in files) {
            FluffyCompiler.permissive().compile(binding, f.readText(), f).eval()
        }
        return binding
    }

    companion object {

        /**
         * A plain binding, without any extension functions etc, but does include
         * [importStandardClasses].
         * This is rarely used, as [standard] has some really useful stuff, such as println,
         * which is useful in most circumstances.
         */
        fun plain() = Binding().apply { importStandardClasses() }

        /**
         * Creates a Binding, with lots of useful functions/extension functions, as well as
         * some classes/packages imported.
         *
         * This is similar to Java.lang.* being imported within Java code.
         * (You don't need to import Double for example, you can use it without an import).
         */
        fun standard() = Binding(standardBinding)

        /**
         * Similar to [standard], but also includes some extension functions for 'dangerous'
         * classes, such as File.
         */
        fun extra() = Binding(extraBinding)

        /**
         * Loads 'standard.fluffy' within the fluffy-core jar file.
         */
        private val standardBinding by lazy {
            Binding().apply { importStandardClasses() }.with(BuiltInScript.STANDARD)
        }

        /**
         * Loads 'extra.fluffy' within the fluffy-core jar file on top of [standardBinding].
         */
        private val extraBinding by lazy {
            standardBinding.with(BuiltInScript.EXTRA)
        }

    }

}

/**
 * Some fluffy code is included in the fluffy-core jar file.
 * This is a simple way to read that code, which you can then pass to [FluffyCompiler.compile].
 */
class BuiltInScript(private val resourceName: String, private val baseClass: Class<*> = Binding::class.java) {

    fun read(): String = baseClass.getResource(resourceName).readText()

    override fun toString() = resourceName

    companion object {

        /**
         * Some very useful functions, almost vital, such as print, println,
         * downTo, until (for character and integer ranges).
         *
         * @see [Binding.standardBinding]
         */
        @JvmStatic
        val STANDARD = BuiltInScript("standard.fluffy")

        /**
         * Only useful in trusted environments, as it makes used of "dangerous" classes such as File
         * (which gives the script writer the ability to delete files!).
         *
         * @see [Binding.extraBinding]
         */
        @JvmStatic
        val EXTRA = BuiltInScript("extra.fluffy")

    }
}
