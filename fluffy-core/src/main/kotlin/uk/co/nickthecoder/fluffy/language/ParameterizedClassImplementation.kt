package uk.co.nickthecoder.fluffy.language

import java.lang.reflect.MalformedParameterizedTypeException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*


/**
 * This is a near identical copy of the implementation in sun.reflect.generics.reflectiveObjects.
 * Needed to re-implement it because the sun package is not a public API.
 */
class ParameterizedTypeImplementation(
        private val actualTypeArguments: Array<out Type>,
        private val rawType: Class<*>,
        ownerType: Type? = null
) : ParameterizedType {

    private val ownerType: Type? = ownerType ?: rawType.declaringClass

    init {
        if (actualTypeArguments.size != rawType.typeParameters.size) {
            throw MalformedParameterizedTypeException()
        }
    }

    override fun getActualTypeArguments() = actualTypeArguments

    override fun getRawType() = rawType

    override fun getOwnerType(): Type? = ownerType

    /**
     * Look at the Java implementation, and see how HUGE it is!
     */
    override fun equals(other: Any?) =
            this === other || (other is ParameterizedType &&
                    ownerType == other.ownerType &&
                    rawType == other.rawType &&
                    Arrays.equals(actualTypeArguments, other.actualTypeArguments)
                    )

    override fun hashCode() =
            Arrays.hashCode(actualTypeArguments) xor
                    Objects.hashCode(ownerType) xor
                    Objects.hashCode(rawType)


    override fun toString() =
            (rawType as? Class<*>)?.name + actualTypeArguments.joinToString(prefix = "<", separator = ",", postfix = ">") {
                if (it is Class<*>) it.name else it.toString()
            }


    companion object {

        fun forList(elementType: Type, klass: Class<List<*>> = List::class.java) = ParameterizedTypeImplementation(
                arrayOf(elementType), klass
        )

        fun forMap(keyType: Type, valueType: Type, klass: Class<Map<*, *>> = Map::class.java) = ParameterizedTypeImplementation(
                arrayOf(keyType, valueType), klass
        )


        fun forPair(firstType: Type, secondType: Type) = ParameterizedTypeImplementation(
                arrayOf(firstType, secondType), Pair::class.java
        )
    }
}
