package uk.co.nickthecoder.fluffy.language

interface BooleanExpression : Expression {

    override fun type() = booleanClass

    companion object {
        fun not(a: Expression) = UnaryOperator(java.lang.Boolean::class.java, a) { va: Boolean -> !va }

        fun and(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Boolean, vb: Boolean -> va && vb }
        fun or(a: Expression, b: Expression) = BinaryOperator(java.lang.Boolean::class.java, a, b) { va: Boolean, vb: Boolean -> va || vb }

    }
}
