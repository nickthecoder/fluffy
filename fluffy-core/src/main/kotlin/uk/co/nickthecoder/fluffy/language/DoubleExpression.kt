package uk.co.nickthecoder.fluffy.language


interface DoubleExpression : Expression {

    override fun type() = doubleClass

    companion object {
        fun numberToDouble(a: Expression) = UnaryOperator(doubleClass, a) { va: Number -> va.toDouble() }

        fun unaryMinus(a: Expression) = UnaryOperator(doubleClass, a) { va: Double -> -va }

        fun plus(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> va + vb }
        fun minus(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> va - vb }
        fun times(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> va * vb }
        fun divide(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> va / vb }
        fun power(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> Math.pow(va, vb) }
        fun floorDiv(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> Math.floor(va / vb) }
        fun mod(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double -> va % vb }


        fun addAssign(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double ->
            a.set(va + vb)
            a.eval()
        }

        fun subAssign(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double ->
            a.set(va - vb)
            a.eval()
        }

        fun multAssign(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double ->
            a.set(va * vb)
            a.eval()
        }

        fun floorDivAssign(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double ->
            a.set(Math.floor(va / vb))
            a.eval()
        }

        fun divideAssign(a: Expression, b: Expression) = BinaryOperator(doubleClass, a, b) { va: Double, vb: Double ->
            a.set(va / vb)
            a.eval()
        }

        fun increment(a: Expression) = UnaryOperator(doubleClass, a) { va: Double ->
            a.set(va + 1)
            a.eval()
        }

        fun decrement(a: Expression) = UnaryOperator(doubleClass, a) { va: Double ->
            a.set(va - 1)
            a.eval()
        }
    }

}
