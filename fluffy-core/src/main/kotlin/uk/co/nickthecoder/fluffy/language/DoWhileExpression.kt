package uk.co.nickthecoder.fluffy.language

class DoWhileExpression(
        val condition: Expression,
        val whileBlock: Expression
) : Expression {

    override fun type() = unitClass

    override fun eval() {
        do {
            try {
                whileBlock.eval()
            } catch (e: Continue) {// Threw by a ContinueStatement
                // Do nothing
            } catch (e: Break) { // Threw by a BreakStatement
                return
            }
        } while (condition.eval() == true)
    }
}
