package uk.co.nickthecoder.fluffy.language

import uk.co.nickthecoder.fluffy.FluffyEvalException
import uk.co.nickthecoder.fluffy.FluffyPosition
import java.lang.reflect.Type

class Function(
        val name: String,
        val block: Block,
        val isSimpleExpression : Boolean,
        returnType: Type,
        val parameters: List<FunctionParameter>,
        val infix: Boolean = false
) {

    var returnType: Type = returnType
        set(v) {
            if (field != Unknown::class.java) {
                throw RuntimeException("Can only change the return type if it is Unknown")
            }
            field = v
        }

    override fun toString() = "Function args=$parameters return type : $returnType"

    data class FunctionParameter(val name: String, val type: Class<*>) {
        override fun toString() = "$name:${type.simpleName}"
    }

}

/**
 * Used as the return type for functions during the first pass.
 * The second pass will replace the function using [Function.replaceReturnType].
 *
 * No instances of this class are ever created!
 */
class Unknown private constructor()

class FunctionCall(val position: FluffyPosition, val func: Function, val arguments: List<Expression>) : Expression {

    override fun type() = func.returnType

    val infix
        get() = func.infix

    override fun eval(): Any? {

        return try {
            val funcContext = func.block.context

            // I admit, I made a mistake in the design :-(
            // We don't have a concept of a "stack", and expressions that reference local variables and
            // function parameters are directly bound to the varaibles in the Function's block's context.
            // So, fo recursion to work correctly, we need to save the state, and restore after the call.
            // Not ideal, but it will do for now. It will require a lot of work to fix it properly :-(
            val state = funcContext.saveState()

            arguments.forEachIndexed { index, expression ->
                val param = funcContext.findVariable(func.parameters[index].name)
                        ?: throw Exception("Parameter #$index not found in function context")
                param.set(expression.eval())
            }
            val result = try {
                func.block.eval()
            } catch (r: ReturnValue) {
                r.value
            }

            funcContext.restoreState(state)
            if (func.returnType == unitClass) Unit else result

        } catch (e: FluffyEvalException) {
            e.callStack.last().functionName = func.name
            e.callStack.add(position)
            throw e
        }

    }
}


abstract class Jump : Throwable()

class ReturnValue(val value: Any?) : Jump()

class ReturnStatement(val exp: Expression) : Expression {
    override fun eval() = throw ReturnValue(exp.eval())
    override fun type() = exp.type()
}

class Break : Jump()

class BreakStatement : Expression {
    override fun eval() = throw Break()
    override fun type() = unitClass
}

class Continue : Jump()

class ContinueStatement : Expression {
    override fun eval() = throw Continue()
    override fun type() = unitClass
}
