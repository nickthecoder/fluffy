package uk.co.nickthecoder.fluffy.language

class With(val thisExp: Expression, val block: Block) : Expression {

    override fun type() = block.type()

    override fun eval(): Any? {
        block.context.findVariable("this")!!.set(thisExp.eval())
        return block.eval()
    }

}
