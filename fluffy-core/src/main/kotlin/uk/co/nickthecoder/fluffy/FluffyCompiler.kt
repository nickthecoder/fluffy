package uk.co.nickthecoder.fluffy

import org.antlr.v4.runtime.*
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode
import uk.co.nickthecoder.fluffy.FluffyCompiler.Worker.MainPassListener
import uk.co.nickthecoder.fluffy.FluffyCompiler.Worker.PrePassListener
import uk.co.nickthecoder.fluffy.commands.CommandRunner
import uk.co.nickthecoder.fluffy.commands.DisabledCommandRunner
import uk.co.nickthecoder.fluffy.commands.UnixCommandRunner
import uk.co.nickthecoder.fluffy.language.*
import uk.co.nickthecoder.fluffy.language.Function
import uk.co.nickthecoder.fluffy.parser.FluffyLexer
import uk.co.nickthecoder.fluffy.parser.FluffyParser
import uk.co.nickthecoder.fluffy.parser.FluffyParserBaseListener
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Compiles source code to a [Script].
 * The script can then be run as many times as you wish using [Script.eval].
 *
 * If the script only contains variable and function definitions, then [Script.eval]
 * will return Unit (void for you Java folks).
 * Otherwise it will return the value of the last expression.
 *
 * Note. If you have code containing global variables, then they are only set after [Script.eval] is called.
 */
class FluffyCompiler {

    /**
     * How commands are run, which shell is used...
     * The default is [DisabledCommandRunner] (for safety reasons).
     * If you have the shell 'sh', then you can use [UnixCommandRunner] (I believe you could use this
     * on Windows machines too, if you install bash/dash etc). But I wouldn't bother trying to use
     * CMD.EXE because it is pretty useless!
     * There doesn't seem to be any way to quote command line arguments, as the escaping is not done
     * at the CMD.EXE level, but within every command! Ouch!
     */
    var commandRunner: CommandRunner = DisabledCommandRunner()

    /**
     * The default sandbox is [WhitelistSandbox], pre-configured with some useful classes/packages in
     * the whitelist.
     */
    var sandbox: Sandbox = WhitelistSandbox.standard()

    /**
     * Are operators/methods/fields dynamically found?
     * e.g. if the variable `foo` is defined as type `Any`, but contains an `Int`,
     * should `foo + 1` compile?
     */
    var allowDynamic = true

    var declutterStackTrace = true

    /**
     * Compiles the code, without executing it.
     * The returned [Script] can then be executed (as many times as you need) using [Script.eval].
     *
     * @param scriptId Used within [FluffyPosition] so that when combining several scripts,
     * error messages are fully descriptive about the positions of errors.
     * When compiling single pieces of code, this can be null.
     * Consider using types String, File or URL.
     * For the `standard` and `extra` fluffy scripts, the enum [BuiltInScript] is used.
     *
     * If a [FluffyEvalException] is thrown during [Script.eval], then [scriptId].toString() will
     * be used in the stack trace.
     */
    @Throws(FluffyCompilerException::class)
    fun compile(binding: Binding, code: String, scriptId: Any? = null): Script {
        return try {

            val existing = binding.context.findVariable("_commandRunner")
            if (existing == null) {
                binding.declare("_commandRunner", commandRunner, CommandRunner::class.java)
            } else {
                existing.set(commandRunner)
            }

            Worker(binding, scriptId).compile(CharStreams.fromString(code))
        } catch (e: FluffyCompilerException) {
            // De-clutter the stack trace, removing the inner-workings which are of no use to clients.
            if (declutterStackTrace) {
                throw FluffyCompilerException(e.rawMessage, e.position, e.cause)
            } else {
                throw e
            }
        }
    }

    /**
     * Created each time [FluffyCompiler.compile] is called.
     * By having this inner class, FluffyCompiler has no additional state during compilation, and it
     * can therefore be used in a multi-threaded environment.
     *
     * It also helps to keep the state required during the compilation separate from the 'preferences'
     * of the client, such as [commandRunner] and [sandbox] etc.
     *
     * Compilation is a two step process, and has separate listeners for each.
     * [PrePassListener] add to the [functions] list, and does no more.
     * [MainPassListener] does the bulk of the work.
     */
    private inner class Worker(val binding: Binding, val scriptId: Any?) {

        /**
         * A list of [Function] objects. These are created in the first pass by [PrePassListener], but at this
         * stage, their [Block]s are empty (the statements haven't been compiled yet).
         *
         * The second pass [MainPassListener], then compiles the function body, adding [Expression]s their
         * [Block]s.
         */
        private val functions = mutableListOf<Function>()


        fun compile(input: CharStream): Script {

            val lexer = FluffyLexer(input)
            val tokens = CommonTokenStream(lexer)
            lexer.removeErrorListeners()

            val parser = FluffyParser(tokens)
            parser.removeErrorListeners()

            var errorCount = 0
            var firstErrorPosition: FluffyPosition? = null
            var firstErrorMessage: String? = null

            val errorListener = object : BaseErrorListener() {
                override fun syntaxError(
                    recognizer: Recognizer<*, *>?,
                    offendingSymbol: Any?,
                    line: Int,
                    charPositionInLine: Int,
                    msg: String?,
                    e: RecognitionException?
                ) {
                    if (errorCount == 0) {
                        firstErrorPosition = FluffyPosition(line, charPositionInLine, scriptId)
                        firstErrorMessage = msg
                    }
                    errorCount++
                }
            }

            lexer.addErrorListener(errorListener)
            parser.addErrorListener(errorListener)

            val tree = parser.script()


            val prePassListener = PrePassListener()
            ParseTreeWalker.DEFAULT.walk(prePassListener, tree)

            val compilerListener = MainPassListener()
            ParseTreeWalker.DEFAULT.walk(compilerListener, tree)

            if (errorCount > 0) {
                throw FluffyCompilerException(firstErrorMessage!!, firstErrorPosition!!)
            }

            return compilerListener.createScript()
        }

        private fun createPosition(ctx: ParserRuleContext) =
            FluffyPosition(ctx.start.line, ctx.start.charPositionInLine, scriptId)

        private fun createPosition(token: Token) = FluffyPosition(token.line, token.charPositionInLine, scriptId)

        private fun createPosition(tn: TerminalNode) = createPosition(tn.symbol)

        // Some utility methods used by both listeners

        private fun classFromTypeContext(t: FluffyParser.TypeContext?, binding: Binding): Class<*>? {
            return if (t == null) {
                null
            } else {
                val sut = t.simpleUserType()
                val name = sut?.text?.removeWhiteSpace() ?: "Unit"
                binding.findClass(name)
            }
        }

        private fun functionInfo(ctx: FluffyParser.FunctionDeclarationSetupContext): Pair<String, Class<*>?> {
            val id = ctx.id?.text ?: throw FluffyCompilerException("Bad function name", createPosition(ctx))
            val dot = id.lastIndexOf('.')
            val name = if (dot < 0) {
                id
            } else {
                id.substring(dot + 1)
            }
            return if (dot < 0) {
                Pair(name, null)
            } else {
                val klass = binding.findClass(id.substring(0, dot))
                    ?: throw FluffyCompilerException(
                        "Unknown class '${
                            id.substring(
                                0,
                                dot
                            )
                        }' for extension function $id", createPosition(ctx.id)
                    )
                Pair(name, klass)
            }
        }


        /**
         * A pre-pass of the code finds all of the Function Declarations.
         * It creates a [Function] object for each, and declares it in the [Binding]'s [Context].
         * NOTE. Fluffy currently doesn't support nested functions.
         *
         * At this point, the [Function]'s block will be empty (the statements are NOT added to it yet!).
         * However, it will know the argument types, and the return type for the function.
         *
         * We keep a [List] of [Function]s defined in the code. Then [MainPassListener] keeps a counter
         * of functions defined, and can use the counter to index into the list within
         * [MainPassListener.exitFunctionDeclarationSetup].
         *
         */
        private inner class PrePassListener : FluffyParserBaseListener() {

            private var functionParameters = mutableListOf<Function.FunctionParameter>()

            private lateinit var functionBlock: Block


            override fun exitImportStatement(ctx: FluffyParser.ImportStatementContext) {
                val id = ctx.identifier().text
                val star = ctx.MULT()
                if (star != null) {
                    binding.importPackage(id)
                } else {
                    val klass = Class.forName(id)
                    val alias = ctx.importAlias()
                    if (alias == null) {
                        binding.importClass(klass.simpleName, klass)
                    } else {
                        binding.importClass(alias.simpleIdentifier().text, klass)
                    }
                }
            }

            override fun enterFunctionDeclaration(ctx: FluffyParser.FunctionDeclarationContext) {
                functionBlock = Block(Context(binding.context))
                functionParameters = mutableListOf()
            }

            override fun exitFunctionDeclaration(ctx: FluffyParser.FunctionDeclarationContext) {

                val isSimpleExpression = ctx.functionBody().ASSIGNMENT() != null

                val setupContext = ctx.functionDeclarationSetup()
                    ?: throw FluffyCompilerException("Bad function declaration", createPosition(ctx))

                // If the return type is explicitly stated, use that, otherwise, use Unit for `fun foo() {...}`
                // and Unknown for `fun foo() = ...`
                // The second pass will replace Unknown with the appropriate type.
                val returnType: Type = classFromTypeContext(setupContext.type(), binding)
                    ?: if (ctx.functionBody().ASSIGNMENT() == null) unitClass else Unknown::class.java
                val (name, receiverClass) = functionInfo(setupContext)

                val isInfix = setupContext.INFIX() != null
                if (isInfix && functionParameters.size != 2) {
                    throw FluffyCompilerException(
                        "Infix functions must have 2 parameters",
                        createPosition(setupContext.id)
                    )
                }

                if (receiverClass == null) {
                    val func =
                        Function(name, functionBlock, isSimpleExpression, returnType, functionParameters, isInfix)
                    functions.add(func)
                    binding.context.declareFunction(name, func)

                } else {
                    if (isInfix) {
                        throw FluffyCompilerException(
                            "An extension function cannot also be an infix function",
                            createPosition(setupContext.id)
                        )
                    }

                    val thisP = Function.FunctionParameter("this", receiverClass)
                    functionBlock.context.declareVariable("this", Variable(receiverClass, null, false))
                    val func = Function(
                        name,
                        functionBlock,
                        isSimpleExpression,
                        returnType,
                        mutableListOf(thisP).apply { addAll(functionParameters) })
                    functions.add(func)

                    binding.context.declareExtensionFunction(name, func)
                }
            }

            /**
             * Declare variables (with null values) for each of the function's parameters.
             */
            override fun exitFunctionValueParameters(ctx: FluffyParser.FunctionValueParametersContext) {
                functionParameters.forEach { fp ->
                    functionBlock.context.declareVariable(fp.name, Variable(fp.type, null, false))
                }
            }

            override fun exitParameter(ctx: FluffyParser.ParameterContext) {
                val name = ctx.simpleIdentifier().text
                val type = classFromTypeContext(ctx.type(), binding)
                    ?: throw FluffyCompilerException(
                        "Unknown type ${ctx.type().text} for parameter $name",
                        createPosition(ctx.type())
                    )
                functionParameters.add(Function.FunctionParameter(name, type))
            }

        }

        /**
         * The "main" pass, done after [PrePassListener]
         */
        private inner class MainPassListener : FluffyParserBaseListener() {

            private var context: Context = binding.context

            private val blockList = mutableListOf<Block>()

            private var block = Block(context)

            private var operands = block.operands


            private var functionDepth: Int = 0

            private val global = Value(Unknown::class.java, null)

            /**
             * Called after the tree has been fully walked.
             */
            fun createScript(): Script {
                return Script(block)
            }

            /**
             * Sometimes, such as [exitFunctionDeclaration], we open a block manually, rather than
             * waiting for the [enterBlock] to do it. In such cases we want the [enterBlock] to do nothing
             */
            private var skipOpenBlock = false

            private fun manuallyOpenBlock(block: Block? = null) {
                openBlock(block)
                skipOpenBlock = true // Now enterBlock() will do nothing.
            }

            private fun openBlock(newBlock: Block? = null) {
                if (skipOpenBlock) {
                    skipOpenBlock = false
                } else {
                    blockList.add(block) // Save the old block to a stack

                    block = newBlock ?: Block(Context(context))
                    context = block.context
                    operands = block.operands
                }
            }

            private fun closeBlock() {
                val oldBlock = block
                block = blockList.pop()
                operands = block.operands
                context = block.context
                operands.push(oldBlock)
                skipOpenBlock = false
            }

            override fun exitExpression(ctx: FluffyParser.ExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitDisjunction(ctx: FluffyParser.DisjunctionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitConjunction(ctx: FluffyParser.ConjunctionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitEqualityComparison(ctx: FluffyParser.EqualityComparisonContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitComparison(ctx: FluffyParser.ComparisonContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitNamedInfix(ctx: FluffyParser.NamedInfixContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitElvisExpression(ctx: FluffyParser.ElvisExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitRangeExpression(ctx: FluffyParser.RangeExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitAdditiveExpression(ctx: FluffyParser.AdditiveExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitMultiplicativeExpression(ctx: FluffyParser.MultiplicativeExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            private fun fromSupertScript(text: String): String {
                return text.map {
                    when (it) {
                        '⁺' -> '+'
                        '⁻' -> '-'
                        '⁰' -> '0'
                        '¹' -> '1'
                        '²' -> '2'
                        '³' -> '3'
                        '⁴' -> '4'
                        '⁵' -> '5'
                        '⁶' -> '6'
                        '⁷' -> '7'
                        '⁸' -> '8'
                        '⁹' -> '9'
                        else -> it
                    }
                }.joinToString(separator = "")
            }

            override fun exitPowerExpression(ctx: FluffyParser.PowerExpressionContext) {
                ctx.bop?.let {
                    exitBop(it)
                    return
                }
                ctx.PowerLiteral()?.text?.let {
                    val a = operands.pop()
                    val b = Value(fromSupertScript(it).toInt())

                    createBinaryOperator(FluffyParser.POWER, a, b)?.let { op ->
                        operands.push(op)
                        return
                    }

                    val position = createPosition(ctx.PowerLiteral())
                    val funcName = "pow"

                    ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, funcName, a, listOf(b))
                        ?.let { method ->
                            operands.push(method)
                            return
                        }

                    throw FluffyCompilerException("Unexpected power literal", createPosition(ctx.PowerLiteral()))
                }
            }

            override fun exitUberExpression(ctx: FluffyParser.UberExpressionContext) {
                ctx.bop?.let { exitBop(it) }
            }

            override fun exitPrefixUnaryExpression(ctx: FluffyParser.PrefixUnaryExpressionContext) {
                ctx.uop?.let { exitUop(it, true) }
            }


            override fun exitArrayAccess(ctx: FluffyParser.ArrayAccessContext) {

                val i = operands.pop()
                val obj = operands.pop()
                val getter = ObjectMapping.findMethod(createPosition(ctx), sandbox, "get", obj, listOf(i))
                    ?: throw FluffyCompilerException(
                        "Cannot perform array/map access on ${obj.type()} with argument type ${i.type()}",
                        createPosition(ctx)
                    )

                val op = if (listClass.isAssignableFrom(obj.klass())) {
                    IndexAccess(getter, obj, i)
                } else if (mapClass.isAssignableFrom(obj.klass())) {
                    MapAccess(getter, obj, i)
                } else {
                    getter
                }

                operands.push(op)
            }

            private fun checkUnknown(position: FluffyPosition, func: Function) {
                if (func.returnType === Unknown::class.java) {
                    throw FluffyCompilerException(
                        "Functions with implied return types must be placed BEFORE the call",
                        position
                    )
                }
            }

            override fun exitInfixFunctionCall(ctx: FluffyParser.InfixFunctionCallContext) {
                val name = ctx.simpleIdentifier()?.text ?: return
                val position = createPosition(ctx.simpleIdentifier())

                val expB = operands.pop()
                val expA = operands.pop()
                val arguments = listOf(expA, expB)

                val func = binding.context.findFunction(name, arguments.map { it.klass() })
                if (func == null || !func.infix) {
                    throw FluffyCompilerException(
                        "Unknown infix function '$name' arguments ${expA.klass().simpleName}, ${expB.klass().simpleName}",
                        position
                    )
                } else {
                    checkUnknown(position, func)
                    operands.push(FunctionCall(position, func, listOf(expA, expB)))
                }
            }

            override fun exitPostfixUnaryExpression(ctx: FluffyParser.PostfixUnaryExpressionContext) {
                ctx.uop?.let { exitUop(it, false) }
            }


            private fun arguments(ctx: FluffyParser.CallSuffixContext): List<Expression> {

                val argCount = ctx.valueArgument()?.size ?: 0
                val arguments = mutableListOf<Expression>()
                for (i in 1..argCount) {
                    arguments.add(0, operands.pop())
                }
                return arguments
            }

            override fun exitMethodCall(ctx: FluffyParser.MethodCallContext) {
                val name = ctx.simpleIdentifier().text
                val position = createPosition(ctx.simpleIdentifier())

                val arguments = arguments(ctx.callSuffix())
                val ref = operands.pop()

                if (ref === global) {
                    val func = binding.context.findFunction(name, arguments.map { it.klass() })
                    if (func == null) {
                        println("UFN 1")
                        throw FluffyCompilerException("Unknown function name '$name'", position)
                    } else {
                        checkUnknown(position, func)
                        operands.push(FunctionCall(position, func, arguments))
                    }
                    return
                }

                val method = ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, name, ref, arguments)

                if (method == null) {
                    if (allowDynamic) {
                        //println("Warning... Using dynamic method call $name on $ref : ${ref.eval()}")
                        operands.push(DynamicMethodCall(position, sandbox, binding, name, ref, arguments))
                    } else {
                        throw FluffyCompilerException(
                            "Method '$name' not found in ${ref.klass().name}",
                            position
                        )
                    }
                } else {
                    operands.push(method)
                }
            }

            override fun exitFunctionCall(ctx: FluffyParser.FunctionCallContext) {
                val name = ctx.simpleIdentifier().text
                val position = createPosition(ctx.simpleIdentifier())
                val arguments = arguments(ctx.callSuffix())

                val construct = ObjectMapping.findConstructor(sandbox, binding, name, arguments)
                if (construct != null) {
                    operands.push(construct)
                    return
                }

                val thisExp = context.findVariable("this")
                if (thisExp != null) {

                    try {
                        val method =
                            ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, name, thisExp, arguments)
                        if (method != null) {
                            operands.push(method)
                            return
                        }

                    } catch (e: Exception) {
                        // TODO I think I can do away with the try catch now!
                    }
                }

                val argTypes = arguments.map { it.klass() }
                val func = binding.context.findFunction(name, argTypes)
                if (func == null) {

                    // If name is a variable, then maybe we have an "invoke" going on...
                    findVariable(name, position)?.let { ref ->
                        val extArgTypes = mutableListOf(ref.klass()).apply {
                            addAll(argTypes)
                        }
                        val func2 = binding.context.findExtensionFunction("invoke", extArgTypes)
                        if (func2 == null) {
                            val argTypesString = argTypes.joinToString { it.simpleName }
                            throw FluffyCompilerException(
                                "Cannot invoke arguments '$argTypesString' on '${ref.klass()}'",
                                position
                            )
                        } else {
                            val refAndArguments = mutableListOf(ref).apply { addAll(arguments) }
                            operands.push(FunctionCall(position, func2, refAndArguments))
                            return
                        }
                    }

                    throw FluffyCompilerException("Unknown function name '$name'", position)
                } else {
                    checkUnknown(position, func)
                    operands.push(FunctionCall(position, func, arguments))
                }
            }

            override fun exitFieldAccess(ctx: FluffyParser.FieldAccessContext) {
                val name = ctx.simpleIdentifier().text
                val position = createPosition(ctx.simpleIdentifier())

                val ref = operands.pop()

                if (ref === global) {
                    context.findVariable(name)?.let {
                        operands.push(it)
                        return
                    }
                    throw FluffyCompilerException("Unknown global identifier '$name'", position)
                }
                val field = ObjectMapping.findFieldOrExtGetter(position, sandbox, binding, name, ref)
                    ?: if (allowDynamic) {
                        //println("Warning... Using dynamic field $name on $ref : ${ref.eval()}")
                        DynamicField(position, sandbox, binding, name, ref)
                    } else {
                        throw FluffyCompilerException(
                            "Field '$name' not found in ${ref.klass().name}",
                            position
                        )
                    }

                operands.push(field)
            }

            /**
             * Note, the identifier (name) could be referring to different things :
             * A Class, a local variable, a field (if there's an implied "this"), or a global variable and we should check them in that order!
             */
            override fun exitVariableAccess(ctx: FluffyParser.VariableAccessContext) {
                val name = ctx.simpleIdentifier().text
                val position = createPosition(ctx.simpleIdentifier())

                if (name == "global") {
                    operands.push(global)
                    return
                }

                binding.findClass(name)?.let {
                    operands.push(ClassConstant(it))
                    return
                }

                findVariable(name, position)?.let {
                    operands.push(it)
                    return
                }

                throw FluffyCompilerException("Unknown identifier '$name'", position)
            }

            private fun findVariable(name: String, position: FluffyPosition): Expression? {

                context.findLocalVariable(name)?.let {
                    return it
                }

                context.findVariable("this")?.let { thisExp ->
                    ObjectMapping.findFieldOrExtGetter(position, sandbox, binding, name, thisExp)?.let {
                        return it
                    }
                }

                context.findVariable(name)?.let {
                    return it
                }

                return null
            }

            /**
             * Such as `10(m)`. ref is 10, and m is an argument to an `invoke` extensions function of ref.
             */
            override fun exitInvokableExpression(ctx: FluffyParser.InvokableExpressionContext) {
                val callSuffix = ctx.callSuffix() ?: return

                val arguments = arguments(callSuffix)
                val ref = operands.pop()

                val position = createPosition(callSuffix)
                val extArgTypes = mutableListOf(ref.klass()).apply { addAll(arguments.map { it.klass() }) }

                val func = binding.context.findExtensionFunction("invoke", extArgTypes)
                if (func == null) {
                    val argTypes = arguments.joinToString { it.klass().simpleName }
                    throw FluffyCompilerException("Cannot invoke arguments '$argTypes' on '${ref.klass()}'", position)
                } else {
                    val refAndArguments = mutableListOf(ref).apply { addAll(arguments) }
                    operands.push(FunctionCall(position, func, refAndArguments))
                }
            }

            private fun exitBop(bop: Token) {

                val b = operands.pop()
                val a = operands.pop()

                createBinaryOperator(bop, a, b)?.let { op ->
                    operands.push(op)
                    return
                }

                val position = createPosition(bop)

                val funcName = when (bop.type) {
                    FluffyParser.ADD -> "plus"
                    FluffyParser.SUB -> "minus"
                    FluffyParser.MULT -> "times"
                    FluffyParser.DIVIDE -> "div"
                    FluffyParser.FLOOR_DIV -> "floorDiv"
                    FluffyParser.MOD -> "mod"
                    FluffyParser.POWER -> "pow"
                    FluffyParser.BACKSLASH -> "backslash"
                    else -> null
                }

                if (funcName != null) {
                    ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, funcName, a, listOf(b))
                        ?.let { method ->
                            operands.push(method)
                            return
                        }
                }

                if (allowDynamic && (b.klass() == anyClass || a.klass() == anyClass)) {
                    operands.push(DynamicBinaryOperator(createPosition(bop), bop, a, b))
                } else {
                    throw FluffyCompilerException(
                        "Operator ${bop.text} @ ${bop.line},${bop.charPositionInLine} cannot be applied to ${a.klass().simpleName} and ${b.klass().simpleName}",
                        createPosition(bop)
                    )
                }
            }

            private fun exitUop(uop: Token, isPrefix: Boolean) {

                val a = operands.pop()

                val op = if (isPrefix) createPrefixUnaryOperator(uop, a) else createPostfixUnaryOperator(uop, a)

                if (op == null) {
                    val position = createPosition(uop)

                    val funcName = when (uop.type) {
                        FluffyParser.SUB -> "unaryMinus"
                        FluffyParser.EXCL -> if (isPrefix) "not" else "factorial"
                        else -> null
                    }

                    if (funcName != null) {
                        ObjectMapping.findMethodOrExtFunc(position, sandbox, binding, funcName, a, emptyList())
                            ?.let { method ->
                                operands.push(method)
                                return
                            }
                    }

                    if (allowDynamic && a.klass() == anyClass) {
                        operands.push(DynamicUnaryOperator(createPosition(uop), uop, a, isPrefix))
                    } else {
                        throw FluffyCompilerException(
                            "Operator ${uop.text} @ ${uop.line},${uop.charPositionInLine} cannot be applied to ${a.klass().simpleName}",
                            createPosition(uop)
                        )
                    }
                } else {
                    operands.push(op)
                }
            }


            private var functionCount = 0

            override fun enterFunctionDeclaration(ctx: FluffyParser.FunctionDeclarationContext) {
                functionDepth++
            }

            override fun enterFunctionDeclarationSetup(ctx: FluffyParser.FunctionDeclarationSetupContext) {
                manuallyOpenBlock(functions[functionCount].block)
                // Only skip an open block, if the function wasn't a simple expression.
                // i.e. fun foo() = try { ... } catch { ... } should NOT skip, as that would skip the beginning
                // of the try block!
                skipOpenBlock = !functions[functionCount].isSimpleExpression
            }

            override fun exitFunctionDeclaration(ctx: FluffyParser.FunctionDeclarationContext) {

                functionDepth--
                // Was a plain expression, not a block, but we opened a block in [enterFunctionDeclarationSetup]
                if (ctx.functionBody().block() == null) {
                    closeBlock()
                }

                val oldBlock = operands.pop()
                assert(oldBlock is Block)
                // Replace the return type if it is currently unknown.
                // The first pass uses "Unknown" when the return type is implied i.e : fun foo() = ...
                val func = functions[functionCount]

                if (func.returnType === Unknown::class.java) {
                    func.returnType = oldBlock.type()

                } else if (func.returnType != unitClass && ctx.functionBody().block() != null) {
                    // A return is expected (Not Unit, and not fun foo() = ...
                    if (func.block.operands.lastOrNull() !is ReturnStatement) {
                        throw FluffyCompilerException("Missing return statement", createPosition(ctx.functionBody()))
                    }
                }

                functionCount++
                skipOpenBlock = false
            }


            override fun exitReturnStatement(ctx: FluffyParser.ReturnStatementContext) {
                if (functionDepth == 0) {
                    throw FluffyCompilerException(
                        "return statement not expected outside of a function",
                        createPosition(ctx)
                    )

                } else {
                    val func = functions[functionCount]
                    val returnType = func.returnType

                    if (ctx.expression() == null) {
                        if (returnType != unitClass) {
                            throw FluffyCompilerException(
                                "Expected a return value of type '${returnType}'",
                                createPosition(ctx)
                            )
                        }
                        operands.push(ReturnStatement(Value.UNIT))
                    } else {
                        val exp = operands.pop()
                        val coercedExp = ObjectMapping.coerceInput(exp, returnType)
                            ?: throw FluffyCompilerException(
                                "Cannot convert return value type '${exp.type()}' to '$returnType'",
                                createPosition(ctx)
                            )

                        operands.push(ReturnStatement(coercedExp))
                    }
                }
            }

            /**
             * Keeps track of the nesting level of loops. continue and break are only allowed if
             * [loopCount] > 0
             */
            private
            var loopCount = 0

            override fun exitBreakStatement(ctx: FluffyParser.BreakStatementContext) {
                if (loopCount > 0) {
                    operands.push(BreakStatement())
                } else {
                    throw FluffyCompilerException("break statement not within a loop", createPosition(ctx))
                }
            }

            override fun exitContinueStatement(ctx: FluffyParser.ContinueStatementContext) {
                if (loopCount > 0) {
                    operands.push(ContinueStatement())
                } else {
                    throw FluffyCompilerException("continue statement not within a loop", createPosition(ctx))
                }
            }

            override fun exitPropertyDeclaration(ctx: FluffyParser.PropertyDeclarationContext) {

                val isSettable = ctx.def.type == FluffyParser.VAR

                val vd = ctx.variableDeclaration()
                val id = vd.simpleIdentifier().text.removeWhiteSpace()
                val declaredType = vd.type()
                val value = if (ctx.expression() == null) null else operands.pop()

                var type: Type = anyClass

                if (declaredType == null && value != null) {
                    type = value.type()
                } else if (declaredType != null) {
                    val ut = declaredType.simpleUserType()
                    val name = ut?.text?.removeWhiteSpace() ?: "Any"
                    type = binding.findClass(name)
                        ?: throw FluffyCompilerException("Unknown type $name", createPosition(ut))
                }

                val variable = Variable(convertPrimitiveType(type), null, isSettable)
                if (value != null) {
                    val coerced = ObjectMapping.coerceInput(value, variable.type)
                        ?: throw FluffyCompilerException(
                            "Cannot coerce '${value.type()}' to '${variable.type()}'",
                            createPosition(ctx.expression())
                        )
                    operands.push(Assignment(variable, coerced))
                }

                context.declareVariable(id, variable)
            }


            override fun exitNullLiteral(ctx: FluffyParser.NullLiteralContext) {
                operands.push(Value.NULL)
            }

            override fun exitBooleanLiteral(ctx: FluffyParser.BooleanLiteralContext) {
                operands.push(Value(ctx.text!!.toBoolean()))
            }

            override fun exitIntegerLiteral(ctx: FluffyParser.IntegerLiteralContext) {
                operands.push(Value(ctx.text.replace("_", "").toInt()))
            }

            override fun exitRealLiteral(ctx: FluffyParser.RealLiteralContext) {
                operands.push(Value(ctx.text.replace("_", "").toDouble()))
            }


            override fun exitListLiteral(ctx: FluffyParser.ListLiteralContext) {

                val firstTypeName = ctx.first?.Identifier()?.text
                val secondTypeName = ctx.second?.Identifier()?.text

                val expressions = mutableListOf<Expression>()

                if (firstTypeName == null) {
                    // No types specified, lets guess!

                    var allPairs = ctx.expression().size > 0

                    for (i in 1..ctx.expression().size) {
                        val exp = operands.pop()
                        allPairs = allPairs && (exp.klass() == Pair::class.java)
                        expressions.add(exp)
                    }
                    if (allPairs) {

                        val keyTypes = expressions.map {
                            (it.type() as? ParameterizedType)?.actualTypeArguments?.get(0) as? Class<*>
                        }.filterNotNull()
                        val keyType = if (keyTypes.isEmpty()) anyClass else nearestSuperClass(* keyTypes.toTypedArray())

                        val valueTypes = expressions.map {
                            (it.type() as? ParameterizedType)?.actualTypeArguments?.get(1) as? Class<*>
                        }.filterNotNull()
                        val valueType =
                            if (valueTypes.isEmpty()) anyClass else nearestSuperClass(* valueTypes.toTypedArray())

                        operands.push(
                            MapExpression(
                                expressions.reversed(),
                                ParameterizedTypeImplementation.forMap(keyType, valueType)
                            )
                        )

                    } else {
                        val elementType = nearestSuperClass(* expressions.map { it.klass() }.toTypedArray())
                        operands.push(
                            ListExpression(
                                expressions.reversed(),
                                ParameterizedTypeImplementation.forList(elementType)
                            )
                        )
                    }

                } else {

                    val firstType = binding.findClass(firstTypeName)
                        ?: throw FluffyCompilerException("Unknown type : $firstTypeName", createPosition(ctx.first))

                    if (secondTypeName == null) {
                        // A list with a named type
                        // TODO Check types of each element
                        operands.push(
                            ListExpression(
                                expressions.reversed(),
                                ParameterizedTypeImplementation.forList(firstType)
                            )
                        )

                    } else {
                        // a map with named types

                        val secondType = binding.findClass(secondTypeName)
                            ?: throw FluffyCompilerException(
                                "Unknown type : $secondTypeName",
                                createPosition(ctx.second)
                            )
                        // TODO Check types of each element
                        operands.push(
                            MapExpression(
                                expressions.reversed(),
                                ParameterizedTypeImplementation.forMap(firstType, secondType)
                            )
                        )
                    }
                }
            }


            /**
             * Set to true in [enterLineStringLiteral] and [enterMultiLineStringLiteral] so that the first
             * part of a string is simply pushed onto the operands stack, and subsequent items are concatenated with
             * what is already there.
             * An empty string, then [firstStringContent] will still be true, and we need to add an empty string
             * instead of popping the stack
             */

            private fun addText(exp: Expression) {
                if (firstStringContent) {
                    if (exp.klass() == stringClass) {
                        operands.push(exp)
                    } else {
                        operands.push(Expression.toString(exp))
                    }
                    firstStringContent = false
                } else {
                    val prev = operands.pop()
                    operands.push(StringExpression.concatenate(prev, exp))
                }
            }

            override fun exitCharacterLiteral(ctx: FluffyParser.CharacterLiteralContext) {
                operands.push(Value(ctx.CharacterLiteral().text[1]))
            }


            /**
             * A stack of ints, each time we enter and exit a string, we push/pop the [quoteCount].
             */
            private
            val quoteCounts = mutableListOf<Int>()

            private
            var quoteCount = -1

            private
            val firstStringList = mutableListOf<Boolean>()

            private
            var firstStringContent = true

            private fun enterAnyStringLiteral(isCommand: Boolean) {
                quoteCounts.push(quoteCount)
                firstStringList.push(firstStringContent)
                firstStringContent = true
                quoteCount = if (isCommand) 0 else -1
            }

            private fun exitAnyStringLiteral() {
                quoteCount = quoteCounts.pop()
                firstStringContent = firstStringList.pop()
            }

            override fun enterLineStringLiteral(ctx: FluffyParser.LineStringLiteralContext) {
                enterAnyStringLiteral(false)
            }

            override fun enterMultiLineStringLiteral(ctx: FluffyParser.MultiLineStringLiteralContext) {
                enterAnyStringLiteral(false)
            }

            override fun enterBackTickCommandLiteral(ctx: FluffyParser.BackTickCommandLiteralContext) {
                enterAnyStringLiteral(true)
            }

            override fun enterCommandLiteral(ctx: FluffyParser.CommandLiteralContext?) {
                enterAnyStringLiteral(true)
            }

            private fun exitStringContent(ctx: ParserRuleContext, ref: TerminalNode?) {
                if (ref == null) {
                    // Must be constant text

                    val text = unescape(ctx.text)
                    if (quoteCount >= 0) {
                        quoteCount += text.count { it == '\'' }
                    }
                    addText(Value(text))
                } else {
                    // $IDENTIFIER
                    val id = ctx.text.substring(1)
                    val exp = context.findVariable(id)
                        ?: throw FluffyCompilerException("Unknown identifier '$id'", createPosition(ctx))

                    addPossiblyQuoteEscapedText(exp)
                }
            }

            private fun addPossiblyQuoteEscapedText(exp: Expression) {
                if (quoteCount > 0 && quoteCount % 2 == 1) {
                    addText(
                        UnaryOperator(
                            stringClass,
                            exp.toStringExp()
                        ) { str: String -> commandRunner.escapeQuote(str) })
                } else {
                    addText(exp)
                }
            }

            override fun exitLineStringContent(ctx: FluffyParser.LineStringContentContext) {
                exitStringContent(ctx, ctx.LineStrRef())
            }

            override fun exitMultiLineStringContent(ctx: FluffyParser.MultiLineStringContentContext) {
                exitStringContent(ctx, ctx.MultiLineStrRef())
            }

            override fun exitBackTickCommandContent(ctx: FluffyParser.BackTickCommandContentContext) {
                exitStringContent(ctx, ctx.BackTickRef())
            }

            override fun exitCommandContent(ctx: FluffyParser.CommandContentContext) {
                exitStringContent(ctx, ctx.CommandRef())
            }


            override fun exitLineStringExpression(ctx: FluffyParser.LineStringExpressionContext) {
                // The expression will have been put on the stack
                addText(operands.pop())
            }

            override fun exitMultiLineStringExpression(ctx: FluffyParser.MultiLineStringExpressionContext) {
                // The expression will have been put on the stack
                addText(operands.pop())
            }

            override fun exitBackTickExpression(ctx: FluffyParser.BackTickExpressionContext) {
                // The expression will have been put on the stack
                addPossiblyQuoteEscapedText(operands.pop())
            }

            override fun exitCommandExpression(ctx: FluffyParser.CommandExpressionContext) {
                // The expression will have been put on the stack
                addPossiblyQuoteEscapedText(operands.pop())
            }


            private fun exitStringValue() = if (firstStringContent) {
                Value.EMPTY_STRING
            } else {
                operands.pop()
            }


            override fun exitLineStringLiteral(ctx: FluffyParser.LineStringLiteralContext) {
                if (firstStringContent) {
                    operands.push(Value.EMPTY_STRING)
                }
                exitAnyStringLiteral()
            }

            override fun exitMultiLineStringLiteral(ctx: FluffyParser.MultiLineStringLiteralContext) {
                if (firstStringContent) {
                    operands.push(Value.EMPTY_STRING)
                }
                exitAnyStringLiteral()

            }

            override fun exitBackTickCommandLiteral(ctx: FluffyParser.BackTickCommandLiteralContext) {
                operands.push(CommandExpression(commandRunner, exitStringValue(), false))
                exitAnyStringLiteral()

            }

            override fun exitCommandLiteral(ctx: FluffyParser.CommandLiteralContext) {
                // The top operand will be the command string.
                // Replace it with a CommandExpression (which will run the command when evaluated)
                operands.push(CommandExpression(commandRunner, exitStringValue(), true))
                exitAnyStringLiteral()
            }


            override fun enterBlock(ctx: FluffyParser.BlockContext) {
                openBlock()
            }

            override fun exitBlock(ctx: FluffyParser.BlockContext) {
                closeBlock()
            }

            override fun exitForInSetup(ctx: FluffyParser.ForInSetupContext) {
                val iterable = operands.pop()

                val position = createPosition(ctx.IN())

                val type = iterable.klass()

                // Find the type for the loop variable
                var iterator = ObjectMapping.findMethod(position, sandbox, "iterator", iterable, emptyList())

                if (iterator == null) {
                    val func = binding.context.findExtensionFunction("iterator", listOf(type))
                    if (func != null) {
                        checkUnknown(position, func)
                        iterator = FunctionCall(createPosition(ctx.IN()), func, listOf(iterable))
                    }
                }

                if (iterator != null) {
                    val next = ObjectMapping.findMethod(position, sandbox, "next", iterator, emptyList())
                        ?: throw FluffyCompilerException("Invalid iterator", createPosition(ctx.IN()))

                    operands.push(iterator)
                    manuallyOpenBlock()

                    val loopVarName = ctx.loopVar.text
                    block.context.declareVariable(loopVarName, Variable(next.type(), null, false))
                    ctx.counterVar?.text?.let {
                        block.context.declareVariable(it, Variable(intClass, null, false))
                    }

                } else {
                    throw FluffyCompilerException("Cannot iterate over type ${type.name}", position)
                }
                loopCount++
            }

            override fun exitForStatement(ctx: FluffyParser.ForStatementContext) {
                // Was a plain expression, not a block, but we created a block in exitFunctionDeclarationSetup
                if (ctx.controlStructureBody().block() == null) {
                    closeBlock()
                }
                val loopBlock = operands.pop() as Block
                val iterator = operands.pop()
                val loopVarName = ctx.forInSetup().loopVar.text
                val counterName = ctx.forInSetup().counterVar?.text

                operands.push(ForIn(iterator, loopVarName, loopBlock, counterName))
                loopCount--
            }


            override fun exitIfExpression(ctx: FluffyParser.IfExpressionContext) {
                val elseBlock = if (ctx.ELSE() == null) {
                    null
                } else {
                    operands.pop()
                }
                val ifBlock = operands.pop()
                val condition = operands.pop()
                operands.push(IfExpression(condition, ifBlock, elseBlock))
            }

            override fun enterWhileExpression(ctx: FluffyParser.WhileExpressionContext) {
                loopCount++
            }

            override fun enterDoWhileExpression(ctx: FluffyParser.DoWhileExpressionContext) {
                loopCount++
            }

            override fun exitDoWhileExpression(ctx: FluffyParser.DoWhileExpressionContext) {
                val condition = operands.pop()
                val whileBlock = operands.pop()
                operands.push(DoWhileExpression(condition, whileBlock))
                loopCount--
            }

            override fun exitWhileExpression(ctx: FluffyParser.WhileExpressionContext) {
                val whileBlock = operands.pop()
                val condition = operands.pop()
                operands.push(WhileExpression(condition, whileBlock))
            }

            override fun exitWithSetup(ctx: FluffyParser.WithSetupContext) {
                val thisExp = operands.peek()
                manuallyOpenBlock()
                block.context.declareVariable("this", Variable(thisExp.type(), null, false))
            }

            override fun exitWithExpression(ctx: FluffyParser.WithExpressionContext) {
                val block = operands.pop() as Block
                val thisExp = operands.pop()
                operands.push(With(thisExp, block))
            }

            override fun exitTryExpression(ctx: FluffyParser.TryExpressionContext) {

                val finallyExp = if (ctx.finallyBlock() == null) null else operands.pop() as Block
                val catchExp = if (ctx.catchBlock() == null) null else operands.pop() as Block
                val tryExp = operands.pop() as Block
                val catchVariableName = ctx.catchBlock()?.simpleIdentifier()?.text

                operands.push(TryExpression(tryExp, catchExp, catchVariableName, finallyExp))
            }

            override fun enterCatchBlock(ctx: FluffyParser.CatchBlockContext) {
                val context = Context(context)
                context.declareVariable(ctx.simpleIdentifier().text, Variable(Throwable::class.java, null, false))
                manuallyOpenBlock(Block(context))
            }

            override fun exitThrowStatement(ctx: FluffyParser.ThrowStatementContext) {
                val throwable = operands.pop()
                if (!Throwable::class.java.isAssignableFrom(throwable.klass())) {
                    throw FluffyCompilerException("Not an Exception (or Throwable)", createPosition(ctx))
                }
                operands.push(ThrowExpression(createPosition(ctx), throwable))
            }

            private fun unescape(str: String): String {
                return if (str.contains("\\")) {
                    val buffer = StringBuffer(str.length)
                    var escape = false
                    for (c in str) {
                        if (escape) {
                            buffer.append(
                                when (c) {
                                    'n' -> '\n'
                                    't' -> '\t'
                                    else -> c
                                }
                            )
                            escape = false
                        } else {
                            if (c == '\\') {
                                escape = true
                            } else {
                                buffer.append(c)
                            }
                        }
                    }
                    buffer.toString()
                } else {
                    str
                }
            }
        }

    }

    companion object {

        /**
         * Creates a FluffyCompiler, which has full access to all Java objects, as well as
         * full access to commands.
         *
         * Only use this in trusted environments. (The script writer could delete files, and do other
         * nasty things!)
         */
        fun permissive() = FluffyCompiler().apply {
            commandRunner = UnixCommandRunner()
            sandbox = PermissiveSandbox()
        }

        @JvmStatic
        fun main(vararg argv: String) {
            val text = if (argv.isEmpty()) {
                "1 + 2 * 3 + 4"
            } else {
                argv[0]
            }
            FluffyCompiler().compile(Binding(), text)
        }

    }
}


private fun String.removeWhiteSpace() = this.replace(Regex("\\s"), "")

private fun <T> MutableList<T>.pop(): T {
    return this.removeAt(this.size - 1)
}

private fun <T> MutableList<T>.peek(): T = this.last()

private fun <T> MutableList<T>.push(value: T) {
    add(value)
}
