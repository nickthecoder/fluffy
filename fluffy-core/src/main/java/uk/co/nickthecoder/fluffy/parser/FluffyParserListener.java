// Generated from /home/nick/projects/fluffy/fluffy-core/src/dist/FluffyParser.g4 by ANTLR 4.9.1
package uk.co.nickthecoder.fluffy.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FluffyParser}.
 */
public interface FluffyParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FluffyParser#script}.
	 * @param ctx the parse tree
	 */
	void enterScript(FluffyParser.ScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#script}.
	 * @param ctx the parse tree
	 */
	void exitScript(FluffyParser.ScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#topLevelObject}.
	 * @param ctx the parse tree
	 */
	void enterTopLevelObject(FluffyParser.TopLevelObjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#topLevelObject}.
	 * @param ctx the parse tree
	 */
	void exitTopLevelObject(FluffyParser.TopLevelObjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(FluffyParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(FluffyParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#importAlias}.
	 * @param ctx the parse tree
	 */
	void enterImportAlias(FluffyParser.ImportAliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#importAlias}.
	 * @param ctx the parse tree
	 */
	void exitImportAlias(FluffyParser.ImportAliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(FluffyParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(FluffyParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#functionDeclarationSetup}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclarationSetup(FluffyParser.FunctionDeclarationSetupContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#functionDeclarationSetup}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclarationSetup(FluffyParser.FunctionDeclarationSetupContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#functionValueParameters}.
	 * @param ctx the parse tree
	 */
	void enterFunctionValueParameters(FluffyParser.FunctionValueParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#functionValueParameters}.
	 * @param ctx the parse tree
	 */
	void exitFunctionValueParameters(FluffyParser.FunctionValueParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(FluffyParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(FluffyParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(FluffyParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(FluffyParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(FluffyParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(FluffyParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(FluffyParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(FluffyParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#ifExpression}.
	 * @param ctx the parse tree
	 */
	void enterIfExpression(FluffyParser.IfExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#ifExpression}.
	 * @param ctx the parse tree
	 */
	void exitIfExpression(FluffyParser.IfExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(FluffyParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(FluffyParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#forInSetup}.
	 * @param ctx the parse tree
	 */
	void enterForInSetup(FluffyParser.ForInSetupContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#forInSetup}.
	 * @param ctx the parse tree
	 */
	void exitForInSetup(FluffyParser.ForInSetupContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void enterWhileExpression(FluffyParser.WhileExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#whileExpression}.
	 * @param ctx the parse tree
	 */
	void exitWhileExpression(FluffyParser.WhileExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#doWhileExpression}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileExpression(FluffyParser.DoWhileExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#doWhileExpression}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileExpression(FluffyParser.DoWhileExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#withExpression}.
	 * @param ctx the parse tree
	 */
	void enterWithExpression(FluffyParser.WithExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#withExpression}.
	 * @param ctx the parse tree
	 */
	void exitWithExpression(FluffyParser.WithExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#withSetup}.
	 * @param ctx the parse tree
	 */
	void enterWithSetup(FluffyParser.WithSetupContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#withSetup}.
	 * @param ctx the parse tree
	 */
	void exitWithSetup(FluffyParser.WithSetupContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#withBody}.
	 * @param ctx the parse tree
	 */
	void enterWithBody(FluffyParser.WithBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#withBody}.
	 * @param ctx the parse tree
	 */
	void exitWithBody(FluffyParser.WithBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#controlStructureBody}.
	 * @param ctx the parse tree
	 */
	void enterControlStructureBody(FluffyParser.ControlStructureBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#controlStructureBody}.
	 * @param ctx the parse tree
	 */
	void exitControlStructureBody(FluffyParser.ControlStructureBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#propertyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterPropertyDeclaration(FluffyParser.PropertyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#propertyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitPropertyDeclaration(FluffyParser.PropertyDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(FluffyParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(FluffyParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(FluffyParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(FluffyParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void enterDisjunction(FluffyParser.DisjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#disjunction}.
	 * @param ctx the parse tree
	 */
	void exitDisjunction(FluffyParser.DisjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void enterConjunction(FluffyParser.ConjunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#conjunction}.
	 * @param ctx the parse tree
	 */
	void exitConjunction(FluffyParser.ConjunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#equalityComparison}.
	 * @param ctx the parse tree
	 */
	void enterEqualityComparison(FluffyParser.EqualityComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#equalityComparison}.
	 * @param ctx the parse tree
	 */
	void exitEqualityComparison(FluffyParser.EqualityComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(FluffyParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(FluffyParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#namedInfix}.
	 * @param ctx the parse tree
	 */
	void enterNamedInfix(FluffyParser.NamedInfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#namedInfix}.
	 * @param ctx the parse tree
	 */
	void exitNamedInfix(FluffyParser.NamedInfixContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#elvisExpression}.
	 * @param ctx the parse tree
	 */
	void enterElvisExpression(FluffyParser.ElvisExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#elvisExpression}.
	 * @param ctx the parse tree
	 */
	void exitElvisExpression(FluffyParser.ElvisExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#infixFunctionCall}.
	 * @param ctx the parse tree
	 */
	void enterInfixFunctionCall(FluffyParser.InfixFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#infixFunctionCall}.
	 * @param ctx the parse tree
	 */
	void exitInfixFunctionCall(FluffyParser.InfixFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#rangeExpression}.
	 * @param ctx the parse tree
	 */
	void enterRangeExpression(FluffyParser.RangeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#rangeExpression}.
	 * @param ctx the parse tree
	 */
	void exitRangeExpression(FluffyParser.RangeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(FluffyParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(FluffyParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(FluffyParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(FluffyParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void enterPowerExpression(FluffyParser.PowerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void exitPowerExpression(FluffyParser.PowerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#uberExpression}.
	 * @param ctx the parse tree
	 */
	void enterUberExpression(FluffyParser.UberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#uberExpression}.
	 * @param ctx the parse tree
	 */
	void exitUberExpression(FluffyParser.UberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#typeRHS}.
	 * @param ctx the parse tree
	 */
	void enterTypeRHS(FluffyParser.TypeRHSContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#typeRHS}.
	 * @param ctx the parse tree
	 */
	void exitTypeRHS(FluffyParser.TypeRHSContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#prefixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrefixUnaryExpression(FluffyParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#prefixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrefixUnaryExpression(FluffyParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#invokableExpression}.
	 * @param ctx the parse tree
	 */
	void enterInvokableExpression(FluffyParser.InvokableExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#invokableExpression}.
	 * @param ctx the parse tree
	 */
	void exitInvokableExpression(FluffyParser.InvokableExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#postfixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixUnaryExpression(FluffyParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#postfixUnaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixUnaryExpression(FluffyParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#variableAccess}.
	 * @param ctx the parse tree
	 */
	void enterVariableAccess(FluffyParser.VariableAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#variableAccess}.
	 * @param ctx the parse tree
	 */
	void exitVariableAccess(FluffyParser.VariableAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(FluffyParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(FluffyParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(FluffyParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(FluffyParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void enterFieldAccess(FluffyParser.FieldAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#fieldAccess}.
	 * @param ctx the parse tree
	 */
	void exitFieldAccess(FluffyParser.FieldAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void enterArrayAccess(FluffyParser.ArrayAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#arrayAccess}.
	 * @param ctx the parse tree
	 */
	void exitArrayAccess(FluffyParser.ArrayAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#callSuffix}.
	 * @param ctx the parse tree
	 */
	void enterCallSuffix(FluffyParser.CallSuffixContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#callSuffix}.
	 * @param ctx the parse tree
	 */
	void exitCallSuffix(FluffyParser.CallSuffixContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#valueArgument}.
	 * @param ctx the parse tree
	 */
	void enterValueArgument(FluffyParser.ValueArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#valueArgument}.
	 * @param ctx the parse tree
	 */
	void exitValueArgument(FluffyParser.ValueArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#atomicExpression}.
	 * @param ctx the parse tree
	 */
	void enterAtomicExpression(FluffyParser.AtomicExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#atomicExpression}.
	 * @param ctx the parse tree
	 */
	void exitAtomicExpression(FluffyParser.AtomicExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void enterTryExpression(FluffyParser.TryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#tryExpression}.
	 * @param ctx the parse tree
	 */
	void exitTryExpression(FluffyParser.TryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#catchBlock}.
	 * @param ctx the parse tree
	 */
	void enterCatchBlock(FluffyParser.CatchBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#catchBlock}.
	 * @param ctx the parse tree
	 */
	void exitCatchBlock(FluffyParser.CatchBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#finallyBlock}.
	 * @param ctx the parse tree
	 */
	void enterFinallyBlock(FluffyParser.FinallyBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#finallyBlock}.
	 * @param ctx the parse tree
	 */
	void exitFinallyBlock(FluffyParser.FinallyBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void enterThrowStatement(FluffyParser.ThrowStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void exitThrowStatement(FluffyParser.ThrowStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(FluffyParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(FluffyParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(FluffyParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(FluffyParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(FluffyParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(FluffyParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedExpression(FluffyParser.ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedExpression(FluffyParser.ParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(FluffyParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(FluffyParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#simpleUserType}.
	 * @param ctx the parse tree
	 */
	void enterSimpleUserType(FluffyParser.SimpleUserTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#simpleUserType}.
	 * @param ctx the parse tree
	 */
	void exitSimpleUserType(FluffyParser.SimpleUserTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(FluffyParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(FluffyParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(FluffyParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(FluffyParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#simpleIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterSimpleIdentifier(FluffyParser.SimpleIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#simpleIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitSimpleIdentifier(FluffyParser.SimpleIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#listLiteral}.
	 * @param ctx the parse tree
	 */
	void enterListLiteral(FluffyParser.ListLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#listLiteral}.
	 * @param ctx the parse tree
	 */
	void exitListLiteral(FluffyParser.ListLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void enterLiteralConstant(FluffyParser.LiteralConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void exitLiteralConstant(FluffyParser.LiteralConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void enterNullLiteral(FluffyParser.NullLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#nullLiteral}.
	 * @param ctx the parse tree
	 */
	void exitNullLiteral(FluffyParser.NullLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#characterLiteral}.
	 * @param ctx the parse tree
	 */
	void enterCharacterLiteral(FluffyParser.CharacterLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#characterLiteral}.
	 * @param ctx the parse tree
	 */
	void exitCharacterLiteral(FluffyParser.CharacterLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiteral(FluffyParser.BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#booleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiteral(FluffyParser.BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void enterRealLiteral(FluffyParser.RealLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#realLiteral}.
	 * @param ctx the parse tree
	 */
	void exitRealLiteral(FluffyParser.RealLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void enterIntegerLiteral(FluffyParser.IntegerLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#integerLiteral}.
	 * @param ctx the parse tree
	 */
	void exitIntegerLiteral(FluffyParser.IntegerLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#commandLiteral}.
	 * @param ctx the parse tree
	 */
	void enterCommandLiteral(FluffyParser.CommandLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#commandLiteral}.
	 * @param ctx the parse tree
	 */
	void exitCommandLiteral(FluffyParser.CommandLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#commandContent}.
	 * @param ctx the parse tree
	 */
	void enterCommandContent(FluffyParser.CommandContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#commandContent}.
	 * @param ctx the parse tree
	 */
	void exitCommandContent(FluffyParser.CommandContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#commandExpression}.
	 * @param ctx the parse tree
	 */
	void enterCommandExpression(FluffyParser.CommandExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#commandExpression}.
	 * @param ctx the parse tree
	 */
	void exitCommandExpression(FluffyParser.CommandExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#backTickCommandLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBackTickCommandLiteral(FluffyParser.BackTickCommandLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#backTickCommandLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBackTickCommandLiteral(FluffyParser.BackTickCommandLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#backTickCommandContent}.
	 * @param ctx the parse tree
	 */
	void enterBackTickCommandContent(FluffyParser.BackTickCommandContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#backTickCommandContent}.
	 * @param ctx the parse tree
	 */
	void exitBackTickCommandContent(FluffyParser.BackTickCommandContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#backTickExpression}.
	 * @param ctx the parse tree
	 */
	void enterBackTickExpression(FluffyParser.BackTickExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#backTickExpression}.
	 * @param ctx the parse tree
	 */
	void exitBackTickExpression(FluffyParser.BackTickExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(FluffyParser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(FluffyParser.StringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#lineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterLineStringLiteral(FluffyParser.LineStringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#lineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitLineStringLiteral(FluffyParser.LineStringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#multiLineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringLiteral(FluffyParser.MultiLineStringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#multiLineStringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringLiteral(FluffyParser.MultiLineStringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#lineStringContent}.
	 * @param ctx the parse tree
	 */
	void enterLineStringContent(FluffyParser.LineStringContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#lineStringContent}.
	 * @param ctx the parse tree
	 */
	void exitLineStringContent(FluffyParser.LineStringContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#lineStringExpression}.
	 * @param ctx the parse tree
	 */
	void enterLineStringExpression(FluffyParser.LineStringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#lineStringExpression}.
	 * @param ctx the parse tree
	 */
	void exitLineStringExpression(FluffyParser.LineStringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#multiLineStringContent}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringContent(FluffyParser.MultiLineStringContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#multiLineStringContent}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringContent(FluffyParser.MultiLineStringContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#multiLineStringExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiLineStringExpression(FluffyParser.MultiLineStringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#multiLineStringExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiLineStringExpression(FluffyParser.MultiLineStringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#semi}.
	 * @param ctx the parse tree
	 */
	void enterSemi(FluffyParser.SemiContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#semi}.
	 * @param ctx the parse tree
	 */
	void exitSemi(FluffyParser.SemiContext ctx);
	/**
	 * Enter a parse tree produced by {@link FluffyParser#anysemi}.
	 * @param ctx the parse tree
	 */
	void enterAnysemi(FluffyParser.AnysemiContext ctx);
	/**
	 * Exit a parse tree produced by {@link FluffyParser#anysemi}.
	 * @param ctx the parse tree
	 */
	void exitAnysemi(FluffyParser.AnysemiContext ctx);
}