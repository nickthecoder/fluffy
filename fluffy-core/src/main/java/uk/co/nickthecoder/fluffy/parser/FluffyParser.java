// Generated from /home/nick/projects/fluffy/fluffy-core/src/dist/FluffyParser.g4 by ANTLR 4.9.1
package uk.co.nickthecoder.fluffy.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FluffyParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		DelimitedComment=1, LineComment=2, WS=3, NL=4, DOT=5, COMMA=6, LPAREN=7, 
		RPAREN=8, LSQUARE=9, RSQUARE=10, LCURL=11, RCURL=12, POWER=13, MULT=14, 
		MOD=15, DIVIDE=16, BACKSLASH=17, FLOOR_DIV=18, ADD=19, SUB=20, INCR=21, 
		DECR=22, CONJ=23, DISJ=24, EXCL_EXCL=25, EXCL=26, COLON=27, SEMICOLON=28, 
		ASSIGNMENT=29, ADD_ASSIGNMENT=30, SUB_ASSIGNMENT=31, MULT_ASSIGNMENT=32, 
		FLOOR_DIV_ASSIGNMENT=33, DIVIDE_ASSIGNMENT=34, MOD_ASSIGNMENT=35, PAIR_TO=36, 
		RANGE=37, RANGE_EXCLUSIVE=38, ELVIS=39, LANGLE=40, RANGLE=41, LE=42, GE=43, 
		EXCL_EQ=44, EXCL_EQEQ=45, AS_SAFE=46, EQEQ=47, EQEQEQ=48, SINGLE_QUOTE=49, 
		IMPORT=50, FUN=51, VAR=52, VAL=53, IF=54, ELSE=55, TRY=56, CATCH=57, FINALLY=58, 
		THROW=59, FOR=60, DO=61, WHILE=62, WITH=63, RETURN=64, CONTINUE=65, BREAK=66, 
		AS=67, IS=68, IN=69, COUNTER=70, NOT_IS=71, NOT_IN=72, INFIX=73, BACKTICK_OPEN=74, 
		COMMAND_OPEN=75, QUOTE_OPEN=76, TRIPLE_QUOTE_OPEN=77, RealLiteral=78, 
		FloatLiteral=79, DoubleLiteral=80, LongLiteral=81, IntegerLiteral=82, 
		PowerLiteral=83, PowerSign=84, PowerDigit=85, HexLiteral=86, BinLiteral=87, 
		BooleanLiteral=88, NullLiteral=89, Identifier=90, LabelReference=91, LabelDefinition=92, 
		FieldIdentifier=93, CharacterLiteral=94, UNICODE_CLASS_LL=95, UNICODE_CLASS_LM=96, 
		UNICODE_CLASS_LO=97, UNICODE_CLASS_LT=98, UNICODE_CLASS_LU=99, UNICODE_CLASS_ND=100, 
		UNICODE_CLASS_NL=101, Inside_Comment=102, Inside_WS=103, Inside_NL=104, 
		BACKTICK_CLOSE=105, BackTickRef=106, BackTickText=107, BackTickEscapedChar=108, 
		BackTickExprStart=109, COMMAND_CLOSE=110, CommandRef=111, CommandText=112, 
		CommandEscapedChar=113, CommandExprStart=114, QUOTE_CLOSE=115, LineStrRef=116, 
		LineStrText=117, LineStrEscapedChar=118, LineStrExprStart=119, TRIPLE_QUOTE_CLOSE=120, 
		MultiLineStringQuote=121, MultiLineStrRef=122, MultiLineStrText=123, MultiLineStrEscapedChar=124, 
		MultiLineStrExprStart=125, MultiLineNL=126, StrExpr_IN=127, StrExpr_COUNTER=128, 
		StrExpr_Comment=129, StrExpr_WS=130, StrExpr_NL=131;
	public static final int
		RULE_script = 0, RULE_topLevelObject = 1, RULE_importStatement = 2, RULE_importAlias = 3, 
		RULE_functionDeclaration = 4, RULE_functionDeclarationSetup = 5, RULE_functionValueParameters = 6, 
		RULE_functionBody = 7, RULE_block = 8, RULE_statements = 9, RULE_statement = 10, 
		RULE_ifExpression = 11, RULE_forStatement = 12, RULE_forInSetup = 13, 
		RULE_whileExpression = 14, RULE_doWhileExpression = 15, RULE_withExpression = 16, 
		RULE_withSetup = 17, RULE_withBody = 18, RULE_controlStructureBody = 19, 
		RULE_propertyDeclaration = 20, RULE_variableDeclaration = 21, RULE_expression = 22, 
		RULE_disjunction = 23, RULE_conjunction = 24, RULE_equalityComparison = 25, 
		RULE_comparison = 26, RULE_namedInfix = 27, RULE_elvisExpression = 28, 
		RULE_infixFunctionCall = 29, RULE_rangeExpression = 30, RULE_additiveExpression = 31, 
		RULE_multiplicativeExpression = 32, RULE_powerExpression = 33, RULE_uberExpression = 34, 
		RULE_typeRHS = 35, RULE_prefixUnaryExpression = 36, RULE_invokableExpression = 37, 
		RULE_postfixUnaryExpression = 38, RULE_variableAccess = 39, RULE_functionCall = 40, 
		RULE_methodCall = 41, RULE_fieldAccess = 42, RULE_arrayAccess = 43, RULE_callSuffix = 44, 
		RULE_valueArgument = 45, RULE_atomicExpression = 46, RULE_tryExpression = 47, 
		RULE_catchBlock = 48, RULE_finallyBlock = 49, RULE_throwStatement = 50, 
		RULE_returnStatement = 51, RULE_continueStatement = 52, RULE_breakStatement = 53, 
		RULE_parenthesizedExpression = 54, RULE_type = 55, RULE_simpleUserType = 56, 
		RULE_parameter = 57, RULE_identifier = 58, RULE_simpleIdentifier = 59, 
		RULE_listLiteral = 60, RULE_literalConstant = 61, RULE_nullLiteral = 62, 
		RULE_characterLiteral = 63, RULE_booleanLiteral = 64, RULE_realLiteral = 65, 
		RULE_integerLiteral = 66, RULE_commandLiteral = 67, RULE_commandContent = 68, 
		RULE_commandExpression = 69, RULE_backTickCommandLiteral = 70, RULE_backTickCommandContent = 71, 
		RULE_backTickExpression = 72, RULE_stringLiteral = 73, RULE_lineStringLiteral = 74, 
		RULE_multiLineStringLiteral = 75, RULE_lineStringContent = 76, RULE_lineStringExpression = 77, 
		RULE_multiLineStringContent = 78, RULE_multiLineStringExpression = 79, 
		RULE_semi = 80, RULE_anysemi = 81;
	private static String[] makeRuleNames() {
		return new String[] {
			"script", "topLevelObject", "importStatement", "importAlias", "functionDeclaration", 
			"functionDeclarationSetup", "functionValueParameters", "functionBody", 
			"block", "statements", "statement", "ifExpression", "forStatement", "forInSetup", 
			"whileExpression", "doWhileExpression", "withExpression", "withSetup", 
			"withBody", "controlStructureBody", "propertyDeclaration", "variableDeclaration", 
			"expression", "disjunction", "conjunction", "equalityComparison", "comparison", 
			"namedInfix", "elvisExpression", "infixFunctionCall", "rangeExpression", 
			"additiveExpression", "multiplicativeExpression", "powerExpression", 
			"uberExpression", "typeRHS", "prefixUnaryExpression", "invokableExpression", 
			"postfixUnaryExpression", "variableAccess", "functionCall", "methodCall", 
			"fieldAccess", "arrayAccess", "callSuffix", "valueArgument", "atomicExpression", 
			"tryExpression", "catchBlock", "finallyBlock", "throwStatement", "returnStatement", 
			"continueStatement", "breakStatement", "parenthesizedExpression", "type", 
			"simpleUserType", "parameter", "identifier", "simpleIdentifier", "listLiteral", 
			"literalConstant", "nullLiteral", "characterLiteral", "booleanLiteral", 
			"realLiteral", "integerLiteral", "commandLiteral", "commandContent", 
			"commandExpression", "backTickCommandLiteral", "backTickCommandContent", 
			"backTickExpression", "stringLiteral", "lineStringLiteral", "multiLineStringLiteral", 
			"lineStringContent", "lineStringExpression", "multiLineStringContent", 
			"multiLineStringExpression", "semi", "anysemi"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, "'.'", "','", "'('", null, "'['", null, 
			"'{'", "'}'", "'^'", "'*'", "'%'", "'/'", "'\\'", "'~/'", "'+'", "'-'", 
			"'++'", "'--'", "'&&'", "'||'", "'!!'", "'!'", "':'", "';'", "'='", "'+='", 
			"'-='", "'*='", "'~/='", "'/='", "'%='", "'=>'", "'..'", "'..<'", "'?:'", 
			"'<'", "'>'", "'<='", "'>='", "'!='", "'!=='", "'as?'", "'=='", "'==='", 
			"'''", "'import'", "'fun'", "'var'", "'val'", "'if'", "'else'", "'try'", 
			"'catch'", "'finally'", "'throw'", "'for'", "'do'", "'while'", "'with'", 
			"'return'", "'continue'", "'break'", "'as'", "'is'", "'in'", "'counter'", 
			null, null, "'infix'", null, "'$('", null, "'\"\"\"'", null, null, null, 
			null, null, null, null, null, null, null, null, "'null'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "DelimitedComment", "LineComment", "WS", "NL", "DOT", "COMMA", 
			"LPAREN", "RPAREN", "LSQUARE", "RSQUARE", "LCURL", "RCURL", "POWER", 
			"MULT", "MOD", "DIVIDE", "BACKSLASH", "FLOOR_DIV", "ADD", "SUB", "INCR", 
			"DECR", "CONJ", "DISJ", "EXCL_EXCL", "EXCL", "COLON", "SEMICOLON", "ASSIGNMENT", 
			"ADD_ASSIGNMENT", "SUB_ASSIGNMENT", "MULT_ASSIGNMENT", "FLOOR_DIV_ASSIGNMENT", 
			"DIVIDE_ASSIGNMENT", "MOD_ASSIGNMENT", "PAIR_TO", "RANGE", "RANGE_EXCLUSIVE", 
			"ELVIS", "LANGLE", "RANGLE", "LE", "GE", "EXCL_EQ", "EXCL_EQEQ", "AS_SAFE", 
			"EQEQ", "EQEQEQ", "SINGLE_QUOTE", "IMPORT", "FUN", "VAR", "VAL", "IF", 
			"ELSE", "TRY", "CATCH", "FINALLY", "THROW", "FOR", "DO", "WHILE", "WITH", 
			"RETURN", "CONTINUE", "BREAK", "AS", "IS", "IN", "COUNTER", "NOT_IS", 
			"NOT_IN", "INFIX", "BACKTICK_OPEN", "COMMAND_OPEN", "QUOTE_OPEN", "TRIPLE_QUOTE_OPEN", 
			"RealLiteral", "FloatLiteral", "DoubleLiteral", "LongLiteral", "IntegerLiteral", 
			"PowerLiteral", "PowerSign", "PowerDigit", "HexLiteral", "BinLiteral", 
			"BooleanLiteral", "NullLiteral", "Identifier", "LabelReference", "LabelDefinition", 
			"FieldIdentifier", "CharacterLiteral", "UNICODE_CLASS_LL", "UNICODE_CLASS_LM", 
			"UNICODE_CLASS_LO", "UNICODE_CLASS_LT", "UNICODE_CLASS_LU", "UNICODE_CLASS_ND", 
			"UNICODE_CLASS_NL", "Inside_Comment", "Inside_WS", "Inside_NL", "BACKTICK_CLOSE", 
			"BackTickRef", "BackTickText", "BackTickEscapedChar", "BackTickExprStart", 
			"COMMAND_CLOSE", "CommandRef", "CommandText", "CommandEscapedChar", "CommandExprStart", 
			"QUOTE_CLOSE", "LineStrRef", "LineStrText", "LineStrEscapedChar", "LineStrExprStart", 
			"TRIPLE_QUOTE_CLOSE", "MultiLineStringQuote", "MultiLineStrRef", "MultiLineStrText", 
			"MultiLineStrEscapedChar", "MultiLineStrExprStart", "MultiLineNL", "StrExpr_IN", 
			"StrExpr_COUNTER", "StrExpr_Comment", "StrExpr_WS", "StrExpr_NL"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "FluffyParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public FluffyParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ScriptContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(FluffyParser.EOF, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public List<AnysemiContext> anysemi() {
			return getRuleContexts(AnysemiContext.class);
		}
		public AnysemiContext anysemi(int i) {
			return getRuleContext(AnysemiContext.class,i);
		}
		public List<TopLevelObjectContext> topLevelObject() {
			return getRuleContexts(TopLevelObjectContext.class);
		}
		public TopLevelObjectContext topLevelObject(int i) {
			return getRuleContext(TopLevelObjectContext.class,i);
		}
		public ScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_script; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitScript(this);
		}
	}

	public final ScriptContext script() throws RecognitionException {
		ScriptContext _localctx = new ScriptContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_script);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(167);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(164);
					match(NL);
					}
					} 
				}
				setState(169);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(173);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL || _la==SEMICOLON) {
				{
				{
				setState(170);
				anysemi();
				}
				}
				setState(175);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(190);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << IMPORT) | (1L << FUN) | (1L << VAR) | (1L << VAL) | (1L << IF) | (1L << TRY) | (1L << THROW) | (1L << FOR) | (1L << DO) | (1L << WHILE) | (1L << WITH))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (INFIX - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
				{
				setState(176);
				topLevelObject();
				setState(187);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL || _la==SEMICOLON) {
					{
					{
					setState(178); 
					_errHandler.sync(this);
					_alt = 1;
					do {
						switch (_alt) {
						case 1:
							{
							{
							setState(177);
							anysemi();
							}
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(180); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
					} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
					setState(183);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << IMPORT) | (1L << FUN) | (1L << VAR) | (1L << VAL) | (1L << IF) | (1L << TRY) | (1L << THROW) | (1L << FOR) | (1L << DO) | (1L << WHILE) | (1L << WITH))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (INFIX - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
						{
						setState(182);
						topLevelObject();
						}
					}

					}
					}
					setState(189);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(192);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TopLevelObjectContext extends ParserRuleContext {
		public FunctionDeclarationContext functionDeclaration() {
			return getRuleContext(FunctionDeclarationContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public List<AnysemiContext> anysemi() {
			return getRuleContexts(AnysemiContext.class);
		}
		public AnysemiContext anysemi(int i) {
			return getRuleContext(AnysemiContext.class,i);
		}
		public ImportStatementContext importStatement() {
			return getRuleContext(ImportStatementContext.class,0);
		}
		public TopLevelObjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_topLevelObject; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterTopLevelObject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitTopLevelObject(this);
		}
	}

	public final TopLevelObjectContext topLevelObject() throws RecognitionException {
		TopLevelObjectContext _localctx = new TopLevelObjectContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_topLevelObject);
		try {
			int _alt;
			setState(205);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FUN:
			case INFIX:
				enterOuterAlt(_localctx, 1);
				{
				setState(194);
				functionDeclaration();
				}
				break;
			case LPAREN:
			case LSQUARE:
			case ADD:
			case SUB:
			case EXCL:
			case VAR:
			case VAL:
			case IF:
			case TRY:
			case THROW:
			case FOR:
			case DO:
			case WHILE:
			case WITH:
			case RETURN:
			case CONTINUE:
			case BREAK:
			case AS:
			case IN:
			case BACKTICK_OPEN:
			case COMMAND_OPEN:
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
			case RealLiteral:
			case IntegerLiteral:
			case BooleanLiteral:
			case NullLiteral:
			case Identifier:
			case CharacterLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(195);
				statement();
				setState(199);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(196);
						anysemi();
						}
						} 
					}
					setState(201);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				}
				setState(202);
				statements();
				}
				break;
			case IMPORT:
				enterOuterAlt(_localctx, 3);
				{
				setState(204);
				importStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportStatementContext extends ParserRuleContext {
		public TerminalNode IMPORT() { return getToken(FluffyParser.IMPORT, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode DOT() { return getToken(FluffyParser.DOT, 0); }
		public TerminalNode MULT() { return getToken(FluffyParser.MULT, 0); }
		public ImportAliasContext importAlias() {
			return getRuleContext(ImportAliasContext.class,0);
		}
		public SemiContext semi() {
			return getRuleContext(SemiContext.class,0);
		}
		public ImportStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterImportStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitImportStatement(this);
		}
	}

	public final ImportStatementContext importStatement() throws RecognitionException {
		ImportStatementContext _localctx = new ImportStatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_importStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			match(IMPORT);
			setState(208);
			identifier();
			setState(212);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOT:
				{
				setState(209);
				match(DOT);
				setState(210);
				match(MULT);
				}
				break;
			case AS:
				{
				setState(211);
				importAlias();
				}
				break;
			case EOF:
			case NL:
			case SEMICOLON:
				break;
			default:
				break;
			}
			setState(215);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(214);
				semi();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportAliasContext extends ParserRuleContext {
		public TerminalNode AS() { return getToken(FluffyParser.AS, 0); }
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public ImportAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importAlias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterImportAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitImportAlias(this);
		}
	}

	public final ImportAliasContext importAlias() throws RecognitionException {
		ImportAliasContext _localctx = new ImportAliasContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_importAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(217);
			match(AS);
			setState(218);
			simpleIdentifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationContext extends ParserRuleContext {
		public FunctionDeclarationSetupContext functionDeclarationSetup() {
			return getRuleContext(FunctionDeclarationSetupContext.class,0);
		}
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public FunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFunctionDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFunctionDeclaration(this);
		}
	}

	public final FunctionDeclarationContext functionDeclaration() throws RecognitionException {
		FunctionDeclarationContext _localctx = new FunctionDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_functionDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			functionDeclarationSetup();
			setState(224);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(221);
				match(NL);
				}
				}
				setState(226);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(227);
			functionBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationSetupContext extends ParserRuleContext {
		public IdentifierContext id;
		public TerminalNode FUN() { return getToken(FluffyParser.FUN, 0); }
		public FunctionValueParametersContext functionValueParameters() {
			return getRuleContext(FunctionValueParametersContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode INFIX() { return getToken(FluffyParser.INFIX, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public TerminalNode COLON() { return getToken(FluffyParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FunctionDeclarationSetupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclarationSetup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFunctionDeclarationSetup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFunctionDeclarationSetup(this);
		}
	}

	public final FunctionDeclarationSetupContext functionDeclarationSetup() throws RecognitionException {
		FunctionDeclarationSetupContext _localctx = new FunctionDeclarationSetupContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_functionDeclarationSetup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INFIX) {
				{
				setState(229);
				match(INFIX);
				}
			}

			setState(232);
			match(FUN);
			setState(236);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(233);
				match(NL);
				}
				}
				setState(238);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(239);
			((FunctionDeclarationSetupContext)_localctx).id = identifier();
			setState(243);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(240);
				match(NL);
				}
				}
				setState(245);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(246);
			functionValueParameters();
			setState(261);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				{
				setState(250);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(247);
					match(NL);
					}
					}
					setState(252);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(253);
				match(COLON);
				setState(257);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(254);
					match(NL);
					}
					}
					setState(259);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(260);
				type();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionValueParametersContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(FluffyParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(FluffyParser.RPAREN, 0); }
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FluffyParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FluffyParser.COMMA, i);
		}
		public FunctionValueParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionValueParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFunctionValueParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFunctionValueParameters(this);
		}
	}

	public final FunctionValueParametersContext functionValueParameters() throws RecognitionException {
		FunctionValueParametersContext _localctx = new FunctionValueParametersContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_functionValueParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(263);
			match(LPAREN);
			setState(272);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (AS - 67)) | (1L << (IN - 67)) | (1L << (Identifier - 67)))) != 0)) {
				{
				setState(264);
				parameter();
				setState(269);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(265);
					match(COMMA);
					setState(266);
					parameter();
					}
					}
					setState(271);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(274);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionBodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode ASSIGNMENT() { return getToken(FluffyParser.ASSIGNMENT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public FunctionBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFunctionBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFunctionBody(this);
		}
	}

	public final FunctionBodyContext functionBody() throws RecognitionException {
		FunctionBodyContext _localctx = new FunctionBodyContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_functionBody);
		int _la;
		try {
			setState(285);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LCURL:
				enterOuterAlt(_localctx, 1);
				{
				setState(276);
				block();
				}
				break;
			case ASSIGNMENT:
				enterOuterAlt(_localctx, 2);
				{
				setState(277);
				match(ASSIGNMENT);
				setState(281);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(278);
					match(NL);
					}
					}
					setState(283);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(284);
				expression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LCURL() { return getToken(FluffyParser.LCURL, 0); }
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public TerminalNode RCURL() { return getToken(FluffyParser.RCURL, 0); }
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(287);
			match(LCURL);
			setState(288);
			statements();
			setState(289);
			match(RCURL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementsContext extends ParserRuleContext {
		public List<AnysemiContext> anysemi() {
			return getRuleContexts(AnysemiContext.class);
		}
		public AnysemiContext anysemi(int i) {
			return getRuleContext(AnysemiContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitStatements(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_statements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(294);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(291);
					anysemi();
					}
					} 
				}
				setState(296);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			setState(311);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << VAR) | (1L << VAL) | (1L << IF) | (1L << TRY) | (1L << THROW) | (1L << FOR) | (1L << DO) | (1L << WHILE) | (1L << WITH))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
				{
				setState(297);
				statement();
				setState(308);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(299); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(298);
								anysemi();
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(301); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
						} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
						setState(304);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << VAR) | (1L << VAL) | (1L << IF) | (1L << TRY) | (1L << THROW) | (1L << FOR) | (1L << DO) | (1L << WHILE) | (1L << WITH))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
							{
							setState(303);
							statement();
							}
						}

						}
						} 
					}
					setState(310);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public PropertyDeclarationContext propertyDeclaration() {
			return getRuleContext(PropertyDeclarationContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForStatementContext forStatement() {
			return getRuleContext(ForStatementContext.class,0);
		}
		public WhileExpressionContext whileExpression() {
			return getRuleContext(WhileExpressionContext.class,0);
		}
		public DoWhileExpressionContext doWhileExpression() {
			return getRuleContext(DoWhileExpressionContext.class,0);
		}
		public WithExpressionContext withExpression() {
			return getRuleContext(WithExpressionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_statement);
		try {
			setState(319);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VAR:
			case VAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(313);
				propertyDeclaration();
				}
				break;
			case LPAREN:
			case LSQUARE:
			case ADD:
			case SUB:
			case EXCL:
			case IF:
			case TRY:
			case THROW:
			case RETURN:
			case CONTINUE:
			case BREAK:
			case AS:
			case IN:
			case BACKTICK_OPEN:
			case COMMAND_OPEN:
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
			case RealLiteral:
			case IntegerLiteral:
			case BooleanLiteral:
			case NullLiteral:
			case Identifier:
			case CharacterLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(314);
				expression();
				}
				break;
			case FOR:
				enterOuterAlt(_localctx, 3);
				{
				setState(315);
				forStatement();
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 4);
				{
				setState(316);
				whileExpression();
				}
				break;
			case DO:
				enterOuterAlt(_localctx, 5);
				{
				setState(317);
				doWhileExpression();
				}
				break;
			case WITH:
				enterOuterAlt(_localctx, 6);
				{
				setState(318);
				withExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfExpressionContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(FluffyParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public List<ControlStructureBodyContext> controlStructureBody() {
			return getRuleContexts(ControlStructureBodyContext.class);
		}
		public ControlStructureBodyContext controlStructureBody(int i) {
			return getRuleContext(ControlStructureBodyContext.class,i);
		}
		public TerminalNode SEMICOLON() { return getToken(FluffyParser.SEMICOLON, 0); }
		public TerminalNode ELSE() { return getToken(FluffyParser.ELSE, 0); }
		public IfExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterIfExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitIfExpression(this);
		}
	}

	public final IfExpressionContext ifExpression() throws RecognitionException {
		IfExpressionContext _localctx = new IfExpressionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_ifExpression);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(321);
			match(IF);
			setState(325);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(322);
				match(NL);
				}
				}
				setState(327);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(328);
			expression();
			setState(332);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(329);
					match(NL);
					}
					} 
				}
				setState(334);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			setState(336);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				{
				setState(335);
				controlStructureBody();
				}
				break;
			}
			setState(339);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				{
				setState(338);
				match(SEMICOLON);
				}
				break;
			}
			setState(355);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(344);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(341);
					match(NL);
					}
					}
					setState(346);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(347);
				match(ELSE);
				setState(351);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(348);
					match(NL);
					}
					}
					setState(353);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(354);
				controlStructureBody();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatementContext extends ParserRuleContext {
		public ForInSetupContext forInSetup() {
			return getRuleContext(ForInSetupContext.class,0);
		}
		public ControlStructureBodyContext controlStructureBody() {
			return getRuleContext(ControlStructureBodyContext.class,0);
		}
		public ForStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitForStatement(this);
		}
	}

	public final ForStatementContext forStatement() throws RecognitionException {
		ForStatementContext _localctx = new ForStatementContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_forStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			forInSetup();
			setState(358);
			controlStructureBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForInSetupContext extends ParserRuleContext {
		public SimpleIdentifierContext loopVar;
		public SimpleIdentifierContext counterVar;
		public TerminalNode FOR() { return getToken(FluffyParser.FOR, 0); }
		public TerminalNode IN() { return getToken(FluffyParser.IN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<SimpleIdentifierContext> simpleIdentifier() {
			return getRuleContexts(SimpleIdentifierContext.class);
		}
		public SimpleIdentifierContext simpleIdentifier(int i) {
			return getRuleContext(SimpleIdentifierContext.class,i);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public TerminalNode COUNTER() { return getToken(FluffyParser.COUNTER, 0); }
		public TerminalNode LPAREN() { return getToken(FluffyParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(FluffyParser.RPAREN, 0); }
		public ForInSetupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forInSetup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterForInSetup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitForInSetup(this);
		}
	}

	public final ForInSetupContext forInSetup() throws RecognitionException {
		ForInSetupContext _localctx = new ForInSetupContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_forInSetup);
		int _la;
		try {
			setState(403);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(360);
				match(FOR);
				setState(361);
				((ForInSetupContext)_localctx).loopVar = simpleIdentifier();
				setState(365);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(362);
					match(NL);
					}
					}
					setState(367);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(368);
				match(IN);
				setState(372);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(369);
					match(NL);
					}
					}
					setState(374);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(375);
				expression();
				setState(378);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COUNTER) {
					{
					setState(376);
					match(COUNTER);
					setState(377);
					((ForInSetupContext)_localctx).counterVar = simpleIdentifier();
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(380);
				match(FOR);
				setState(381);
				match(LPAREN);
				setState(382);
				((ForInSetupContext)_localctx).loopVar = simpleIdentifier();
				setState(386);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(383);
					match(NL);
					}
					}
					setState(388);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(389);
				match(IN);
				setState(393);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(390);
					match(NL);
					}
					}
					setState(395);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(396);
				expression();
				setState(399);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COUNTER) {
					{
					setState(397);
					match(COUNTER);
					setState(398);
					((ForInSetupContext)_localctx).counterVar = simpleIdentifier();
					}
				}

				setState(401);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileExpressionContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(FluffyParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ControlStructureBodyContext controlStructureBody() {
			return getRuleContext(ControlStructureBodyContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public WhileExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterWhileExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitWhileExpression(this);
		}
	}

	public final WhileExpressionContext whileExpression() throws RecognitionException {
		WhileExpressionContext _localctx = new WhileExpressionContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_whileExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(405);
			match(WHILE);
			setState(409);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(406);
				match(NL);
				}
				}
				setState(411);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(412);
			expression();
			setState(416);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(413);
				match(NL);
				}
				}
				setState(418);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(419);
			controlStructureBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoWhileExpressionContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(FluffyParser.DO, 0); }
		public ControlStructureBodyContext controlStructureBody() {
			return getRuleContext(ControlStructureBodyContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(FluffyParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public DoWhileExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doWhileExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterDoWhileExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitDoWhileExpression(this);
		}
	}

	public final DoWhileExpressionContext doWhileExpression() throws RecognitionException {
		DoWhileExpressionContext _localctx = new DoWhileExpressionContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_doWhileExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(421);
			match(DO);
			setState(425);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(422);
				match(NL);
				}
				}
				setState(427);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(428);
			controlStructureBody();
			setState(429);
			match(WHILE);
			setState(433);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(430);
				match(NL);
				}
				}
				setState(435);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(436);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithExpressionContext extends ParserRuleContext {
		public WithSetupContext withSetup() {
			return getRuleContext(WithSetupContext.class,0);
		}
		public WithBodyContext withBody() {
			return getRuleContext(WithBodyContext.class,0);
		}
		public WithExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterWithExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitWithExpression(this);
		}
	}

	public final WithExpressionContext withExpression() throws RecognitionException {
		WithExpressionContext _localctx = new WithExpressionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_withExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(438);
			withSetup();
			setState(439);
			withBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithSetupContext extends ParserRuleContext {
		public TerminalNode WITH() { return getToken(FluffyParser.WITH, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public WithSetupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withSetup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterWithSetup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitWithSetup(this);
		}
	}

	public final WithSetupContext withSetup() throws RecognitionException {
		WithSetupContext _localctx = new WithSetupContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_withSetup);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(441);
			match(WITH);
			setState(442);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithBodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public WithBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterWithBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitWithBody(this);
		}
	}

	public final WithBodyContext withBody() throws RecognitionException {
		WithBodyContext _localctx = new WithBodyContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_withBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(444);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ControlStructureBodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ControlStructureBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_controlStructureBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterControlStructureBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitControlStructureBody(this);
		}
	}

	public final ControlStructureBodyContext controlStructureBody() throws RecognitionException {
		ControlStructureBodyContext _localctx = new ControlStructureBodyContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_controlStructureBody);
		try {
			setState(448);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LCURL:
				enterOuterAlt(_localctx, 1);
				{
				setState(446);
				block();
				}
				break;
			case LPAREN:
			case LSQUARE:
			case ADD:
			case SUB:
			case EXCL:
			case IF:
			case TRY:
			case THROW:
			case RETURN:
			case CONTINUE:
			case BREAK:
			case AS:
			case IN:
			case BACKTICK_OPEN:
			case COMMAND_OPEN:
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
			case RealLiteral:
			case IntegerLiteral:
			case BooleanLiteral:
			case NullLiteral:
			case Identifier:
			case CharacterLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(447);
				expression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyDeclarationContext extends ParserRuleContext {
		public Token def;
		public TerminalNode VAR() { return getToken(FluffyParser.VAR, 0); }
		public TerminalNode VAL() { return getToken(FluffyParser.VAL, 0); }
		public VariableDeclarationContext variableDeclaration() {
			return getRuleContext(VariableDeclarationContext.class,0);
		}
		public TerminalNode ASSIGNMENT() { return getToken(FluffyParser.ASSIGNMENT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public PropertyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterPropertyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitPropertyDeclaration(this);
		}
	}

	public final PropertyDeclarationContext propertyDeclaration() throws RecognitionException {
		PropertyDeclarationContext _localctx = new PropertyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_propertyDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(450);
			((PropertyDeclarationContext)_localctx).def = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==VAR || _la==VAL) ) {
				((PropertyDeclarationContext)_localctx).def = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			{
			setState(454);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(451);
				match(NL);
				}
				}
				setState(456);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(457);
			variableDeclaration();
			}
			setState(473);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				{
				setState(462);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(459);
					match(NL);
					}
					}
					setState(464);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(465);
				match(ASSIGNMENT);
				setState(469);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(466);
					match(NL);
					}
					}
					setState(471);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(472);
				expression();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public TerminalNode COLON() { return getToken(FluffyParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitVariableDeclaration(this);
		}
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_variableDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(475);
			simpleIdentifier();
			setState(478);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COLON) {
				{
				setState(476);
				match(COLON);
				setState(477);
				type();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Token bop;
		public List<DisjunctionContext> disjunction() {
			return getRuleContexts(DisjunctionContext.class);
		}
		public DisjunctionContext disjunction(int i) {
			return getRuleContext(DisjunctionContext.class,i);
		}
		public List<TerminalNode> ASSIGNMENT() { return getTokens(FluffyParser.ASSIGNMENT); }
		public TerminalNode ASSIGNMENT(int i) {
			return getToken(FluffyParser.ASSIGNMENT, i);
		}
		public List<TerminalNode> ADD_ASSIGNMENT() { return getTokens(FluffyParser.ADD_ASSIGNMENT); }
		public TerminalNode ADD_ASSIGNMENT(int i) {
			return getToken(FluffyParser.ADD_ASSIGNMENT, i);
		}
		public List<TerminalNode> SUB_ASSIGNMENT() { return getTokens(FluffyParser.SUB_ASSIGNMENT); }
		public TerminalNode SUB_ASSIGNMENT(int i) {
			return getToken(FluffyParser.SUB_ASSIGNMENT, i);
		}
		public List<TerminalNode> MULT_ASSIGNMENT() { return getTokens(FluffyParser.MULT_ASSIGNMENT); }
		public TerminalNode MULT_ASSIGNMENT(int i) {
			return getToken(FluffyParser.MULT_ASSIGNMENT, i);
		}
		public List<TerminalNode> DIVIDE_ASSIGNMENT() { return getTokens(FluffyParser.DIVIDE_ASSIGNMENT); }
		public TerminalNode DIVIDE_ASSIGNMENT(int i) {
			return getToken(FluffyParser.DIVIDE_ASSIGNMENT, i);
		}
		public List<TerminalNode> FLOOR_DIV_ASSIGNMENT() { return getTokens(FluffyParser.FLOOR_DIV_ASSIGNMENT); }
		public TerminalNode FLOOR_DIV_ASSIGNMENT(int i) {
			return getToken(FluffyParser.FLOOR_DIV_ASSIGNMENT, i);
		}
		public List<TerminalNode> MOD_ASSIGNMENT() { return getTokens(FluffyParser.MOD_ASSIGNMENT); }
		public TerminalNode MOD_ASSIGNMENT(int i) {
			return getToken(FluffyParser.MOD_ASSIGNMENT, i);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_expression);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(480);
			disjunction(0);
			setState(485);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(481);
					((ExpressionContext)_localctx).bop = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGNMENT) | (1L << ADD_ASSIGNMENT) | (1L << SUB_ASSIGNMENT) | (1L << MULT_ASSIGNMENT) | (1L << FLOOR_DIV_ASSIGNMENT) | (1L << DIVIDE_ASSIGNMENT) | (1L << MOD_ASSIGNMENT))) != 0)) ) {
						((ExpressionContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(482);
					disjunction(0);
					}
					} 
				}
				setState(487);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DisjunctionContext extends ParserRuleContext {
		public Token bop;
		public ConjunctionContext conjunction() {
			return getRuleContext(ConjunctionContext.class,0);
		}
		public List<DisjunctionContext> disjunction() {
			return getRuleContexts(DisjunctionContext.class);
		}
		public DisjunctionContext disjunction(int i) {
			return getRuleContext(DisjunctionContext.class,i);
		}
		public TerminalNode DISJ() { return getToken(FluffyParser.DISJ, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public DisjunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_disjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterDisjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitDisjunction(this);
		}
	}

	public final DisjunctionContext disjunction() throws RecognitionException {
		return disjunction(0);
	}

	private DisjunctionContext disjunction(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		DisjunctionContext _localctx = new DisjunctionContext(_ctx, _parentState);
		DisjunctionContext _prevctx = _localctx;
		int _startState = 46;
		enterRecursionRule(_localctx, 46, RULE_disjunction, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(489);
			conjunction(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(508);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new DisjunctionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_disjunction);
					setState(491);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(495);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(492);
						match(NL);
						}
						}
						setState(497);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(498);
					((DisjunctionContext)_localctx).bop = match(DISJ);
					setState(502);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(499);
						match(NL);
						}
						}
						setState(504);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(505);
					disjunction(3);
					}
					} 
				}
				setState(510);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConjunctionContext extends ParserRuleContext {
		public Token bop;
		public EqualityComparisonContext equalityComparison() {
			return getRuleContext(EqualityComparisonContext.class,0);
		}
		public List<ConjunctionContext> conjunction() {
			return getRuleContexts(ConjunctionContext.class);
		}
		public ConjunctionContext conjunction(int i) {
			return getRuleContext(ConjunctionContext.class,i);
		}
		public TerminalNode CONJ() { return getToken(FluffyParser.CONJ, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public ConjunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conjunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterConjunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitConjunction(this);
		}
	}

	public final ConjunctionContext conjunction() throws RecognitionException {
		return conjunction(0);
	}

	private ConjunctionContext conjunction(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ConjunctionContext _localctx = new ConjunctionContext(_ctx, _parentState);
		ConjunctionContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_conjunction, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(512);
			equalityComparison(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(531);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ConjunctionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_conjunction);
					setState(514);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(518);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(515);
						match(NL);
						}
						}
						setState(520);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(521);
					((ConjunctionContext)_localctx).bop = match(CONJ);
					setState(525);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(522);
						match(NL);
						}
						}
						setState(527);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(528);
					conjunction(3);
					}
					} 
				}
				setState(533);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualityComparisonContext extends ParserRuleContext {
		public Token bop;
		public ComparisonContext comparison() {
			return getRuleContext(ComparisonContext.class,0);
		}
		public List<EqualityComparisonContext> equalityComparison() {
			return getRuleContexts(EqualityComparisonContext.class);
		}
		public EqualityComparisonContext equalityComparison(int i) {
			return getRuleContext(EqualityComparisonContext.class,i);
		}
		public TerminalNode EXCL_EQ() { return getToken(FluffyParser.EXCL_EQ, 0); }
		public TerminalNode EXCL_EQEQ() { return getToken(FluffyParser.EXCL_EQEQ, 0); }
		public TerminalNode EQEQ() { return getToken(FluffyParser.EQEQ, 0); }
		public TerminalNode EQEQEQ() { return getToken(FluffyParser.EQEQEQ, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public EqualityComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalityComparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterEqualityComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitEqualityComparison(this);
		}
	}

	public final EqualityComparisonContext equalityComparison() throws RecognitionException {
		return equalityComparison(0);
	}

	private EqualityComparisonContext equalityComparison(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualityComparisonContext _localctx = new EqualityComparisonContext(_ctx, _parentState);
		EqualityComparisonContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_equalityComparison, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(535);
			comparison();
			}
			_ctx.stop = _input.LT(-1);
			setState(548);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new EqualityComparisonContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_equalityComparison);
					setState(537);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(538);
					((EqualityComparisonContext)_localctx).bop = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EXCL_EQ) | (1L << EXCL_EQEQ) | (1L << EQEQ) | (1L << EQEQEQ))) != 0)) ) {
						((EqualityComparisonContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(542);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(539);
						match(NL);
						}
						}
						setState(544);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(545);
					equalityComparison(3);
					}
					} 
				}
				setState(550);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public Token bop;
		public List<NamedInfixContext> namedInfix() {
			return getRuleContexts(NamedInfixContext.class);
		}
		public NamedInfixContext namedInfix(int i) {
			return getRuleContext(NamedInfixContext.class,i);
		}
		public TerminalNode LANGLE() { return getToken(FluffyParser.LANGLE, 0); }
		public TerminalNode RANGLE() { return getToken(FluffyParser.RANGLE, 0); }
		public TerminalNode LE() { return getToken(FluffyParser.LE, 0); }
		public TerminalNode GE() { return getToken(FluffyParser.GE, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitComparison(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_comparison);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(551);
			namedInfix();
			setState(560);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				{
				setState(552);
				((ComparisonContext)_localctx).bop = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LANGLE) | (1L << RANGLE) | (1L << LE) | (1L << GE))) != 0)) ) {
					((ComparisonContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(556);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(553);
					match(NL);
					}
					}
					setState(558);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(559);
				namedInfix();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NamedInfixContext extends ParserRuleContext {
		public Token bop;
		public ElvisExpressionContext elvisExpression() {
			return getRuleContext(ElvisExpressionContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IS() { return getToken(FluffyParser.IS, 0); }
		public TerminalNode NOT_IS() { return getToken(FluffyParser.NOT_IS, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public NamedInfixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_namedInfix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterNamedInfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitNamedInfix(this);
		}
	}

	public final NamedInfixContext namedInfix() throws RecognitionException {
		NamedInfixContext _localctx = new NamedInfixContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_namedInfix);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(562);
			elvisExpression(0);
			setState(571);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
			case 1:
				{
				{
				setState(563);
				((NamedInfixContext)_localctx).bop = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==IS || _la==NOT_IS) ) {
					((NamedInfixContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(567);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(564);
					match(NL);
					}
					}
					setState(569);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(570);
				type();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElvisExpressionContext extends ParserRuleContext {
		public Token bop;
		public InfixFunctionCallContext infixFunctionCall() {
			return getRuleContext(InfixFunctionCallContext.class,0);
		}
		public List<ElvisExpressionContext> elvisExpression() {
			return getRuleContexts(ElvisExpressionContext.class);
		}
		public ElvisExpressionContext elvisExpression(int i) {
			return getRuleContext(ElvisExpressionContext.class,i);
		}
		public TerminalNode ELVIS() { return getToken(FluffyParser.ELVIS, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public ElvisExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elvisExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterElvisExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitElvisExpression(this);
		}
	}

	public final ElvisExpressionContext elvisExpression() throws RecognitionException {
		return elvisExpression(0);
	}

	private ElvisExpressionContext elvisExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ElvisExpressionContext _localctx = new ElvisExpressionContext(_ctx, _parentState);
		ElvisExpressionContext _prevctx = _localctx;
		int _startState = 56;
		enterRecursionRule(_localctx, 56, RULE_elvisExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(574);
			infixFunctionCall(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(593);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ElvisExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_elvisExpression);
					setState(576);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(580);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(577);
						match(NL);
						}
						}
						setState(582);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(583);
					((ElvisExpressionContext)_localctx).bop = match(ELVIS);
					setState(587);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(584);
						match(NL);
						}
						}
						setState(589);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(590);
					elvisExpression(3);
					}
					} 
				}
				setState(595);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InfixFunctionCallContext extends ParserRuleContext {
		public RangeExpressionContext rangeExpression() {
			return getRuleContext(RangeExpressionContext.class,0);
		}
		public List<InfixFunctionCallContext> infixFunctionCall() {
			return getRuleContexts(InfixFunctionCallContext.class);
		}
		public InfixFunctionCallContext infixFunctionCall(int i) {
			return getRuleContext(InfixFunctionCallContext.class,i);
		}
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public InfixFunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_infixFunctionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterInfixFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitInfixFunctionCall(this);
		}
	}

	public final InfixFunctionCallContext infixFunctionCall() throws RecognitionException {
		return infixFunctionCall(0);
	}

	private InfixFunctionCallContext infixFunctionCall(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		InfixFunctionCallContext _localctx = new InfixFunctionCallContext(_ctx, _parentState);
		InfixFunctionCallContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, 58, RULE_infixFunctionCall, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(597);
			rangeExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(611);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new InfixFunctionCallContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_infixFunctionCall);
					setState(599);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(600);
					simpleIdentifier();
					setState(604);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(601);
						match(NL);
						}
						}
						setState(606);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(607);
					infixFunctionCall(3);
					}
					} 
				}
				setState(613);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RangeExpressionContext extends ParserRuleContext {
		public Token bop;
		public List<AdditiveExpressionContext> additiveExpression() {
			return getRuleContexts(AdditiveExpressionContext.class);
		}
		public AdditiveExpressionContext additiveExpression(int i) {
			return getRuleContext(AdditiveExpressionContext.class,i);
		}
		public TerminalNode RANGE() { return getToken(FluffyParser.RANGE, 0); }
		public TerminalNode RANGE_EXCLUSIVE() { return getToken(FluffyParser.RANGE_EXCLUSIVE, 0); }
		public TerminalNode PAIR_TO() { return getToken(FluffyParser.PAIR_TO, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public RangeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterRangeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitRangeExpression(this);
		}
	}

	public final RangeExpressionContext rangeExpression() throws RecognitionException {
		RangeExpressionContext _localctx = new RangeExpressionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_rangeExpression);
		int _la;
		try {
			setState(625);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,70,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(614);
				additiveExpression(0);
				setState(615);
				((RangeExpressionContext)_localctx).bop = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PAIR_TO) | (1L << RANGE) | (1L << RANGE_EXCLUSIVE))) != 0)) ) {
					((RangeExpressionContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(619);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(616);
					match(NL);
					}
					}
					setState(621);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(622);
				additiveExpression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(624);
				additiveExpression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public Token bop;
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public List<AdditiveExpressionContext> additiveExpression() {
			return getRuleContexts(AdditiveExpressionContext.class);
		}
		public AdditiveExpressionContext additiveExpression(int i) {
			return getRuleContext(AdditiveExpressionContext.class,i);
		}
		public TerminalNode ADD() { return getToken(FluffyParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(FluffyParser.SUB, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitAdditiveExpression(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		return additiveExpression(0);
	}

	private AdditiveExpressionContext additiveExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, _parentState);
		AdditiveExpressionContext _prevctx = _localctx;
		int _startState = 62;
		enterRecursionRule(_localctx, 62, RULE_additiveExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(628);
			multiplicativeExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(641);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,72,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AdditiveExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
					setState(630);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(631);
					((AdditiveExpressionContext)_localctx).bop = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==ADD || _la==SUB) ) {
						((AdditiveExpressionContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(635);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(632);
						match(NL);
						}
						}
						setState(637);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(638);
					additiveExpression(3);
					}
					} 
				}
				setState(643);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,72,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public Token bop;
		public PowerExpressionContext powerExpression() {
			return getRuleContext(PowerExpressionContext.class,0);
		}
		public List<MultiplicativeExpressionContext> multiplicativeExpression() {
			return getRuleContexts(MultiplicativeExpressionContext.class);
		}
		public MultiplicativeExpressionContext multiplicativeExpression(int i) {
			return getRuleContext(MultiplicativeExpressionContext.class,i);
		}
		public TerminalNode MULT() { return getToken(FluffyParser.MULT, 0); }
		public TerminalNode FLOOR_DIV() { return getToken(FluffyParser.FLOOR_DIV, 0); }
		public TerminalNode DIVIDE() { return getToken(FluffyParser.DIVIDE, 0); }
		public TerminalNode MOD() { return getToken(FluffyParser.MOD, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitMultiplicativeExpression(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		return multiplicativeExpression(0);
	}

	private MultiplicativeExpressionContext multiplicativeExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, _parentState);
		MultiplicativeExpressionContext _prevctx = _localctx;
		int _startState = 64;
		enterRecursionRule(_localctx, 64, RULE_multiplicativeExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(645);
			powerExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(658);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,74,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new MultiplicativeExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
					setState(647);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(648);
					((MultiplicativeExpressionContext)_localctx).bop = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << MOD) | (1L << DIVIDE) | (1L << FLOOR_DIV))) != 0)) ) {
						((MultiplicativeExpressionContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(652);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(649);
						match(NL);
						}
						}
						setState(654);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(655);
					multiplicativeExpression(3);
					}
					} 
				}
				setState(660);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,74,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PowerExpressionContext extends ParserRuleContext {
		public Token bop;
		public UberExpressionContext uberExpression() {
			return getRuleContext(UberExpressionContext.class,0);
		}
		public List<PowerExpressionContext> powerExpression() {
			return getRuleContexts(PowerExpressionContext.class);
		}
		public PowerExpressionContext powerExpression(int i) {
			return getRuleContext(PowerExpressionContext.class,i);
		}
		public TerminalNode POWER() { return getToken(FluffyParser.POWER, 0); }
		public TerminalNode PowerLiteral() { return getToken(FluffyParser.PowerLiteral, 0); }
		public PowerExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_powerExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterPowerExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitPowerExpression(this);
		}
	}

	public final PowerExpressionContext powerExpression() throws RecognitionException {
		return powerExpression(0);
	}

	private PowerExpressionContext powerExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PowerExpressionContext _localctx = new PowerExpressionContext(_ctx, _parentState);
		PowerExpressionContext _prevctx = _localctx;
		int _startState = 66;
		enterRecursionRule(_localctx, 66, RULE_powerExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(662);
			uberExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(671);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(669);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
					case 1:
						{
						_localctx = new PowerExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_powerExpression);
						setState(664);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(665);
						((PowerExpressionContext)_localctx).bop = match(POWER);
						setState(666);
						powerExpression(4);
						}
						break;
					case 2:
						{
						_localctx = new PowerExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_powerExpression);
						setState(667);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(668);
						match(PowerLiteral);
						}
						break;
					}
					} 
				}
				setState(673);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UberExpressionContext extends ParserRuleContext {
		public Token bop;
		public TypeRHSContext typeRHS() {
			return getRuleContext(TypeRHSContext.class,0);
		}
		public UberExpressionContext uberExpression() {
			return getRuleContext(UberExpressionContext.class,0);
		}
		public TerminalNode BACKSLASH() { return getToken(FluffyParser.BACKSLASH, 0); }
		public UberExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uberExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterUberExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitUberExpression(this);
		}
	}

	public final UberExpressionContext uberExpression() throws RecognitionException {
		UberExpressionContext _localctx = new UberExpressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_uberExpression);
		try {
			setState(679);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,77,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(674);
				typeRHS();
				setState(675);
				((UberExpressionContext)_localctx).bop = match(BACKSLASH);
				setState(676);
				uberExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(678);
				typeRHS();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeRHSContext extends ParserRuleContext {
		public Token bop;
		public PrefixUnaryExpressionContext prefixUnaryExpression() {
			return getRuleContext(PrefixUnaryExpressionContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode AS() { return getToken(FluffyParser.AS, 0); }
		public TerminalNode AS_SAFE() { return getToken(FluffyParser.AS_SAFE, 0); }
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public TypeRHSContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeRHS; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterTypeRHS(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitTypeRHS(this);
		}
	}

	public final TypeRHSContext typeRHS() throws RecognitionException {
		TypeRHSContext _localctx = new TypeRHSContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_typeRHS);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(681);
			prefixUnaryExpression();
			setState(690);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
			case 1:
				{
				setState(685);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(682);
					match(NL);
					}
					}
					setState(687);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(688);
				((TypeRHSContext)_localctx).bop = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==AS_SAFE || _la==AS) ) {
					((TypeRHSContext)_localctx).bop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(689);
				type();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixUnaryExpressionContext extends ParserRuleContext {
		public Token uop;
		public PrefixUnaryExpressionContext prefixUnaryExpression() {
			return getRuleContext(PrefixUnaryExpressionContext.class,0);
		}
		public TerminalNode ADD() { return getToken(FluffyParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(FluffyParser.SUB, 0); }
		public TerminalNode EXCL() { return getToken(FluffyParser.EXCL, 0); }
		public InvokableExpressionContext invokableExpression() {
			return getRuleContext(InvokableExpressionContext.class,0);
		}
		public PrefixUnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixUnaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterPrefixUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitPrefixUnaryExpression(this);
		}
	}

	public final PrefixUnaryExpressionContext prefixUnaryExpression() throws RecognitionException {
		PrefixUnaryExpressionContext _localctx = new PrefixUnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_prefixUnaryExpression);
		int _la;
		try {
			setState(695);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ADD:
			case SUB:
			case EXCL:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(692);
				((PrefixUnaryExpressionContext)_localctx).uop = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << EXCL))) != 0)) ) {
					((PrefixUnaryExpressionContext)_localctx).uop = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				setState(693);
				prefixUnaryExpression();
				}
				break;
			case LPAREN:
			case LSQUARE:
			case IF:
			case TRY:
			case THROW:
			case RETURN:
			case CONTINUE:
			case BREAK:
			case AS:
			case IN:
			case BACKTICK_OPEN:
			case COMMAND_OPEN:
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
			case RealLiteral:
			case IntegerLiteral:
			case BooleanLiteral:
			case NullLiteral:
			case Identifier:
			case CharacterLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(694);
				invokableExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvokableExpressionContext extends ParserRuleContext {
		public PostfixUnaryExpressionContext postfixUnaryExpression() {
			return getRuleContext(PostfixUnaryExpressionContext.class,0);
		}
		public CallSuffixContext callSuffix() {
			return getRuleContext(CallSuffixContext.class,0);
		}
		public InvokableExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invokableExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterInvokableExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitInvokableExpression(this);
		}
	}

	public final InvokableExpressionContext invokableExpression() throws RecognitionException {
		InvokableExpressionContext _localctx = new InvokableExpressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_invokableExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(697);
			postfixUnaryExpression(0);
			setState(699);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,81,_ctx) ) {
			case 1:
				{
				setState(698);
				callSuffix();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixUnaryExpressionContext extends ParserRuleContext {
		public Token uop;
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public VariableAccessContext variableAccess() {
			return getRuleContext(VariableAccessContext.class,0);
		}
		public AtomicExpressionContext atomicExpression() {
			return getRuleContext(AtomicExpressionContext.class,0);
		}
		public PostfixUnaryExpressionContext postfixUnaryExpression() {
			return getRuleContext(PostfixUnaryExpressionContext.class,0);
		}
		public ArrayAccessContext arrayAccess() {
			return getRuleContext(ArrayAccessContext.class,0);
		}
		public MethodCallContext methodCall() {
			return getRuleContext(MethodCallContext.class,0);
		}
		public FieldAccessContext fieldAccess() {
			return getRuleContext(FieldAccessContext.class,0);
		}
		public TerminalNode INCR() { return getToken(FluffyParser.INCR, 0); }
		public TerminalNode DECR() { return getToken(FluffyParser.DECR, 0); }
		public TerminalNode EXCL() { return getToken(FluffyParser.EXCL, 0); }
		public PostfixUnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfixUnaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterPostfixUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitPostfixUnaryExpression(this);
		}
	}

	public final PostfixUnaryExpressionContext postfixUnaryExpression() throws RecognitionException {
		return postfixUnaryExpression(0);
	}

	private PostfixUnaryExpressionContext postfixUnaryExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PostfixUnaryExpressionContext _localctx = new PostfixUnaryExpressionContext(_ctx, _parentState);
		PostfixUnaryExpressionContext _prevctx = _localctx;
		int _startState = 76;
		enterRecursionRule(_localctx, 76, RULE_postfixUnaryExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(705);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				{
				setState(702);
				functionCall();
				}
				break;
			case 2:
				{
				setState(703);
				variableAccess();
				}
				break;
			case 3:
				{
				setState(704);
				atomicExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(716);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new PostfixUnaryExpressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_postfixUnaryExpression);
					setState(707);
					if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
					setState(712);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,83,_ctx) ) {
					case 1:
						{
						setState(708);
						((PostfixUnaryExpressionContext)_localctx).uop = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INCR) | (1L << DECR) | (1L << EXCL))) != 0)) ) {
							((PostfixUnaryExpressionContext)_localctx).uop = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case 2:
						{
						setState(709);
						arrayAccess();
						}
						break;
					case 3:
						{
						setState(710);
						methodCall();
						}
						break;
					case 4:
						{
						setState(711);
						fieldAccess();
						}
						break;
					}
					}
					} 
				}
				setState(718);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,84,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class VariableAccessContext extends ParserRuleContext {
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public VariableAccessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableAccess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterVariableAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitVariableAccess(this);
		}
	}

	public final VariableAccessContext variableAccess() throws RecognitionException {
		VariableAccessContext _localctx = new VariableAccessContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_variableAccess);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(719);
			simpleIdentifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public CallSuffixContext callSuffix() {
			return getRuleContext(CallSuffixContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFunctionCall(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_functionCall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(721);
			simpleIdentifier();
			setState(722);
			callSuffix();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodCallContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(FluffyParser.DOT, 0); }
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public CallSuffixContext callSuffix() {
			return getRuleContext(CallSuffixContext.class,0);
		}
		public MethodCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterMethodCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitMethodCall(this);
		}
	}

	public final MethodCallContext methodCall() throws RecognitionException {
		MethodCallContext _localctx = new MethodCallContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_methodCall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(724);
			match(DOT);
			setState(725);
			simpleIdentifier();
			setState(726);
			callSuffix();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldAccessContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(FluffyParser.DOT, 0); }
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public FieldAccessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldAccess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFieldAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFieldAccess(this);
		}
	}

	public final FieldAccessContext fieldAccess() throws RecognitionException {
		FieldAccessContext _localctx = new FieldAccessContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_fieldAccess);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(728);
			match(DOT);
			setState(729);
			simpleIdentifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayAccessContext extends ParserRuleContext {
		public TerminalNode LSQUARE() { return getToken(FluffyParser.LSQUARE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RSQUARE() { return getToken(FluffyParser.RSQUARE, 0); }
		public ArrayAccessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayAccess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterArrayAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitArrayAccess(this);
		}
	}

	public final ArrayAccessContext arrayAccess() throws RecognitionException {
		ArrayAccessContext _localctx = new ArrayAccessContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_arrayAccess);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(731);
			match(LSQUARE);
			setState(732);
			expression();
			setState(733);
			match(RSQUARE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallSuffixContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(FluffyParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(FluffyParser.RPAREN, 0); }
		public List<ValueArgumentContext> valueArgument() {
			return getRuleContexts(ValueArgumentContext.class);
		}
		public ValueArgumentContext valueArgument(int i) {
			return getRuleContext(ValueArgumentContext.class,i);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FluffyParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FluffyParser.COMMA, i);
		}
		public CallSuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callSuffix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCallSuffix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCallSuffix(this);
		}
	}

	public final CallSuffixContext callSuffix() throws RecognitionException {
		CallSuffixContext _localctx = new CallSuffixContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_callSuffix);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(735);
			match(LPAREN);
			setState(758);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NL) | (1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << IF) | (1L << TRY) | (1L << THROW))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
				{
				setState(739);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(736);
					match(NL);
					}
					}
					setState(741);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(742);
				valueArgument();
				setState(755);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << COMMA) | (1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << IF) | (1L << TRY) | (1L << THROW))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
					{
					{
					setState(750);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==COMMA) {
						{
						setState(743);
						match(COMMA);
						setState(747);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==NL) {
							{
							{
							setState(744);
							match(NL);
							}
							}
							setState(749);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(752);
					valueArgument();
					}
					}
					setState(757);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(760);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueArgumentContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ValueArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueArgument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterValueArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitValueArgument(this);
		}
	}

	public final ValueArgumentContext valueArgument() throws RecognitionException {
		ValueArgumentContext _localctx = new ValueArgumentContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_valueArgument);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(762);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicExpressionContext extends ParserRuleContext {
		public ParenthesizedExpressionContext parenthesizedExpression() {
			return getRuleContext(ParenthesizedExpressionContext.class,0);
		}
		public ReturnStatementContext returnStatement() {
			return getRuleContext(ReturnStatementContext.class,0);
		}
		public ContinueStatementContext continueStatement() {
			return getRuleContext(ContinueStatementContext.class,0);
		}
		public BreakStatementContext breakStatement() {
			return getRuleContext(BreakStatementContext.class,0);
		}
		public ThrowStatementContext throwStatement() {
			return getRuleContext(ThrowStatementContext.class,0);
		}
		public LiteralConstantContext literalConstant() {
			return getRuleContext(LiteralConstantContext.class,0);
		}
		public ListLiteralContext listLiteral() {
			return getRuleContext(ListLiteralContext.class,0);
		}
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public IfExpressionContext ifExpression() {
			return getRuleContext(IfExpressionContext.class,0);
		}
		public TryExpressionContext tryExpression() {
			return getRuleContext(TryExpressionContext.class,0);
		}
		public AtomicExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterAtomicExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitAtomicExpression(this);
		}
	}

	public final AtomicExpressionContext atomicExpression() throws RecognitionException {
		AtomicExpressionContext _localctx = new AtomicExpressionContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_atomicExpression);
		try {
			setState(774);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(764);
				parenthesizedExpression();
				}
				break;
			case RETURN:
				enterOuterAlt(_localctx, 2);
				{
				setState(765);
				returnStatement();
				}
				break;
			case CONTINUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(766);
				continueStatement();
				}
				break;
			case BREAK:
				enterOuterAlt(_localctx, 4);
				{
				setState(767);
				breakStatement();
				}
				break;
			case THROW:
				enterOuterAlt(_localctx, 5);
				{
				setState(768);
				throwStatement();
				}
				break;
			case BACKTICK_OPEN:
			case COMMAND_OPEN:
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
			case RealLiteral:
			case IntegerLiteral:
			case BooleanLiteral:
			case NullLiteral:
			case CharacterLiteral:
				enterOuterAlt(_localctx, 6);
				{
				setState(769);
				literalConstant();
				}
				break;
			case LSQUARE:
				enterOuterAlt(_localctx, 7);
				{
				setState(770);
				listLiteral();
				}
				break;
			case AS:
			case IN:
			case Identifier:
				enterOuterAlt(_localctx, 8);
				{
				setState(771);
				simpleIdentifier();
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 9);
				{
				setState(772);
				ifExpression();
				}
				break;
			case TRY:
				enterOuterAlt(_localctx, 10);
				{
				setState(773);
				tryExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TryExpressionContext extends ParserRuleContext {
		public TerminalNode TRY() { return getToken(FluffyParser.TRY, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public CatchBlockContext catchBlock() {
			return getRuleContext(CatchBlockContext.class,0);
		}
		public FinallyBlockContext finallyBlock() {
			return getRuleContext(FinallyBlockContext.class,0);
		}
		public TryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterTryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitTryExpression(this);
		}
	}

	public final TryExpressionContext tryExpression() throws RecognitionException {
		TryExpressionContext _localctx = new TryExpressionContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_tryExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(776);
			match(TRY);
			setState(780);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(777);
				match(NL);
				}
				}
				setState(782);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(783);
			block();
			setState(791);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,93,_ctx) ) {
			case 1:
				{
				setState(787);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(784);
					match(NL);
					}
					}
					setState(789);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(790);
				catchBlock();
				}
				break;
			}
			setState(800);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
			case 1:
				{
				setState(796);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(793);
					match(NL);
					}
					}
					setState(798);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(799);
				finallyBlock();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchBlockContext extends ParserRuleContext {
		public TerminalNode CATCH() { return getToken(FluffyParser.CATCH, 0); }
		public TerminalNode LPAREN() { return getToken(FluffyParser.LPAREN, 0); }
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FluffyParser.RPAREN, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public CatchBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCatchBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCatchBlock(this);
		}
	}

	public final CatchBlockContext catchBlock() throws RecognitionException {
		CatchBlockContext _localctx = new CatchBlockContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_catchBlock);
		int _la;
		try {
			setState(836);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,100,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(802);
				match(CATCH);
				setState(806);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(803);
					match(NL);
					}
					}
					setState(808);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(809);
				match(LPAREN);
				setState(810);
				simpleIdentifier();
				setState(811);
				match(RPAREN);
				setState(815);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(812);
					match(NL);
					}
					}
					setState(817);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(818);
				block();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(820);
				match(CATCH);
				setState(824);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(821);
					match(NL);
					}
					}
					setState(826);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(827);
				simpleIdentifier();
				setState(831);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(828);
					match(NL);
					}
					}
					setState(833);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(834);
				block();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FinallyBlockContext extends ParserRuleContext {
		public TerminalNode FINALLY() { return getToken(FluffyParser.FINALLY, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public FinallyBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finallyBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterFinallyBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitFinallyBlock(this);
		}
	}

	public final FinallyBlockContext finallyBlock() throws RecognitionException {
		FinallyBlockContext _localctx = new FinallyBlockContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_finallyBlock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(838);
			match(FINALLY);
			setState(842);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(839);
				match(NL);
				}
				}
				setState(844);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(845);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThrowStatementContext extends ParserRuleContext {
		public TerminalNode THROW() { return getToken(FluffyParser.THROW, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public ThrowStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_throwStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterThrowStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitThrowStatement(this);
		}
	}

	public final ThrowStatementContext throwStatement() throws RecognitionException {
		ThrowStatementContext _localctx = new ThrowStatementContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_throwStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(847);
			match(THROW);
			setState(851);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(848);
				match(NL);
				}
				}
				setState(853);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(854);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatementContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(FluffyParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitReturnStatement(this);
		}
	}

	public final ReturnStatementContext returnStatement() throws RecognitionException {
		ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_returnStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(856);
			match(RETURN);
			setState(858);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
			case 1:
				{
				setState(857);
				expression();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueStatementContext extends ParserRuleContext {
		public TerminalNode CONTINUE() { return getToken(FluffyParser.CONTINUE, 0); }
		public ContinueStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterContinueStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitContinueStatement(this);
		}
	}

	public final ContinueStatementContext continueStatement() throws RecognitionException {
		ContinueStatementContext _localctx = new ContinueStatementContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_continueStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(860);
			match(CONTINUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakStatementContext extends ParserRuleContext {
		public TerminalNode BREAK() { return getToken(FluffyParser.BREAK, 0); }
		public BreakStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBreakStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBreakStatement(this);
		}
	}

	public final BreakStatementContext breakStatement() throws RecognitionException {
		BreakStatementContext _localctx = new BreakStatementContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_breakStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(862);
			match(BREAK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParenthesizedExpressionContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(FluffyParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FluffyParser.RPAREN, 0); }
		public ParenthesizedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parenthesizedExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterParenthesizedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitParenthesizedExpression(this);
		}
	}

	public final ParenthesizedExpressionContext parenthesizedExpression() throws RecognitionException {
		ParenthesizedExpressionContext _localctx = new ParenthesizedExpressionContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_parenthesizedExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(864);
			match(LPAREN);
			setState(865);
			expression();
			setState(866);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public SimpleUserTypeContext simpleUserType() {
			return getRuleContext(SimpleUserTypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(868);
			simpleUserType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleUserTypeContext extends ParserRuleContext {
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public SimpleUserTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleUserType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterSimpleUserType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitSimpleUserType(this);
		}
	}

	public final SimpleUserTypeContext simpleUserType() throws RecognitionException {
		SimpleUserTypeContext _localctx = new SimpleUserTypeContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_simpleUserType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(870);
			simpleIdentifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public SimpleIdentifierContext simpleIdentifier() {
			return getRuleContext(SimpleIdentifierContext.class,0);
		}
		public TerminalNode COLON() { return getToken(FluffyParser.COLON, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(872);
			simpleIdentifier();
			setState(873);
			match(COLON);
			setState(874);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public List<SimpleIdentifierContext> simpleIdentifier() {
			return getRuleContexts(SimpleIdentifierContext.class);
		}
		public SimpleIdentifierContext simpleIdentifier(int i) {
			return getRuleContext(SimpleIdentifierContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(FluffyParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(FluffyParser.DOT, i);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitIdentifier(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_identifier);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(876);
			simpleIdentifier();
			setState(887);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,105,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(880);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(877);
						match(NL);
						}
						}
						setState(882);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(883);
					match(DOT);
					setState(884);
					simpleIdentifier();
					}
					} 
				}
				setState(889);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,105,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleIdentifierContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(FluffyParser.Identifier, 0); }
		public TerminalNode IN() { return getToken(FluffyParser.IN, 0); }
		public TerminalNode AS() { return getToken(FluffyParser.AS, 0); }
		public SimpleIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterSimpleIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitSimpleIdentifier(this);
		}
	}

	public final SimpleIdentifierContext simpleIdentifier() throws RecognitionException {
		SimpleIdentifierContext _localctx = new SimpleIdentifierContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_simpleIdentifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(890);
			_la = _input.LA(1);
			if ( !(((((_la - 67)) & ~0x3f) == 0 && ((1L << (_la - 67)) & ((1L << (AS - 67)) | (1L << (IN - 67)) | (1L << (Identifier - 67)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListLiteralContext extends ParserRuleContext {
		public SimpleIdentifierContext first;
		public SimpleIdentifierContext second;
		public TerminalNode LSQUARE() { return getToken(FluffyParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(FluffyParser.RSQUARE, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FluffyParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FluffyParser.COMMA, i);
		}
		public TerminalNode LANGLE() { return getToken(FluffyParser.LANGLE, 0); }
		public TerminalNode RANGLE() { return getToken(FluffyParser.RANGLE, 0); }
		public List<SimpleIdentifierContext> simpleIdentifier() {
			return getRuleContexts(SimpleIdentifierContext.class);
		}
		public SimpleIdentifierContext simpleIdentifier(int i) {
			return getRuleContext(SimpleIdentifierContext.class,i);
		}
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public ListLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterListLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitListLiteral(this);
		}
	}

	public final ListLiteralContext listLiteral() throws RecognitionException {
		ListLiteralContext _localctx = new ListLiteralContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_listLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(892);
			match(LSQUARE);
			setState(894);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << LSQUARE) | (1L << ADD) | (1L << SUB) | (1L << EXCL) | (1L << IF) | (1L << TRY) | (1L << THROW))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (RETURN - 64)) | (1L << (CONTINUE - 64)) | (1L << (BREAK - 64)) | (1L << (AS - 64)) | (1L << (IN - 64)) | (1L << (BACKTICK_OPEN - 64)) | (1L << (COMMAND_OPEN - 64)) | (1L << (QUOTE_OPEN - 64)) | (1L << (TRIPLE_QUOTE_OPEN - 64)) | (1L << (RealLiteral - 64)) | (1L << (IntegerLiteral - 64)) | (1L << (BooleanLiteral - 64)) | (1L << (NullLiteral - 64)) | (1L << (Identifier - 64)) | (1L << (CharacterLiteral - 64)))) != 0)) {
				{
				setState(893);
				expression();
				}
			}

			setState(906);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(896);
				match(COMMA);
				setState(900);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(897);
					match(NL);
					}
					}
					setState(902);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(903);
				expression();
				}
				}
				setState(908);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(909);
			match(RSQUARE);
			setState(918);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,110,_ctx) ) {
			case 1:
				{
				setState(910);
				match(LANGLE);
				setState(911);
				((ListLiteralContext)_localctx).first = simpleIdentifier();
				setState(914);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(912);
					match(COMMA);
					setState(913);
					((ListLiteralContext)_localctx).second = simpleIdentifier();
					}
				}

				setState(916);
				match(RANGLE);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralConstantContext extends ParserRuleContext {
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public IntegerLiteralContext integerLiteral() {
			return getRuleContext(IntegerLiteralContext.class,0);
		}
		public StringLiteralContext stringLiteral() {
			return getRuleContext(StringLiteralContext.class,0);
		}
		public CharacterLiteralContext characterLiteral() {
			return getRuleContext(CharacterLiteralContext.class,0);
		}
		public CommandLiteralContext commandLiteral() {
			return getRuleContext(CommandLiteralContext.class,0);
		}
		public BackTickCommandLiteralContext backTickCommandLiteral() {
			return getRuleContext(BackTickCommandLiteralContext.class,0);
		}
		public RealLiteralContext realLiteral() {
			return getRuleContext(RealLiteralContext.class,0);
		}
		public NullLiteralContext nullLiteral() {
			return getRuleContext(NullLiteralContext.class,0);
		}
		public LiteralConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalConstant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterLiteralConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitLiteralConstant(this);
		}
	}

	public final LiteralConstantContext literalConstant() throws RecognitionException {
		LiteralConstantContext _localctx = new LiteralConstantContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_literalConstant);
		try {
			setState(928);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BooleanLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(920);
				booleanLiteral();
				}
				break;
			case IntegerLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(921);
				integerLiteral();
				}
				break;
			case QUOTE_OPEN:
			case TRIPLE_QUOTE_OPEN:
				enterOuterAlt(_localctx, 3);
				{
				setState(922);
				stringLiteral();
				}
				break;
			case CharacterLiteral:
				enterOuterAlt(_localctx, 4);
				{
				setState(923);
				characterLiteral();
				}
				break;
			case COMMAND_OPEN:
				enterOuterAlt(_localctx, 5);
				{
				setState(924);
				commandLiteral();
				}
				break;
			case BACKTICK_OPEN:
				enterOuterAlt(_localctx, 6);
				{
				setState(925);
				backTickCommandLiteral();
				}
				break;
			case RealLiteral:
				enterOuterAlt(_localctx, 7);
				{
				setState(926);
				realLiteral();
				}
				break;
			case NullLiteral:
				enterOuterAlt(_localctx, 8);
				{
				setState(927);
				nullLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullLiteralContext extends ParserRuleContext {
		public TerminalNode NullLiteral() { return getToken(FluffyParser.NullLiteral, 0); }
		public NullLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterNullLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitNullLiteral(this);
		}
	}

	public final NullLiteralContext nullLiteral() throws RecognitionException {
		NullLiteralContext _localctx = new NullLiteralContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_nullLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(930);
			match(NullLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharacterLiteralContext extends ParserRuleContext {
		public TerminalNode CharacterLiteral() { return getToken(FluffyParser.CharacterLiteral, 0); }
		public CharacterLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_characterLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCharacterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCharacterLiteral(this);
		}
	}

	public final CharacterLiteralContext characterLiteral() throws RecognitionException {
		CharacterLiteralContext _localctx = new CharacterLiteralContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_characterLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(932);
			match(CharacterLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralContext extends ParserRuleContext {
		public TerminalNode BooleanLiteral() { return getToken(FluffyParser.BooleanLiteral, 0); }
		public BooleanLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBooleanLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBooleanLiteral(this);
		}
	}

	public final BooleanLiteralContext booleanLiteral() throws RecognitionException {
		BooleanLiteralContext _localctx = new BooleanLiteralContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_booleanLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(934);
			match(BooleanLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealLiteralContext extends ParserRuleContext {
		public TerminalNode RealLiteral() { return getToken(FluffyParser.RealLiteral, 0); }
		public RealLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_realLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterRealLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitRealLiteral(this);
		}
	}

	public final RealLiteralContext realLiteral() throws RecognitionException {
		RealLiteralContext _localctx = new RealLiteralContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_realLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(936);
			match(RealLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerLiteralContext extends ParserRuleContext {
		public TerminalNode IntegerLiteral() { return getToken(FluffyParser.IntegerLiteral, 0); }
		public IntegerLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterIntegerLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitIntegerLiteral(this);
		}
	}

	public final IntegerLiteralContext integerLiteral() throws RecognitionException {
		IntegerLiteralContext _localctx = new IntegerLiteralContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_integerLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(938);
			match(IntegerLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandLiteralContext extends ParserRuleContext {
		public TerminalNode COMMAND_OPEN() { return getToken(FluffyParser.COMMAND_OPEN, 0); }
		public TerminalNode COMMAND_CLOSE() { return getToken(FluffyParser.COMMAND_CLOSE, 0); }
		public List<CommandContentContext> commandContent() {
			return getRuleContexts(CommandContentContext.class);
		}
		public CommandContentContext commandContent(int i) {
			return getRuleContext(CommandContentContext.class,i);
		}
		public List<CommandExpressionContext> commandExpression() {
			return getRuleContexts(CommandExpressionContext.class);
		}
		public CommandExpressionContext commandExpression(int i) {
			return getRuleContext(CommandExpressionContext.class,i);
		}
		public CommandLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCommandLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCommandLiteral(this);
		}
	}

	public final CommandLiteralContext commandLiteral() throws RecognitionException {
		CommandLiteralContext _localctx = new CommandLiteralContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_commandLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(940);
			match(COMMAND_OPEN);
			setState(945);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 111)) & ~0x3f) == 0 && ((1L << (_la - 111)) & ((1L << (CommandRef - 111)) | (1L << (CommandText - 111)) | (1L << (CommandEscapedChar - 111)) | (1L << (CommandExprStart - 111)))) != 0)) {
				{
				setState(943);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CommandRef:
				case CommandText:
				case CommandEscapedChar:
					{
					setState(941);
					commandContent();
					}
					break;
				case CommandExprStart:
					{
					setState(942);
					commandExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(947);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(948);
			match(COMMAND_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContentContext extends ParserRuleContext {
		public List<TerminalNode> CommandText() { return getTokens(FluffyParser.CommandText); }
		public TerminalNode CommandText(int i) {
			return getToken(FluffyParser.CommandText, i);
		}
		public List<TerminalNode> CommandEscapedChar() { return getTokens(FluffyParser.CommandEscapedChar); }
		public TerminalNode CommandEscapedChar(int i) {
			return getToken(FluffyParser.CommandEscapedChar, i);
		}
		public TerminalNode CommandRef() { return getToken(FluffyParser.CommandRef, 0); }
		public CommandContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCommandContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCommandContent(this);
		}
	}

	public final CommandContentContext commandContent() throws RecognitionException {
		CommandContentContext _localctx = new CommandContentContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_commandContent);
		int _la;
		try {
			int _alt;
			setState(956);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CommandText:
			case CommandEscapedChar:
				enterOuterAlt(_localctx, 1);
				{
				setState(951); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(950);
						_la = _input.LA(1);
						if ( !(_la==CommandText || _la==CommandEscapedChar) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(953); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,114,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case CommandRef:
				enterOuterAlt(_localctx, 2);
				{
				setState(955);
				match(CommandRef);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandExpressionContext extends ParserRuleContext {
		public TerminalNode CommandExprStart() { return getToken(FluffyParser.CommandExprStart, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RCURL() { return getToken(FluffyParser.RCURL, 0); }
		public CommandExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commandExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterCommandExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitCommandExpression(this);
		}
	}

	public final CommandExpressionContext commandExpression() throws RecognitionException {
		CommandExpressionContext _localctx = new CommandExpressionContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_commandExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(958);
			match(CommandExprStart);
			setState(959);
			expression();
			setState(960);
			match(RCURL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BackTickCommandLiteralContext extends ParserRuleContext {
		public TerminalNode BACKTICK_OPEN() { return getToken(FluffyParser.BACKTICK_OPEN, 0); }
		public TerminalNode BACKTICK_CLOSE() { return getToken(FluffyParser.BACKTICK_CLOSE, 0); }
		public List<BackTickCommandContentContext> backTickCommandContent() {
			return getRuleContexts(BackTickCommandContentContext.class);
		}
		public BackTickCommandContentContext backTickCommandContent(int i) {
			return getRuleContext(BackTickCommandContentContext.class,i);
		}
		public List<BackTickExpressionContext> backTickExpression() {
			return getRuleContexts(BackTickExpressionContext.class);
		}
		public BackTickExpressionContext backTickExpression(int i) {
			return getRuleContext(BackTickExpressionContext.class,i);
		}
		public BackTickCommandLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_backTickCommandLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBackTickCommandLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBackTickCommandLiteral(this);
		}
	}

	public final BackTickCommandLiteralContext backTickCommandLiteral() throws RecognitionException {
		BackTickCommandLiteralContext _localctx = new BackTickCommandLiteralContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_backTickCommandLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(962);
			match(BACKTICK_OPEN);
			setState(967);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 106)) & ~0x3f) == 0 && ((1L << (_la - 106)) & ((1L << (BackTickRef - 106)) | (1L << (BackTickText - 106)) | (1L << (BackTickEscapedChar - 106)) | (1L << (BackTickExprStart - 106)))) != 0)) {
				{
				setState(965);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BackTickRef:
				case BackTickText:
				case BackTickEscapedChar:
					{
					setState(963);
					backTickCommandContent();
					}
					break;
				case BackTickExprStart:
					{
					setState(964);
					backTickExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(969);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(970);
			match(BACKTICK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BackTickCommandContentContext extends ParserRuleContext {
		public List<TerminalNode> BackTickText() { return getTokens(FluffyParser.BackTickText); }
		public TerminalNode BackTickText(int i) {
			return getToken(FluffyParser.BackTickText, i);
		}
		public List<TerminalNode> BackTickEscapedChar() { return getTokens(FluffyParser.BackTickEscapedChar); }
		public TerminalNode BackTickEscapedChar(int i) {
			return getToken(FluffyParser.BackTickEscapedChar, i);
		}
		public TerminalNode BackTickRef() { return getToken(FluffyParser.BackTickRef, 0); }
		public BackTickCommandContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_backTickCommandContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBackTickCommandContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBackTickCommandContent(this);
		}
	}

	public final BackTickCommandContentContext backTickCommandContent() throws RecognitionException {
		BackTickCommandContentContext _localctx = new BackTickCommandContentContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_backTickCommandContent);
		int _la;
		try {
			int _alt;
			setState(978);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BackTickText:
			case BackTickEscapedChar:
				enterOuterAlt(_localctx, 1);
				{
				setState(973); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(972);
						_la = _input.LA(1);
						if ( !(_la==BackTickText || _la==BackTickEscapedChar) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(975); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,118,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case BackTickRef:
				enterOuterAlt(_localctx, 2);
				{
				setState(977);
				match(BackTickRef);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BackTickExpressionContext extends ParserRuleContext {
		public TerminalNode BackTickExprStart() { return getToken(FluffyParser.BackTickExprStart, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RCURL() { return getToken(FluffyParser.RCURL, 0); }
		public BackTickExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_backTickExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterBackTickExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitBackTickExpression(this);
		}
	}

	public final BackTickExpressionContext backTickExpression() throws RecognitionException {
		BackTickExpressionContext _localctx = new BackTickExpressionContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_backTickExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(980);
			match(BackTickExprStart);
			setState(981);
			expression();
			setState(982);
			match(RCURL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralContext extends ParserRuleContext {
		public LineStringLiteralContext lineStringLiteral() {
			return getRuleContext(LineStringLiteralContext.class,0);
		}
		public MultiLineStringLiteralContext multiLineStringLiteral() {
			return getRuleContext(MultiLineStringLiteralContext.class,0);
		}
		public StringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterStringLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitStringLiteral(this);
		}
	}

	public final StringLiteralContext stringLiteral() throws RecognitionException {
		StringLiteralContext _localctx = new StringLiteralContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_stringLiteral);
		try {
			setState(986);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case QUOTE_OPEN:
				enterOuterAlt(_localctx, 1);
				{
				setState(984);
				lineStringLiteral();
				}
				break;
			case TRIPLE_QUOTE_OPEN:
				enterOuterAlt(_localctx, 2);
				{
				setState(985);
				multiLineStringLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineStringLiteralContext extends ParserRuleContext {
		public TerminalNode QUOTE_OPEN() { return getToken(FluffyParser.QUOTE_OPEN, 0); }
		public TerminalNode QUOTE_CLOSE() { return getToken(FluffyParser.QUOTE_CLOSE, 0); }
		public List<LineStringContentContext> lineStringContent() {
			return getRuleContexts(LineStringContentContext.class);
		}
		public LineStringContentContext lineStringContent(int i) {
			return getRuleContext(LineStringContentContext.class,i);
		}
		public List<LineStringExpressionContext> lineStringExpression() {
			return getRuleContexts(LineStringExpressionContext.class);
		}
		public LineStringExpressionContext lineStringExpression(int i) {
			return getRuleContext(LineStringExpressionContext.class,i);
		}
		public LineStringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lineStringLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterLineStringLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitLineStringLiteral(this);
		}
	}

	public final LineStringLiteralContext lineStringLiteral() throws RecognitionException {
		LineStringLiteralContext _localctx = new LineStringLiteralContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_lineStringLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(988);
			match(QUOTE_OPEN);
			setState(993);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 116)) & ~0x3f) == 0 && ((1L << (_la - 116)) & ((1L << (LineStrRef - 116)) | (1L << (LineStrText - 116)) | (1L << (LineStrEscapedChar - 116)) | (1L << (LineStrExprStart - 116)))) != 0)) {
				{
				setState(991);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LineStrRef:
				case LineStrText:
				case LineStrEscapedChar:
					{
					setState(989);
					lineStringContent();
					}
					break;
				case LineStrExprStart:
					{
					setState(990);
					lineStringExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(995);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(996);
			match(QUOTE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiLineStringLiteralContext extends ParserRuleContext {
		public TerminalNode TRIPLE_QUOTE_OPEN() { return getToken(FluffyParser.TRIPLE_QUOTE_OPEN, 0); }
		public TerminalNode TRIPLE_QUOTE_CLOSE() { return getToken(FluffyParser.TRIPLE_QUOTE_CLOSE, 0); }
		public List<MultiLineStringContentContext> multiLineStringContent() {
			return getRuleContexts(MultiLineStringContentContext.class);
		}
		public MultiLineStringContentContext multiLineStringContent(int i) {
			return getRuleContext(MultiLineStringContentContext.class,i);
		}
		public List<MultiLineStringExpressionContext> multiLineStringExpression() {
			return getRuleContexts(MultiLineStringExpressionContext.class);
		}
		public MultiLineStringExpressionContext multiLineStringExpression(int i) {
			return getRuleContext(MultiLineStringExpressionContext.class,i);
		}
		public List<TerminalNode> MultiLineStringQuote() { return getTokens(FluffyParser.MultiLineStringQuote); }
		public TerminalNode MultiLineStringQuote(int i) {
			return getToken(FluffyParser.MultiLineStringQuote, i);
		}
		public MultiLineStringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiLineStringLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterMultiLineStringLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitMultiLineStringLiteral(this);
		}
	}

	public final MultiLineStringLiteralContext multiLineStringLiteral() throws RecognitionException {
		MultiLineStringLiteralContext _localctx = new MultiLineStringLiteralContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_multiLineStringLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(998);
			match(TRIPLE_QUOTE_OPEN);
			setState(1004);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 121)) & ~0x3f) == 0 && ((1L << (_la - 121)) & ((1L << (MultiLineStringQuote - 121)) | (1L << (MultiLineStrRef - 121)) | (1L << (MultiLineStrText - 121)) | (1L << (MultiLineStrEscapedChar - 121)) | (1L << (MultiLineStrExprStart - 121)))) != 0)) {
				{
				setState(1002);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case MultiLineStrRef:
				case MultiLineStrText:
				case MultiLineStrEscapedChar:
					{
					setState(999);
					multiLineStringContent();
					}
					break;
				case MultiLineStrExprStart:
					{
					setState(1000);
					multiLineStringExpression();
					}
					break;
				case MultiLineStringQuote:
					{
					setState(1001);
					match(MultiLineStringQuote);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(1006);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1007);
			match(TRIPLE_QUOTE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineStringContentContext extends ParserRuleContext {
		public Token ref;
		public List<TerminalNode> LineStrText() { return getTokens(FluffyParser.LineStrText); }
		public TerminalNode LineStrText(int i) {
			return getToken(FluffyParser.LineStrText, i);
		}
		public List<TerminalNode> LineStrEscapedChar() { return getTokens(FluffyParser.LineStrEscapedChar); }
		public TerminalNode LineStrEscapedChar(int i) {
			return getToken(FluffyParser.LineStrEscapedChar, i);
		}
		public TerminalNode LineStrRef() { return getToken(FluffyParser.LineStrRef, 0); }
		public LineStringContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lineStringContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterLineStringContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitLineStringContent(this);
		}
	}

	public final LineStringContentContext lineStringContent() throws RecognitionException {
		LineStringContentContext _localctx = new LineStringContentContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_lineStringContent);
		int _la;
		try {
			int _alt;
			setState(1015);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LineStrText:
			case LineStrEscapedChar:
				enterOuterAlt(_localctx, 1);
				{
				setState(1010); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1009);
						_la = _input.LA(1);
						if ( !(_la==LineStrText || _la==LineStrEscapedChar) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1012); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,125,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case LineStrRef:
				enterOuterAlt(_localctx, 2);
				{
				setState(1014);
				((LineStringContentContext)_localctx).ref = match(LineStrRef);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineStringExpressionContext extends ParserRuleContext {
		public TerminalNode LineStrExprStart() { return getToken(FluffyParser.LineStrExprStart, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RCURL() { return getToken(FluffyParser.RCURL, 0); }
		public LineStringExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lineStringExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterLineStringExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitLineStringExpression(this);
		}
	}

	public final LineStringExpressionContext lineStringExpression() throws RecognitionException {
		LineStringExpressionContext _localctx = new LineStringExpressionContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_lineStringExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1017);
			match(LineStrExprStart);
			setState(1018);
			expression();
			setState(1019);
			match(RCURL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiLineStringContentContext extends ParserRuleContext {
		public Token ref;
		public List<TerminalNode> MultiLineStrText() { return getTokens(FluffyParser.MultiLineStrText); }
		public TerminalNode MultiLineStrText(int i) {
			return getToken(FluffyParser.MultiLineStrText, i);
		}
		public List<TerminalNode> MultiLineStrEscapedChar() { return getTokens(FluffyParser.MultiLineStrEscapedChar); }
		public TerminalNode MultiLineStrEscapedChar(int i) {
			return getToken(FluffyParser.MultiLineStrEscapedChar, i);
		}
		public TerminalNode MultiLineStrRef() { return getToken(FluffyParser.MultiLineStrRef, 0); }
		public MultiLineStringContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiLineStringContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterMultiLineStringContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitMultiLineStringContent(this);
		}
	}

	public final MultiLineStringContentContext multiLineStringContent() throws RecognitionException {
		MultiLineStringContentContext _localctx = new MultiLineStringContentContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_multiLineStringContent);
		int _la;
		try {
			int _alt;
			setState(1027);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MultiLineStrText:
			case MultiLineStrEscapedChar:
				enterOuterAlt(_localctx, 1);
				{
				setState(1022); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1021);
						_la = _input.LA(1);
						if ( !(_la==MultiLineStrText || _la==MultiLineStrEscapedChar) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1024); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,127,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case MultiLineStrRef:
				enterOuterAlt(_localctx, 2);
				{
				setState(1026);
				((MultiLineStringContentContext)_localctx).ref = match(MultiLineStrRef);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiLineStringExpressionContext extends ParserRuleContext {
		public TerminalNode MultiLineStrExprStart() { return getToken(FluffyParser.MultiLineStrExprStart, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RCURL() { return getToken(FluffyParser.RCURL, 0); }
		public MultiLineStringExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiLineStringExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterMultiLineStringExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitMultiLineStringExpression(this);
		}
	}

	public final MultiLineStringExpressionContext multiLineStringExpression() throws RecognitionException {
		MultiLineStringExpressionContext _localctx = new MultiLineStringExpressionContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_multiLineStringExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1029);
			match(MultiLineStrExprStart);
			setState(1030);
			expression();
			setState(1031);
			match(RCURL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SemiContext extends ParserRuleContext {
		public List<TerminalNode> NL() { return getTokens(FluffyParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(FluffyParser.NL, i);
		}
		public TerminalNode SEMICOLON() { return getToken(FluffyParser.SEMICOLON, 0); }
		public SemiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_semi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterSemi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitSemi(this);
		}
	}

	public final SemiContext semi() throws RecognitionException {
		SemiContext _localctx = new SemiContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_semi);
		int _la;
		try {
			int _alt;
			setState(1051);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1034); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1033);
						match(NL);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1036); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,129,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1041);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(1038);
					match(NL);
					}
					}
					setState(1043);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1044);
				match(SEMICOLON);
				setState(1048);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,131,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1045);
						match(NL);
						}
						} 
					}
					setState(1050);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,131,_ctx);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnysemiContext extends ParserRuleContext {
		public TerminalNode NL() { return getToken(FluffyParser.NL, 0); }
		public TerminalNode SEMICOLON() { return getToken(FluffyParser.SEMICOLON, 0); }
		public AnysemiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_anysemi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).enterAnysemi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FluffyParserListener ) ((FluffyParserListener)listener).exitAnysemi(this);
		}
	}

	public final AnysemiContext anysemi() throws RecognitionException {
		AnysemiContext _localctx = new AnysemiContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_anysemi);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1053);
			_la = _input.LA(1);
			if ( !(_la==NL || _la==SEMICOLON) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 23:
			return disjunction_sempred((DisjunctionContext)_localctx, predIndex);
		case 24:
			return conjunction_sempred((ConjunctionContext)_localctx, predIndex);
		case 25:
			return equalityComparison_sempred((EqualityComparisonContext)_localctx, predIndex);
		case 28:
			return elvisExpression_sempred((ElvisExpressionContext)_localctx, predIndex);
		case 29:
			return infixFunctionCall_sempred((InfixFunctionCallContext)_localctx, predIndex);
		case 31:
			return additiveExpression_sempred((AdditiveExpressionContext)_localctx, predIndex);
		case 32:
			return multiplicativeExpression_sempred((MultiplicativeExpressionContext)_localctx, predIndex);
		case 33:
			return powerExpression_sempred((PowerExpressionContext)_localctx, predIndex);
		case 38:
			return postfixUnaryExpression_sempred((PostfixUnaryExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean disjunction_sempred(DisjunctionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean conjunction_sempred(ConjunctionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean equalityComparison_sempred(EqualityComparisonContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean elvisExpression_sempred(ElvisExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean infixFunctionCall_sempred(InfixFunctionCallContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean additiveExpression_sempred(AdditiveExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean multiplicativeExpression_sempred(MultiplicativeExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean powerExpression_sempred(PowerExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 3);
		case 8:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean postfixUnaryExpression_sempred(PostfixUnaryExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u0085\u0422\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\3\2\7"+
		"\2\u00a8\n\2\f\2\16\2\u00ab\13\2\3\2\7\2\u00ae\n\2\f\2\16\2\u00b1\13\2"+
		"\3\2\3\2\6\2\u00b5\n\2\r\2\16\2\u00b6\3\2\5\2\u00ba\n\2\7\2\u00bc\n\2"+
		"\f\2\16\2\u00bf\13\2\5\2\u00c1\n\2\3\2\3\2\3\3\3\3\3\3\7\3\u00c8\n\3\f"+
		"\3\16\3\u00cb\13\3\3\3\3\3\3\3\5\3\u00d0\n\3\3\4\3\4\3\4\3\4\3\4\5\4\u00d7"+
		"\n\4\3\4\5\4\u00da\n\4\3\5\3\5\3\5\3\6\3\6\7\6\u00e1\n\6\f\6\16\6\u00e4"+
		"\13\6\3\6\3\6\3\7\5\7\u00e9\n\7\3\7\3\7\7\7\u00ed\n\7\f\7\16\7\u00f0\13"+
		"\7\3\7\3\7\7\7\u00f4\n\7\f\7\16\7\u00f7\13\7\3\7\3\7\7\7\u00fb\n\7\f\7"+
		"\16\7\u00fe\13\7\3\7\3\7\7\7\u0102\n\7\f\7\16\7\u0105\13\7\3\7\5\7\u0108"+
		"\n\7\3\b\3\b\3\b\3\b\7\b\u010e\n\b\f\b\16\b\u0111\13\b\5\b\u0113\n\b\3"+
		"\b\3\b\3\t\3\t\3\t\7\t\u011a\n\t\f\t\16\t\u011d\13\t\3\t\5\t\u0120\n\t"+
		"\3\n\3\n\3\n\3\n\3\13\7\13\u0127\n\13\f\13\16\13\u012a\13\13\3\13\3\13"+
		"\6\13\u012e\n\13\r\13\16\13\u012f\3\13\5\13\u0133\n\13\7\13\u0135\n\13"+
		"\f\13\16\13\u0138\13\13\5\13\u013a\n\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0142"+
		"\n\f\3\r\3\r\7\r\u0146\n\r\f\r\16\r\u0149\13\r\3\r\3\r\7\r\u014d\n\r\f"+
		"\r\16\r\u0150\13\r\3\r\5\r\u0153\n\r\3\r\5\r\u0156\n\r\3\r\7\r\u0159\n"+
		"\r\f\r\16\r\u015c\13\r\3\r\3\r\7\r\u0160\n\r\f\r\16\r\u0163\13\r\3\r\5"+
		"\r\u0166\n\r\3\16\3\16\3\16\3\17\3\17\3\17\7\17\u016e\n\17\f\17\16\17"+
		"\u0171\13\17\3\17\3\17\7\17\u0175\n\17\f\17\16\17\u0178\13\17\3\17\3\17"+
		"\3\17\5\17\u017d\n\17\3\17\3\17\3\17\3\17\7\17\u0183\n\17\f\17\16\17\u0186"+
		"\13\17\3\17\3\17\7\17\u018a\n\17\f\17\16\17\u018d\13\17\3\17\3\17\3\17"+
		"\5\17\u0192\n\17\3\17\3\17\5\17\u0196\n\17\3\20\3\20\7\20\u019a\n\20\f"+
		"\20\16\20\u019d\13\20\3\20\3\20\7\20\u01a1\n\20\f\20\16\20\u01a4\13\20"+
		"\3\20\3\20\3\21\3\21\7\21\u01aa\n\21\f\21\16\21\u01ad\13\21\3\21\3\21"+
		"\3\21\7\21\u01b2\n\21\f\21\16\21\u01b5\13\21\3\21\3\21\3\22\3\22\3\22"+
		"\3\23\3\23\3\23\3\24\3\24\3\25\3\25\5\25\u01c3\n\25\3\26\3\26\7\26\u01c7"+
		"\n\26\f\26\16\26\u01ca\13\26\3\26\3\26\3\26\7\26\u01cf\n\26\f\26\16\26"+
		"\u01d2\13\26\3\26\3\26\7\26\u01d6\n\26\f\26\16\26\u01d9\13\26\3\26\5\26"+
		"\u01dc\n\26\3\27\3\27\3\27\5\27\u01e1\n\27\3\30\3\30\3\30\7\30\u01e6\n"+
		"\30\f\30\16\30\u01e9\13\30\3\31\3\31\3\31\3\31\3\31\7\31\u01f0\n\31\f"+
		"\31\16\31\u01f3\13\31\3\31\3\31\7\31\u01f7\n\31\f\31\16\31\u01fa\13\31"+
		"\3\31\7\31\u01fd\n\31\f\31\16\31\u0200\13\31\3\32\3\32\3\32\3\32\3\32"+
		"\7\32\u0207\n\32\f\32\16\32\u020a\13\32\3\32\3\32\7\32\u020e\n\32\f\32"+
		"\16\32\u0211\13\32\3\32\7\32\u0214\n\32\f\32\16\32\u0217\13\32\3\33\3"+
		"\33\3\33\3\33\3\33\3\33\7\33\u021f\n\33\f\33\16\33\u0222\13\33\3\33\7"+
		"\33\u0225\n\33\f\33\16\33\u0228\13\33\3\34\3\34\3\34\7\34\u022d\n\34\f"+
		"\34\16\34\u0230\13\34\3\34\5\34\u0233\n\34\3\35\3\35\3\35\7\35\u0238\n"+
		"\35\f\35\16\35\u023b\13\35\3\35\5\35\u023e\n\35\3\36\3\36\3\36\3\36\3"+
		"\36\7\36\u0245\n\36\f\36\16\36\u0248\13\36\3\36\3\36\7\36\u024c\n\36\f"+
		"\36\16\36\u024f\13\36\3\36\7\36\u0252\n\36\f\36\16\36\u0255\13\36\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\7\37\u025d\n\37\f\37\16\37\u0260\13\37\3\37"+
		"\3\37\7\37\u0264\n\37\f\37\16\37\u0267\13\37\3 \3 \3 \7 \u026c\n \f \16"+
		" \u026f\13 \3 \3 \3 \5 \u0274\n \3!\3!\3!\3!\3!\3!\7!\u027c\n!\f!\16!"+
		"\u027f\13!\3!\7!\u0282\n!\f!\16!\u0285\13!\3\"\3\"\3\"\3\"\3\"\3\"\7\""+
		"\u028d\n\"\f\"\16\"\u0290\13\"\3\"\7\"\u0293\n\"\f\"\16\"\u0296\13\"\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\7#\u02a0\n#\f#\16#\u02a3\13#\3$\3$\3$\3$\3$\5$"+
		"\u02aa\n$\3%\3%\7%\u02ae\n%\f%\16%\u02b1\13%\3%\3%\5%\u02b5\n%\3&\3&\3"+
		"&\5&\u02ba\n&\3\'\3\'\5\'\u02be\n\'\3(\3(\3(\3(\5(\u02c4\n(\3(\3(\3(\3"+
		"(\3(\5(\u02cb\n(\7(\u02cd\n(\f(\16(\u02d0\13(\3)\3)\3*\3*\3*\3+\3+\3+"+
		"\3+\3,\3,\3,\3-\3-\3-\3-\3.\3.\7.\u02e4\n.\f.\16.\u02e7\13.\3.\3.\3.\7"+
		".\u02ec\n.\f.\16.\u02ef\13.\5.\u02f1\n.\3.\7.\u02f4\n.\f.\16.\u02f7\13"+
		".\5.\u02f9\n.\3.\3.\3/\3/\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60"+
		"\3\60\5\60\u0309\n\60\3\61\3\61\7\61\u030d\n\61\f\61\16\61\u0310\13\61"+
		"\3\61\3\61\7\61\u0314\n\61\f\61\16\61\u0317\13\61\3\61\5\61\u031a\n\61"+
		"\3\61\7\61\u031d\n\61\f\61\16\61\u0320\13\61\3\61\5\61\u0323\n\61\3\62"+
		"\3\62\7\62\u0327\n\62\f\62\16\62\u032a\13\62\3\62\3\62\3\62\3\62\7\62"+
		"\u0330\n\62\f\62\16\62\u0333\13\62\3\62\3\62\3\62\3\62\7\62\u0339\n\62"+
		"\f\62\16\62\u033c\13\62\3\62\3\62\7\62\u0340\n\62\f\62\16\62\u0343\13"+
		"\62\3\62\3\62\5\62\u0347\n\62\3\63\3\63\7\63\u034b\n\63\f\63\16\63\u034e"+
		"\13\63\3\63\3\63\3\64\3\64\7\64\u0354\n\64\f\64\16\64\u0357\13\64\3\64"+
		"\3\64\3\65\3\65\5\65\u035d\n\65\3\66\3\66\3\67\3\67\38\38\38\38\39\39"+
		"\3:\3:\3;\3;\3;\3;\3<\3<\7<\u0371\n<\f<\16<\u0374\13<\3<\3<\7<\u0378\n"+
		"<\f<\16<\u037b\13<\3=\3=\3>\3>\5>\u0381\n>\3>\3>\7>\u0385\n>\f>\16>\u0388"+
		"\13>\3>\7>\u038b\n>\f>\16>\u038e\13>\3>\3>\3>\3>\3>\5>\u0395\n>\3>\3>"+
		"\5>\u0399\n>\3?\3?\3?\3?\3?\3?\3?\3?\5?\u03a3\n?\3@\3@\3A\3A\3B\3B\3C"+
		"\3C\3D\3D\3E\3E\3E\7E\u03b2\nE\fE\16E\u03b5\13E\3E\3E\3F\6F\u03ba\nF\r"+
		"F\16F\u03bb\3F\5F\u03bf\nF\3G\3G\3G\3G\3H\3H\3H\7H\u03c8\nH\fH\16H\u03cb"+
		"\13H\3H\3H\3I\6I\u03d0\nI\rI\16I\u03d1\3I\5I\u03d5\nI\3J\3J\3J\3J\3K\3"+
		"K\5K\u03dd\nK\3L\3L\3L\7L\u03e2\nL\fL\16L\u03e5\13L\3L\3L\3M\3M\3M\3M"+
		"\7M\u03ed\nM\fM\16M\u03f0\13M\3M\3M\3N\6N\u03f5\nN\rN\16N\u03f6\3N\5N"+
		"\u03fa\nN\3O\3O\3O\3O\3P\6P\u0401\nP\rP\16P\u0402\3P\5P\u0406\nP\3Q\3"+
		"Q\3Q\3Q\3R\6R\u040d\nR\rR\16R\u040e\3R\7R\u0412\nR\fR\16R\u0415\13R\3"+
		"R\3R\7R\u0419\nR\fR\16R\u041c\13R\5R\u041e\nR\3S\3S\3S\2\13\60\62\64:"+
		"<@BDNT\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<"+
		">@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a"+
		"\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2"+
		"\u00a4\2\23\3\2\66\67\3\2\37%\4\2./\61\62\3\2*-\4\2FFII\3\2&(\3\2\25\26"+
		"\4\2\20\22\24\24\4\2\60\60EE\4\2\25\26\34\34\4\2\27\30\34\34\5\2EEGG\\"+
		"\\\3\2rs\3\2mn\3\2wx\3\2}~\4\2\6\6\36\36\2\u046c\2\u00a9\3\2\2\2\4\u00cf"+
		"\3\2\2\2\6\u00d1\3\2\2\2\b\u00db\3\2\2\2\n\u00de\3\2\2\2\f\u00e8\3\2\2"+
		"\2\16\u0109\3\2\2\2\20\u011f\3\2\2\2\22\u0121\3\2\2\2\24\u0128\3\2\2\2"+
		"\26\u0141\3\2\2\2\30\u0143\3\2\2\2\32\u0167\3\2\2\2\34\u0195\3\2\2\2\36"+
		"\u0197\3\2\2\2 \u01a7\3\2\2\2\"\u01b8\3\2\2\2$\u01bb\3\2\2\2&\u01be\3"+
		"\2\2\2(\u01c2\3\2\2\2*\u01c4\3\2\2\2,\u01dd\3\2\2\2.\u01e2\3\2\2\2\60"+
		"\u01ea\3\2\2\2\62\u0201\3\2\2\2\64\u0218\3\2\2\2\66\u0229\3\2\2\28\u0234"+
		"\3\2\2\2:\u023f\3\2\2\2<\u0256\3\2\2\2>\u0273\3\2\2\2@\u0275\3\2\2\2B"+
		"\u0286\3\2\2\2D\u0297\3\2\2\2F\u02a9\3\2\2\2H\u02ab\3\2\2\2J\u02b9\3\2"+
		"\2\2L\u02bb\3\2\2\2N\u02c3\3\2\2\2P\u02d1\3\2\2\2R\u02d3\3\2\2\2T\u02d6"+
		"\3\2\2\2V\u02da\3\2\2\2X\u02dd\3\2\2\2Z\u02e1\3\2\2\2\\\u02fc\3\2\2\2"+
		"^\u0308\3\2\2\2`\u030a\3\2\2\2b\u0346\3\2\2\2d\u0348\3\2\2\2f\u0351\3"+
		"\2\2\2h\u035a\3\2\2\2j\u035e\3\2\2\2l\u0360\3\2\2\2n\u0362\3\2\2\2p\u0366"+
		"\3\2\2\2r\u0368\3\2\2\2t\u036a\3\2\2\2v\u036e\3\2\2\2x\u037c\3\2\2\2z"+
		"\u037e\3\2\2\2|\u03a2\3\2\2\2~\u03a4\3\2\2\2\u0080\u03a6\3\2\2\2\u0082"+
		"\u03a8\3\2\2\2\u0084\u03aa\3\2\2\2\u0086\u03ac\3\2\2\2\u0088\u03ae\3\2"+
		"\2\2\u008a\u03be\3\2\2\2\u008c\u03c0\3\2\2\2\u008e\u03c4\3\2\2\2\u0090"+
		"\u03d4\3\2\2\2\u0092\u03d6\3\2\2\2\u0094\u03dc\3\2\2\2\u0096\u03de\3\2"+
		"\2\2\u0098\u03e8\3\2\2\2\u009a\u03f9\3\2\2\2\u009c\u03fb\3\2\2\2\u009e"+
		"\u0405\3\2\2\2\u00a0\u0407\3\2\2\2\u00a2\u041d\3\2\2\2\u00a4\u041f\3\2"+
		"\2\2\u00a6\u00a8\7\6\2\2\u00a7\u00a6\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9"+
		"\u00a7\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00af\3\2\2\2\u00ab\u00a9\3\2"+
		"\2\2\u00ac\u00ae\5\u00a4S\2\u00ad\u00ac\3\2\2\2\u00ae\u00b1\3\2\2\2\u00af"+
		"\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0\u00c0\3\2\2\2\u00b1\u00af\3\2"+
		"\2\2\u00b2\u00bd\5\4\3\2\u00b3\u00b5\5\u00a4S\2\u00b4\u00b3\3\2\2\2\u00b5"+
		"\u00b6\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2"+
		"\2\2\u00b8\u00ba\5\4\3\2\u00b9\u00b8\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba"+
		"\u00bc\3\2\2\2\u00bb\u00b4\3\2\2\2\u00bc\u00bf\3\2\2\2\u00bd\u00bb\3\2"+
		"\2\2\u00bd\u00be\3\2\2\2\u00be\u00c1\3\2\2\2\u00bf\u00bd\3\2\2\2\u00c0"+
		"\u00b2\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c3\7\2"+
		"\2\3\u00c3\3\3\2\2\2\u00c4\u00d0\5\n\6\2\u00c5\u00c9\5\26\f\2\u00c6\u00c8"+
		"\5\u00a4S\2\u00c7\u00c6\3\2\2\2\u00c8\u00cb\3\2\2\2\u00c9\u00c7\3\2\2"+
		"\2\u00c9\u00ca\3\2\2\2\u00ca\u00cc\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cc\u00cd"+
		"\5\24\13\2\u00cd\u00d0\3\2\2\2\u00ce\u00d0\5\6\4\2\u00cf\u00c4\3\2\2\2"+
		"\u00cf\u00c5\3\2\2\2\u00cf\u00ce\3\2\2\2\u00d0\5\3\2\2\2\u00d1\u00d2\7"+
		"\64\2\2\u00d2\u00d6\5v<\2\u00d3\u00d4\7\7\2\2\u00d4\u00d7\7\20\2\2\u00d5"+
		"\u00d7\5\b\5\2\u00d6\u00d3\3\2\2\2\u00d6\u00d5\3\2\2\2\u00d6\u00d7\3\2"+
		"\2\2\u00d7\u00d9\3\2\2\2\u00d8\u00da\5\u00a2R\2\u00d9\u00d8\3\2\2\2\u00d9"+
		"\u00da\3\2\2\2\u00da\7\3\2\2\2\u00db\u00dc\7E\2\2\u00dc\u00dd\5x=\2\u00dd"+
		"\t\3\2\2\2\u00de\u00e2\5\f\7\2\u00df\u00e1\7\6\2\2\u00e0\u00df\3\2\2\2"+
		"\u00e1\u00e4\3\2\2\2\u00e2\u00e0\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e5"+
		"\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\u00e6\5\20\t\2\u00e6\13\3\2\2\2\u00e7"+
		"\u00e9\7K\2\2\u00e8\u00e7\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\3\2"+
		"\2\2\u00ea\u00ee\7\65\2\2\u00eb\u00ed\7\6\2\2\u00ec\u00eb\3\2\2\2\u00ed"+
		"\u00f0\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f1\3\2"+
		"\2\2\u00f0\u00ee\3\2\2\2\u00f1\u00f5\5v<\2\u00f2\u00f4\7\6\2\2\u00f3\u00f2"+
		"\3\2\2\2\u00f4\u00f7\3\2\2\2\u00f5\u00f3\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6"+
		"\u00f8\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f8\u0107\5\16\b\2\u00f9\u00fb\7"+
		"\6\2\2\u00fa\u00f9\3\2\2\2\u00fb\u00fe\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc"+
		"\u00fd\3\2\2\2\u00fd\u00ff\3\2\2\2\u00fe\u00fc\3\2\2\2\u00ff\u0103\7\35"+
		"\2\2\u0100\u0102\7\6\2\2\u0101\u0100\3\2\2\2\u0102\u0105\3\2\2\2\u0103"+
		"\u0101\3\2\2\2\u0103\u0104\3\2\2\2\u0104\u0106\3\2\2\2\u0105\u0103\3\2"+
		"\2\2\u0106\u0108\5p9\2\u0107\u00fc\3\2\2\2\u0107\u0108\3\2\2\2\u0108\r"+
		"\3\2\2\2\u0109\u0112\7\t\2\2\u010a\u010f\5t;\2\u010b\u010c\7\b\2\2\u010c"+
		"\u010e\5t;\2\u010d\u010b\3\2\2\2\u010e\u0111\3\2\2\2\u010f\u010d\3\2\2"+
		"\2\u010f\u0110\3\2\2\2\u0110\u0113\3\2\2\2\u0111\u010f\3\2\2\2\u0112\u010a"+
		"\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\7\n\2\2\u0115"+
		"\17\3\2\2\2\u0116\u0120\5\22\n\2\u0117\u011b\7\37\2\2\u0118\u011a\7\6"+
		"\2\2\u0119\u0118\3\2\2\2\u011a\u011d\3\2\2\2\u011b\u0119\3\2\2\2\u011b"+
		"\u011c\3\2\2\2\u011c\u011e\3\2\2\2\u011d\u011b\3\2\2\2\u011e\u0120\5."+
		"\30\2\u011f\u0116\3\2\2\2\u011f\u0117\3\2\2\2\u0120\21\3\2\2\2\u0121\u0122"+
		"\7\r\2\2\u0122\u0123\5\24\13\2\u0123\u0124\7\16\2\2\u0124\23\3\2\2\2\u0125"+
		"\u0127\5\u00a4S\2\u0126\u0125\3\2\2\2\u0127\u012a\3\2\2\2\u0128\u0126"+
		"\3\2\2\2\u0128\u0129\3\2\2\2\u0129\u0139\3\2\2\2\u012a\u0128\3\2\2\2\u012b"+
		"\u0136\5\26\f\2\u012c\u012e\5\u00a4S\2\u012d\u012c\3\2\2\2\u012e\u012f"+
		"\3\2\2\2\u012f\u012d\3\2\2\2\u012f\u0130\3\2\2\2\u0130\u0132\3\2\2\2\u0131"+
		"\u0133\5\26\f\2\u0132\u0131\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0135\3"+
		"\2\2\2\u0134\u012d\3\2\2\2\u0135\u0138\3\2\2\2\u0136\u0134\3\2\2\2\u0136"+
		"\u0137\3\2\2\2\u0137\u013a\3\2\2\2\u0138\u0136\3\2\2\2\u0139\u012b\3\2"+
		"\2\2\u0139\u013a\3\2\2\2\u013a\25\3\2\2\2\u013b\u0142\5*\26\2\u013c\u0142"+
		"\5.\30\2\u013d\u0142\5\32\16\2\u013e\u0142\5\36\20\2\u013f\u0142\5 \21"+
		"\2\u0140\u0142\5\"\22\2\u0141\u013b\3\2\2\2\u0141\u013c\3\2\2\2\u0141"+
		"\u013d\3\2\2\2\u0141\u013e\3\2\2\2\u0141\u013f\3\2\2\2\u0141\u0140\3\2"+
		"\2\2\u0142\27\3\2\2\2\u0143\u0147\78\2\2\u0144\u0146\7\6\2\2\u0145\u0144"+
		"\3\2\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2\2\2\u0147\u0148\3\2\2\2\u0148"+
		"\u014a\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u014e\5.\30\2\u014b\u014d\7\6"+
		"\2\2\u014c\u014b\3\2\2\2\u014d\u0150\3\2\2\2\u014e\u014c\3\2\2\2\u014e"+
		"\u014f\3\2\2\2\u014f\u0152\3\2\2\2\u0150\u014e\3\2\2\2\u0151\u0153\5("+
		"\25\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0155\3\2\2\2\u0154"+
		"\u0156\7\36\2\2\u0155\u0154\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0165\3"+
		"\2\2\2\u0157\u0159\7\6\2\2\u0158\u0157\3\2\2\2\u0159\u015c\3\2\2\2\u015a"+
		"\u0158\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u015d\3\2\2\2\u015c\u015a\3\2"+
		"\2\2\u015d\u0161\79\2\2\u015e\u0160\7\6\2\2\u015f\u015e\3\2\2\2\u0160"+
		"\u0163\3\2\2\2\u0161\u015f\3\2\2\2\u0161\u0162\3\2\2\2\u0162\u0164\3\2"+
		"\2\2\u0163\u0161\3\2\2\2\u0164\u0166\5(\25\2\u0165\u015a\3\2\2\2\u0165"+
		"\u0166\3\2\2\2\u0166\31\3\2\2\2\u0167\u0168\5\34\17\2\u0168\u0169\5(\25"+
		"\2\u0169\33\3\2\2\2\u016a\u016b\7>\2\2\u016b\u016f\5x=\2\u016c\u016e\7"+
		"\6\2\2\u016d\u016c\3\2\2\2\u016e\u0171\3\2\2\2\u016f\u016d\3\2\2\2\u016f"+
		"\u0170\3\2\2\2\u0170\u0172\3\2\2\2\u0171\u016f\3\2\2\2\u0172\u0176\7G"+
		"\2\2\u0173\u0175\7\6\2\2\u0174\u0173\3\2\2\2\u0175\u0178\3\2\2\2\u0176"+
		"\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0179\3\2\2\2\u0178\u0176\3\2"+
		"\2\2\u0179\u017c\5.\30\2\u017a\u017b\7H\2\2\u017b\u017d\5x=\2\u017c\u017a"+
		"\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u0196\3\2\2\2\u017e\u017f\7>\2\2\u017f"+
		"\u0180\7\t\2\2\u0180\u0184\5x=\2\u0181\u0183\7\6\2\2\u0182\u0181\3\2\2"+
		"\2\u0183\u0186\3\2\2\2\u0184\u0182\3\2\2\2\u0184\u0185\3\2\2\2\u0185\u0187"+
		"\3\2\2\2\u0186\u0184\3\2\2\2\u0187\u018b\7G\2\2\u0188\u018a\7\6\2\2\u0189"+
		"\u0188\3\2\2\2\u018a\u018d\3\2\2\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2"+
		"\2\2\u018c\u018e\3\2\2\2\u018d\u018b\3\2\2\2\u018e\u0191\5.\30\2\u018f"+
		"\u0190\7H\2\2\u0190\u0192\5x=\2\u0191\u018f\3\2\2\2\u0191\u0192\3\2\2"+
		"\2\u0192\u0193\3\2\2\2\u0193\u0194\7\n\2\2\u0194\u0196\3\2\2\2\u0195\u016a"+
		"\3\2\2\2\u0195\u017e\3\2\2\2\u0196\35\3\2\2\2\u0197\u019b\7@\2\2\u0198"+
		"\u019a\7\6\2\2\u0199\u0198\3\2\2\2\u019a\u019d\3\2\2\2\u019b\u0199\3\2"+
		"\2\2\u019b\u019c\3\2\2\2\u019c\u019e\3\2\2\2\u019d\u019b\3\2\2\2\u019e"+
		"\u01a2\5.\30\2\u019f\u01a1\7\6\2\2\u01a0\u019f\3\2\2\2\u01a1\u01a4\3\2"+
		"\2\2\u01a2\u01a0\3\2\2\2\u01a2\u01a3\3\2\2\2\u01a3\u01a5\3\2\2\2\u01a4"+
		"\u01a2\3\2\2\2\u01a5\u01a6\5(\25\2\u01a6\37\3\2\2\2\u01a7\u01ab\7?\2\2"+
		"\u01a8\u01aa\7\6\2\2\u01a9\u01a8\3\2\2\2\u01aa\u01ad\3\2\2\2\u01ab\u01a9"+
		"\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac\u01ae\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ae"+
		"\u01af\5(\25\2\u01af\u01b3\7@\2\2\u01b0\u01b2\7\6\2\2\u01b1\u01b0\3\2"+
		"\2\2\u01b2\u01b5\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b3\u01b4\3\2\2\2\u01b4"+
		"\u01b6\3\2\2\2\u01b5\u01b3\3\2\2\2\u01b6\u01b7\5.\30\2\u01b7!\3\2\2\2"+
		"\u01b8\u01b9\5$\23\2\u01b9\u01ba\5&\24\2\u01ba#\3\2\2\2\u01bb\u01bc\7"+
		"A\2\2\u01bc\u01bd\5.\30\2\u01bd%\3\2\2\2\u01be\u01bf\5\22\n\2\u01bf\'"+
		"\3\2\2\2\u01c0\u01c3\5\22\n\2\u01c1\u01c3\5.\30\2\u01c2\u01c0\3\2\2\2"+
		"\u01c2\u01c1\3\2\2\2\u01c3)\3\2\2\2\u01c4\u01c8\t\2\2\2\u01c5\u01c7\7"+
		"\6\2\2\u01c6\u01c5\3\2\2\2\u01c7\u01ca\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c8"+
		"\u01c9\3\2\2\2\u01c9\u01cb\3\2\2\2\u01ca\u01c8\3\2\2\2\u01cb\u01cc\5,"+
		"\27\2\u01cc\u01db\3\2\2\2\u01cd\u01cf\7\6\2\2\u01ce\u01cd\3\2\2\2\u01cf"+
		"\u01d2\3\2\2\2\u01d0\u01ce\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d3\3\2"+
		"\2\2\u01d2\u01d0\3\2\2\2\u01d3\u01d7\7\37\2\2\u01d4\u01d6\7\6\2\2\u01d5"+
		"\u01d4\3\2\2\2\u01d6\u01d9\3\2\2\2\u01d7\u01d5\3\2\2\2\u01d7\u01d8\3\2"+
		"\2\2\u01d8\u01da\3\2\2\2\u01d9\u01d7\3\2\2\2\u01da\u01dc\5.\30\2\u01db"+
		"\u01d0\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc+\3\2\2\2\u01dd\u01e0\5x=\2\u01de"+
		"\u01df\7\35\2\2\u01df\u01e1\5p9\2\u01e0\u01de\3\2\2\2\u01e0\u01e1\3\2"+
		"\2\2\u01e1-\3\2\2\2\u01e2\u01e7\5\60\31\2\u01e3\u01e4\t\3\2\2\u01e4\u01e6"+
		"\5\60\31\2\u01e5\u01e3\3\2\2\2\u01e6\u01e9\3\2\2\2\u01e7\u01e5\3\2\2\2"+
		"\u01e7\u01e8\3\2\2\2\u01e8/\3\2\2\2\u01e9\u01e7\3\2\2\2\u01ea\u01eb\b"+
		"\31\1\2\u01eb\u01ec\5\62\32\2\u01ec\u01fe\3\2\2\2\u01ed\u01f1\f\4\2\2"+
		"\u01ee\u01f0\7\6\2\2\u01ef\u01ee\3\2\2\2\u01f0\u01f3\3\2\2\2\u01f1\u01ef"+
		"\3\2\2\2\u01f1\u01f2\3\2\2\2\u01f2\u01f4\3\2\2\2\u01f3\u01f1\3\2\2\2\u01f4"+
		"\u01f8\7\32\2\2\u01f5\u01f7\7\6\2\2\u01f6\u01f5\3\2\2\2\u01f7\u01fa\3"+
		"\2\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9\u01fb\3\2\2\2\u01fa"+
		"\u01f8\3\2\2\2\u01fb\u01fd\5\60\31\5\u01fc\u01ed\3\2\2\2\u01fd\u0200\3"+
		"\2\2\2\u01fe\u01fc\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff\61\3\2\2\2\u0200"+
		"\u01fe\3\2\2\2\u0201\u0202\b\32\1\2\u0202\u0203\5\64\33\2\u0203\u0215"+
		"\3\2\2\2\u0204\u0208\f\4\2\2\u0205\u0207\7\6\2\2\u0206\u0205\3\2\2\2\u0207"+
		"\u020a\3\2\2\2\u0208\u0206\3\2\2\2\u0208\u0209\3\2\2\2\u0209\u020b\3\2"+
		"\2\2\u020a\u0208\3\2\2\2\u020b\u020f\7\31\2\2\u020c\u020e\7\6\2\2\u020d"+
		"\u020c\3\2\2\2\u020e\u0211\3\2\2\2\u020f\u020d\3\2\2\2\u020f\u0210\3\2"+
		"\2\2\u0210\u0212\3\2\2\2\u0211\u020f\3\2\2\2\u0212\u0214\5\62\32\5\u0213"+
		"\u0204\3\2\2\2\u0214\u0217\3\2\2\2\u0215\u0213\3\2\2\2\u0215\u0216\3\2"+
		"\2\2\u0216\63\3\2\2\2\u0217\u0215\3\2\2\2\u0218\u0219\b\33\1\2\u0219\u021a"+
		"\5\66\34\2\u021a\u0226\3\2\2\2\u021b\u021c\f\4\2\2\u021c\u0220\t\4\2\2"+
		"\u021d\u021f\7\6\2\2\u021e\u021d\3\2\2\2\u021f\u0222\3\2\2\2\u0220\u021e"+
		"\3\2\2\2\u0220\u0221\3\2\2\2\u0221\u0223\3\2\2\2\u0222\u0220\3\2\2\2\u0223"+
		"\u0225\5\64\33\5\u0224\u021b\3\2\2\2\u0225\u0228\3\2\2\2\u0226\u0224\3"+
		"\2\2\2\u0226\u0227\3\2\2\2\u0227\65\3\2\2\2\u0228\u0226\3\2\2\2\u0229"+
		"\u0232\58\35\2\u022a\u022e\t\5\2\2\u022b\u022d\7\6\2\2\u022c\u022b\3\2"+
		"\2\2\u022d\u0230\3\2\2\2\u022e\u022c\3\2\2\2\u022e\u022f\3\2\2\2\u022f"+
		"\u0231\3\2\2\2\u0230\u022e\3\2\2\2\u0231\u0233\58\35\2\u0232\u022a\3\2"+
		"\2\2\u0232\u0233\3\2\2\2\u0233\67\3\2\2\2\u0234\u023d\5:\36\2\u0235\u0239"+
		"\t\6\2\2\u0236\u0238\7\6\2\2\u0237\u0236\3\2\2\2\u0238\u023b\3\2\2\2\u0239"+
		"\u0237\3\2\2\2\u0239\u023a\3\2\2\2\u023a\u023c\3\2\2\2\u023b\u0239\3\2"+
		"\2\2\u023c\u023e\5p9\2\u023d\u0235\3\2\2\2\u023d\u023e\3\2\2\2\u023e9"+
		"\3\2\2\2\u023f\u0240\b\36\1\2\u0240\u0241\5<\37\2\u0241\u0253\3\2\2\2"+
		"\u0242\u0246\f\4\2\2\u0243\u0245\7\6\2\2\u0244\u0243\3\2\2\2\u0245\u0248"+
		"\3\2\2\2\u0246\u0244\3\2\2\2\u0246\u0247\3\2\2\2\u0247\u0249\3\2\2\2\u0248"+
		"\u0246\3\2\2\2\u0249\u024d\7)\2\2\u024a\u024c\7\6\2\2\u024b\u024a\3\2"+
		"\2\2\u024c\u024f\3\2\2\2\u024d\u024b\3\2\2\2\u024d\u024e\3\2\2\2\u024e"+
		"\u0250\3\2\2\2\u024f\u024d\3\2\2\2\u0250\u0252\5:\36\5\u0251\u0242\3\2"+
		"\2\2\u0252\u0255\3\2\2\2\u0253\u0251\3\2\2\2\u0253\u0254\3\2\2\2\u0254"+
		";\3\2\2\2\u0255\u0253\3\2\2\2\u0256\u0257\b\37\1\2\u0257\u0258\5> \2\u0258"+
		"\u0265\3\2\2\2\u0259\u025a\f\4\2\2\u025a\u025e\5x=\2\u025b\u025d\7\6\2"+
		"\2\u025c\u025b\3\2\2\2\u025d\u0260\3\2\2\2\u025e\u025c\3\2\2\2\u025e\u025f"+
		"\3\2\2\2\u025f\u0261\3\2\2\2\u0260\u025e\3\2\2\2\u0261\u0262\5<\37\5\u0262"+
		"\u0264\3\2\2\2\u0263\u0259\3\2\2\2\u0264\u0267\3\2\2\2\u0265\u0263\3\2"+
		"\2\2\u0265\u0266\3\2\2\2\u0266=\3\2\2\2\u0267\u0265\3\2\2\2\u0268\u0269"+
		"\5@!\2\u0269\u026d\t\7\2\2\u026a\u026c\7\6\2\2\u026b\u026a\3\2\2\2\u026c"+
		"\u026f\3\2\2\2\u026d\u026b\3\2\2\2\u026d\u026e\3\2\2\2\u026e\u0270\3\2"+
		"\2\2\u026f\u026d\3\2\2\2\u0270\u0271\5@!\2\u0271\u0274\3\2\2\2\u0272\u0274"+
		"\5@!\2\u0273\u0268\3\2\2\2\u0273\u0272\3\2\2\2\u0274?\3\2\2\2\u0275\u0276"+
		"\b!\1\2\u0276\u0277\5B\"\2\u0277\u0283\3\2\2\2\u0278\u0279\f\4\2\2\u0279"+
		"\u027d\t\b\2\2\u027a\u027c\7\6\2\2\u027b\u027a\3\2\2\2\u027c\u027f\3\2"+
		"\2\2\u027d\u027b\3\2\2\2\u027d\u027e\3\2\2\2\u027e\u0280\3\2\2\2\u027f"+
		"\u027d\3\2\2\2\u0280\u0282\5@!\5\u0281\u0278\3\2\2\2\u0282\u0285\3\2\2"+
		"\2\u0283\u0281\3\2\2\2\u0283\u0284\3\2\2\2\u0284A\3\2\2\2\u0285\u0283"+
		"\3\2\2\2\u0286\u0287\b\"\1\2\u0287\u0288\5D#\2\u0288\u0294\3\2\2\2\u0289"+
		"\u028a\f\4\2\2\u028a\u028e\t\t\2\2\u028b\u028d\7\6\2\2\u028c\u028b\3\2"+
		"\2\2\u028d\u0290\3\2\2\2\u028e\u028c\3\2\2\2\u028e\u028f\3\2\2\2\u028f"+
		"\u0291\3\2\2\2\u0290\u028e\3\2\2\2\u0291\u0293\5B\"\5\u0292\u0289\3\2"+
		"\2\2\u0293\u0296\3\2\2\2\u0294\u0292\3\2\2\2\u0294\u0295\3\2\2\2\u0295"+
		"C\3\2\2\2\u0296\u0294\3\2\2\2\u0297\u0298\b#\1\2\u0298\u0299\5F$\2\u0299"+
		"\u02a1\3\2\2\2\u029a\u029b\f\5\2\2\u029b\u029c\7\17\2\2\u029c\u02a0\5"+
		"D#\6\u029d\u029e\f\4\2\2\u029e\u02a0\7U\2\2\u029f\u029a\3\2\2\2\u029f"+
		"\u029d\3\2\2\2\u02a0\u02a3\3\2\2\2\u02a1\u029f\3\2\2\2\u02a1\u02a2\3\2"+
		"\2\2\u02a2E\3\2\2\2\u02a3\u02a1\3\2\2\2\u02a4\u02a5\5H%\2\u02a5\u02a6"+
		"\7\23\2\2\u02a6\u02a7\5F$\2\u02a7\u02aa\3\2\2\2\u02a8\u02aa\5H%\2\u02a9"+
		"\u02a4\3\2\2\2\u02a9\u02a8\3\2\2\2\u02aaG\3\2\2\2\u02ab\u02b4\5J&\2\u02ac"+
		"\u02ae\7\6\2\2\u02ad\u02ac\3\2\2\2\u02ae\u02b1\3\2\2\2\u02af\u02ad\3\2"+
		"\2\2\u02af\u02b0\3\2\2\2\u02b0\u02b2\3\2\2\2\u02b1\u02af\3\2\2\2\u02b2"+
		"\u02b3\t\n\2\2\u02b3\u02b5\5p9\2\u02b4\u02af\3\2\2\2\u02b4\u02b5\3\2\2"+
		"\2\u02b5I\3\2\2\2\u02b6\u02b7\t\13\2\2\u02b7\u02ba\5J&\2\u02b8\u02ba\5"+
		"L\'\2\u02b9\u02b6\3\2\2\2\u02b9\u02b8\3\2\2\2\u02baK\3\2\2\2\u02bb\u02bd"+
		"\5N(\2\u02bc\u02be\5Z.\2\u02bd\u02bc\3\2\2\2\u02bd\u02be\3\2\2\2\u02be"+
		"M\3\2\2\2\u02bf\u02c0\b(\1\2\u02c0\u02c4\5R*\2\u02c1\u02c4\5P)\2\u02c2"+
		"\u02c4\5^\60\2\u02c3\u02bf\3\2\2\2\u02c3\u02c1\3\2\2\2\u02c3\u02c2\3\2"+
		"\2\2\u02c4\u02ce\3\2\2\2\u02c5\u02ca\f\6\2\2\u02c6\u02cb\t\f\2\2\u02c7"+
		"\u02cb\5X-\2\u02c8\u02cb\5T+\2\u02c9\u02cb\5V,\2\u02ca\u02c6\3\2\2\2\u02ca"+
		"\u02c7\3\2\2\2\u02ca\u02c8\3\2\2\2\u02ca\u02c9\3\2\2\2\u02cb\u02cd\3\2"+
		"\2\2\u02cc\u02c5\3\2\2\2\u02cd\u02d0\3\2\2\2\u02ce\u02cc\3\2\2\2\u02ce"+
		"\u02cf\3\2\2\2\u02cfO\3\2\2\2\u02d0\u02ce\3\2\2\2\u02d1\u02d2\5x=\2\u02d2"+
		"Q\3\2\2\2\u02d3\u02d4\5x=\2\u02d4\u02d5\5Z.\2\u02d5S\3\2\2\2\u02d6\u02d7"+
		"\7\7\2\2\u02d7\u02d8\5x=\2\u02d8\u02d9\5Z.\2\u02d9U\3\2\2\2\u02da\u02db"+
		"\7\7\2\2\u02db\u02dc\5x=\2\u02dcW\3\2\2\2\u02dd\u02de\7\13\2\2\u02de\u02df"+
		"\5.\30\2\u02df\u02e0\7\f\2\2\u02e0Y\3\2\2\2\u02e1\u02f8\7\t\2\2\u02e2"+
		"\u02e4\7\6\2\2\u02e3\u02e2\3\2\2\2\u02e4\u02e7\3\2\2\2\u02e5\u02e3\3\2"+
		"\2\2\u02e5\u02e6\3\2\2\2\u02e6\u02e8\3\2\2\2\u02e7\u02e5\3\2\2\2\u02e8"+
		"\u02f5\5\\/\2\u02e9\u02ed\7\b\2\2\u02ea\u02ec\7\6\2\2\u02eb\u02ea\3\2"+
		"\2\2\u02ec\u02ef\3\2\2\2\u02ed\u02eb\3\2\2\2\u02ed\u02ee\3\2\2\2\u02ee"+
		"\u02f1\3\2\2\2\u02ef\u02ed\3\2\2\2\u02f0\u02e9\3\2\2\2\u02f0\u02f1\3\2"+
		"\2\2\u02f1\u02f2\3\2\2\2\u02f2\u02f4\5\\/\2\u02f3\u02f0\3\2\2\2\u02f4"+
		"\u02f7\3\2\2\2\u02f5\u02f3\3\2\2\2\u02f5\u02f6\3\2\2\2\u02f6\u02f9\3\2"+
		"\2\2\u02f7\u02f5\3\2\2\2\u02f8\u02e5\3\2\2\2\u02f8\u02f9\3\2\2\2\u02f9"+
		"\u02fa\3\2\2\2\u02fa\u02fb\7\n\2\2\u02fb[\3\2\2\2\u02fc\u02fd\5.\30\2"+
		"\u02fd]\3\2\2\2\u02fe\u0309\5n8\2\u02ff\u0309\5h\65\2\u0300\u0309\5j\66"+
		"\2\u0301\u0309\5l\67\2\u0302\u0309\5f\64\2\u0303\u0309\5|?\2\u0304\u0309"+
		"\5z>\2\u0305\u0309\5x=\2\u0306\u0309\5\30\r\2\u0307\u0309\5`\61\2\u0308"+
		"\u02fe\3\2\2\2\u0308\u02ff\3\2\2\2\u0308\u0300\3\2\2\2\u0308\u0301\3\2"+
		"\2\2\u0308\u0302\3\2\2\2\u0308\u0303\3\2\2\2\u0308\u0304\3\2\2\2\u0308"+
		"\u0305\3\2\2\2\u0308\u0306\3\2\2\2\u0308\u0307\3\2\2\2\u0309_\3\2\2\2"+
		"\u030a\u030e\7:\2\2\u030b\u030d\7\6\2\2\u030c\u030b\3\2\2\2\u030d\u0310"+
		"\3\2\2\2\u030e\u030c\3\2\2\2\u030e\u030f\3\2\2\2\u030f\u0311\3\2\2\2\u0310"+
		"\u030e\3\2\2\2\u0311\u0319\5\22\n\2\u0312\u0314\7\6\2\2\u0313\u0312\3"+
		"\2\2\2\u0314\u0317\3\2\2\2\u0315\u0313\3\2\2\2\u0315\u0316\3\2\2\2\u0316"+
		"\u0318\3\2\2\2\u0317\u0315\3\2\2\2\u0318\u031a\5b\62\2\u0319\u0315\3\2"+
		"\2\2\u0319\u031a\3\2\2\2\u031a\u0322\3\2\2\2\u031b\u031d\7\6\2\2\u031c"+
		"\u031b\3\2\2\2\u031d\u0320\3\2\2\2\u031e\u031c\3\2\2\2\u031e\u031f\3\2"+
		"\2\2\u031f\u0321\3\2\2\2\u0320\u031e\3\2\2\2\u0321\u0323\5d\63\2\u0322"+
		"\u031e\3\2\2\2\u0322\u0323\3\2\2\2\u0323a\3\2\2\2\u0324\u0328\7;\2\2\u0325"+
		"\u0327\7\6\2\2\u0326\u0325\3\2\2\2\u0327\u032a\3\2\2\2\u0328\u0326\3\2"+
		"\2\2\u0328\u0329\3\2\2\2\u0329\u032b\3\2\2\2\u032a\u0328\3\2\2\2\u032b"+
		"\u032c\7\t\2\2\u032c\u032d\5x=\2\u032d\u0331\7\n\2\2\u032e\u0330\7\6\2"+
		"\2\u032f\u032e\3\2\2\2\u0330\u0333\3\2\2\2\u0331\u032f\3\2\2\2\u0331\u0332"+
		"\3\2\2\2\u0332\u0334\3\2\2\2\u0333\u0331\3\2\2\2\u0334\u0335\5\22\n\2"+
		"\u0335\u0347\3\2\2\2\u0336\u033a\7;\2\2\u0337\u0339\7\6\2\2\u0338\u0337"+
		"\3\2\2\2\u0339\u033c\3\2\2\2\u033a\u0338\3\2\2\2\u033a\u033b\3\2\2\2\u033b"+
		"\u033d\3\2\2\2\u033c\u033a\3\2\2\2\u033d\u0341\5x=\2\u033e\u0340\7\6\2"+
		"\2\u033f\u033e\3\2\2\2\u0340\u0343\3\2\2\2\u0341\u033f\3\2\2\2\u0341\u0342"+
		"\3\2\2\2\u0342\u0344\3\2\2\2\u0343\u0341\3\2\2\2\u0344\u0345\5\22\n\2"+
		"\u0345\u0347\3\2\2\2\u0346\u0324\3\2\2\2\u0346\u0336\3\2\2\2\u0347c\3"+
		"\2\2\2\u0348\u034c\7<\2\2\u0349\u034b\7\6\2\2\u034a\u0349\3\2\2\2\u034b"+
		"\u034e\3\2\2\2\u034c\u034a\3\2\2\2\u034c\u034d\3\2\2\2\u034d\u034f\3\2"+
		"\2\2\u034e\u034c\3\2\2\2\u034f\u0350\5\22\n\2\u0350e\3\2\2\2\u0351\u0355"+
		"\7=\2\2\u0352\u0354\7\6\2\2\u0353\u0352\3\2\2\2\u0354\u0357\3\2\2\2\u0355"+
		"\u0353\3\2\2\2\u0355\u0356\3\2\2\2\u0356\u0358\3\2\2\2\u0357\u0355\3\2"+
		"\2\2\u0358\u0359\5.\30\2\u0359g\3\2\2\2\u035a\u035c\7B\2\2\u035b\u035d"+
		"\5.\30\2\u035c\u035b\3\2\2\2\u035c\u035d\3\2\2\2\u035di\3\2\2\2\u035e"+
		"\u035f\7C\2\2\u035fk\3\2\2\2\u0360\u0361\7D\2\2\u0361m\3\2\2\2\u0362\u0363"+
		"\7\t\2\2\u0363\u0364\5.\30\2\u0364\u0365\7\n\2\2\u0365o\3\2\2\2\u0366"+
		"\u0367\5r:\2\u0367q\3\2\2\2\u0368\u0369\5x=\2\u0369s\3\2\2\2\u036a\u036b"+
		"\5x=\2\u036b\u036c\7\35\2\2\u036c\u036d\5p9\2\u036du\3\2\2\2\u036e\u0379"+
		"\5x=\2\u036f\u0371\7\6\2\2\u0370\u036f\3\2\2\2\u0371\u0374\3\2\2\2\u0372"+
		"\u0370\3\2\2\2\u0372\u0373\3\2\2\2\u0373\u0375\3\2\2\2\u0374\u0372\3\2"+
		"\2\2\u0375\u0376\7\7\2\2\u0376\u0378\5x=\2\u0377\u0372\3\2\2\2\u0378\u037b"+
		"\3\2\2\2\u0379\u0377\3\2\2\2\u0379\u037a\3\2\2\2\u037aw\3\2\2\2\u037b"+
		"\u0379\3\2\2\2\u037c\u037d\t\r\2\2\u037dy\3\2\2\2\u037e\u0380\7\13\2\2"+
		"\u037f\u0381\5.\30\2\u0380\u037f\3\2\2\2\u0380\u0381\3\2\2\2\u0381\u038c"+
		"\3\2\2\2\u0382\u0386\7\b\2\2\u0383\u0385\7\6\2\2\u0384\u0383\3\2\2\2\u0385"+
		"\u0388\3\2\2\2\u0386\u0384\3\2\2\2\u0386\u0387\3\2\2\2\u0387\u0389\3\2"+
		"\2\2\u0388\u0386\3\2\2\2\u0389\u038b\5.\30\2\u038a\u0382\3\2\2\2\u038b"+
		"\u038e\3\2\2\2\u038c\u038a\3\2\2\2\u038c\u038d\3\2\2\2\u038d\u038f\3\2"+
		"\2\2\u038e\u038c\3\2\2\2\u038f\u0398\7\f\2\2\u0390\u0391\7*\2\2\u0391"+
		"\u0394\5x=\2\u0392\u0393\7\b\2\2\u0393\u0395\5x=\2\u0394\u0392\3\2\2\2"+
		"\u0394\u0395\3\2\2\2\u0395\u0396\3\2\2\2\u0396\u0397\7+\2\2\u0397\u0399"+
		"\3\2\2\2\u0398\u0390\3\2\2\2\u0398\u0399\3\2\2\2\u0399{\3\2\2\2\u039a"+
		"\u03a3\5\u0082B\2\u039b\u03a3\5\u0086D\2\u039c\u03a3\5\u0094K\2\u039d"+
		"\u03a3\5\u0080A\2\u039e\u03a3\5\u0088E\2\u039f\u03a3\5\u008eH\2\u03a0"+
		"\u03a3\5\u0084C\2\u03a1\u03a3\5~@\2\u03a2\u039a\3\2\2\2\u03a2\u039b\3"+
		"\2\2\2\u03a2\u039c\3\2\2\2\u03a2\u039d\3\2\2\2\u03a2\u039e\3\2\2\2\u03a2"+
		"\u039f\3\2\2\2\u03a2\u03a0\3\2\2\2\u03a2\u03a1\3\2\2\2\u03a3}\3\2\2\2"+
		"\u03a4\u03a5\7[\2\2\u03a5\177\3\2\2\2\u03a6\u03a7\7`\2\2\u03a7\u0081\3"+
		"\2\2\2\u03a8\u03a9\7Z\2\2\u03a9\u0083\3\2\2\2\u03aa\u03ab\7P\2\2\u03ab"+
		"\u0085\3\2\2\2\u03ac\u03ad\7T\2\2\u03ad\u0087\3\2\2\2\u03ae\u03b3\7M\2"+
		"\2\u03af\u03b2\5\u008aF\2\u03b0\u03b2\5\u008cG\2\u03b1\u03af\3\2\2\2\u03b1"+
		"\u03b0\3\2\2\2\u03b2\u03b5\3\2\2\2\u03b3\u03b1\3\2\2\2\u03b3\u03b4\3\2"+
		"\2\2\u03b4\u03b6\3\2\2\2\u03b5\u03b3\3\2\2\2\u03b6\u03b7\7p\2\2\u03b7"+
		"\u0089\3\2\2\2\u03b8\u03ba\t\16\2\2\u03b9\u03b8\3\2\2\2\u03ba\u03bb\3"+
		"\2\2\2\u03bb\u03b9\3\2\2\2\u03bb\u03bc\3\2\2\2\u03bc\u03bf\3\2\2\2\u03bd"+
		"\u03bf\7q\2\2\u03be\u03b9\3\2\2\2\u03be\u03bd\3\2\2\2\u03bf\u008b\3\2"+
		"\2\2\u03c0\u03c1\7t\2\2\u03c1\u03c2\5.\30\2\u03c2\u03c3\7\16\2\2\u03c3"+
		"\u008d\3\2\2\2\u03c4\u03c9\7L\2\2\u03c5\u03c8\5\u0090I\2\u03c6\u03c8\5"+
		"\u0092J\2\u03c7\u03c5\3\2\2\2\u03c7\u03c6\3\2\2\2\u03c8\u03cb\3\2\2\2"+
		"\u03c9\u03c7\3\2\2\2\u03c9\u03ca\3\2\2\2\u03ca\u03cc\3\2\2\2\u03cb\u03c9"+
		"\3\2\2\2\u03cc\u03cd\7k\2\2\u03cd\u008f\3\2\2\2\u03ce\u03d0\t\17\2\2\u03cf"+
		"\u03ce\3\2\2\2\u03d0\u03d1\3\2\2\2\u03d1\u03cf\3\2\2\2\u03d1\u03d2\3\2"+
		"\2\2\u03d2\u03d5\3\2\2\2\u03d3\u03d5\7l\2\2\u03d4\u03cf\3\2\2\2\u03d4"+
		"\u03d3\3\2\2\2\u03d5\u0091\3\2\2\2\u03d6\u03d7\7o\2\2\u03d7\u03d8\5.\30"+
		"\2\u03d8\u03d9\7\16\2\2\u03d9\u0093\3\2\2\2\u03da\u03dd\5\u0096L\2\u03db"+
		"\u03dd\5\u0098M\2\u03dc\u03da\3\2\2\2\u03dc\u03db\3\2\2\2\u03dd\u0095"+
		"\3\2\2\2\u03de\u03e3\7N\2\2\u03df\u03e2\5\u009aN\2\u03e0\u03e2\5\u009c"+
		"O\2\u03e1\u03df\3\2\2\2\u03e1\u03e0\3\2\2\2\u03e2\u03e5\3\2\2\2\u03e3"+
		"\u03e1\3\2\2\2\u03e3\u03e4\3\2\2\2\u03e4\u03e6\3\2\2\2\u03e5\u03e3\3\2"+
		"\2\2\u03e6\u03e7\7u\2\2\u03e7\u0097\3\2\2\2\u03e8\u03ee\7O\2\2\u03e9\u03ed"+
		"\5\u009eP\2\u03ea\u03ed\5\u00a0Q\2\u03eb\u03ed\7{\2\2\u03ec\u03e9\3\2"+
		"\2\2\u03ec\u03ea\3\2\2\2\u03ec\u03eb\3\2\2\2\u03ed\u03f0\3\2\2\2\u03ee"+
		"\u03ec\3\2\2\2\u03ee\u03ef\3\2\2\2\u03ef\u03f1\3\2\2\2\u03f0\u03ee\3\2"+
		"\2\2\u03f1\u03f2\7z\2\2\u03f2\u0099\3\2\2\2\u03f3\u03f5\t\20\2\2\u03f4"+
		"\u03f3\3\2\2\2\u03f5\u03f6\3\2\2\2\u03f6\u03f4\3\2\2\2\u03f6\u03f7\3\2"+
		"\2\2\u03f7\u03fa\3\2\2\2\u03f8\u03fa\7v\2\2\u03f9\u03f4\3\2\2\2\u03f9"+
		"\u03f8\3\2\2\2\u03fa\u009b\3\2\2\2\u03fb\u03fc\7y\2\2\u03fc\u03fd\5.\30"+
		"\2\u03fd\u03fe\7\16\2\2\u03fe\u009d\3\2\2\2\u03ff\u0401\t\21\2\2\u0400"+
		"\u03ff\3\2\2\2\u0401\u0402\3\2\2\2\u0402\u0400\3\2\2\2\u0402\u0403\3\2"+
		"\2\2\u0403\u0406\3\2\2\2\u0404\u0406\7|\2\2\u0405\u0400\3\2\2\2\u0405"+
		"\u0404\3\2\2\2\u0406\u009f\3\2\2\2\u0407\u0408\7\177\2\2\u0408\u0409\5"+
		".\30\2\u0409\u040a\7\16\2\2\u040a\u00a1\3\2\2\2\u040b\u040d\7\6\2\2\u040c"+
		"\u040b\3\2\2\2\u040d\u040e\3\2\2\2\u040e\u040c\3\2\2\2\u040e\u040f\3\2"+
		"\2\2\u040f\u041e\3\2\2\2\u0410\u0412\7\6\2\2\u0411\u0410\3\2\2\2\u0412"+
		"\u0415\3\2\2\2\u0413\u0411\3\2\2\2\u0413\u0414\3\2\2\2\u0414\u0416\3\2"+
		"\2\2\u0415\u0413\3\2\2\2\u0416\u041a\7\36\2\2\u0417\u0419\7\6\2\2\u0418"+
		"\u0417\3\2\2\2\u0419\u041c\3\2\2\2\u041a\u0418\3\2\2\2\u041a\u041b\3\2"+
		"\2\2\u041b\u041e\3\2\2\2\u041c\u041a\3\2\2\2\u041d\u040c\3\2\2\2\u041d"+
		"\u0413\3\2\2\2\u041e\u00a3\3\2\2\2\u041f\u0420\t\22\2\2\u0420\u00a5\3"+
		"\2\2\2\u0087\u00a9\u00af\u00b6\u00b9\u00bd\u00c0\u00c9\u00cf\u00d6\u00d9"+
		"\u00e2\u00e8\u00ee\u00f5\u00fc\u0103\u0107\u010f\u0112\u011b\u011f\u0128"+
		"\u012f\u0132\u0136\u0139\u0141\u0147\u014e\u0152\u0155\u015a\u0161\u0165"+
		"\u016f\u0176\u017c\u0184\u018b\u0191\u0195\u019b\u01a2\u01ab\u01b3\u01c2"+
		"\u01c8\u01d0\u01d7\u01db\u01e0\u01e7\u01f1\u01f8\u01fe\u0208\u020f\u0215"+
		"\u0220\u0226\u022e\u0232\u0239\u023d\u0246\u024d\u0253\u025e\u0265\u026d"+
		"\u0273\u027d\u0283\u028e\u0294\u029f\u02a1\u02a9\u02af\u02b4\u02b9\u02bd"+
		"\u02c3\u02ca\u02ce\u02e5\u02ed\u02f0\u02f5\u02f8\u0308\u030e\u0315\u0319"+
		"\u031e\u0322\u0328\u0331\u033a\u0341\u0346\u034c\u0355\u035c\u0372\u0379"+
		"\u0380\u0386\u038c\u0394\u0398\u03a2\u03b1\u03b3\u03bb\u03be\u03c7\u03c9"+
		"\u03d1\u03d4\u03dc\u03e1\u03e3\u03ec\u03ee\u03f6\u03f9\u0402\u0405\u040e"+
		"\u0413\u041a\u041d";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}