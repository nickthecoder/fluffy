package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler


class TestWhile : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testWhile() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "while ( a < 3 ) { b = b*2 \n a ++ }").eval()
        assertEquals(3, binding.valueOf("a"))
        assertEquals(8, binding.valueOf("b"))

        compiler.compile(binding, "while ( a < 3 ) { b = b*2 \n a ++ }").eval()
        assertEquals(3, binding.valueOf("a"))
        assertEquals(8, binding.valueOf("b"))
    }


    @Test
    fun testWhileWithoutBrackets() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "while a < 3 { b = b*2 \n a ++ }").eval()
        assertEquals(3, binding.valueOf("a"))
        assertEquals(8, binding.valueOf("b"))
    }


    @Test
    fun testDoWhile() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "do { b = b*2 \n a ++ } while ( a < 3 ) ").eval()
        assertEquals(3, binding.valueOf("a"))
        assertEquals(8, binding.valueOf("b"))

        // Unlike testWhile, this will run the block once more
        compiler.compile(binding, "do { b = b*2 \n a ++ } while ( a < 3 ) ").eval()
        assertEquals(4, binding.valueOf("a"))
        assertEquals(16, binding.valueOf("b"))

    }

    @Test
    fun testDoWhileWithoutBrackets() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "do { b = b*2 \n a ++ } while a < 3 ").eval()
        assertEquals(3, binding.valueOf("a"))
        assertEquals(8, binding.valueOf("b"))

        // Unlike testWhile, this will run the block once more
        compiler.compile(binding, "do { b = b*2 \n a ++ } while a < 3 ").eval()
        assertEquals(4, binding.valueOf("a"))
        assertEquals(16, binding.valueOf("b"))

    }

}
