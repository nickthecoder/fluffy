package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler


class TestFor : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testFor() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for i in 1..3 { b = b*2 }").eval()
        assertEquals(16, binding.valueOf("b"))
    }

    @Test
    fun testCannotAssignLoopVar() {
        val binding = Binding()

        assertFails { compiler.compile(binding, "for i in 1..3 { i = 10 }").eval() }
    }

    @Test
    fun testCannotAssignCounterVar() {
        val binding = Binding()

        assertFails { compiler.compile(binding, "for i in 1..3 counter x { x = 10}").eval() }
    }

    @Test
    fun testForNoBlock() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for i in 1..3 b = b*2").eval()
        assertEquals(16, binding.valueOf("b"))
    }


    @Test
    fun testForWithBrackets() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for ( i in 1..3 ) b = b*2").eval()
        assertEquals(16, binding.valueOf("b"))
    }

    @Test
    fun testForIndex() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for i in 1..3 counter c b = i * c").eval()
        assertEquals(6, binding.valueOf("b"))

    }

    @Test
    fun testForIndexWithBrackets() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for ( i in 1..3 counter c ) b = i * c").eval()
        assertEquals(6, binding.valueOf("b"))
    }


    @Test
    fun testForExclusive() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for i in 1..<3 { b = b*2 }").eval()
        assertEquals(8, binding.valueOf("b"))
    }

    @Test
    fun testSingleStatement() {
        val binding = Binding()

        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "for i in 1..<3 b = b*2").eval()
        assertEquals(8, binding.valueOf("b"))
    }


}
