package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler


class TestVariables : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testDef() {
        val binding = Binding()
        assertEquals(3, FluffyCompiler().compile(binding, "var a = 3\na").eval())
        assertEquals(4, FluffyCompiler().compile(binding, "var b = 3\nb=4\nb").eval())
    }

    @Test
    fun testIncDec() {
        val binding = Binding()

        assertEquals(4, FluffyCompiler().compile(binding, "var a = 3\na++").eval())
        assertEquals(2, FluffyCompiler().compile(binding, "var b = 3\nb--").eval())

        assertEquals(4.1, FluffyCompiler().compile(binding, "var c = 3.1\nc++").eval())
        assertEquals(2.1, FluffyCompiler().compile(binding, "var d = 3.1\nd--").eval())
    }

    @Test
    fun testAddAssign() {
        val binding = Binding()

        assertEquals(5, FluffyCompiler().compile(binding, "var a = 3\na+=2").eval())
        assertEquals(1, FluffyCompiler().compile(binding, "var b = 3\nb-=2").eval())

        assertEquals(5.1, FluffyCompiler().compile(binding, "var c = 3.1\nc+=2").eval())
        assertEquals(1.1, FluffyCompiler().compile(binding, "var d = 3.1\nd-=2").eval())

        assertEquals(5.2, FluffyCompiler().compile(binding, "var e = 3.1\ne+=2.1").eval())
        assertEquals(1.0, FluffyCompiler().compile(binding, "var f = 3.3\nf-=2.3").eval())


    }

    @Test
    fun testMultAssign() {
        val binding = Binding()

        assertEquals(8, FluffyCompiler().compile(binding, "var a = 4 \n a*=2 \n a").eval())
        assertEquals(3, FluffyCompiler().compile(binding, "var b = 6 \n b~/=2 \n b").eval())

        assertEquals(6.2, FluffyCompiler().compile(binding, "var c = 3.1 \n c*=2 \n c").eval())
        assertEquals(2.1, FluffyCompiler().compile(binding, "var d = 4.2 \n d/=2 \n d").eval())

        assertEquals(3.1 * 2.1, FluffyCompiler().compile(binding, "var e = 3.1 \n e*=2.1 \n e").eval())
        assertEquals(3.3 / 2.3, FluffyCompiler().compile(binding, "var f = 3.3 \n f/=2.3 \n f").eval())
    }

    @Test
    fun testReEval() {
        val binding = Binding()

        val script = FluffyCompiler().compile(binding, "var a = 3\na = a + 2\na")
        assertEquals(5, script.eval())
        assertEquals(5, script.eval())


        val script2 = FluffyCompiler().compile(binding, "var b = 3\nb + 1")
        assertEquals(4, script2.eval())
        assertEquals(4, script2.eval())
    }

    /**
     * For [testBind]. Appears as a getter setter pair to the Jvm
     */
    var beanie = 13

    /**
     * For [testBind]. Appears as a regular Java field, not a get/set method pair.
     */
    @JvmField
    var field = 3

    val readOnly = 1

    @JvmField
    val alsoReadOnly = 2

    @Test
    fun testBind() {
        val binding = Binding()
        binding.bind("field", this, "field")

        assertEquals(3, compiler.compile(binding, "field").eval())

        // Changing the bean changes the fluffy variable?
        field = 4
        assertEquals(4, compiler.compile(binding, "field").eval())

        // Changing fluffy variable changes the bean ?
        compiler.compile(binding, "field = 10").eval()
        assertEquals(10, field)


        // Now do the same for the get/set pair 'beanie'


        binding.bind("beanie", this, "beanie")

        assertEquals(13, compiler.compile(binding, "beanie").eval())
        beanie = 14
        assertEquals(14, compiler.compile(binding, "beanie").eval())

        compiler.compile(binding, "beanie = 20").eval()
        assertEquals(20, beanie)

    }

    @Test
    fun testReadOnlyBinding() {
        val binding = Binding()
        binding.bind("alsoReadOnly", this, "alsoReadOnly")

        assertEquals(2, compiler.compile(binding, "alsoReadOnly").eval())
        // The field is final, so the set fails.
        assertFails { compiler.compile(binding, "alsoReadOnly = -2").eval() }

        // No setter, so the set fails.
        binding.bind("y", this, "readOnly")
        assertEquals(1, compiler.compile(binding, "y").eval())
        assertFails { compiler.compile(binding, "y = -1").eval() }

    }

    @Test
    fun testIllegalBindings() {
        val binding = Binding()

        assertFails { binding.bind("x", this, "doesNotExist") }
    }

    // TODO This fails. Not sure what it SHOULD do!
    @Test
    fun fails_testIntLiteralWithDoubleVar() {
        val binding = Binding()

        assertEquals(1.5, FluffyCompiler().compile(binding, "var a : Double = 3\na / 2").eval())
    }

    // TODO This fails. Not sure what it SHOULD do!
    @Test
    fun fails_testDoubleLiteralWithIntVar() {
        val binding = Binding()

        assertEquals(3, FluffyCompiler().compile(binding, "var a : Int = 7.0\na / 2").eval())
    }
}
