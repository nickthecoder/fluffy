package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.FluffyEvalException
import uk.co.nickthecoder.fluffy.FluffyException

/**
 * This tests for excecptions thrown by [FluffyCompiler].
 * It's tests that the position of the error is correct, and is not a complete set of tests for all possible errors.
 *
 */
class TestExceptions : TestCase() {

    val compiler = FluffyCompiler.permissive().apply { declutterStackTrace = false }

    var binding = Binding()

    /**
     * Tests that need dynamic calls can set this back to true.
     */
    override fun setUp() {
        compiler.allowDynamic = false
        binding = Binding()
        binding.declare("me", this)
    }

    private fun assertFailsWithCause(code: String) {
        try {
            compiler.compile(binding, code).eval()

        } catch (e: FluffyException) {
            if (e.cause == null) e.printStackTrace()
            assertNotNull("($e) FluffyException.cause is not null", e.cause)
            return
        }
        fail("Was expected to fail")

    }

    private fun assertFailsAtColumn(column: Int, code: String) = assertFailsAt(1, column, code)

    private fun assertFailsAt(line: Int, column: Int, code: String) {
        try {
            compiler.compile(binding, code).eval()
        } catch (e: FluffyException) {
            assertEquals("($e) Incorrect line", line, e.position.line)
            assertEquals("($e) Incorrect column", column, e.position.column)
            return
        }
        fail("Was expected to fail")

    }

    val throwsException: Int
        get() = throw Exception("A random exception")

    fun throwsException() {
        throw Exception("A random exception")
    }


    // These functions were originally in the same order as found in [FluffyCompiler]
    @Test
    fun testMismatchedInput() {
        assertFailsAtColumn(7, "fun foo")
    }

    @Test
    fun testUnknownExtensionClass() {
        assertFailsAtColumn(4, "fun Foo.bar() {}")
    }

    /**
     * Calling a function without an explicit type is only allow AFTER the function declaration.
     */
    @Test
    fun testFunEqualsWithoutReturnType() {
        assertFailsAt(1, 0, "baz()\nfun baz() = 3")
    }

    @Test
    fun testInfix2Operands() {
        assertFailsAtColumn(10, "infix fun none() {}")
        assertFailsAtColumn(10, "infix fun one( a : Int ) {}")
        assertFailsAtColumn(10, "infix fun three( a : Int, b: Int, c : Int ) {}")
    }

    @Test
    fun testInfixAndExt() {
        assertFailsAtColumn(10, "infix fun File.foo(a : Int, b: Int) {}")
    }

    @Test
    fun testBadParameterType() {
        assertFailsAtColumn(12, "fun foo(a : Bad) {}")
        assertFailsAtColumn(21, "fun foo(a : Int, b : Bad) {}")
    }

    @Test
    fun testArrayAccess() {
        assertFailsAt(2, 1, "val i = 3\ni[0]")
    }

    @Test
    fun testUnknownInfix() {
        assertFailsAtColumn(2, "1 foo 2")
    }

    @Test
    fun testUnknownIdentifier() {
        assertFailsAtColumn(4, "1 + foo + 2")
    }

    @Test
    fun testUnknownFunction() {
        assertFailsAtColumn(4, "1 + foo() + 2")
    }

    @Test
    fun testUnknownField() {
        assertFailsAtColumn(7, "1 + me.foo")
    }

    @Test
    fun testUnknownMethod() {
        assertFailsAtColumn(7, "1 + me.foo()")
    }

    @Test
    fun testIllegalBop() {
        assertFailsAtColumn(2, "1 + 'a'")
    }

    @Test
    fun testIllegalUop() {
        assertFailsAtColumn(8, "true && ! 1")
    }

    @Test
    fun testReturnOutsideFunction() {
        assertFailsAtColumn(1, " return 1")
    }

    @Test
    fun testBrekakOutsideLoop() {
        assertFailsAtColumn(1, " break")
    }

    @Test
    fun testContinueOutsideLoop() {
        assertFailsAtColumn(1, " continue")
    }

    @Test
    fun testUnknownVariableType() {
        assertFailsAtColumn(8, "val a : Foo = 1")
    }

    @Test
    fun testUnknownListItemType() {
        assertFailsAtColumn(16, "val a = [1,2,3]<Inty>")
    }

    @Test
    fun testUnknownMapValueType() {
        assertFailsAtColumn(35, "val a = [1=>'a',2=>'c',3=>'d']<Int,Charish>")
    }

    @Test
    fun testUnknownIdInString() {
        assertFailsAtColumn(9, "val a = \"\$foo\"")
        assertFailsAtColumn(11, "val a = \"\"\"\$foo\"\"\"")
    }

    @Test
    fun testInNotIterator() {
        // NOTE, position is on the 'in' keyword.
        assertFailsAtColumn(6, "for i in 13")
        assertFailsAtColumn(6, "for i in 'a'")
    }


    // The following test dynamic exceptions (i.e. thrown during Script.eval()

    @Test
    fun testDynamicOperator() {
        compiler.allowDynamic = true

        binding.declare("list", listOf("a", "b", "c"))
        assertFailsAtColumn(8, "list[0] - 1")
    }

    @Test
    fun testBinaryDynamicOperator() {
        compiler.allowDynamic = true

        binding.declare("list", listOf("a", "b", "c"))
        assertFailsAtColumn(8, "true || ! list[0]")
    }


    @Test
    fun testDynamicFunction() {
        compiler.allowDynamic = true

        binding.declare("list", listOf("a", "b", "c"))
        assertFailsAtColumn(8, "list[0].foo()")
    }

    @Test
    fun testDynamicField() {
        compiler.allowDynamic = true

        binding.declare("list", listOf("a", "b", "c"))
        assertFailsAtColumn(8, "list[0].foo")
    }

    // The following test dynamic exceptions which were caused by non-FluffyExceptions.

    @Test
    fun testDynamicMethodThrows() {
        compiler.allowDynamic = true

        binding.declare("list", listOf(this))

        assertFailsAtColumn(8, "list[0].throwsException()")
        assertFailsWithCause("list[0].throwsException()")

    }

    @Test
    fun testDynamicFieldThrows() {
        compiler.allowDynamic = true

        binding.declare("list", listOf(this))

        assertFailsAtColumn(8, "list[0].throwsException")
        assertFailsWithCause("list[0].throwsException")

    }

    // The following test non-dynamic exceptions which were caused by non-FluffyExceptions.


    @Test
    fun testMethodThrows() {
        assertFailsAtColumn(3, "me.throwsException()")
        assertFailsWithCause("me.throwsException()")

    }

    @Test
    fun testFieldThrows() {
        assertFailsAtColumn(3, "me.throwsException")
        assertFailsWithCause("me.throwsException")

    }

    @Test
    fun testStackTrace() {
        try {
            compiler.compile(binding, """
fun foo() {
    bar()
}
fun bar() {
    me.throwsException
}
foo()
""").eval()
        } catch (e: FluffyEvalException) {
            val st = e.stackTrace
            assertEquals("bar", st[0].methodName)
            assertEquals(6, st[0].lineNumber)

            assertEquals("foo", st[1].methodName)
            assertEquals(3, st[1].lineNumber)

            //assertEquals(st[2].methodName = "<top-level>") // Subject to change, so do not test!
            assertEquals(8, st[2].lineNumber)
        }
    }

    @Test
    fun testReturnType() {
        // Return type is wrong. char vs int.
        assertFailsAtColumn(21, "fun fails1() : Int { return 'a' }")

        // Missing return
        assertFailsAtColumn(19, "fun fails2() : Int { val c = 'a' }")
    }

    @Test
    fun testDeclarationType() {
        // Return type is wrong. char vs int.
        assertFailsAtColumn(16, "val foo : Int = 'a'")

    }

}
