package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

/**
 * I use this to test a single thing, either from an existing Test (which failed),
 * or a new test, which then goes into the main set of Tests once it passes.
 */
class TestSolo : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @Test
    fun testSolo() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 2").eval()

        compiler.compile(binding, "while ( a < 3 ) { b = b*2 \n a ++ }").eval()

    }

}
