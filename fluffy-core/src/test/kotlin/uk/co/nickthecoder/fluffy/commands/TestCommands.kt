package uk.co.nickthecoder.fluffy.commands

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

/**

 */
class TestCommands : TestCase() {

    lateinit var compiler: FluffyCompiler

    override fun setUp() {
        compiler = FluffyCompiler()
    }

    @Test
    fun testCommand() {
        val binding = Binding()
        compiler.commandRunner = UnixCommandRunner()
        assertEquals("Hello\n", compiler.compile(binding, "`echo Hello`.out").eval())
    }

    @Test
    fun testQuotes() {
        val binding = Binding()
        compiler.commandRunner = UnixCommandRunner()
        compiler.compile(binding, "val a = \"It's ok\"").eval()
        assertEquals("It's ok\n", compiler.compile(binding, "`echo '\$a'`.out").eval())
    }

    @Test
    fun testInvalidCommand() {
        val binding = Binding()
        compiler.commandRunner = UnixCommandRunner()
        assertEquals(CommandState.FINISHED, (compiler.compile(binding, "`dkjsahdkjsah`").eval() as CommandResult).state)

        // NOTE, this is platform dependent? Is there a standard exit status for an invalid command?
        // Is 127 actually -1 in disguise? (Unsigned vs signed byte?)
        assertEquals(127, (compiler.compile(binding, "`dkjsahdkjsah`").eval() as CommandResult).exitValue)
    }

    @Test
    fun BADtestTimeout() {
        val binding = Binding()
        compiler.commandRunner = UnixCommandRunner()
        compiler.commandRunner.setTimeoutSeconds(1)

        val result = compiler.compile(binding, "$(sleep 2)").eval() as CommandResult
        result.process?.destroy()
        assertEquals(CommandState.INTERRUPTED, result.state)

        // NOTE. Even though the timeout works (the main thread returns), another thread called "process reaper" continues
        // until the process finishes. I changed the sleep to 10, and dumped the threads' stack :
        //Thread.sleep(8000)
        //Thread.getAllStackTraces().forEach { k, v -> println("Thread : $k\nStackTrace\n${v.toList().joinToString(separator = "\n")}\n\n") }

        //println("\n\n\n")

        //Thread.sleep(3000)
        //Thread.getAllStackTraces().forEach { k, v -> println("Thread : $k\nStackTrace\n${v.toList().joinToString(separator = "\n")}\n\n") }

    }

    @Test
    fun testDisabled() {
        val compiler = FluffyCompiler()
        val binding = Binding()
        assertEquals("", compiler.compile(binding, "`echo Hello`.out").eval())
        assertEquals(CommandState.FAILED, compiler.compile(binding, "`echo Hello`.state").eval())
    }

    @Test
    fun testNestedString() {
        val compiler = FluffyCompiler.permissive()
        val binding = Binding()
        compiler.compile(binding, "val word=\"It's\"").eval()
        //assertEquals("It's", compiler.compile(binding, "word").eval())
        //var result = compiler.compile(binding, "`echo '\$word'`").eval() as CommandResult
        //assertEquals("echo 'It'\\''s'", result.command)

        var result = compiler.compile(binding, "`echo '\${word}'`").eval() as CommandResult
        assertEquals("echo 'It'\\''s'", result.command)
        result = compiler.compile(binding, "`echo '\$word'`").eval() as CommandResult
        assertEquals("echo 'It'\\''s'", result.command)
    }
}
