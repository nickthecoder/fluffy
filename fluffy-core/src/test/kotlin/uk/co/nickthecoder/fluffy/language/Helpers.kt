package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

fun assertFails(action: () -> Any?) {
    TestCase.assertTrue(fails(action))
}

fun fails(action: () -> Any?): Boolean {
    try {
        action()
        return false
    } catch (e: Exception) {
        return true
    }
}

fun assertEqualsString(expected: String, actual: Any?) = TestCase.assertEquals("'$expected'", "'$actual'")


fun assertEvalToString(expected: String, compiler: FluffyCompiler, binding: Binding, code: String) {
    assertEqualsString(expected, compiler.compile(binding, code).eval())
}
