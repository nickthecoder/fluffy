package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.WhitelistSandbox

class TestSandbox : TestCase() {

    lateinit var sandbox: WhitelistSandbox
    lateinit var compiler: FluffyCompiler

    override fun setUp() {
        sandbox = WhitelistSandbox.standard()
        compiler = FluffyCompiler()
        compiler.sandbox = sandbox
    }

    @Test
    fun testStaticField() {
        val binding = Binding()
        binding.importPackage("java.lang")

        // System not allowed.
        assertFails { compiler.compile(binding, "System.out").eval() }

        sandbox.whitelistClasses.add(System::class.java.name)

        // Now it is allowed
        assertEquals(System.out, compiler.compile(binding, "System.out").eval())
    }

    @Test
    fun testStaticMethod() {
        val binding = Binding()
        binding.importPackage("java.lang")

        // System not allowed.
        assertFails { compiler.compile(binding, "System.console()").eval() }

        sandbox.whitelistClasses.add(System::class.java.name)

        // Now it is allowed
        assertEquals(System.console(), compiler.compile(binding, "System.console()").eval())
    }


    @Test
    fun testMethod() {
        val binding = Binding()

        val testInstance = TestClass()
        binding.declare("a", testInstance)

        // TestClass not allowed yet
        assertFails { compiler.compile(binding, "a.bar()").eval() }

        sandbox.whitelistClasses.add(TestClass::class.java.name)

        // Now it is allowed
        assertEquals(testInstance.bar(), compiler.compile(binding, "a.bar()").eval())

        // We can't call methods on it as BannedClass is still banned
        assertFails { compiler.compile(binding, "a.bar().mthd()").eval() }

    }


    @Test
    fun testField() {
        val binding = Binding()

        val testInstance = TestClass()
        binding.declare("a", testInstance)

        // TestClass not allowed yet
        assertFails { compiler.compile(binding, "a.foo").eval() }

        sandbox.whitelistClasses.add(TestClass::class.java.name)

        // Now it is allowed
        assertEquals(testInstance.foo, compiler.compile(binding, "a.foo").eval())

        // We can't call access fields  on it as BannedClass is still banned
        assertFails { compiler.compile(binding, "a.bar().fld").eval() }
    }

    @Test
    fun testInsideClass() {
        val binding = Binding()

        val testInstance = InsideClass()
        binding.declare("a", testInstance)

        // InsideClass not allowed yet
        assertFails { compiler.compile(binding, "a.foo").eval() }

        sandbox.whitelistClasses.add(TestSandbox::class.java.name)

        // Now it is allowed (as it is inside an allowed class)
        assertEquals(testInstance.foo, compiler.compile(binding, "a.foo").eval())
    }

    class InsideClass {
        val foo = 100
    }
}

class TestClass(val inside: TestSandbox.InsideClass? = null) {
    @JvmField
    val foo = BannedClass()

    fun bar() = foo
}

class BannedClass() {
    @JvmField
    val fld = 1

    fun mthd() = 2
}
