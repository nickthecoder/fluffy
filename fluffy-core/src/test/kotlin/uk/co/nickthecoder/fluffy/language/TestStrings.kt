package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler


class TestStrings : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testLiterals() {
        val binding = Binding()

        assertEquals("Hello World", FluffyCompiler().compile(binding, """"Hello World"""").eval())

        assertEquals("Hello\nWorld", FluffyCompiler().compile(binding, """"Hello\nWorld"""").eval())
        assertEquals("Hello\tWorld", FluffyCompiler().compile(binding, """"Hello\tWorld"""").eval())
        assertEquals("Hello\"World", FluffyCompiler().compile(binding, """"Hello\"World"""").eval())
        assertEquals("Hello\\World", FluffyCompiler().compile(binding, """"Hello\\World"""").eval())

    }

    @Test
    fun testEmptyString() {
        val binding = Binding()

        assertEquals("", FluffyCompiler().compile(binding, "\"\"").eval())

        assertEquals("1", FluffyCompiler().compile(binding, "\"\" + \"1\"").eval())

    }

    @Test
    fun testMultiLineLiterals() {
        val binding = Binding()

        assertEquals("Hello", FluffyCompiler().compile(binding, "\"\"\"Hello\"\"\"").eval())
        assertEquals("Hello\nWorld", FluffyCompiler().compile(binding, "\"\"\"Hello\nWorld\"\"\"").eval())
    }

    @Test
    fun testConcat() {

        val binding = Binding()

        assertEquals("HelloWorld", FluffyCompiler().compile(binding, """"Hello" + "World"""").eval())

    }

    @Test
    fun testRef() {
        val binding = Binding()

        FluffyCompiler().compile(binding, "var a=3").eval()

        assertEquals("3", FluffyCompiler().compile(binding, "\"\$a\"").eval())
        assertEquals("a=3.", FluffyCompiler().compile(binding, "\"a=\$a.\"").eval())
        assertEquals("Next=4.", FluffyCompiler().compile(binding, "\"Next=\${a+1}.\"").eval())

        // Multi-line versions of the above.

        assertEquals("3", FluffyCompiler().compile(binding, "\"\"\"\$a\"\"\"").eval())
        assertEquals("a=3.", FluffyCompiler().compile(binding, "\"\"\"a=\$a.\"\"\"").eval())
        assertEquals("Next=4.", FluffyCompiler().compile(binding, "\"\"\"Next=\${a+1}.\"\"\"").eval())
    }

    /**
     * Uses the extension function CharSequence.iterator() in standard.fluffy.
     */
    @Test
    fun testForIn() {
        val binding = Binding.standard()
        binding.declare("output", "->")

        compiler.compile(binding, "for (c in \"Hello\") { output += c }").eval()
        assertEquals("->Hello", binding.valueOf("output"))

    }
}
