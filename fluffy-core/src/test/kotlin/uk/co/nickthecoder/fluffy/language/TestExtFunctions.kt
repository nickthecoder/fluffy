package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestExtFunctions : TestCase() {

    val compiler = FluffyCompiler.permissive()


    @Test
    fun testNoArgs() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(1))
        compiler.compile(binding, "fun Example.plus1() : Int = this.a + 1")

        assertEquals(2, compiler.compile(binding, "example.plus1()").eval())
    }


    @Test
    fun testFieldImpliedThis() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(1))
        compiler.compile(binding, "fun Example.plus1() : Int = a + 1")

        assertEquals(2, compiler.compile(binding, "example.plus1()").eval())
    }


    @Test
    fun testFieldClashGlobal() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(1))
        binding.declare("a", 10)

        // This test always worked...
        compiler.compile(binding, "fun Example.explicitThis() : Int = this.a")
        assertEquals(1, compiler.compile(binding, "example.explicitThis()").eval())

        // ...But this one didn't because "a" is a field AND a global variable, and the global was taking precedence.
        // And therefore 10 was returned instead of 1
        compiler.compile(binding, "fun Example.impliedThis() : Int = a")
        assertEquals(1, compiler.compile(binding, "example.impliedThis()").eval())
    }

    @Test
    fun testMethodImpliedThis() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(1))
        compiler.compile(binding, "fun Example.plus1() : Int = foo() + 1")

        assertEquals(4, compiler.compile(binding, "example.plus1()").eval())
    }

    @Test
    fun testOneArg() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(1))
        compiler.compile(binding, "fun Example.plus( b : Int ) : Int = this.a + b")

        assertEquals(4, compiler.compile(binding, "example.plus( 3 )").eval())
    }

    @Test
    fun testGetter() {
        val binding = Binding()

        compiler.compile(binding, "import uk.co.nickthecoder.fluffy.language.*")
        binding.declare("example", Example(4))
        compiler.compile(binding, "fun Example.getNegative() : Int = -this.a")

        // Call it as a function
        assertEquals(-4, compiler.compile(binding, "example.getNegative()").eval())
        // Access like a field
        assertEquals(-4, compiler.compile(binding, "example.negative").eval())
    }

}

class Example(val a: Int) {
    fun foo() = 3
}
