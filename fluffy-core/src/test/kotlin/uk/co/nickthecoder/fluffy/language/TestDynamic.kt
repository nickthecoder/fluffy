package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestDynamic : TestCase() {


    val compiler = FluffyCompiler()
    val list = mutableListOf<Any>()

    @Test
    fun testOperators() {
        val binding = Binding()
        list.clear()
        list.addAll(listOf(1, 3, 5.1, 4.2))
        binding.declare("list", list)

        TestCase.assertEquals(4, compiler.compile(binding, "list[0] + list[1]").eval())
        TestCase.assertEquals(8.1, compiler.compile(binding, "list[1] + list[2]").eval())
        TestCase.assertEquals(9.3, compiler.compile(binding, "list[2] + list[3]").eval())

        TestCase.assertEquals(-1, compiler.compile(binding, "-list[0]").eval())
        TestCase.assertEquals(-5.1, compiler.compile(binding, "-list[2]").eval())

    }

    @Test
    fun testMethod() {
        val binding = Binding()
        list.clear()
        list.addAll(listOf(listOf("Hello"), listOf("World")))
        binding.declare("list", list)

        TestCase.assertEquals(1, compiler.compile(binding, "list[0].size()").eval())

    }

    public var foo = 5

    @Test
    fun testField() {
        val compiler = FluffyCompiler.permissive()

        val binding = Binding()
        list.clear()
        list.add(this)
        binding.declare("list", list)

        TestCase.assertEquals(5, compiler.compile(binding, "list[0].foo").eval())
    }

    @Test
    fun testDynamicGetterExtension() {
        val compiler = FluffyCompiler.permissive()
        val binding = Binding.extra() // Include File extensions.

        compiler.compile(binding, """
import java.io.File
val list = [1]
list.add(File("./todo.txt"))
""").eval()

        assertEquals("txt", compiler.compile(binding, "list[1].extension").eval())

    }

    @Test
    fun testDynamicExtensionFunction() {
        val compiler = FluffyCompiler.permissive()
        val binding = Binding.extra() // Include File extensions.

        compiler.compile(binding, """
import java.io.File
var list = [1]
list.add(File("./todo.txt"))
""").eval()

        assertEquals("txt", compiler.compile(binding, "list[1].getExtension()").eval())

    }

}
