package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestStandard : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testUntil() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for i in 1 until 5 output += i").eval()
        assertEquals("1234", binding.valueOf("output"))
    }

    @Test
    fun testUntilStep() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for i in 1 until 7 step 2 output += i").eval()
        assertEquals("135", binding.valueOf("output"))
    }

    @Test
    fun testUntilChar() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for c in 'a' until 'd' output += c").eval()
        assertEquals("abc", binding.valueOf("output"))
    }

    @Test
    fun testUntilCharStep() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for c in 'a' until 'g' step 2 output += c").eval()
        assertEquals("ace", binding.valueOf("output"))
    }


    @Test
    fun testDownTo() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for i in 4 downTo 1 output += i").eval()
        assertEquals("4321", binding.valueOf("output"))
    }
    @Test
    fun testDownToStep() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for i in 8 downTo 1 step 2 output += i").eval()
        assertEquals("8642", binding.valueOf("output"))
    }

    @Test
    fun testDownToChar() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for c in 'c' downTo 'a' output += c").eval()
        assertEquals("cba", binding.valueOf("output"))
    }


    @Test
    fun testDownToCharStep() {
        val binding = Binding.standard()

        binding.declare("output", "")
        compiler.compile(binding, "for c in 'g' downTo 'a' step 2 output += c").eval()
        assertEquals("geca", binding.valueOf("output"))
    }

    /**
     * Not a real test, as it doesn't assert anything. Just need to look at the output :-(
     */
    @Test
    fun testPrint() {
        val binding = Binding.standard()
        binding.declare( "name", "Nick")
        binding.declare( "ok", true)

        compiler.compile(binding, """
            print( "Hello " )
            println( "${'$'}name." )
            println( "Ok? ${'$'}ok")"""
        ).eval()
    }
}
