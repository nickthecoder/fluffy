package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestArray : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testAccess() {
        val binding = Binding()
        val list = mutableListOf("hello", "world")
        binding.declare("list", list)

        TestCase.assertEquals("hello", compiler.compile(binding, "list[0]").eval())

    }

    @Test
    fun testSet() {
        val binding = Binding()
        val list = mutableListOf("hello", "world")
        binding.declare("list", list)

        compiler.compile(binding, "list[0] = \"hi\"").eval()
        TestCase.assertEquals("hi", list[0])

    }

}
