package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import uk.co.nickthecoder.fluffy.FluffyEvalException

class TestTry : TestCase() {

    val compiler = FluffyCompiler.permissive()

    lateinit var binding: Binding

    override fun setUp() {
        binding = Binding()
    }

    @Test
    fun testTryCatch() {

        assertEquals("caught", compiler.compile(binding, """
            import java.lang.Exception

            var caught = false
            try {
                throw Exception( "Deliberate" )
                "end"
            } catch ( e ) {
                caught=true
                "caught"
            }
        """.trimIndent()).eval())

        assertEquals(true, binding.valueOf("caught"))

    }

    @Test
    fun testTryNoThrow() {

        assertEquals("end", compiler.compile(binding, """
            import java.lang.Exception

            var caught = false
            try {
                "end"
            } catch ( e ) {
                caught=true
                "caught"
            }
        """.trimIndent()).eval())

        assertEquals(false, binding.valueOf("caught"))
    }


    @Test
    fun testTryFinally() {
        val binding = Binding()

        assertEquals("Ha!", compiler.compile(binding, """
            import java.lang.Exception

            var caught = false
            var f = false
            try {
                throw Exception( "Ha!" )
                "end"
            } catch ( e ) {
                caught=true
                e.message
            } finally {
                f = true
            }
        """.trimIndent()).eval())

        assertEquals(true, binding.valueOf("caught"))
        assertEquals(true, binding.valueOf("f"))

    }

    @Test
    fun testTryFinallyNoThrow() {

        assertEquals("end", compiler.compile(binding, """
            import java.lang.Exception

            var caught = false
            var f = false
            try {
                "end"
            } catch ( e ) {
                caught=true
                "caught"
            } finally {
                f = true
            }
        """.trimIndent()).eval())

        assertEquals(false, binding.valueOf("caught"))
        assertEquals(true, binding.valueOf("f"))

    }

    @Test
    fun testThrow() {

        val script = compiler.compile(binding, """
            import java.lang.Exception
            fun foo() {
                throw Exception( "An error!" )
            }
            foo()
        """.trimIndent())

        try {
            script.eval()
        } catch (e: FluffyEvalException) {
            val st = e.stackTrace

            assertEquals(3, st[0].lineNumber)
            assertEquals("foo", st[0].methodName)

            assertEquals(5, st[1].lineNumber)
            assertEquals("<top-level>", st[1].methodName)

            return
        }
        assertFalse("Should not get here!", true)
    }

    fun testAsExpression() {

        assertEquals(4, compiler.compile(binding, """
            try {
                1 + 3
            } catch (e) {
                4 + 6
            }
        """.trimIndent()).eval())

        assertEquals(10, compiler.compile(binding, """
            import java.lang.Exception
            try {
                throw Exception( "" )
            } catch (e) {
                4 + 6
            }
        """.trimIndent()).eval())
    }

    /**
     * This was failing, due to the function declaration opening a block,
     * and leaving the "skipOpenBlock" to true, which caused the try's block to be skipped.
     */
    fun testWithinFun() {

        assertEquals(10, compiler.compile(binding, """
            import java.lang.Exception
            fun foo() =
                try {
                    throw Exception( "" )
                } catch (e) {
                    4 + 6
                }
            foo()
        """.trimIndent()).eval())

    }
}
