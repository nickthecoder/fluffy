package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestCompiler : TestCase() {

    @Test
    fun testUnaryMinus() {
        val binding = Binding()

        assertEquals(-1, FluffyCompiler().compile(binding, "-1").eval())
        assertEquals(+1, FluffyCompiler().compile(binding, "+1").eval())

        assertEquals(1, FluffyCompiler().compile(binding, "- -1").eval())
    }

    @Test
    fun testPlus() {
        val binding = Binding()

        assertEquals(1 + 2, FluffyCompiler().compile(binding, "1+2").eval())
        assertEquals(1 + -4, FluffyCompiler().compile(binding, "1 + -4").eval())
        assertEquals(1 + -4, FluffyCompiler().compile(binding, "1 +-4").eval())

        assertEquals(1.1 + 2.1, FluffyCompiler().compile(binding, "1.1 + 2.1").eval())
        assertEquals(1.1 + 2, FluffyCompiler().compile(binding, "1.1 + 2").eval())
        assertEquals(1 + 2.1, FluffyCompiler().compile(binding, "1 + 2.1").eval())

        assertEquals(1 + 2 + 3, FluffyCompiler().compile(binding, "1+2+3").eval())
    }


    @Test
    fun testMinus() {
        val binding = Binding()

        assertEquals(1 - 3, FluffyCompiler().compile(binding, "1 - 3").eval())
        assertEquals(1 - -3, FluffyCompiler().compile(binding, "1 - -3").eval())
        // 1 -- 3 should throw

        assertEquals(5.3 - 2.1, FluffyCompiler().compile(binding, "5.3 - 2.1").eval())
        assertEquals(5.3 - 2, FluffyCompiler().compile(binding, "5.3 - 2").eval())
        assertEquals(5 - 2.1, FluffyCompiler().compile(binding, "5 - 2.1").eval())

        assertEquals(1 - 3 - 5, FluffyCompiler().compile(binding, "1 - 3 - 5").eval())

    }


    @Test
    fun testTimes() {
        val binding = Binding()

        assertEquals(2 * 3, FluffyCompiler().compile(binding, "2 * 3").eval())
        assertEquals(1.1 * 3, FluffyCompiler().compile(binding, "1.1 * 3").eval())
        assertEquals(1 * 3.1, FluffyCompiler().compile(binding, "1 * 3.1").eval())
        assertEquals(1.1 * 3.1, FluffyCompiler().compile(binding, "1.1 * 3.1").eval())

        assertEquals(2 * 3 * 4, FluffyCompiler().compile(binding, "2 * 3 * 4").eval())

        assertEquals("Hello".repeat(3), FluffyCompiler().compile(binding, """"Hello" * 3""").eval())
    }

    @Test
    fun testDivide() {
        val binding = Binding()

        assertEquals(7.0 / 3, FluffyCompiler().compile(binding, "7 / 3").eval())
        assertEquals(7 / 3, FluffyCompiler().compile(binding, "7 ~/ 3").eval())
        assertEquals(7.1 / 3, FluffyCompiler().compile(binding, "7.1 / 3").eval())
        assertEquals(7 / 3.1, FluffyCompiler().compile(binding, "7 / 3.1").eval())
        assertEquals(7.1 / 3.1, FluffyCompiler().compile(binding, "7.1 / 3.1").eval())

        assertEquals(20.0 / 5 / 2, FluffyCompiler().compile(binding, "20/5/2").eval())
        assertEquals(20 / 5 / 2, FluffyCompiler().compile(binding, "20~/5~/2").eval())
    }

    @Test
    fun testMod() {
        val binding = Binding()

        assertEquals(7 % 3, FluffyCompiler().compile(binding, "7 % 3").eval())
        assertEquals(7.1 % 3, FluffyCompiler().compile(binding, "7.1 % 3").eval())
        assertEquals(7 % 3.1, FluffyCompiler().compile(binding, "7 % 3.1").eval())
        assertEquals(7.1 % 3.1, FluffyCompiler().compile(binding, "7.1 % 3.1").eval())

        assertEquals(30 % 7 % 5, FluffyCompiler().compile(binding, "30 % 7 % 5").eval())

    }

    @Test
    fun testBoolean() {
        val binding = Binding()

        assertEquals(true || true, FluffyCompiler().compile(binding, "true || true").eval())
        assertEquals(true || false, FluffyCompiler().compile(binding, "true || false").eval())
        assertEquals(false || false, FluffyCompiler().compile(binding, "false || false").eval())

        assertEquals(true && true, FluffyCompiler().compile(binding, "true && true").eval())
        assertEquals(true && false, FluffyCompiler().compile(binding, "true && false").eval())
        assertEquals(false && false, FluffyCompiler().compile(binding, "false && false").eval())

        assertEquals(!true, FluffyCompiler().compile(binding, "! true ").eval())
        assertEquals(!false, FluffyCompiler().compile(binding, "! false ").eval())

        assertEquals(false || false || true, FluffyCompiler().compile(binding, "false|| false || true").eval())
        assertEquals(true || false || false, FluffyCompiler().compile(binding, "true || false || false").eval())

        assertEquals(true && true && false, FluffyCompiler().compile(binding, "true && true && false").eval())
        assertEquals(false && true && true, FluffyCompiler().compile(binding, "false && true && true").eval())

        assertEquals(!!true, FluffyCompiler().compile(binding, "! ! true ").eval())

    }

    @Test
    fun testEquals() {
        val binding = Binding()

        assertEquals(1 == 1, FluffyCompiler().compile(binding, "1 == 1").eval())
        assertEquals(1 == 2, FluffyCompiler().compile(binding, "1 == 2").eval())

        assertEquals(1.1 == 1.1, FluffyCompiler().compile(binding, "1.1 == 1.1").eval())
        assertEquals(1.1 == 2.1, FluffyCompiler().compile(binding, "1.1 == 2.1").eval())

        assertEquals(true, FluffyCompiler().compile(binding, "1 == 1.0").eval())
        assertEquals(true, FluffyCompiler().compile(binding, "1.0 == 1").eval())

        assertEquals(true, FluffyCompiler().compile(binding, "\"Hi\" == \"Hi\"").eval())

        assertEquals(1 == 1 == true, FluffyCompiler().compile(binding, "1 == 1 == true").eval())

    }

    @Test
    fun testComparison() {
        val binding = Binding()

        assertEquals(1 < 2, FluffyCompiler().compile(binding, "1 < 2").eval())
        assertEquals(2 < 1, FluffyCompiler().compile(binding, "2 < 1").eval())

        assertEquals(1.0 < 2.0, FluffyCompiler().compile(binding, "1.0 < 2.0").eval())
        assertEquals(2.0 < 1.0, FluffyCompiler().compile(binding, "2.0 < 1.0").eval())

        assertEquals(1 <= 2, FluffyCompiler().compile(binding, "1 <= 2").eval())
        assertEquals(2 <= 2, FluffyCompiler().compile(binding, "2 <= 2").eval())
        assertEquals(3 <= 2, FluffyCompiler().compile(binding, "3 <= 2").eval())


        assertEquals(1 > 2, FluffyCompiler().compile(binding, "1 > 2").eval())
        assertEquals(2 > 1, FluffyCompiler().compile(binding, "2 > 1").eval())

        assertEquals(1.0 > 2.0, FluffyCompiler().compile(binding, "1.0 > 2.0").eval())
        assertEquals(2.0 > 1.0, FluffyCompiler().compile(binding, "2.0 > 1.0").eval())

        assertEquals(1 >= 2, FluffyCompiler().compile(binding, "1 >= 2").eval())
        assertEquals(2 >= 2, FluffyCompiler().compile(binding, "2 >= 2").eval())
        assertEquals(3 >= 2, FluffyCompiler().compile(binding, "3 >= 2").eval())

        assertEquals(1 < 2.0, FluffyCompiler().compile(binding, "1 < 2.0").eval())
        assertEquals(1.0 < 2, FluffyCompiler().compile(binding, "1.0 < 2").eval())
        assertEquals(1 > 2.0, FluffyCompiler().compile(binding, "1 > 2.0").eval())
        assertEquals(1.0 > 2, FluffyCompiler().compile(binding, "1.0 > 2").eval())
        assertEquals(1 <= 2.0, FluffyCompiler().compile(binding, "1 <= 2.0").eval())
        assertEquals(1.0 <= 2, FluffyCompiler().compile(binding, "1.0 <= 2").eval())
        assertEquals(1 >= 2.0, FluffyCompiler().compile(binding, "1 > 2.0").eval())
        assertEquals(1.0 >= 2, FluffyCompiler().compile(binding, "1.0 >= 2").eval())

    }

    @Test
    fun testNull() {
        val binding = Binding()

        assertEquals(null, FluffyCompiler().compile(binding, "null").eval())

    }

}
