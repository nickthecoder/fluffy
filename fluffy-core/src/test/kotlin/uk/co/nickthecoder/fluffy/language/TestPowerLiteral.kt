package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestPowerLiteral : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @Test
    fun testPower() {
        val binding = Binding()

        assertEquals(1, compiler.compile(binding, "2⁰").eval())
        assertEquals(2, compiler.compile(binding, "2¹").eval())
        assertEquals(4, compiler.compile(binding, "2²").eval())
        assertEquals(8, compiler.compile(binding, "2³").eval())
        assertEquals(16, compiler.compile(binding, "2⁴").eval())
        assertEquals(32, compiler.compile(binding, "2⁵").eval())
        assertEquals(64, compiler.compile(binding, "2⁶").eval())
        assertEquals(128, compiler.compile(binding, "2⁷").eval())
        assertEquals(256, compiler.compile(binding, "2⁸").eval())
        assertEquals(512, compiler.compile(binding, "2⁹").eval())
        assertEquals(1024, compiler.compile(binding, "2¹⁰").eval())

        // Using a double, should return a double
        assertEquals(1.0, compiler.compile(binding, "2.0⁰").eval())
        assertEquals(512.0, compiler.compile(binding, "2.0⁹").eval())

        // Negative powers always returns a double
        assertEquals(0.5, compiler.compile(binding, "2.0⁻¹").eval())
        assertEquals(0.25, compiler.compile(binding, "2.0⁻²").eval())
        assertEquals(0.5, compiler.compile(binding, "2⁻¹").eval())
        assertEquals(0.25, compiler.compile(binding, "2⁻²").eval())

        // Optional + prefix
        assertEquals(9, compiler.compile(binding, "3⁺²").eval())
    }

}
