package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import java.io.File

class TestWith : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @Test
    fun testWith() {
        val binding = Binding.extra()

        binding.declare("file", File("./foo.txt"))

        assertEquals("foo.bar.txt", compiler.compile(binding, """
            with ( file ) {
                nameWithoutExtension + ".bar." + extension
            }
            """.trimIndent()).eval())
    }

    fun testGlobalClash() {
        val binding = Binding.extra()

        binding.declare("file", File("./foo.txt"))
        binding.declare("extension", "")

        // The global variable will be hidden!
        assertFails {
            compiler.compile(binding, """
            with ( file ) {
                extension = "!" + this.extension
            }
            """.trimIndent()).eval()
        }
    }


    fun testLocalClash() {
        val binding = Binding.extra()

        binding.declare("file", File("./foo.txt"))

        // The local variable will be NOT be hidden!
        assertEquals("!txt",
                compiler.compile(binding, """
            fun foo() : String {
                var extension = "!"
                with ( file ) {
                    extension += this.extension
                }
                return extension
            }
            foo()
            """.trimIndent()).eval()
        )
    }

}
