package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import java.util.*

class TestMethods : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @Test
    fun testMethods() {
        val binding = Binding()

        TestCase.assertEquals(1.0, compiler.compile(binding, "1.doubleValue()").eval())

    }

    @Test
    fun testConstructors() {
        val binding = Binding()
        binding.importPackage("java.util")

        TestCase.assertEquals(Date::class.java, compiler.compile(binding, "Date()").eval()!!.javaClass)
    }

    /**
     * Used by [testGetterSetter]
     */
    var foo = 3
    val bar = 6

    @Test
    fun testGetterSetter() {
        val binding = Binding()

        binding.declare("me", this)

        TestCase.assertEquals(3, compiler.compile(binding, "me.foo").eval())
        compiler.compile(binding, "me.foo = 4").eval()
        TestCase.assertEquals(4, compiler.compile(binding, "me.foo").eval())
        TestCase.assertEquals(4, foo)

        TestCase.assertEquals(6, compiler.compile(binding, "me.bar").eval())
        assertFails { compiler.compile(binding, "me.bar = 4") }

    }

    @Test
    fun testConvertPrimitiveReturn() {
        val binding = Binding()

        val list: List<Int> = mutableListOf(1, 1, 2, 3, 5)
        binding.declare("l", list)
        assertEquals(4, compiler.compile(binding, "l.size()-1").eval())
    }

    @Test
    fun testPrimitiveArgs() {
        val binding = Binding()

        val list: List<Int> = mutableListOf(1, 1, 2, 3, 5)
        binding.declare("l", list)
        assertEquals(1, compiler.compile(binding, "l.get(0)").eval())
        assertEquals(5, compiler.compile(binding, "l.get(4)").eval())

        assertEquals(5, compiler.compile(binding, "l.get(4.0)").eval())
    }

    /**
     * NOTE. The same as the test in testPrimitiveArgs, but list2 is [Arrays.ArrayList].
     * Without setting Method.isAccessible to true, this would fail.
     */
    @Test
    fun testArraysArrayList() {
        val binding = Binding()

        val list2: List<Int> = listOf(1, 1, 2, 3, 5)
        binding.declare("l2", list2)
        assertEquals(2, compiler.compile(binding, "l2.get(2)").eval())
    }
}
