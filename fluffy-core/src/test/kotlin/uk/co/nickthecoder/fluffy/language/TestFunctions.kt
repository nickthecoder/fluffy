package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestFunctions : TestCase() {

    val compiler = FluffyCompiler.permissive()

    lateinit var binding: Binding

    override fun setUp() {
        binding = Binding()
    }

    @Test
    fun testFun() {

        compiler.compile(binding, "fun doNothing() { var a = 1\n1+2\n }")
        compiler.compile(binding, "fun plus1( a : Int ) : Int { return a + 1 }")
        compiler.compile(binding, "fun times2( x : Int ) : Int = x * 2")

        assertEquals(Unit, compiler.compile(binding, "doNothing()").eval())
        assertEquals(3, compiler.compile(binding, "plus1(2)").eval())
        assertEquals(6, compiler.compile(binding, "times2(3)").eval())

        // Testing that functions with the same name, but different parameter types can co-exist.
        compiler.compile(binding, "fun plus1( a : String ) : String { return a + 1 }")
        compiler.compile(binding, "fun times2( a : String ) : String { return a * 2 }")

        assertEquals("21", compiler.compile(binding, "plus1(\"2\")").eval())
        assertEquals("HelloHello", compiler.compile(binding, "times2(\"Hello\")").eval())

    }

    @Test
    fun testAny() {
        FluffyCompiler().compile(binding, "fun doNothing( a : Any ) { }")
        assertEquals(Unit, FluffyCompiler().compile(binding, "doNothing(1)").eval())
    }

    @Test
    fun testThree() {

        compiler.compile(binding, """
fun foo() : Int { return 1 }
fun bar() : Double { return 2.5 }
fun baz() : String = "Hello"
""").eval()
        assertEquals(1, compiler.compile(binding, "foo()").eval())
        assertEquals(2.5, compiler.compile(binding, "bar()").eval())
        assertEquals("Hello", compiler.compile(binding, "baz()").eval())
    }

    @Test
    fun testCallBelow() {

        compiler.compile(binding, """
fun foo() : Int { return bar() + 3 }
fun bar() : Int { return 2 }
fun baz() : Int { return bar() + 4 }
""").eval()
        assertEquals(5, compiler.compile(binding, "foo()").eval())
        assertEquals(2, compiler.compile(binding, "bar()").eval())
        assertEquals(6, compiler.compile(binding, "baz()").eval())
    }

    /**
     * Tests that a function can be called recursively.
     * This isn't a very hard test to pass, because it can work even if all calls shared the same state
     * [testRecursionAgain] is a better test.
     */
    @Test
    fun testRecursion() {

        compiler.compile(binding, """
fun foo( count : Int, b : Int ) : Int {
    return if ( count == 0 ) {
        b
    } else {
        foo( count -1, b * 2 )
    }
}
""").eval()

        assertEquals(1, compiler.compile(binding, "foo(0, 1)").eval())
        assertEquals(8, compiler.compile(binding, "foo(3, 1)").eval())

    }


    /**
     * I designed this test because I knew it would fail, as the context during each function call
     * is shared.
     * Therefore 'a' won't have the correct value after the return from the recursive function call.
     */
    @Test
    fun testRecursionAgain() {

        compiler.compile(binding, """
fun foo( count : Int, b : Int ) : Int {
    var a = if ( count == 0 ) {
        b
    } else {
        foo( count -1, b * 2 )
    }
    return a + count
}
""").eval()

        assertEquals(foo(0, 1), compiler.compile(binding, "foo(0, 1)").eval())
        assertEquals(foo(1, 1), compiler.compile(binding, "foo(1, 1)").eval())
        assertEquals(foo(3, 1), compiler.compile(binding, "foo(3, 1)").eval())

    }

    fun foo(count: Int, b: Int): Int {
        val a = if (count == 0) {
            b
        } else {
            foo(count - 1, b * 2)
        }
        return a + count
    }

    @Test
    fun testInfix() {

        compiler.compile(binding, "infix fun plus( a : Int, b : Int ) : Int { return a + b }")
        compiler.compile(binding, "fun times( a : Int, b : Int ) : Int = a * b")

        assertEquals(3, compiler.compile(binding, "1 plus 2").eval())
        assertFails { compiler.compile(binding, "2 times 3").eval() }
    }

    /**
     * Calling a function without an explicit type is only allowed AFTER the function declaration.
     */
    @Test
    fun testFunEqualsWithoutReturnType() {
        // Ok, as the function definition is before
        assertEquals(3, compiler.compile(binding, "fun foo() = 3\nfoo()").eval())
        assertEquals(intClass, compiler.compile(binding, "fun bar() = 3\nbar()").returnType())

        // Not ok. The call is before the declaration
        assertFails { compiler.compile(binding, "baz()\nfun baz() = 3").eval() }
    }

    @Test
    fun testReturnType() {

        // Doubles coerced to ints
        assertEquals(3, compiler.compile(binding, "fun three() : Int { return 3.1 }\nthree()").eval())
        assertEquals(2, compiler.compile(binding, "fun two() : Int { return 2.0 }\ntwo()").eval())

    }
}
