package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestLists : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @Test
    fun testList() {
        val binding = Binding()

        compiler.compile(binding, "var list = [ 1, 2, 3 + 1 ]").eval()
        assertEquals(1, compiler.compile(binding, "list[0]").eval())
        assertEquals(4, compiler.compile(binding, "list[2]").eval())

        assertEquals(3, compiler.compile(binding, "list.size()").eval())

        compiler.compile(binding, "list.add(10)").eval()
        assertEquals(10, compiler.compile(binding, "list[3]").eval())

        assertEquals(4, compiler.compile(binding, "list.size()").eval())

        compiler.compile(binding, "list.add(\"Hello\")").eval()
        assertEquals("Hello", compiler.compile(binding, "list[4]").eval())
    }


    @Test
    fun testMap() {
        val binding = Binding()

        compiler.compile(binding, "var map = [ 'a' => 10, 'b' => 11, 'c' => 12 ]").eval()
        assertEquals(11, compiler.compile(binding, "map['b']").eval())

        assertEquals(3, compiler.compile(binding, "map.size()").eval())

        compiler.compile(binding, "map['z'] = 26").eval()
        assertEquals(26, compiler.compile(binding, "map['z']").eval())

    }

    @Test
    fun testArrayToList() {
        val binding = Binding.extra()

        compiler.compile(binding, "var file = File(\".\")").eval()
        val list = compiler.compile(binding, "file.listFiles()").eval()
        assertTrue("File.listFiles converted to a list", list is List<*>)
    }

    @Test
    fun testDeclaredListType() {
        val binding = Binding.extra()
        compiler.compile(binding, "var list = [ 1, 2, 3 + 1 ]<Int>").eval()

        assertEquals(intClass, compiler.compile(binding, "list[0]").returnClass())
    }

    @Test
    fun testUndeclaredListType() {
        val binding = Binding.extra()
        compiler.compile(binding, "var list = [ 1, 2, 3 + 1 ]").eval()

        assertEquals(intClass, compiler.compile(binding, "list[0]").returnClass())
    }


}
