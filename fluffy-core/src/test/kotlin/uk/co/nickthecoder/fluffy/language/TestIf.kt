package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler


class TestIf : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testIf() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 10").eval()

        // If False
        compiler.compile(binding, "if (a == 2) b=20").eval()
        assertEquals(10, compiler.compile(binding, "b").eval())

        // If True
        compiler.compile(binding, "if (a == 1) b=30").eval()
        assertEquals(30, compiler.compile(binding, "b").eval())

        // If true else
        assertEquals(10, compiler.compile(binding, "if (a == 1) 10 else 20").eval())

        // If false else
        assertEquals(20, compiler.compile(binding, "if (a == 2) 10 else 20").eval())

    }

    @Test
    fun testIfBlock() {
        val binding = Binding()

        compiler.compile(binding, "var a = 1").eval()
        compiler.compile(binding, "var b = 10").eval()
        compiler.compile(binding, "var c = 100").eval()

        // If False
        compiler.compile(binding, "if (a == 2) {b=20\nc=200\n}").eval()
        assertEquals(10, compiler.compile(binding, "b").eval())
        assertEquals(100, compiler.compile(binding, "c").eval())

        // If True
        compiler.compile(binding, "if (a == 1) {b=30\nc=300}").eval()
        assertEquals(30, compiler.compile(binding, "b").eval())
        assertEquals(300, compiler.compile(binding, "c").eval())

        // If true else
        assertEquals(10, compiler.compile(binding, "if (a == 1) { c=110 \n 10 } else { c=120 \n 20 }").eval())
        assertEquals(110, compiler.compile(binding, "c").eval())

        // If false else
        assertEquals(20, compiler.compile(binding, "if (a == 2) { c=110 \n 10 } else { c=120 \n 20 }").eval())
        assertEquals(120, compiler.compile(binding, "c").eval())

    }

}
