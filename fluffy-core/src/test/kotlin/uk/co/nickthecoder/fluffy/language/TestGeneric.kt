package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler
import java.lang.reflect.ParameterizedType

/**
 * NOTE. [FluffyCompiler.allowDynamic] is set to false, so that all methods/fields/operators only use the type
 * information available at compile time.
 */
class TestGeneric : TestCase() {

    val compiler = FluffyCompiler.permissive().apply {
        allowDynamic = false
    }

    val list = mutableListOf<Int>()

    @Test
    fun testList() {
        val binding = Binding()
        list.clear()
        list.addAll(listOf(1, 3, 5, 4))
        binding.declare("list", this, "list")
        binding.declare("anyList", list) // No generic type information

        assertEquals(3, compiler.compile(binding, "list.get(1)").eval())
        assertEquals(intClass, compiler.compile(binding, "list.get(1)").returnClass())

        assertEquals(3, compiler.compile(binding, "list[1]").eval())
        assertEquals(intClass, compiler.compile(binding, "list[1]").returnClass())

        // The list with generic info should work, and the other list should fail.
        assertEquals(4, compiler.compile(binding, "list.get(1) + 1").eval())
        assertFails { compiler.compile(binding, "anyList.get(1) + 1").eval() }
        assertEquals(4, compiler.compile(binding, "list[1] + 1").eval())
        assertFails { compiler.compile(binding, "anyList[1] + 1").eval() }
    }

    @Test
    fun testMutableList() {
        val binding = Binding()
        list.clear()
        list.addAll(mutableListOf(1, 3, 5, 4))

        binding.declare("list", this, "list")
        binding.declare("anyList", list) // No generic type information

        compiler.compile(binding, "list[1] = 100").eval()
        assertEquals(100, compiler.compile(binding, "list[1]").eval())

        compiler.compile(binding, "list[1] = \"Hello\"").eval()

    }

    class WrappedVal<T>(val value: T)

    val greeting = WrappedVal("Hello")

    @Test
    fun testWrappedVal() {

        val binding = Binding()
        binding.declare("greeting", this, "greeting")

        assertEquals("Hello", compiler.compile(binding, "greeting.value").eval())
        assertEquals(stringClass, compiler.compile(binding, "greeting.value").returnClass())
    }

    class WrappedVar<T>(var value: T)

    val greeting2 = WrappedVar("Hi")

    @Test
    fun testWrappedVar() {

        val binding = Binding()
        binding.declare("greeting2", this, "greeting2")

        assertEquals("Hi", compiler.compile(binding, "greeting2.value").eval())
        assertEquals(stringClass, compiler.compile(binding, "greeting2.value").returnClass())

    }


    class MyMap<K, T>(map: Map<K, T>) : HashMap<K, T>() {
        init {
            putAll(map)
        }

        fun getByKey(key: K) = get(key)
    }

    val stringToString = MyMap(mapOf("HI" to "Hello", "BYE" to "Goodbye"))
    val intToString = MyMap(mapOf(1 to "Hello", 2 to "Goodbye"))


    @Test
    fun testGenericArgs() {

        val binding = Binding()
        binding.declare("stringToString", this, "stringToString")
        binding.declare("intToString", this, "intToString")

        assertEquals("Hello", compiler.compile(binding, "stringToString.getByKey( \"HI\")").eval())
        assertFails { compiler.compile(binding, "stringToString.getByKey( 1 )").eval() }

        assertEquals("Hello", compiler.compile(binding, "intToString.getByKey( 1 )").eval())
        assertEquals("Hello", compiler.compile(binding, "intToString.getByKey( 1.0 )").eval())
        assertFails { compiler.compile(binding, "intToString.getByKey( \"1\" )").eval() }

    }


    val map = mapOf("a" to 1)


    @Test
    fun testMap() {
        this.map
        val getMap = this.javaClass.getMethod("getMap")
        val mapType = getMap.genericReturnType as ParameterizedType
        val resolvedMapType = ObjectMapping.resolveGenericType(this.javaClass, mapType)
        assertEquals("java.util.Map<java.lang.String, java.lang.Integer>",
                resolvedMapType.toString())

        println("\n")

        this.map.entries
        val getEntries = resolvedMapType.rawType().getMethod("entrySet")
        val entriesType = getEntries.genericReturnType as ParameterizedType
        val resolvedEntriesType = ObjectMapping.resolveGenericType(mapType, entriesType)
        assertEquals("java.util.Set<java.util.Map\$Entry<java.lang.String,java.lang.Integer>>",
                resolvedEntriesType.toString())

        println("\n")

        this.map.entries.iterator()
        val iterator = resolvedEntriesType.rawType().getMethod("iterator")
        val iteratorType = iterator.genericReturnType as ParameterizedType
        val resolvedIteratorType = ObjectMapping.resolveGenericType(resolvedEntriesType, iteratorType)
        assertEquals("java.util.Iterator<java.util.Map\$Entry<java.lang.String,java.lang.Integer>>",
                resolvedIteratorType.toString())

        this.map.entries.iterator().next()
        val next = resolvedIteratorType.rawType().getMethod("next")
        val nextType = next.genericReturnType
        val resolvedNextType = ObjectMapping.resolveGenericType(resolvedIteratorType, nextType)
        assertEquals("java.util.Map\$Entry<java.lang.String,java.lang.Integer>",
                resolvedNextType.toString())

        this.map.entries.iterator().next().key
        val key = resolvedNextType.rawType().getMethod("getKey")
        val keyType = key.genericReturnType
        val resolvedKeyType = ObjectMapping.resolveGenericType(resolvedNextType, keyType)
        assertEquals("class java.lang.String",
                resolvedKeyType.toString())
    }

}
