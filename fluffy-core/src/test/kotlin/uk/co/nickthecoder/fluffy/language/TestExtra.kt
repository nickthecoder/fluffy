package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestExtra : TestCase() {

    val compiler = FluffyCompiler.permissive()


    @Test
    fun testFileExtension() {
        val binding = Binding.extra()

        assertEquals("txt", compiler.compile(binding, "File(\"./todo.txt\").extension").eval())
        assertEquals("gz", compiler.compile(binding, "File(\"./foo.tar.gz\").extension").eval())
        assertEquals("", compiler.compile(binding, "File(\"./.config\").extension").eval())
        assertEquals("txt", compiler.compile(binding, "File(\"./.config.txt\").extension").eval())

    }

    @Test
    fun testNameWithoutExtension() {
        val binding = Binding.extra()

        assertEquals("todo", compiler.compile(binding, "File(\"./todo.txt\").nameWithoutExtension").eval())
        assertEquals("foo.tar", compiler.compile(binding, "File(\"./foo.tar.gz\").nameWithoutExtension").eval())
        assertEquals(".config", compiler.compile(binding, "File(\"./.config\").nameWithoutExtension").eval())
        assertEquals(".config", compiler.compile(binding, "File(\"./.config.txt\").nameWithoutExtension").eval())

    }


    @Test
    fun testPathWithoutExtension() {
        val binding = Binding.extra()

        assertEquals("todo", compiler.compile(binding, "File(\"todo.txt\").pathWithoutExtension").eval())
        assertEquals("./todo", compiler.compile(binding, "File(\"./todo.txt\").pathWithoutExtension").eval())
        assertEquals("./foo.tar", compiler.compile(binding, "File(\"./foo.tar.gz\").pathWithoutExtension").eval())
        assertEquals("./.config", compiler.compile(binding, "File(\"./.config\").pathWithoutExtension").eval())
        assertEquals("./.config", compiler.compile(binding, "File(\"./.config.txt\").pathWithoutExtension").eval())

    }
}
