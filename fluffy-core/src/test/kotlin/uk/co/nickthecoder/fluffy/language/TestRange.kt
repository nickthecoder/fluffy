package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestRange : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testRange() {
        val binding = Binding()
        binding.importClass(IntProgression::class.java)
        binding.importClass(CharProgression::class.java)

        assertEquals(1..5, compiler.compile(binding, "1..5").eval())
        assertEquals(1 until 5, compiler.compile(binding, "1..<5").eval())
        assertEquals(10 downTo 5, compiler.compile(binding, "IntProgression(10,5,-1)").eval())

        assertEquals('a'..'z', compiler.compile(binding, "'a'..'z'").eval())
        assertEquals('a' until 'z', compiler.compile(binding, "'a'..<'z'").eval())
        assertEquals('z' downTo 'a', compiler.compile(binding, "CharProgression('z','a',-1)").eval())

    }
}