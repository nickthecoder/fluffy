package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestPrecedence : TestCase() {

    @Test
    fun testSimple() {
        val binding = Binding()

        assertEquals(2 + 3 * 4, FluffyCompiler().compile(binding, "2+3*4").eval())
        assertEquals(2 * 3 + 4, FluffyCompiler().compile(binding, "2*3+4").eval())

        assertEquals((2 + 3) * 4, FluffyCompiler().compile(binding, "(2+3)*4").eval())
        assertEquals(2 + (3 * 4), FluffyCompiler().compile(binding, "2+(3*4)").eval())
        assertEquals((2 * 3) + 4, FluffyCompiler().compile(binding, "(2*3)+4").eval())
        assertEquals(2 * (3 + 4), FluffyCompiler().compile(binding, "2*(3+4)").eval())
    }


}
