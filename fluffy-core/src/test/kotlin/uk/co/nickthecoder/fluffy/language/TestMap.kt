package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestMap : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testAccess() {
        val binding = Binding()
        val map = mutableMapOf("greeting" to "hello", "place" to "world")
        binding.declare("map", map)

        TestCase.assertEquals("hello", compiler.compile(binding, "map[\"greeting\"]").eval())

    }

    @Test
    fun testSet() {
        val binding = Binding()
        val map = mutableMapOf("greeting" to "hello", "place" to "world")
        binding.declare("map", map)

        compiler.compile(binding, "map[\"greeting\"] = \"hi\"").eval()
        TestCase.assertEquals("hi", map["greeting"])

    }

    @Test
    fun testDeclaredMapType() {
        val binding = Binding.extra()
        compiler.compile(binding, "var map = [ 'a' =>1, 'b' => 2, 'c' => 3 + 1 ]<Char,Int>").eval()

        assertEquals(intClass, compiler.compile(binding, "map['a']").returnClass())
        assertEquals(charClass, compiler.compile(binding, "map.entrySet().iterator().next().key").returnType())
    }

    @Test
    fun testUndeclaredMapType() {
        val binding = Binding()//.extra()
        compiler.compile(binding, "var map = [ 'a' => 1, 'b' => 2, 'c' => 3 + 1 ]").eval()

        assertEquals(intClass, compiler.compile(binding, "map['a']").returnType())
        assertEquals(charClass, compiler.compile(binding, "map.entrySet().iterator().next().key").returnType())
    }
}
