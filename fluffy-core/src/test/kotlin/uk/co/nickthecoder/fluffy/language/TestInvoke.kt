package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestInvoke : TestCase() {

    val compiler = FluffyCompiler.permissive().apply { declutterStackTrace = false }

    var binding = Binding()

    /**
     * Tests that need dynamic calls can set this back to true.
     */
    override fun setUp() {
        compiler.allowDynamic = false
        binding = Binding.standard()
    }

    @Test
    fun testInvoke() {
        compiler.compile(binding, "fun String.invoke(n : Int) = this * n")

        assertEquals( "HiHi", compiler.compile(binding, "\"Hi\"(2)").eval())
    }
}