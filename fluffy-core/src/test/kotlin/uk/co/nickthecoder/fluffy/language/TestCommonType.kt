package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.*

class TestCommonType : TestCase() {

    val compiler = FluffyCompiler.permissive()

    @JvmField
    val colors = listOf("Red", "Green", "Blue")

    @JvmField
    val days = listOf("Saturday", "Sunday")

    @JvmField
    val perm = listOf(PermissiveSandbox())

    @JvmField
    val white = listOf(WhitelistSandbox())

    /**
     * Ensure that if expressions have a type of the nearest common ancestor class/interface of the
     * two branches.
     */
    @Test
    fun testIf() {
        val binding = Binding()

        binding.declare("a", PermissiveSandbox())
        binding.declare("b", WhitelistSandbox())

        assertEquals(Sandbox::class.java, compiler.compile(binding, "if true a else b").returnClass())
    }

    /**
     * Ensure that if expressions have a parameterized Type of the nearest common ancestor class/interface of the
     * two branches.
     */
    @Test
    fun testGenericIf() {
        val binding = Binding()
        binding.declare("me", this)

        compiler.compile(binding, "val colors = me.colors").eval()
        compiler.compile(binding, "val days = me.days").eval()

        // NOTE we are NOT evaluating it, so the type is calculated at compile time.
        assertEquals(String::class.java, compiler.compile(binding, "colors[0]").returnClass())
        assertEquals(String::class.java, compiler.compile(binding, "colors[0]").returnClass())
        assertEquals(String::class.java, compiler.compile(binding, "(if true colors else days)[0]").returnClass())
        assertEquals(String::class.java, compiler.compile(binding, "(if true colors else days)[0]").returnClass())
    }

    // TODO This fails, but I don't want to fix it yet, so I've prefixed with "fail", and added to the todo.txt
    /**
     * Ensure that if expressions have a parameterized Type of the nearest common ancestor class/interface of the
     * two branches.
     */
    @Test
    fun fails_testGenericIfTricky() {
        val binding = Binding()
        binding.declare("me", this)

        compiler.compile(binding, "val perm = me.perm").eval()
        compiler.compile(binding, "val white = me.white").eval()

        // NOTE we are NOT evaluating it, so the type is calculated at compile time.
        assertEquals(Sandbox::class.java, compiler.compile(binding, "(if true perm else white)[0]").returnClass())
    }

}
