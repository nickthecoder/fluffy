package uk.co.nickthecoder.fluffy.language

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fluffy.Binding
import uk.co.nickthecoder.fluffy.FluffyCompiler

class TestContinue : TestCase() {

    val compiler = FluffyCompiler()

    @Test
    fun testNotInALoop() {
        val binding = Binding()

        assertFails { compiler.compile(binding, "continue").eval() }
        assertFails { compiler.compile(binding, "for(i in 1..2) {}\ncontinue").eval() }


        assertFails { compiler.compile(binding, "break").eval() }
        assertFails { compiler.compile(binding, "for(i in 1..2) {}\nbreak").eval() }
    }

    @Test
    fun testContinue() {
        val binding = Binding()

        compiler.compile(binding, "var a = 0\nfor (i in 1..10) { if ( i == 3 ) continue\n a ++ }").eval()
        assertEquals(9, binding.valueOf("a"))

    }

    @Test
    fun testBreak() {
        val binding = Binding()

        compiler.compile(binding, "var a = 0\nfor (i in 1..10) { if ( i == 3 ) break\n a ++ }").eval()
        assertEquals(2, binding.valueOf("a"))
    }

    @Test
    fun testWhileContinue() {
        val binding = Binding()

        compiler.compile(binding, "var i = 1\nvar a = 1\nwhile (i < 10) { i ++ \n if ( i == 3 ) continue\n a ++ }").eval()
        assertEquals(10, binding.valueOf("i"))
        assertEquals(9, binding.valueOf("a"))
    }

    @Test
    fun testWhileBreak() {
        val binding = Binding()

        compiler.compile(binding, "var i = 1\nvar a = 1\nwhile (i < 10) { i ++ \n if ( i == 3 ) break\n a ++ }").eval()
        assertEquals(3, binding.valueOf("i"))
        assertEquals(2, binding.valueOf("a"))
    }


    @Test
    fun testDoContinue() {
        val binding = Binding()

        compiler.compile(binding, "var i = 1\nvar a = 1\ndo { i ++ \n if ( i == 3 ) continue\n a ++ } while (i < 10)").eval()
        assertEquals(10, binding.valueOf("i"))
        assertEquals(9, binding.valueOf("a"))
    }

    @Test
    fun testDoBreak() {
        val binding = Binding()

        compiler.compile(binding, "var i = 1\nvar a = 1\ndo { i ++ \n if ( i == 3 ) break\n a ++ } while (i < 10)").eval()
        assertEquals(3, binding.valueOf("i"))
        assertEquals(2, binding.valueOf("a"))
    }
}