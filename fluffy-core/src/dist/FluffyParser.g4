/*
 [The "BSD licence"]
 Copyright (c) 2013 Terence Parr, Sam Harwell
 Copyright (c) 2017 Ivan Kochurkin (upgrade to Java 8)
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
    Inspired by the Kotlin Parser :
    https://github.com/antlr/grammars-v4/blob/master/kotlin/KotlinParser.g4
*/

parser grammar FluffyParser;

options { tokenVocab=FluffyLexer; }

script
    : NL* anysemi* (topLevelObject (anysemi+ topLevelObject?)*)? EOF
    ;

topLevelObject
    : functionDeclaration
    | statement anysemi* statements
    | importStatement
    ;

importStatement
    : IMPORT identifier (DOT MULT | importAlias)? semi?
;

importAlias
    : AS simpleIdentifier
;

functionDeclaration
    : functionDeclarationSetup NL* functionBody
    ;

functionDeclarationSetup
    : INFIX? FUN
    NL* id=identifier
    NL* functionValueParameters
    (NL* COLON NL* type)?
    ;

functionValueParameters
    : LPAREN (parameter (COMMA parameter)*)?  RPAREN
    ;

functionBody
    : block
    | ASSIGNMENT NL* expression
    ;

block
    : LCURL statements RCURL
    ;


statements
    : anysemi* (statement (anysemi+ statement?)*)?
    ;

statement
    : propertyDeclaration
    | expression
    | forStatement
    | whileExpression
    | doWhileExpression
    | withExpression
    ;


ifExpression
    // IF NL* LPAREN expression RPAREN NL* controlStructureBody? SEMICOLON?
    : IF NL* expression NL* controlStructureBody? SEMICOLON?
    (NL* ELSE NL* controlStructureBody)?
    ;

forStatement
    : forInSetup controlStructureBody
    ;

forInSetup
    : FOR        loopVar=simpleIdentifier NL* IN NL* expression ( COUNTER counterVar=simpleIdentifier )?
    | FOR LPAREN loopVar=simpleIdentifier NL* IN NL* expression ( COUNTER counterVar=simpleIdentifier )? RPAREN
    ;

whileExpression
    : WHILE  NL*        expression        NL* controlStructureBody
   // WHILE  NL* LPAREN expression RPAREN NL* controlStructureBody
    ;

doWhileExpression
    // DO NL* controlStructureBody WHILE NL* LPAREN expression RPAREN
    : DO NL* controlStructureBody WHILE NL* expression
    ;

withExpression
    : withSetup withBody
    ;

withSetup
    : WITH expression
    ;

withBody
    : block
    ;

controlStructureBody
    : block
    | expression
    ;

propertyDeclaration
    : def=( VAR | VAL )
    (NL* variableDeclaration)
    (NL* ASSIGNMENT NL* expression)?
    ;

variableDeclaration
    : simpleIdentifier (COLON type)?
;

expression
    : disjunction (bop=(ASSIGNMENT | ADD_ASSIGNMENT | SUB_ASSIGNMENT | MULT_ASSIGNMENT | DIVIDE_ASSIGNMENT | FLOOR_DIV_ASSIGNMENT | MOD_ASSIGNMENT) disjunction)*
    ;

disjunction
    : disjunction NL* bop=DISJ NL* disjunction
    | conjunction
    ;

conjunction
    : conjunction NL* bop=CONJ NL* conjunction
    | equalityComparison
    ;

equalityComparison
    : equalityComparison bop=( EXCL_EQ | EXCL_EQEQ | EQEQ | EQEQEQ ) NL* equalityComparison
    | comparison
    ;

comparison
    : namedInfix ( bop=( LANGLE | RANGLE | LE | GE ) NL* namedInfix )?
    ;

namedInfix
    : elvisExpression ( ( bop=( IS | NOT_IS ) NL* type) )?
    ;

elvisExpression
    : elvisExpression NL* bop=ELVIS NL* elvisExpression
    | infixFunctionCall
    ;

infixFunctionCall
    : infixFunctionCall simpleIdentifier NL* infixFunctionCall
    | rangeExpression
    ;

rangeExpression
    : additiveExpression bop=( RANGE | RANGE_EXCLUSIVE | PAIR_TO ) NL* additiveExpression
    | additiveExpression
    ;

additiveExpression
    : additiveExpression bop=( ADD | SUB ) NL* additiveExpression
    | multiplicativeExpression
    ;

multiplicativeExpression
    : multiplicativeExpression bop=( MULT | FLOOR_DIV | DIVIDE | MOD )  NL* multiplicativeExpression
    | powerExpression
    ;

powerExpression
    : powerExpression bop=POWER powerExpression
    | powerExpression PowerLiteral
    | uberExpression
    ;

uberExpression
    : typeRHS bop=BACKSLASH uberExpression
    | typeRHS
    ;

typeRHS
    : prefixUnaryExpression (NL* bop=(AS | AS_SAFE) type)?
    ;

// Note, EXCL is being used here as logical NOT, whereas as a postfix, it is factorial.
prefixUnaryExpression
    : ( uop=( ADD | SUB | EXCL ) ) prefixUnaryExpression
    | invokableExpression
    ;

invokableExpression
    : postfixUnaryExpression callSuffix?
    ;

// Note, EXCL is being used here as factorial, whereas as a prefix, it is logical NOT.
postfixUnaryExpression
    : postfixUnaryExpression ( uop=( INCR | DECR | EXCL ) | arrayAccess | methodCall | fieldAccess )
    | functionCall
    | variableAccess
    | atomicExpression
    ;

variableAccess // Or a field access with implied "this."
    : simpleIdentifier
    ;

functionCall // Or a method call with implied "this."
    : simpleIdentifier callSuffix
    ;

methodCall
    : DOT simpleIdentifier callSuffix
    ;

fieldAccess
    : DOT simpleIdentifier
    ;

arrayAccess
    : LSQUARE expression RSQUARE
    ;

callSuffix
    : LPAREN (NL* valueArgument ( (COMMA NL*)? valueArgument)*)? RPAREN
    ;

valueArgument
    : expression
    ;

atomicExpression
    : parenthesizedExpression
    | returnStatement
    | continueStatement
    | breakStatement
    | throwStatement
    | literalConstant
    | listLiteral
    | simpleIdentifier
    | ifExpression
    | tryExpression
    ;

tryExpression
    : TRY NL* block (NL* catchBlock)? (NL* finallyBlock)?
    ;

catchBlock
    : CATCH NL* LPAREN simpleIdentifier RPAREN NL* block
    | CATCH NL*        simpleIdentifier        NL* block
    ;

finallyBlock
    : FINALLY NL* block
;

throwStatement
    : THROW NL* expression
    ;

returnStatement
    : RETURN ( expression )?
    ;

continueStatement
    : CONTINUE
    ;

breakStatement
    : BREAK
    ;

parenthesizedExpression
    : LPAREN expression RPAREN
    ;

type
    : simpleUserType // (NL* DOT NL* type)? // For inner classes?
    ;

simpleUserType
    : simpleIdentifier // (NL* typeArguments)?
    ;

parameter
    : simpleIdentifier COLON type
    ;


identifier
    : simpleIdentifier (NL* DOT simpleIdentifier)*
    ;

simpleIdentifier
    : Identifier | IN | AS  // Some keywords may also be used as simple identifiers!
    ;

listLiteral
    : LSQUARE expression? ( COMMA NL* expression )* RSQUARE ( LANGLE first=simpleIdentifier ( COMMA second=simpleIdentifier )? RANGLE )?
    // e.g. [1,2,3]<Int> or ["a"=>1, "b"=>2]<String,Int>
    ;

literalConstant
    : booleanLiteral
    | integerLiteral
    | stringLiteral
    | characterLiteral
    | commandLiteral
    | backTickCommandLiteral
    | realLiteral
    | nullLiteral
    ;

nullLiteral : NullLiteral ;

characterLiteral : CharacterLiteral ;

booleanLiteral : BooleanLiteral ;

realLiteral : RealLiteral ;

integerLiteral : IntegerLiteral ;


commandLiteral
    : COMMAND_OPEN ( commandContent | commandExpression )* COMMAND_CLOSE
    ;

commandContent
    : (CommandText | CommandEscapedChar)+
    | CommandRef
    ;

commandExpression
    : CommandExprStart expression RCURL
    ;

backTickCommandLiteral
    : BACKTICK_OPEN ( backTickCommandContent | backTickExpression )* BACKTICK_CLOSE
    ;


backTickCommandContent
    : (BackTickText | BackTickEscapedChar)+
    | BackTickRef
    ;

backTickExpression
    : BackTickExprStart expression RCURL
    ;


stringLiteral
    : lineStringLiteral
    | multiLineStringLiteral
    ;

lineStringLiteral
    : QUOTE_OPEN (lineStringContent | lineStringExpression)* QUOTE_CLOSE
    ;

multiLineStringLiteral
    : TRIPLE_QUOTE_OPEN (multiLineStringContent | multiLineStringExpression | MultiLineStringQuote)* TRIPLE_QUOTE_CLOSE
    ;

lineStringContent
    : (LineStrText | LineStrEscapedChar)+
    | ref=LineStrRef
    ;

lineStringExpression
    : LineStrExprStart expression RCURL
    ;

multiLineStringContent
    : (MultiLineStrText | MultiLineStrEscapedChar)+
    | ref=MultiLineStrRef
    ;

multiLineStringExpression
    : MultiLineStrExprStart expression RCURL
    ;

semi: NL+ | NL* SEMICOLON NL*;

anysemi: NL | SEMICOLON;
