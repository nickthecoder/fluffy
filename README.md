# Fluffy

A simple computer language which runs inside a JVM, and can be locked-down,
with a Sandbox.

It was originally designed to be used as a scripting language for games,
so that players can design and share levels which contain code
without fear of malware.

Fluffy has now been superseded by Feather (another of my projects),
which compiles down to Java Byte Code, and therefore is *much* quicker than Fluffy.

I still use Fluffy, but only as a weird calculator.
There is a GUI, which has a text area.
Type expressions, and hit the equals key to evaluate them.
The results appear below the expression.
Why do most calculator applications mimic a "desktop calculator"?
Why do they have buttons for the digits 0 to 9?
Is your keyboard broken ;-)

The calculator has some neat features.
Its data types include fractions, complex numbers and measurements
(numbers with units, e.g. 30(g/m³) - a density).
We aren't limited to integers and doubles ;-)

For more information about the calculator, have a look at its
[help pages](fluffy-calculator/src/main/resources/uk/co/nickthecoder/fluffy/calculator/help/Index.md)

Fluffy's syntax is inspired by Kotlin (but nowhere near as powerful).

## Features

* All the usual operators, such as + - * / += || && etc with the same precedence as Kotlin.
* for loops, while loops, do...while loops
* if expressions (like kotlin's)
* Access to Java (or Kotlin) classes, and their fields and methods
* An optional `Sandbox` to prevent access to field/methods of 'dangerous' classes
  This is done by whitelisting individual classes, and/or whole packages.
* Function definitions. Functions can be overloaded.
  i.e. using the same name with different argument types.
* Extension functions (similar to Kotlin extension functions).
  Allows us added extra features to existing JVM classes.
* Special `invoke` extension functions, which allow for syntax such as :
  `10(m)` which is equivalent to `10.invoke(m)`.
  You may uses any number of parameters, e.g. `10( a, b, c, d )`
* Infix functions (such as `1 plus 2`)
* `try`...`catch`...`finally` and `throw` statements.
* `with (xxx) {}` expressions (like Kotlin).
* Getter and setter functions can be accessed like fields.
  e.g. `myObj.name` is the same as `myObj.getName()`
* Smart strings, containing expressions such as `"Hello ${person.name}"`
* Multi-line strings (using three double quotes).
* Dynamic methods. e.g. if you have a variable 'foo'
  of type Any (Object), but it actually holds a File, then foo.name can
  be resolved at runtime, instead of compile time.
  Dynamic method calls can be turned off if you wish.
* Binding of Java properties to Fluffy variables
  (changing one also changes the other).
* Integer division, and real division operators.
  `7 / 2 == 3.5`, whereas `7 ~/ 2 == 3`.
  It really bugs me that `/` does not mean `divide` in most languages.
  FYI, `~/` is like Python3's `//` operator, but we can't use that symbol,
  because that is a line comment!
* Run operating system commands with safe and easy escaping of arguments.
  See below. This feature is turned off by default.
* Includes a syntax highlighter for `TediArea` (another project of mine - a
  JavaFX control which extends TextArea, and adds support for styling text).
  This is not in Fluffy-core, so you can use Fluffy-core without depending
  on TediArea.


## Missing Features

* No `switch` statement
* New classes cannot be defined in the Fluffy language
  (but you can access Java classes).
* Limited support for generics.
  List and Map literals support generic types.
  But other than that, you cannot instantiate a generic class, or
  declare a variable using generic types.
* No concept of "package" namespaces. All functions are "global".
* Cannot refer to classes using fully qualified names (must use import instead).
* No lambda expressions or closures.

## Other Limitations

* When dynamic method calls are enabled (which is the default),
  generic type protection can vanish without warning.
  e.g. you can add any objects into a `List<String>`.
* Try statements only have one catch block, which catches all exceptions.
  The type of the exception is not specified. i.e. `try {...} catch ( e ) {...}`
* The syntax for specifying the types of lists and maps is a bit weird.
* If you `throw` without a `catch`, then `Script.eval()`
  will throw a `FluffyEvalException`, with the cause as the original
  exception. (Not really a limitation, but worth mentioning).
* Currently, there are less than 200 test cases, which don't cover
  all the edge cases.

## Build

    git clone https://gitlab.com/nickthecoder/fluffy
    cd fluffy
    ./gradlew

Test it by running the calculator :

    ./gradlew run
  
or

    ./fluffy-calculator/build/install/fluffy-calculator/bin/fluffy-calculator

## Usage

I'll use Kotlin code here, I hope Java programmers can follow along.
(Why are you still using Java? Kotlin is so much better ;-)

Create a `FluffyCompiler` object, and use it to evaluate an expression :

    val compiler = FluffyCompiler()
    val binding = Binding()
    compiler.compile( binding, "1+1" ).eval() // Returns 2

By default, the compiler is quite locked down. You cannot access many
java objects, and commands are disabled.
So if you are in a trusted environment, you may want to relax it :

    val compiler = FluffyCompiler.permissive()

Or you could do the same thing in a more verbose way :

    val compiler = FluffyCompiler()
    compiler.sandbox = PermissiveSandbox()
    compiler.commandRunner = UnixCommandRunner()

Or we could add specific classes/packages to the sandbox :

    val compiler = FluffyCompiler()
    val sandbox = WhitelistSandbox.standard() // Allows access to many basic classes
    sandbox.whitelistClasses.add( "foo.bar.Baz" )
    sandbox.whitelistPackages.add( "foo.abc" )

    compiler.sandbox = sandbox

#### Adding values to the Binding

We often need to pass data from Java into a Fluffy script.
This is what the Binding class is for.

For example is we have a `Person` object, we can pass it into the script :

    val aPerson = Person( "Nick" )
    binding.declare( "person", aPerson )

then we can use it within our script :

    compiler.compile( binding, "person.name" ).eval() // Returns "Nick"

However, if we use the same technique to pass in a list of people, then
it doesn't work as well, because java's List uses generics, and the type
information is erased (google java type erasure).

So, to keep the generic type information we need a little more work...

    class Person( val name: String )

    class Example {

        val somePeople = listOf( Person("Nick") )

        fun otherPeople() : List<Person> {
            return listOf( Person("Emma") )
        }

        fun test() {
            val compiler = FluffyCompiler.permissive()
            val binding = Binding()

            // Looks for a field called "somePeople" in 'this'
            binding.declare( "a", this, "somePeople" )

            // Looks for a method, with no arguments called "otherPeople" in 'this'
            binding.declareFromGetter( "b", this, "otherPeople" )

            // We can now access both from the script, and Fluffy
            // will know the generic types.

            compiler.compile(binding, "a[0].name").eval() // Returns Nick
            compiler.compile(binding, "b[0].name").eval() // Returns Emma

        }
    }

**Note.** You could get away with doing it the simple way, with just :

    binding.declare( "a", this.somePeople )
    binding.declare( "b", this.otherPeople() )

But Fluffy would be using dynamic method/field access.

Also, when/if I add intelli-sense to the code editor, then it
wouldn't know the types in the list, and therefore couldn't auto-complete.


**Note 2.** If you have a `getter` method called `getSomePeople()`,
then the following are identical :

    binding.declare( "a", this, "somePeople" )
    binding.declareFromGetter( "a", this, "getSomePeople" )

#### Retrieving data from Fluffy

To get results back from a Fluffy script, we can use the return
value from `compiler.compile(...).eval()`.

But sometime we want interrogate variables defined in the script.

    compiler.compile( binding, theScript ).eval()
    val fooValue = binding.valueOf("foo")


#### Binding values

We can also bind a Fluffy variable to a Java Bean property.
For example, if we have a Java object `myBean` with a bean called `name`, then :

    binding.bind( "a", myBean, "name" )

We can the use the Fluffy variable `a` inside expressions, and it will
retrieve the value of the bean property each time.
If we change `myBean.name`, then the Fluffy variable `a` reflect that change.

Also, if we change the Fluffy variable `a`, then the java bean `myBean.name`
will change.


## Example Fluffy Code

#### Imports

    import foo.bar.* // Imports all classes from package foo.bar
    import foo.bar.Baz // Imports just class Baz
    import foo.bar.Baz as Barry

NOTE. The `Sandbox` is not involved during imports.
So if Baz is not whitelisted, the import will not report an error.
Only when you try to USE Baz, will the error occur.


#### If Expressions

    if ( condion ) {
        // blah blah
    }

If ... else ...

    if ( condition ) {
        // blah blah
    } else {
        // when false
    }

As usual, the blocks can be replaced by single expressions :

    if ( condition ) foo() else bar()

As with Kotlin, if...else... is an expression. So you can use it like so :

    val word = if ( a == 1 ) "One" else "Not One"

This is similar to java's `condition ? trueExpression : falseExpression`
However, it can be much more useful when used with blocks :

    val word = if ( a == 1 ) {
        println( "debug message!" )
        "One" // The last expression is used as the result of this branch
    } else {
        "Not One"
    }

#### For Loops

    for ( i in 1..10 ) {
        println( i )
    }

The range is inclusive (10 WILL be printed).
For an exclusive range, using ..< (taken from groovy) :

    for ( i in 1..<10 ) {
        println( i )
    }

If you want to iterate downwards, or use a step, there is no special syntax,
but you can use the infix functions `downTo` and `step`
declared in the `standard` (or `extra`) Binding :

    compiler.binding = Binding.standard()

Then you can do :

    for ( i in 10 downTo 0 step 2 ) {
        println(i)
    }

    for( c in 'z' downTo 'a' step 2 ) {
        println(c)
    }

You can also use `until` instead of `..<`
(which is in keeping with Kotlin rather than Groovy) :

    for ( i in 1 until 10 ) {
        println( i )
    }

You can loop over any `java.lang.Iterable` objects.
FYI, the examples above were also iterating over an `Iterable` too.
`10 .. 20` creates an `IntRange`, and `10 downTo 1` creates an `IntProgression`.
Both implement `Iterable<Int>` and are part of the Kotlin standard library.

Here's an example iterating over a list :

    for ( item in myList ) {
        println( "Item : $item" )
    }

Note, the brackets are currently optional, but this may change...
You can also do away with the curly braces if there's only one statement.

    for i in 1..10 println( i )

You can also include a counter variable, which counts from 0 :

    for ( item in myList counter i ) {
        println( "Item $i = $item" )
    }

If you want to iterate over a class, which does not implement Iterator,
then you can define an extension function called `iterator`.
`Binding.standard()` does just that for the CharSequence class, allowing you
to iterate over the characters in a String.]

So this will only work if you use `Binding.standard()` or `Binding.extra()` :

    for ( c in "Hello" ) println( c )

#### While and Do..While

    while ( a < 3 ) {
        a ++
        println( a )
    }

 Do while ...

    do {
        println( a )
        a ++
    } while (a < 3)

#### Define and call a Function

    fun add( a : Int, b : Int ) : Int {
        return a + b
    }

This can also be written as :

    fun add( a : Int, b : Int ) : Int = a + b

Call it as you would expect :

    add( 1, 3 )

#### Extension functions

If you use a 3rd party Java class, you can extend it, using extension functions.

    fun SomeClass.extra( a: Int ) {
        this.exitingMethod( a ) // Calls SomeClass.existingMethod
    }

You can then use `extra` as if it were a real method :

    val a = SomeClass()
    a.extra( 2 )


 NOTE. Currently using "this" is mandatory, the following would NOT work :

     fun SomeClass.extra( a: Int ) {
         exitingMethod( a )
     }

If you call an extension function "getXxxx", then you can access it
as if it were a field.
For example, the `extra` binding declares the following :

    fun File.getNameWithoutExtension() : String {
        val dot = this.name.lastIndexOf(".")
        return if (dot > 0) this.name.substring( 0, dot ) else this.name
    }

And therefore, the `File` class will appear to have a new field called
`nameWithoutExtension`.

#### Infix functions

Declare a function with exactly two parameters with the `infix` modifier :

    infix fun times( a : Int, b : Int ) : Int = a * b

We can then call the function like so :

    3 times 2

The parameters can be any type (not only primitives).
So, we could have replaced the Ints with a `Matrix`.

Note. Fluffy does NOT (currently) support operator overloading, we cannot
redefine the `*` operator.



## Operating System Commands

I hate shell scripts (so hard to use safely),
and traditional languages such as Java and Python don't have support
for commands in their syntax, so I added special syntax to Fluffy.

Also, it was fun, and there's no better reason than that ;-)

NOTE. The default is for operating system commands to be disabled,
because Fluffy will probably be used in untrusted environments,
where you don't want to give access to arbitrary commands!

To turn them on :

    myCompiler = FluffyCompiler()
    myCompiler.commandRunner = UnixCommandRunner()

If you want to use this on windows,
then you will need to install a decent shell (CMD.EXE just isn't good enough!)
Google cygwin, or "windows bash".
If you insist on using CMD.EXE, then write your own implementation of CommandRunner.
I don't think it's worth it though, because AFAIK, it is impossible to
escape arguments in a generic way.
So you can't use it safely, unless you manually escape everything yourself,
and then your might as well use `Runtime.exec()` directly. Yuck!

UnixCommandRunner uses `sh -c` to run the command.
Therefore, you have all the goodies that sh provides, such as piping and
redirecting etc.

### Two Types of Commands

There are two types of operating system commands.
The simplest looks like :

    $( myCommand arg1 arg2 )

The other version uses back ticks :

    `myCommand arg1 arg2`

The later collects the output (both stdout and stderr), so that it
can be used from within your code.

### Return Type : CommandResult

The return type of both types of command is `CommandResult` which has the following fields :

    command: String
    state: CommandState
    process: Process?
    exitValue: Int
    out: String?
    err: String?
    errorMessage: String?

`out` and `err` will both be null when using `$( command )`

It also has some convenience methods, the most useful is `lines()` which
splits `out` into a `List` of `String` separated using `\n`.


### Smart Strings

The insides of of the commands are `Smart Strings`,
i.e. they can contain expressions, such as `$var` or `${expression}`,

However, they work slightly differently to regular smart strings.
When variables or expressions are within single quotes the values
are automatically escaped.

For example, if you have `val myFile = "It's nasty"`, then the command :

    $( rm -- '$myFile' )

will correctly escape the quote. The actual command will be :

    rm -- 'Its'\''s nasty'

If you naively tried :

    $( rm $myFile )

Then the actual command would be :

    rm It's nasty

which is illegal!

FYI `--` is the standard unix way to tell the shell that there
are no more named arguments or flags. So even if `myFile` starts
with `-` or `--` it will not be interpreted as a named argument/flag.

#### Using shell variables

If you want to access shell variables, then you will need to escape the `$` :

    $( ls \$HOME )

#### Changing Directory

If you use `cd` within a command, it may not do what you expect.

    $( cd /bin )
    $( ls )

The cd will have no effect, as the second command uses a new shell, where
the current directory has not been changed.

The following will work though (as cd and ls are in the same shell) :

    $( cd /bin && ls )

Changing the current directory of the JVM process is hard (if not impossible).
Google it!
So I suggest you use fully qualified paths rather than `cd`.


#### Example using ImageMagick to convert all jpg files to png

This example uses both types of commands as well as Fluffy code and
Java objects...

    for name in `ls`.lines() {
        val file = File( name )
        if ( file.extension() == "jpg" ) {
            $( convert -- '$file' '${file.nameWithoutExtension()}.png' )
        }
    }

The `ls` in back ticks calls the ls operating system command, and returns
a `CommandResult` object. The `lines` method returns the results split
at each new line.

The Java class File doesn't have an `extension()` method.
This is defined using a Fluffy extension function :

    fun File.extension() : String {
        val dot = this.name.indexOf( '.' )
        return if (dot<0) {
            ""
        } else {
            name.substring( dot + 1 )
        }
    }

We could do a better job of testing for jpg files though :

    fun File.isJpg() : Boolean {
        val ext = this.extension().toUpperCase()
        return this.isFile() && ( ext == "JPG" || ext == "JPEG" )
    }

Lastly, we could replace `ls` with `find .`, and then the script
would convert a whole directory tree. Be careful! ;-)
